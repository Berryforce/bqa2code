// This THREEx helper makes it easy to handle window resize.
// It will update renderer and camera when window is resized.
//
// # Usage
//
// **Step 1**: Start updating renderer and camera
//
// ```var windowResize = THREEx.WindowResize(aRenderer, aCamera)```
//    
// **Step 2**: Start updating renderer and camera
//
// ```windowResize.stop()```
// # Code

//

/** @namespace */
var THREEx	= THREEx 		|| {};

/**
 * Update renderer and camera when the window is resized
 * 
 * @param {Object} renderer the renderer to update
 * @param {Object} Camera the camera to update
*/
THREEx.WindowResize2	= function(renderer, camera, caller){
    var callback	= function(){
        // notify the renderer of the size change
        console.log('WindowResize2');
        renderer.setSize( caller.body.getWidth(), caller.body.getHeight() );
        // update the camera
        camera.aspect	= caller.body.getWidth() / caller.body.getHeight();
        camera.updateProjectionMatrix();
    }
    // bind the resize event
    //caller.addEventListener('resize', callback, false);
    caller.on("resize", callback, this);
    // return .stop() the function to stop watching window resize
    return {
        /**
         * Stop watching window resize
         */
        stop	: function(){
            caller.removeEventListener('resize', callback);
        }
    };
}