<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <label><!-- Access Type --></label>
        <name>BigMachines__Access_Type__c</name>
        <picklistValues>
            <masterLabel>BuyAccess</masterLabel>
            <translation><!-- BuyAccess --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>ChannelAgent</masterLabel>
            <translation><!-- ChannelAgent --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>FullAccess</masterLabel>
            <translation><!-- FullAccess --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>RestrictedAccess</masterLabel>
            <translation><!-- RestrictedAccess --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SalesAgent</masterLabel>
            <translation><!-- SalesAgent --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Association to Organization --></label>
        <name>BigMachines__Association_to_Organization__c</name>
        <picklistValues>
            <masterLabel>Internal User</masterLabel>
            <translation><!-- Internal User --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Partner User</masterLabel>
            <translation><!-- Partner User --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- If you want to update and synchronize many users at once, you can do so by using the Salesforce Data Loader and the BigMachines Bulk Synchronize feature.  Read this field&apos;s description or check the BigMachines Implementation Guide for more information. --></help>
        <label><!-- Suspend Oracle CPQ Cloud Synchronization --></label>
        <name>BigMachines__Bulk_Synchronization__c</name>
    </fields>
    <fields>
        <label><!-- Currency Preference --></label>
        <name>BigMachines__Currency_Preference__c</name>
        <picklistValues>
            <masterLabel>USD</masterLabel>
            <translation><!-- USD --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Date/Time Format --></label>
        <name>BigMachines__Date_Time_Format__c</name>
        <picklistValues>
            <masterLabel>MM/dd/yyyy HH:mm</masterLabel>
            <translation><!-- MM/dd/yyyy HH:mm --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MM/dd/yyyy h:mm a</masterLabel>
            <translation><!-- MM/dd/yyyy h:mm a --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>dd.MM.yyyy HH:mm</masterLabel>
            <translation><!-- dd.MM.yyyy HH:mm --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>dd/MM/yyyy HH:mm</masterLabel>
            <translation><!-- dd/MM/yyyy HH:mm --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>yyyy/MM/dd HH:mm</masterLabel>
            <translation><!-- yyyy/MM/dd HH:mm --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- If you&apos;d like to have a delegated approver in BigMachines, select that user here.  Please note that the user you select must already be provisioned in BigMachines. --></help>
        <label><!-- Oracle CPQ Cloud Delegated Approver --></label>
        <name>BigMachines__Delegated_Approver__c</name>
        <relationshipLabel><!-- Users (Oracle CPQ Cloud Delegated Approver) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Language Preference --></label>
        <name>BigMachines__Language_Preference__c</name>
        <picklistValues>
            <masterLabel>English</masterLabel>
            <translation><!-- English --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Oracle CPQ Cloud Login --></label>
        <name>BigMachines__Login__c</name>
    </fields>
    <fields>
        <label><!-- Number Format --></label>
        <name>BigMachines__Number_Format__c</name>
        <picklistValues>
            <masterLabel>####,##</masterLabel>
            <translation><!-- ####,## --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>####.##</masterLabel>
            <translation><!-- ####.## --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Partner Organization --></label>
        <name>BigMachines__Partner_Organization__c</name>
    </fields>
    <fields>
        <help><!-- Check this box to establish a link between this Salesforce user and BigMachines.  Linked users will not be prompted to enter login credentials when accessing BigMachines to create and edit quotes. --></help>
        <label><!-- Synchronize with Oracle CPQ Cloud --></label>
        <name>BigMachines__Provisioned__c</name>
    </fields>
    <fields>
        <label><!-- Time Zone --></label>
        <name>BigMachines__Time_Zone__c</name>
        <picklistValues>
            <masterLabel>(GMT+0:00 GMT+0:00) GMT</masterLabel>
            <translation><!-- (GMT+0:00 GMT+0:00) GMT --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+0:00 GMT+1:00) London</masterLabel>
            <translation><!-- (GMT+0:00 GMT+1:00) London --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+10:00 GMT+10:00) Queensland</masterLabel>
            <translation><!-- (GMT+10:00 GMT+10:00) Queensland --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+10:00 GMT+11:00) Vladivostok</masterLabel>
            <translation><!-- (GMT+10:00 GMT+11:00) Vladivostok --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+10:30 GMT+9:30) Adelaide</masterLabel>
            <translation><!-- (GMT+10:30 GMT+9:30) Adelaide --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+11:00 GMT+10:00) Brisbane</masterLabel>
            <translation><!-- (GMT+11:00 GMT+10:00) Brisbane --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+11:00 GMT+10:30) Lord Howe</masterLabel>
            <translation><!-- (GMT+11:00 GMT+10:30) Lord Howe --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+11:00 GMT+11:00) Ponape</masterLabel>
            <translation><!-- (GMT+11:00 GMT+11:00) Ponape --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+11:00 GMT+12:00) Magadan</masterLabel>
            <translation><!-- (GMT+11:00 GMT+12:00) Magadan --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+11:30 GMT+11:30) Norfolk</masterLabel>
            <translation><!-- (GMT+11:30 GMT+11:30) Norfolk --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+12:00 GMT+12:00) Fiji</masterLabel>
            <translation><!-- (GMT+12:00 GMT+12:00) Fiji --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+12:00 GMT+13:00) Kamchatka</masterLabel>
            <translation><!-- (GMT+12:00 GMT+13:00) Kamchatka --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+13:00 GMT+12:00) Auckland</masterLabel>
            <translation><!-- (GMT+13:00 GMT+12:00) Auckland --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+13:00 GMT+13:00) Enderbury</masterLabel>
            <translation><!-- (GMT+13:00 GMT+13:00) Enderbury --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+13:45 GMT+12:45) Chatham</masterLabel>
            <translation><!-- (GMT+13:45 GMT+12:45) Chatham --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+1:00 GMT+1:00) Algiers</masterLabel>
            <translation><!-- (GMT+1:00 GMT+1:00) Algiers --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+1:00 GMT+2:00) Amsterdam</masterLabel>
            <translation><!-- (GMT+1:00 GMT+2:00) Amsterdam --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+2:00 GMT+1:00) Windhoek</masterLabel>
            <translation><!-- (GMT+2:00 GMT+1:00) Windhoek --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+2:00 GMT+2:00) Tripoli</masterLabel>
            <translation><!-- (GMT+2:00 GMT+2:00) Tripoli --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+2:00 GMT+3:00) Israel</masterLabel>
            <translation><!-- (GMT+2:00 GMT+3:00) Israel --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+3:00 GMT+3:00) Nairobi</masterLabel>
            <translation><!-- (GMT+3:00 GMT+3:00) Nairobi --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+3:00 GMT+4:00) Moscow</masterLabel>
            <translation><!-- (GMT+3:00 GMT+4:00) Moscow --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+3:30 GMT+4:30) Tehran</masterLabel>
            <translation><!-- (GMT+3:30 GMT+4:30) Tehran --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+4:00 GMT+4:00) Dubai</masterLabel>
            <translation><!-- (GMT+4:00 GMT+4:00) Dubai --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+4:00 GMT+5:00) Oral</masterLabel>
            <translation><!-- (GMT+4:00 GMT+5:00) Oral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+4:30 GMT+4:30) Kabul</masterLabel>
            <translation><!-- (GMT+4:30 GMT+4:30) Kabul --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+5:00 GMT+5:00) Karachi</masterLabel>
            <translation><!-- (GMT+5:00 GMT+5:00) Karachi --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+5:00 GMT+6:00) Yekaterinburg</masterLabel>
            <translation><!-- (GMT+5:00 GMT+6:00) Yekaterinburg --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+5:30 GMT+5:30) Calcutta</masterLabel>
            <translation><!-- (GMT+5:30 GMT+5:30) Calcutta --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+5:45 GMT+5:45) Katmandu</masterLabel>
            <translation><!-- (GMT+5:45 GMT+5:45) Katmandu --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+6:00 GMT+6:00) Dhaka</masterLabel>
            <translation><!-- (GMT+6:00 GMT+6:00) Dhaka --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+6:00 GMT+7:00) Novosibirsk</masterLabel>
            <translation><!-- (GMT+6:00 GMT+7:00) Novosibirsk --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+6:30 GMT+6:30) Rangoon</masterLabel>
            <translation><!-- (GMT+6:30 GMT+6:30) Rangoon --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+7:00 GMT+7:00) Bangkok</masterLabel>
            <translation><!-- (GMT+7:00 GMT+7:00) Bangkok --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+7:00 GMT+8:00) Krasnoyarsk</masterLabel>
            <translation><!-- (GMT+7:00 GMT+8:00) Krasnoyarsk --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+8:00 GMT+8:00) Taipei</masterLabel>
            <translation><!-- (GMT+8:00 GMT+8:00) Taipei --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+8:00 GMT+9:00) Irkutsk</masterLabel>
            <translation><!-- (GMT+8:00 GMT+9:00) Irkutsk --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+9:00 GMT+10:00) Yakutsk</masterLabel>
            <translation><!-- (GMT+9:00 GMT+10:00) Yakutsk --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+9:00 GMT+9:00) Seoul</masterLabel>
            <translation><!-- (GMT+9:00 GMT+9:00) Seoul --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT+9:30 GMT+9:30) Darwin</masterLabel>
            <translation><!-- (GMT+9:30 GMT+9:30) Darwin --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-10:00 GMT-10:00) Hawaii</masterLabel>
            <translation><!-- (GMT-10:00 GMT-10:00) Hawaii --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-10:00 GMT-9:00) Aleutian</masterLabel>
            <translation><!-- (GMT-10:00 GMT-9:00) Aleutian --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-11:00 GMT-11:00) Samoa</masterLabel>
            <translation><!-- (GMT-11:00 GMT-11:00) Samoa --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-1:00 GMT+0:00) Azores</masterLabel>
            <translation><!-- (GMT-1:00 GMT+0:00) Azores --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-1:00 GMT-1:00) Cape Verde</masterLabel>
            <translation><!-- (GMT-1:00 GMT-1:00) Cape Verde --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-2:00 GMT-2:00) Brazil/DeNoronha</masterLabel>
            <translation><!-- (GMT-2:00 GMT-2:00) Brazil/DeNoronha --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-2:00 GMT-3:00) Sao Paulo</masterLabel>
            <translation><!-- (GMT-2:00 GMT-3:00) Sao Paulo --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-3:00 GMT-3:00) Buenos Aires</masterLabel>
            <translation><!-- (GMT-3:00 GMT-3:00) Buenos Aires --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-3:00 GMT-4:00) Chile</masterLabel>
            <translation><!-- (GMT-3:00 GMT-4:00) Chile --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-3:30 GMT-2:30) Newfoundland</masterLabel>
            <translation><!-- (GMT-3:30 GMT-2:30) Newfoundland --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-4:00 GMT-3:00) Canada Atlantic Time</masterLabel>
            <translation><!-- (GMT-4:00 GMT-3:00) Canada Atlantic Time --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-4:00 GMT-4:00) Puerto Rico</masterLabel>
            <translation><!-- (GMT-4:00 GMT-4:00) Puerto Rico --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-5:00 GMT-4:00) US Eastern Time</masterLabel>
            <translation><!-- (GMT-5:00 GMT-4:00) US Eastern Time --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-5:00 GMT-5:00) Indianapolis</masterLabel>
            <translation><!-- (GMT-5:00 GMT-5:00) Indianapolis --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-6:00 GMT-5:00) US Central Time</masterLabel>
            <translation><!-- (GMT-6:00 GMT-5:00) US Central Time --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-6:00 GMT-6:00) Saskatchewan</masterLabel>
            <translation><!-- (GMT-6:00 GMT-6:00) Saskatchewan --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-7:00 GMT-6:00) Mountain</masterLabel>
            <translation><!-- (GMT-7:00 GMT-6:00) Mountain --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-7:00 GMT-7:00) Arizona</masterLabel>
            <translation><!-- (GMT-7:00 GMT-7:00) Arizona --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-8:00 GMT-7:00) US Pacific Time</masterLabel>
            <translation><!-- (GMT-8:00 GMT-7:00) US Pacific Time --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-8:00 GMT-8:00) Pitcairn</masterLabel>
            <translation><!-- (GMT-8:00 GMT-8:00) Pitcairn --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-9:00 GMT-8:00) Alaska</masterLabel>
            <translation><!-- (GMT-9:00 GMT-8:00) Alaska --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-9:00 GMT-9:00) Gambier</masterLabel>
            <translation><!-- (GMT-9:00 GMT-9:00) Gambier --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>(GMT-9:30 GMT-9:30) Marquesas</masterLabel>
            <translation><!-- (GMT-9:30 GMT-9:30) Marquesas --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Unit System --></label>
        <name>BigMachines__Unit_System__c</name>
        <picklistValues>
            <masterLabel>English</masterLabel>
            <translation><!-- English --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Metric</masterLabel>
            <translation><!-- Metric --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>System Default</masterLabel>
            <translation><!-- System Default --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- User Groups --></label>
        <name>BigMachines__User_Groups__c</name>
        <picklistValues>
            <masterLabel>customerService</masterLabel>
            <translation><!-- customerService --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Branch Manager --></label>
        <name>Branch_Manager__c</name>
        <relationshipLabel><!-- Users (Branch Manager) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Branch Manager&apos;s Email --></label>
        <name>Branch_Manager_s_Email__c</name>
    </fields>
    <fields>
        <label><!-- CORE User ID --></label>
        <name>CORE_User_ID__c</name>
    </fields>
    <fields>
        <label><!-- Create Broadcast Portal Messages --></label>
        <name>Create_Broadcast_Portal_Messages__c</name>
    </fields>
    <fields>
        <label><!-- Create Individual Portal Message --></label>
        <name>Create_Individual_Portal_Message__c</name>
    </fields>
    <fields>
        <label><!-- Custom Screenpop --></label>
        <name>Custom_Screenpop__c</name>
    </fields>
    <fields>
        <help><!-- This is a field that is installed by and used w/ the Adoption Dashboard AppExchange package. If your org already has a similar field, you can change the reports that are part of the Adoption Dashboard package to use your field and then delete this field. --></help>
        <label><!-- DB Region --></label>
        <name>DB_Region__c</name>
        <picklistValues>
            <masterLabel>APAC</masterLabel>
            <translation><!-- APAC --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>EMEA</masterLabel>
            <translation><!-- EMEA --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>LA</masterLabel>
            <translation><!-- LA --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>NA</masterLabel>
            <translation><!-- NA --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Discontinued Date --></label>
        <name>Discontinued_Date__c</name>
    </fields>
    <fields>
        <label><!-- Email for SSO --></label>
        <name>Email_for_SSO__c</name>
    </fields>
    <fields>
        <label><!-- License Type --></label>
        <name>License_Type__c</name>
    </fields>
    <fields>
        <help><!-- Miles UserID to sync with Miles - used to pass as Creator value in XML from Berryforce to Miles --></help>
        <label><!-- MilesID --></label>
        <name>MilesID__c</name>
        <picklistValues>
            <masterLabel>DASI</masterLabel>
            <translation><!-- DASI --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GlueckertShelley</masterLabel>
            <translation><!-- GlueckertShelley --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>JoeHenry</masterLabel>
            <translation><!-- JoeHenry --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>LSeverietti</masterLabel>
            <translation><!-- LSeverietti --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>M538</masterLabel>
            <translation><!-- M538 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>O936</masterLabel>
            <translation><!-- O936 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>P160</masterLabel>
            <translation><!-- P160 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Q502</masterLabel>
            <translation><!-- Q502 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Portal Info --></label>
        <name>Portal_Info__c</name>
    </fields>
    <fields>
        <label><!-- SFDC Query Result --></label>
        <name>SFDC_Query_Result__c</name>
    </fields>
    <fields>
        <label><!-- Terms_And_Conditions_Agreed --></label>
        <name>Terms_And_Conditions_Agreed__c</name>
    </fields>
    <fields>
        <label><!-- Submit Miles 33 Updates --></label>
        <name>USR_Submit_Miles_33_Updates__c</name>
    </fields>
    <fields>
        <label><!-- API License Key --></label>
        <name>c2g__APILicenseKey__c</name>
    </fields>
    <fields>
        <label><!-- Support Data Access Key --></label>
        <name>c2g__SupportDataAccessKey__c</name>
    </fields>
    <fields>
        <label><!-- DSProSFMembershipStatus --></label>
        <name>dsfs__DSProSFMembershipStatus__c</name>
    </fields>
    <fields>
        <label><!-- DSProSFPassword --></label>
        <name>dsfs__DSProSFPassword__c</name>
    </fields>
    <fields>
        <label><!-- DSProSFUsername --></label>
        <name>dsfs__DSProSFUsername__c</name>
    </fields>
    <fields>
        <label><!-- inContact Custom Screenpop --></label>
        <name>icAgentConsole__incCustomScreenpop__c</name>
    </fields>
    <fields>
        <label><!-- inContact Agent ID --></label>
        <name>inContact_Agent_ID__c</name>
    </fields>
    <layouts>
        <layout>Admin User Layout</layout>
        <sections>
            <label><!-- BigMachines Fields --></label>
            <section>BigMachines Fields</section>
        </sections>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- DocuSign Fields --></label>
            <section>DocuSign Fields</section>
        </sections>
        <sections>
            <label><!-- FinancialForce Fields --></label>
            <section>FinancialForce Fields</section>
        </sections>
        <sections>
            <label><!-- InContact Fields --></label>
            <section>InContact Fields</section>
        </sections>
        <sections>
            <label><!-- Portal/Community Fields --></label>
            <section>Portal/Community Fields</section>
        </sections>
    </layouts>
    <layouts>
        <layout>User Layout</layout>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
    </layouts>
    <validationRules>
        <errorMessage><!-- It is highly recommended that you consult a Oracle CPQ Cloud administrator before changing the value of Association to Organization for this linked user. To change the association, you must first deselect the checkbox &quot;Provisioned in Oracle CPQ Cloud&quot;. --></errorMessage>
        <name>BigMachines__BigMachines_Association_to_Organization</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Users are required to have a first name in Oracle CPQ Cloud.  Please provide a value for this field in order to provision this user. --></errorMessage>
        <name>BigMachines__BigMachines_First_Name</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- It is highly recommended that you consult a Oracle CPQ Cloud administrator before changing the value of Username for this linked user.  To change the username, you must first deselect the checkbox &quot;Provisioned in Oracle CPQ Cloud&quot;. --></errorMessage>
        <name>BigMachines__BigMachines_Linked_Username</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- This user cannot be provisioned in Oracle CPQ Cloud because the username is not being converted into a valid Oracle CPQ Cloud login.  Oracle CPQ Cloud logins are based on the part of the username before the &apos;@&apos; symbol and must begin with a letter. --></errorMessage>
        <name>BigMachines__BigMachines_Login</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- In order to link to a Oracle CPQ Cloud Partner User, this Salesforce User must be tied to an Account that is mapped to a Partner Organization in Oracle CPQ Cloud.  Please consult with your System administrator for further details. --></errorMessage>
        <name>BigMachines__BigMachines_Partner_Users</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- You cannot change this field.  Click Cancel to cancel your changes, and contact your System Administrator. --></errorMessage>
        <name>MilesID_from_Nickname</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Phone number is required for Talus Account Creation. --></errorMessage>
        <name>Phone_Field_cant_be_null</name>
    </validationRules>
</CustomObjectTranslation>
