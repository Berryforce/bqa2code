<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Capture_Sequencing_End_Date_Time</fullName>
        <field>Sequencing_End_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Capture Sequencing End Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Capture Sequencing End Date%2FTime</fullName>
        <actions>
            <name>Capture_Sequencing_End_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Pagination_Job__c.Sequencing_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Used to capture the Date/Time that the Sequencing Apex Batch Process completes for a given Pagination Job record</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
