<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Accounting_when_sales_revenue_Account_field_changes_on_the_Product_object</fullName>
        <description>Alert Accounting when sales revenue Account field changes on the Product object</description>
        <protected>false</protected>
        <recipients>
            <recipient>allison.gardner@berryforce.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GL_Account_Change_Products</template>
    </alerts>
    <alerts>
        <fullName>New_Product_alert</fullName>
        <description>New Product alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>allison.gardner@berryforce.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GL_Account_Change_Products_New</template>
    </alerts>
    <alerts>
        <fullName>Product_Creation_Update_Email_Outbound</fullName>
        <description>Product Creation/Update Email Outbound</description>
        <protected>false</protected>
        <recipients>
            <recipient>Product_Admin_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Product_Creation_Update_Outbound</template>
    </alerts>
    <rules>
        <fullName>GL Account Change - Products</fullName>
        <actions>
            <name>Alert_Accounting_when_sales_revenue_Account_field_changes_on_the_Product_object</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Alert accounting when the Sales Revenue Account on the Product object has changed</description>
        <formula>ischanged( c2g__CODASalesRevenueAccount__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Gl Account - New Product Alert</fullName>
        <actions>
            <name>New_Product_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Product Email Outbound</fullName>
        <actions>
            <name>Product_Creation_Update_Email_Outbound</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Send_Email_Alert_On_Update__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Used for when a Product record is created and every time it is updated, an Email will be sent to the appropriate user.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
