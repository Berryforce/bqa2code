<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ABD_Backout_Disconnect_False</fullName>
        <description>If the ABD Pending Review Backout is processed, for Disconnects, set the Disconnect via ABD, Disconnected to False, and set Disconnect Reason to -None-.</description>
        <field>Disconnected__c</field>
        <literalValue>0</literalValue>
        <name>ABD Backout Disconnect False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ABD_Backout_Disconnect_Reaso</fullName>
        <description>If the ABD Pending Review Backout is processed, for Disconnects, set the Disconnect via ABD, Disconnected to False, and set Disconnect Reason to -None-.</description>
        <field>Disconnect_Reason__c</field>
        <name>ABD Backout Disconnect Reaso</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ABD_Backout_Disconnected_Via_ABD</fullName>
        <description>If the ABD Pending Review Backout is processed, for Disconnects, set the Disconnect via ABD, Disconnected to False, and set Disconnect Reason to -None-.</description>
        <field>Disconnected_Via_BOC_Purge__c</field>
        <literalValue>0</literalValue>
        <name>ABD Backout Disconnected Via ABD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Data_Feed_Type</fullName>
        <field>DataFeedType__c</field>
        <formula>Listing__r.Service_Order_Stage_item__r.Data_Feed_Type__c</formula>
        <name>Capture Data Feed Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DL_Normalization_of_Phone</fullName>
        <field>Normalized_Phone__c</field>
        <formula>Service_Order_Stage__r.Normalized_Phone__c</formula>
        <name>DL Normalization of Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DL_Normalization_of_Street_Name</fullName>
        <field>Normalized_Listing_Street_Name__c</field>
        <formula>Service_Order_Stage__r.Normalized_Listing_Street_Name__c</formula>
        <name>DL Normalization of Street Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DL_Normalization_of_Street_Number</fullName>
        <field>Normalized_Listing_Street_Number__c</field>
        <formula>Service_Order_Stage__r.Normalized_Listing_Street_Number__c</formula>
        <name>DL Normalization of Street Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DL_Normalized_Last_Name</fullName>
        <field>Normalized_Last_Name_Business_Name__c</field>
        <formula>Service_Order_Stage__r.Normalized_Last_Name_Business_Name__c</formula>
        <name>DL Normalized Last Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Directory_Listing_RT_to_Non_Override</fullName>
        <description>Used to update a Directory Listing&apos;s RT and Page Layout to Non Override if changed from an Override back to Non Override.</description>
        <field>RecordTypeId</field>
        <lookupValue>Non_Override_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Directory Listing RT to Non Override</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Directory_Listing_RT_to_Override</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Address_Override_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Directory Listing RT to Override</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_CASESAFEID</fullName>
        <field>CASESAFEID__c</field>
        <formula>CASESAFEID( Id )</formula>
        <name>Populate CASESAFEID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_WP_Section_Page_Type_Lookup</fullName>
        <description>Update picklist field to &apos;WP&apos;.</description>
        <field>Section_Page_Type_Lookup__c</field>
        <literalValue>WP</literalValue>
        <name>Populate WP Section Page Type Lookup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_YP_Section_Page_Type_Lookup</fullName>
        <description>Update picklist field to &apos;YP&apos;.</description>
        <field>Section_Page_Type_Lookup__c</field>
        <literalValue>YP</literalValue>
        <name>Populate YP Section Page Type Lookup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Scoped_Listing_RT_to_Non_Override</fullName>
        <description>Used to update a Scoped Listing&apos;s RT and Page Layout to Non Override if changed from an Override back to Non Override.</description>
        <field>RecordTypeId</field>
        <lookupValue>Non_Override_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Scoped Listing RT to Non Override</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IsPhone_to_FALSE</fullName>
        <field>IsPhoneNumber__c</field>
        <literalValue>0</literalValue>
        <name>Set IsPhone to FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Left_TN_for_TOLL_FREE_PHRASE</fullName>
        <field>Left_Telephone_Phrase__c</field>
        <formula>&quot;Toll Free&quot;</formula>
        <name>Set Left TN for TOLL FREE PHRASE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Original_Product</fullName>
        <field>Original_Product__c</field>
        <formula>PRIORVALUE(Product__c)</formula>
        <name>Set Original Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_WP_Logo_Checkbox</fullName>
        <field>WP_Logo_Product__c</field>
        <literalValue>1</literalValue>
        <name>Set WP Logo Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_WP_Logo_Product_Checkbox</fullName>
        <field>WP_Logo_Product__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck WP Logo Product Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DL_RT_to_Caption_RT</fullName>
        <description>Field Update for DL RT to Caption RT</description>
        <field>RecordTypeId</field>
        <lookupValue>Caption_Member</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update DL RT to Caption RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Is_Name_Field</fullName>
        <field>IsName__c</field>
        <literalValue>1</literalValue>
        <name>Update Is Name Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Phone_Area_Code</fullName>
        <field>Area_Code_WF__c</field>
        <formula>LEFT(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(Phone_Number__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), 3)</formula>
        <name>Update Phone Area Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Phone_Exchange</fullName>
        <field>Exchange_WF__c</field>
        <formula>MID(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(Phone_Number__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), 4, 3)</formula>
        <name>Update Phone Exchange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Phone_Line</fullName>
        <field>Line_WF__c</field>
        <formula>RIGHT(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(Phone_Number__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), 4)</formula>
        <name>Update Phone Line</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Phone_Override_to_Include</fullName>
        <description>The Field Update to update the Phone Override field to include the § symbol &amp; the phone number.</description>
        <field>Phone_Override__c</field>
        <formula>&quot;§ &quot; &amp; Phone_Number__c</formula>
        <name>Update Phone Override to Include §</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Phone_Override_to_Remove</fullName>
        <description>When § was included but then removed, clear contents of Phone Override field</description>
        <field>Phone_Override__c</field>
        <name>Update Phone Override to Remove §</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>%C2%A7 Monitoring Symbol REMOVAL</fullName>
        <actions>
            <name>Update_Phone_Override_to_Remove</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Directory_Listing__c.Include_Monitoring_Symbol__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>data.migration@berryforce.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>devbatchuser@csc.com.berry</value>
        </criteriaItems>
        <description>To remove the contents of Phone Override when § was setup but then was removed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%C2%A7 Monitoring Symbol SETUP</fullName>
        <actions>
            <name>Update_Phone_Override_to_Include</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Directory_Listing__c.Include_Monitoring_Symbol__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>data.migration@berryforce.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>devbatchuser@csc.com.berry</value>
        </criteriaItems>
        <description>This workflow rule is used to update the Phone Override text field to include the § in front of the number when Include § Monitoring Symbol is TRUE.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ABD Pending Review Backout</fullName>
        <actions>
            <name>ABD_Backout_Disconnect_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ABD_Backout_Disconnect_Reaso</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ABD_Backout_Disconnected_Via_ABD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If the ABD Pending Review Backout is processed, the ABD Pending Review flag is changed from True to False, and for Disconnects, set the Disconnect via ABD and Disconnected to False, and set Disconnect Reason to -None-.</description>
        <formula>AND ( ABD_Pending_Review__c = FALSE, Disconnected__c = TRUE, Disconnected_Via_BOC_Purge__c = TRUE, !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Capture CASESAFEID</fullName>
        <actions>
            <name>Populate_CASESAFEID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to capture the CASESAFEID for a Scoped Listing</description>
        <formula>AND( ISNEW(), !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capture Original Product</fullName>
        <actions>
            <name>Set_Original_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>To Capture the name of the original Product on the Scoped Appearance</description>
        <formula>AND(OR( AND( ISNEW(),          Product__c != NULL)     ,         AND( ISCHANGED(Product__c),          PRIORVALUE(Product__c) = NULL,          Original_Product__c = NULL) ), !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capture Phone Area Code%2C Exchange%2C Line</fullName>
        <actions>
            <name>Update_Phone_Area_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Phone_Exchange</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Phone_Line</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to capture the Phone&apos;s Area Code, Exchange, Line</description>
        <formula>AND( OR( ISCHANGED( Phone_Number__c ),  Area_Code_WF__c = NULL,  Exchange_WF__c = NULL,  Line_WF__c = NULL  ), !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Normalization of Last Name</fullName>
        <actions>
            <name>DL_Normalized_Last_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populate the Normalized Last Name / Business Name from the Service Order Stage object</description>
        <formula>AND((Normalized_Last_Name_Business_Name__c = NULL), NOT(Service_Order_Stage__r.Normalized_Last_Name_Business_Name__c = NULL),!$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Normalization of Phone</fullName>
        <actions>
            <name>DL_Normalization_of_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populate the Normalized Phone from the Service Order Stage object</description>
        <formula>AND((Normalized_Phone__c = NULL),  NOT(Service_Order_Stage__r.Normalized_Phone__c = NULL), !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Normalization of Street Name</fullName>
        <actions>
            <name>DL_Normalization_of_Street_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populate the Normalized Street Name from the Service Order Stage object</description>
        <formula>AND(( Normalized_Listing_Street_Name__c  = NULL),  NOT( Service_Order_Stage__r.Normalized_Listing_Street_Name__c  = NULL),!$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Normalization of Street Number</fullName>
        <actions>
            <name>DL_Normalization_of_Street_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populate the Normalized Street Number from the Service Order Stage object</description>
        <formula>AND((Normalized_Listing_Street_Number__c = NULL),  NOT(Service_Order_Stage__r.Normalized_Listing_Street_Number__c = NULL),!$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Data Feed Type</fullName>
        <actions>
            <name>Capture_Data_Feed_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( ISNULL(DataFeedType__c), !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate WP Section Page Type Lookup</fullName>
        <actions>
            <name>Populate_WP_Section_Page_Type_Lookup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to populate the Lookup Filter picklist field for Products as it relates to WP.</description>
        <formula>AND( Section_Page_Type__c = &quot;WP&quot;, OR(ISNEW(), ISCHANGED(Directory_Section__c)),!$Setup.ValidationSkip__c.InActive__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate YP Section Page Type Lookup</fullName>
        <actions>
            <name>Populate_YP_Section_Page_Type_Lookup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to populate the Lookup Filter picklist field for Products as it relates to YP.</description>
        <formula>AND( Section_Page_Type__c = &quot;YP&quot;, OR(ISNEW(), ISCHANGED(Directory_Section__c)),!$Setup.ValidationSkip__c.InActive__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Scoped Listing Non Override RT Set</fullName>
        <actions>
            <name>Scoped_Listing_RT_to_Non_Override</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Directory_Listing__c.Address_Override__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Directory_Listing__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Caption Member</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>data.migration@berryforce.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>devbatchuser@csc.com.berry</value>
        </criteriaItems>
        <description>Used to update a Scoped Listing&apos;s Record Type and Page Layout for Non Address Overrides</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Scoped Listing Override RT Set</fullName>
        <actions>
            <name>Directory_Listing_RT_to_Override</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Directory_Listing__c.Address_Override__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Directory_Listing__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Caption Member</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>data.migration@berryforce.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>devbatchuser@csc.com.berry</value>
        </criteriaItems>
        <description>Used to update a Scoped Listing&apos;s Record Type and Page Layout for Address Overrides</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set IsPhone to FALSE for Caption Headers</fullName>
        <actions>
            <name>Set_IsPhone_to_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>By default, a Scoped Listing will have its IsPhone attribute set to TRUE because the majority of Scoped Listings will print a phone number. Caption Headers will not, therefore this Workflow fires when a Scoped Listing is created AND it is a Caption Header</description>
        <formula>AND(Caption_Header__c = TRUE, !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set WP Logo Checkbox</fullName>
        <actions>
            <name>Set_WP_Logo_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to set the WP Logo Checkbox if the Scoped Listing Caption Header&apos;s Product is updated to a WP Logo Product</description>
        <formula>AND( ISCHANGED( Product__c ),      Caption_Header__c = TRUE,      ISPICKVAL(Product__r.Print_Specialty_Product_Type__c , &quot;WP Logo&quot;),!$Setup.ValidationSkip__c.InActive__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck WP Logo Product Checkbox</fullName>
        <actions>
            <name>Uncheck_WP_Logo_Product_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to set the WP Logo Product Checkbox to FALSE if a Scoped Listing Caption Header has a WP Logo Product but is then reverted back to its original, Non WP Logo Product.</description>
        <formula>AND( ISCHANGED( Product__c),      Caption_Header__c = TRUE,      TEXT(Product__r.Print_Specialty_Product_Type__c) != &quot;WP Logo&quot;,      WP_Logo_Product__c = TRUE,!$Setup.ValidationSkip__c.InActive__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update DL RT to Caption RT</fullName>
        <actions>
            <name>Update_DL_RT_to_Caption_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When a Directory Listing is either a Caption Header or a Caption Member, update the RT to Caption Member</description>
        <formula>(Caption_Header__c = TRUE  || Caption_Member__c = TRUE) &amp;&amp; !$Setup.ValidationSkip__c.InActive__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update IsName for Caption Header</fullName>
        <actions>
            <name>Update_Is_Name_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Directory_Listing__c.Caption_Header__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>data.migration@berryforce.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>devbatchuser@csc.com.berry</value>
        </criteriaItems>
        <description>Update IsName for Caption Header</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Left TN Phrase for Toll Free Phrase</fullName>
        <actions>
            <name>Set_Left_TN_for_TOLL_FREE_PHRASE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Directory_Listing__c.Phone_Type__c</field>
            <operation>equals</operation>
            <value>Toll Free</value>
        </criteriaItems>
        <criteriaItems>
            <field>Directory_Listing__c.DataFeedType__c</field>
            <operation>equals</operation>
            <value>Daily Service Orders</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>data.migration@berryforce.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>devbatchuser@csc.com.berry</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
