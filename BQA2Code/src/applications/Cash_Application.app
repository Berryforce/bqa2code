<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>Cash Application</label>
    <logo>SharedDocuments/Berry_Logo.jpg</logo>
    <tab>standard-Account</tab>
    <tab>c2g__codaCashEntry__c</tab>
    <tab>fflbx__Receipt__c</tab>
    <tab>ffirule__IntegrationRuleJob__c</tab>
    <tab>Berry_Lockbox_Batch_Header__c</tab>
    <tab>Elevon_Staging__c</tab>
    <tab>Elavon_Batch__c</tab>
    <tab>c2g__codaInvoice__c</tab>
    <tab>c2g__codaCreditNote__c</tab>
    <tab>standard-Case</tab>
    <tab>Radio_Button_Background_Matching</tab>
    <tab>User_Controlled_Background_Matching</tab>
    <tab>Custom_Cash_Matching</tab>
    <tab>c2g__ExtendedCashMatching</tab>
    <tab>ffps_ct__FlatTransaction__c</tab>
    <tab>ffps_berry__Cash_Entry_Staging__c</tab>
    <tab>standard-report</tab>
</CustomApplication>
