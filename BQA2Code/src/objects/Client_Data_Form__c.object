<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>For Client Data Form for Reps to enter some additional client data from a recent conversation.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <description>For Client Data Form for Reps to enter some additional client data from a recent conversation.</description>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Client Data Forms</relationshipLabel>
        <relationshipName>Client_Data_Forms</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Advertising_Copy_Factors_Print_Digital__c</fullName>
        <description>For Client Data Form for Reps to enter some additional client data from a recent conversation.</description>
        <externalId>false</externalId>
        <inlineHelpText>Reliability * Accreditation * Special Services * Completeness * Illustration * Locations</inlineHelpText>
        <label>Advertising Copy Factors (Print/Digital)</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Business_Details__c</fullName>
        <description>For Client Data Form for Reps to enter some additional client data from a recent conversation.</description>
        <externalId>false</externalId>
        <inlineHelpText>Current Situation, Desired Situation, Gap</inlineHelpText>
        <label>Business Details</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>For Client Data Form for Reps to enter some additional client data from a recent conversation.</description>
        <externalId>false</externalId>
        <label>Contact</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Please search and select a Contact from this Account</errorMessage>
            <filterItems>
                <field>Contact.AccountId</field>
                <operation>equals</operation>
                <valueField>$Source.Account__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Client Data Forms</relationshipLabel>
        <relationshipName>Client_Data_Forms</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Other_Information__c</fullName>
        <description>For Client Data Form for Reps to enter some additional client data from a recent conversation.</description>
        <externalId>false</externalId>
        <label>Other Information</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Preparation__c</fullName>
        <description>For Client Data Form for Reps to enter some additional client data from a recent conversation.</description>
        <externalId>false</externalId>
        <inlineHelpText>Information Discovered Prior to the Call/Painpoint</inlineHelpText>
        <label>Preparation</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Program_Enhancements__c</fullName>
        <description>For Client Data Form for Reps to enter some additional client data from a recent conversation.</description>
        <externalId>false</externalId>
        <inlineHelpText>Where Does the Customer Need Our Help</inlineHelpText>
        <label>Program Enhancements</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Questions_to_Ask__c</fullName>
        <description>For Client Data Form for Reps to enter some additional client data from a recent conversation.</description>
        <externalId>false</externalId>
        <inlineHelpText>Prepped Questions for the Call</inlineHelpText>
        <label>Questions to Ask</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Transactional_or_Consultative__c</fullName>
        <description>For Client Data Form for Reps to enter some additional client data from a recent conversation.</description>
        <externalId>false</externalId>
        <label>Transactional or Consultative</label>
        <picklist>
            <picklistValues>
                <fullName>Transactional</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Consultative</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Why_Did_Should_They_Purchase__c</fullName>
        <description>For Client Data Form for Reps to enter some additional client data from a recent conversation.</description>
        <externalId>false</externalId>
        <inlineHelpText>Value/ROI</inlineHelpText>
        <label>Why Did/Should They Purchase</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <label>Client Data Form</label>
    <nameField>
        <label>CDF Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Client Data Forms</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <webLinks>
        <fullName>New_CDF</fullName>
        <availability>online</availability>
        <description>For use on Account page related list for Client Data Forms to prepopulate the CDF Name with the Account Name</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New CDF</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/a58/e?CF00NG000000D3bPt={!Account.Name}&amp;CF00NG000000D3bPt_lkid={!Account.Id}&amp;Name={!Account.Name}&amp;retURL=%2F{!Account.Id}</url>
    </webLinks>
</CustomObject>
