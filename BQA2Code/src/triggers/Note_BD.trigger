/****************************
Created By : Sathishkumar Periyasamy (speriyasamy2@csc.com)
Created Date : 11/22/2012 10:11 PM
Last Modified By : Sathishkumar Periyasamy (speriyasamy2@csc.com)
Last Modidied Date : 01/24/2013 
Trigger Status : Active
TestMethod Controller Name : NoteBDTriggerTest
Code Coverage : 100%
Use of this Trigger : When customer try to deleted the notes from account to prevent the deletion and show the message.
*****************************/

trigger Note_BD on Note (before delete) {    
    for(Note objNote : Trigger.Old){
        if(objNote != null){
            if(objNote.ParentId != null){            	
            	
                String strAccountPrefix = String.valueOf(objNote.ParentId).substring(0,3);
                
                //Checking deleted row is part of account object realted note.
                if(strAccountPrefix.equals('001')){
                	//Display the message to the UI.
                    objNote.adderror(CommonMessages.preventAccountTaskDelete+ objNote.Title);                    
                }
            }
        }
    }
}