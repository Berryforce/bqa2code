trigger UserAdoptionTeamChatterGroup_trigger on User (after insert, before insert, after update, before update) {
    if ( Trigger.isInsert ) {    
        if ( Trigger.isAfter ) { 
            List<id> UserIds = new List<id>();
            for ( user u: Trigger.new ) {
                if ( u.UserType == 'Standard' ) {
                    UserIds.add(u.id);
                }                 
            } 
            if ( UserIds.size() > 0 ) {
                AddToAdoptionTeamChatterGroupApex.addUserToGroup(UserIds);  
            }           
        }
    }
}