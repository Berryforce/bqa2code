trigger SACRACTransmission_BAIUD on SACRAC_Transmission__c (before insert, before update, after update, after insert) {
	if(trigger.isBefore) {
		if(trigger.isInsert) {
			SACRACTransmissionHandlerController.onBeforeInsert(trigger.new);
		} else if(trigger.isUpdate) {
			SACRACTransmissionHandlerController.onBeforeUpdate(trigger.new);
		}
	}
	if(trigger.isAfter) {
		if(trigger.isInsert) {
			SACRACTransmissionHandlerController.onAfterInsert(trigger.new);
		} else if(trigger.isUpdate) {
			SACRACTransmissionHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
		}
	}
}