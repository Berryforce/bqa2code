trigger DigitalProductRequirement_BUIDAU on Digital_Product_Requirement__c(before update, before insert, before Delete, after update,after insert) {
    if(Test.isRunningtest() || !CommonMethods.skipTriggerLogic(CommonMessages.dffObjectName)) {
    if(trigger.isBefore) {
        if(trigger.isInsert) {
            DFFHandlerController.onBeforeInsert(trigger.new);            
        }
        else if(trigger.isUpdate) {
            DFFHandlerController.onBeforeUpdate(trigger.new, trigger.oldMap);      
        }
    }
    
    if (trigger.isAfter) {
    	if(trigger.isInsert){
    		 DFFHandlerController.onAfterInsert(trigger.new);
    	}
        if (trigger.isUpdate) {
            DFFHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
        }
    }
    }
    /*Decimal DFFmax = 0.0;
    list < Digital_Product_Requirement__c > lstSEQDFF = new list < Digital_Product_Requirement__c >();
    List <Digital_Product_Requirement__c> DFFChildList = new List < Digital_Product_Requirement__c>();
    List <Digital_Product_Requirement__c> DFFChildListOld = new List < Digital_Product_Requirement__c>();
    If((Trigger.isInsert || Trigger.isUpdate || Trigger.isDelete) && (Trigger.isBefore)) {

        Set < Id > Id = new Set < Id > ();
        Set < Id > dffUpdate = new Set < Id > ();

       
        Decimal DFFmaxSEQ = 0.0;
        // select all urn rt 
        map < string, string > rtlist = new map < string, string > ();
        list < RecordType > RTTYPES = new list < RecordType > ();
        RTTYPES = [SELECT Id, Name, DeveloperName, IsActive FROM RecordType WHERE SobjectType = 'Digital_Product_Requirement__c'
            AND(DeveloperName like '%YP%'
                OR DeveloperName like '%Print%')
        ];
        for (RecordType RT: RTTYPES) {
            rtlist.put(RT.id, RT.id);
        }
        
        // create urn for non digital items
        if (RTTYPES.size() > 0) {
             list <Digital_Product_Requirement__c> lstDFF = [SELECT Id, recordtypeid, URN_Number__c FROM Digital_Product_Requirement__c WHERE URN_Number__c != Null AND (recordtype.DeveloperName like '%YP%'
                OR recordtype.DeveloperName like '%Print%')
                 ORDER BY URN_Number__c DESC limit 2];
            
            //Added by Joe Henry to allow Max Number from YPC Graphic Child object 2/17/2014
            //system.debug('**************** lstDFF[0].URN_Number__c ' + lstDFF[0].URN_Number__c + '  ****************');
            
            list <YPC_Graphics__c> lstYPC = [SELECT Id, Name, URN__c FROM YPC_Graphics__c ORDER BY URN__c DESC limit 1];
            //system.debug('**************** lstYPC[0].URN__c ' + lstYPC[0].URN__c+ '  ****************');
            if(lstYPC.size() == 0) {
                DFFmax = lstDFF[0].URN_Number__c;
            }else if(lstDFF[0].URN_Number__c > lstYPC[0].URN__c){
                DFFmax = lstDFF[0].URN_Number__c;
            }else{
                DFFmax = lstYPC[0].URN__c;
            }
            //End Addition 2/17/2014     
            //Added by Joe Henry to get the Max Sequence Number 10/28/2013
            lstSEQDFF = [SELECT Id, recordtypeid, Sequence__c FROM Digital_Product_Requirement__c WHERE Sequence__c != Null AND (recordtype.DeveloperName like '%YP%'
                OR recordtype.DeveloperName like '%Print%')
            ORDER BY URN_Number__c DESC limit 1];
            DFFmaxSEQ = lstSEQDFF[0].Sequence__c;
        }
        
 

        system.debug('**************** URN_Number max ' + DFFmax + '  ****************');
        system.debug('**************** Sequence max ' + DFFmaxSEQ + '  ****************');
        if (trigger.isInsert) {
            system.debug('**************** is in the URN_Number if insert ****************');
            for (Digital_Product_Requirement__c dprinsert: trigger.new) {
                if (rtlist.containsKey(dprinsert.RecordTypeID)) {
                    DFFmax++;
                    DFFmaxSEQ++; //Added by Joe Henry 10/28/2013
                    system.debug('**************** DFFmax is now ' + DFFmax + ' ****************');
                    system.debug('**************** DFFmaxSEQ is now ' + DFFmaxSEQ + ' ****************');
                    dprinsert.URN_Number__c = DFFmax;
                    dprinsert.Sequence__c = DFFmaxSEQ; //Added by Joe Henry 10/28/2013
                    system.debug('****************dprinsert.URN_Number__c = ' + dprinsert.URN_Number__c + ' ****************');
                    system.debug('****************dprinsert.Sequence__c = ' + dprinsert.Sequence__c + ' ****************');
                }
            }
            //Commented by sathish -- Due to facing issue while closing opportunity.
            //DigitalProductRequirement_BIUDtrigger.proofContAddrEmailSet(trigger.New);
            DigitalProductRequirement_BIUDtrigger.updateFields(trigger.New);
        }

        //build update child records for iypc here
        If(Trigger.isUpdate || Trigger.isInsert){
            // get the record types 
            System.debug('limits before recordtype' + Limits.getQueries());
            list < RecordType > YPRecTypes = [SELECT Id, Name, DeveloperName, IsActive, SobjectType FROM  RecordType WHERE DeveloperName like '%YP_Bronz%' OR DeveloperName like '%YP_Sil%' OR DeveloperName like '%YP_Dia%' OR DeveloperName like '%YP_Gold%' OR DeveloperName like '%YP_Plat%' OR DeveloperName like '%YP_Prio%'];
            System.debug('limits after recordtype' + Limits.getQueries());
            system.debug('SEO **************** YPRecTypes.size() is '+ YPRecTypes.size()+'****************');
             // store a list of the correct ids that fit the record type filter
             list <String> tmpidlist = new  list <String>();
             system.debug('SEO **************** lstSEQDFF.size() is '+ lstSEQDFF.size()+'****************');
           /*  
           //Remarked out by Joe Henry 3/7/2014 - This does not make sense
            for(Digital_Product_Requirement__c DFID :lstSEQDFF){               
                 for(RecordType RT :YPRecTypes){
                   system.debug('SEO **************** RT.id is '+ RT.id+'****************'+'DFID.recordtypeid*********'+DFID.recordtypeid);
                   if(DFID.recordtypeid == RT.id){
                       tmpidlist.add(DFID.id);
                   } 
                 } 
             }
             */
           /*  //End Remark my Joe Henry
             system.debug('SEO **************** tmpidlist.size() is '+ tmpidlist.size()+'****************');
             // get the records that are in the id list 
             for (Digital_Product_Requirement__c dff2: trigger.new) {
                  for(RecordType RT :YPRecTypes){
                  system.debug('**************** RT.id from list is '+ RT.Id  +' **** dff2.RecordtypeId is '+ dff2.RecordtypeId +'****************');
                     if(RT.Id == dff2.RecordtypeId){
                         DFFChildList.add(dff2);
                     }
                 }
             }
             // get the Old list of data for updates as needed 
             If(Trigger.isUpdate ){
                 for (Digital_Product_Requirement__c DFFOLD :trigger.old) { 
                     for(Digital_Product_Requirement__c dffchild :DFFChildList){
                         if(dffchild.id == DFFOLD.id){
                             DFFChildListOld.add(DFFOLD);    
                         }
                     }
                 }
             }         
             // exception handler to make sure there are list members to send
             system.debug('SEO **************** DFFChildList.size() is '+DFFChildList.size()+'****************');
             if(DFFChildList.size() > 0 && trigger.isUpdate){    // trigger.isAfter        
                  YPCGraphicHandler_v1.createChildern(DFFChildList, DFFChildListOld , DFFmax, YPRecTypes );
             }
        }


        if (trigger.isUpdate) {

        List<Digital_Product_Requirement__c> ypcDFFvldt = new List<Digital_Product_Requirement__c>();

            for(Digital_Product_Requirement__c dprDFF: trigger.new){

                if((System.Trigger.oldMap.get(dprDFF.id)
                    .CpnDesc_CstLnkUrl_CstLnkTxt__c) == true && (System.Trigger.newMap.get(dprDFF.id)
                    .CpnDesc_CstLnkUrl_CstLnkTxt__c) == true){

                    if(dprDFF.add_ons__c != null){

                    ypcDFFvldt.add(dprDFF); 

                    }                                     
                }
            }

            DigitalProductRequirement_BIUDtrigger.ypcCstmCpnVldtn(ypcDFFvldt);
            DigitalProductRequirement_BIUDtrigger.proofContAddrEmailSetNew(trigger.New);
            system.debug('*******Trigger IsUpdate*********' + trigger.New);
        }

            //Commented the code as the scenario is changed. Now user should be able to update a submitted DFF manually, 
            //however UpdateDFF controller will update only unsubmitted DFF.
            /* User IUser =[Select id from user where Name='Integration User' Limit 1];
        for(Digital_Product_Requirement__c dprnew1: trigger.new)
        {
            
           //Code is for: Do Not Update Submitted DFF's 
           //Is_ByPass__c Field is for allowing modifcation on DFF when ModifyOLI has update with DFF
           if(UserInfo.getUserId()!=IUser.Id) //to bypass DFF Locking for Integration User
           {
                if(dprnew1.Fulfillment_Submit_Status__c == 'Complete' && dprnew1.Is_ByPass__c==false)
                {
                  // commented out by Reddy, this line is stopping me to update DFF with talus return ids 
                  //un-commented the code as the condition is applied to bypass this for Integration User 
                  dprnew1.addError(' DFF record is already submitted and cannot be updated.');
                }
                else
                {
                  dprnew1.Is_ByPass__c=false;
                  Id.add(dprnew1.id);
                }
            }
        }*/
        
        /*
    List<ProcessInstance> processreq= [select Id, TargetObjectId, Status, IsDeleted, CreatedDate, 
                                       CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp 
                                       from ProcessInstance where TargetObjectId=:Id limit 1];
    system.debug('PR:' + processreq);
    
    if(trigger.isUpdate){
        for(Digital_Product_Requirement__c dprnew: trigger.new){
            for(ProcessInstance processreq1: processreq){
            system.debug('EnterCondition');
                               
            }
                        
        }
        
    }    
*/

 /*   }
  
        
    If(Trigger.isUpdate && Trigger.isAfter) {
        Set < Id > dffId = new Set < Id > ();
        Set < Id > ypcDffPost = new Set < Id > ();
        Set < Id > ypcDffUpdt = new Set < Id > ();

        if (trigger.isAfter) {
            for (Digital_Product_Requirement__c dprnew1: trigger.new) {

                if ((System.Trigger.oldMap.get(dprnew1.id).Talus_DFF_Id__c != System.Trigger.newMap.get(dprnew1.id).Talus_DFF_Id__c) && (System.Trigger.oldMap.get(dprnew1.id).Talus_Subscription_Id__c != System.Trigger.newMap.get(dprnew1.id).Talus_Subscription_Id__c)) {

                        //add all the dff ids into a Set for updating OLIs with Talus Subscription and DFF Ids
                        dffId.add(dprnew1.id);
                }

                if ((System.Trigger.oldMap.get(dprnew1.id)
                        .Miles_Status__c != 'Complete') && (System.Trigger.newMap.get(dprnew1.id)
                        .Miles_Status__c == 'Complete') && dprnew1.Talus_DFF_Id__c == null &&
                    dprnew1.Talus_Subscription_ID__c == null) {

                    system.debug('*******GettingInForPost*********');
                    ypcDffPost.add(dprnew1.id);

                }

                if ((System.Trigger.oldMap.get(dprnew1.id)
                        .Miles_Status__c != 'Complete') && (System.Trigger.newMap.get(dprnew1.id)
                        .Miles_Status__c == 'Complete') && dprnew1.Talus_DFF_Id__c != null &&
                    dprnew1.Talus_Subscription_ID__c != null) {

                    system.debug('*******GettingInForPatch*********');
                    ypcDffUpdt.add(dprnew1.id);

                }

            }
            
            if (System.isFuture() || System.isBatch()) {
                //call the updOLN method to update OLI with Talus Subscription Id
                UpdateOLIWithTalusSubscriptionId.updOLNNow(dffId);
            }
            else {
                //call the updOLN method to update OLI with Talus Subscription Id
                UpdateOLIWithTalusSubscriptionId.updOLN(dffId);
            }

            //call the clnDff method to track the list of all the field that got updated
            //this logic is for sending a PATCH call to Talus
            UpdateOLIWithTalusSubscriptionId.clnDff(trigger.new, trigger.oldmap);
            system.debug('*********Submitted Changes for Patch**********');
            
            //call the SendMilesDFFTalus class to automate iYP POST call to Talus
            if(ypcDffPost.size() <= 10){

            //SendMilesDFFTalus.ypcDFFAutoPost(ypcDffPost);
            
            }

            //call the SendMilesDFFTalus class to automate iYP PATCH call to Talus
            if(ypcDffUpdt.size() <= 10){

            //SendMilesDFFTalus.ypcDFFAutoUpdt(ypcDffUpdt);
            
            }
        }
    }*/
}