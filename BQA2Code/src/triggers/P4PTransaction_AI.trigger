trigger P4PTransaction_AI on P4P_Transaction__c (after insert) {
	    P4PTransactionHandlerController.populateCurrentMonthAndPeriodClicks(trigger.new);
}