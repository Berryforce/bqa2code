trigger ChckHeadingDisclaimer on Heading_Disclaimer__c (before update, after update) 
{

    for(Heading_Disclaimer__c HD : Trigger.New) 
    {     
        Heading_Disclaimer__c beforeUpdate = System.Trigger.oldMap.get(HD.Id);        
        if(HD.Disclaimer_Text__c != beforeUpdate.Disclaimer_Text__c) 
        {         
            for(List<Section_Heading_Mapping__c> LstSectionHead: [Select Id,Disclaimer_Text__c from Section_Heading_Mapping__c WHERE Heading_Disclaimer__r.Id =: HD.Id])
            {       
                for(Section_Heading_Mapping__c SH : LstSectionHead) 
                {  
                    SH.Disclaimer_Text__c= HD.Disclaimer_Text__c;
                }  
                update LstSectionHead; 
            }
        }   
    }    
}