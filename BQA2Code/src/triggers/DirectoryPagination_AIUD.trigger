trigger DirectoryPagination_AIUD on Directory_Pagination__c (before insert, before update, after delete, after insert, after update) {
    if(!Test.isRunningtest() || !CommonMethods.skipTriggerLogic(CommonMessages.DPObjectName)) {
        if(trigger.isAfter) {
            if(trigger.isInsert) {
                DirectoryPagination_AIUDTrigger.onAfterInsert(trigger.new);
            } 
        } if(trigger.isBefore) {
            if(trigger.isInsert) {
                DirectoryPagination_AIUDTrigger.onBeforeInsert(trigger.new);
            } else if(trigger.isUpdate) {
                DirectoryPagination_AIUDTrigger.onBeforeUpdate(trigger.new, trigger.oldMap);
            }
        }
        
        /*if(trigger.isInsert && trigger.isafter) {
            DirectoryPagination_AIUDTrigger.generateCounter(trigger.new);
        }
        if(trigger.isInsert && trigger.isbefore) {
            DirectoryPagination_AIUDTrigger.populateValues(trigger.new);
        }
        if(trigger.isUpdate && trigger.isbefore) {
            DirectoryPagination_AIUDTrigger.populateValues(trigger.new, trigger.oldMap);
        }*/
    }
}