@IsTest
public class DirectorySOQLMethodsTest {
    @isTest static void DirectorySOQLMethodsCoverage() {
        User u = TestMethodsUtility.generateStandardUser();
        System.runAs(u) {
            list<Account> lstAccount = new list<Account>();
            lstAccount.add(TestMethodsUtility.generateAccount('telco'));
            lstAccount.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount.add(TestMethodsUtility.generateAccount('publication'));
            insert lstAccount;  
            Account newAccount = new Account();
            Account newPubAccount = new Account();
            Account newTelcoAccount = new Account();
            for(Account iterator : lstAccount) {
                if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                    newAccount = iterator;
                }
                else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                    newPubAccount = iterator;
                }
                else {
                    newTelcoAccount = iterator;
                }
            }
            system.assertNotEquals(newAccount.ID, null);
            system.assertNotEquals(newPubAccount.ID, null);
            system.assertNotEquals(newAccount.Primary_Canvass__c, null);
            system.assertNotEquals(newTelcoAccount.ID, null);
            Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
            objTelco.Telco_Code__c = 'Test';
            update objTelco;
            system.assertNotEquals(newTelcoAccount.ID, null);
            Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
            Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
            Division__c objDiv = TestMethodsUtility.createDivision();
            Directory__c objDir = TestMethodsUtility.generateDirectory();
            objDir.Telco_Provider__c = objTelco.Id;
            objDir.Canvass__c = newAccount.Primary_Canvass__c;        
            objDir.Publication_Company__c = newPubAccount.Id;
            objDir.Division__c = objDiv.Id;
            insert objDir;
            System.assertNotEquals(null, objDir.Id);
            System.assertNotEquals(null, objDir.Canvass__c);
            Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
            Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
            insert objSHM;
            list<Directory_Edition__c> lstDE = new list<Directory_Edition__c>();
            for(Integer x=0; x<1 ; x++){
                Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
                objDirEd.Pub_Date__c=System.today().addMOnths(x);
                objDirEd.LSA_Directory_Version__c = TestMethodsUtility.generateAccountNumber(2000);
                objDirEd.Book_Status__c = 'NI';
                lstDE.add(objDirEd);
            }
            insert lstDE;
            
            System.assertNotEquals(null, lstDE);
            
            set<Id> setDirID = new set<Id>();
            set<String> setDirCode = new set<String>();
            setDirID.add(objDir.Id);
            setDirCode.add(objDir.Directory_Code__c);
            String strDirCode = objDir.Directory_Code__c;
            String canvassId = objDir.Canvass__c;
            Test.startTest();
            DirectorySOQLMethods.getNationalCommissionById(setDirID);
            DirectorySOQLMethods.getDirectoryByCode(setDirCode);
            DirectorySOQLMethods.getDirectoriesByCanvassId(canvassId);
            DirectorySOQLMethods.getDirectoryForMediaTrax();
            DirectorySOQLMethods.getDirectoryUpdateMediaTrax();
            DirectorySOQLMethods.getDirectoryDirsectionById(setDirID);
            DirectorySOQLMethods.getCanvassByDirId(setDirID);
            DirectorySOQLMethods.getDirectoryDEById(setDirID);
            Test.stopTest();
        }
    }
}