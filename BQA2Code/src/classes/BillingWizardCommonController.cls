public class BillingWizardCommonController {
	
	public static void postSalesInvoice(list<c2g.CODAAPICommon.Reference> lstRefAPI) {
		try {
            system.debug('Posting Ref : '+ lstRefAPI);
			c2g.CODAAPISalesInvoice_3_0.BulkPostInvoice(null, lstRefAPI); 
			//CommonMethods.addError('Sales Invoice Posted Successfully');
		}
		catch(Exception ex){
			//CommonMethods.addError('Error Occured while posting Invoice. Error : '+ex);
		}
	}
	
	public static void postSalesCreditNote(list<c2g.CODAAPICommon.Reference> lstRefAPI) {
		try {
			c2g.CODAAPISalesCreditNote_3_0.BulkPostCreditNote(null, lstRefAPI);
			//CommonMethods.addError('Sales Credit Note Posted Successfully');
		}
		catch(Exception ex){
			//CommonMethods.addError('Error Occured while posting sales credit note. Error : '+ex);
		}
	}
	
	
	public static list<c2g.CODAAPICommon.Reference> generateCODAAPICommonReference(set<Id> setIds) {
		list<c2g.CODAAPICommon.Reference> lstRefAPI = new list<c2g.CODAAPICommon.Reference>();
		for(Id iterator : setIds) {
			c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
            ref.Id = iterator;
			lstRefAPI.add(ref);
		}
		return lstRefAPI;
	}

}