global class DirPaginationTriggerForDataMigrBatch implements Database.Batchable<sObject>{
    
    global string filtercondition;
    
    global DirPaginationTriggerForDataMigrBatch(String condition){
        filtercondition=condition;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query ='Select d.uitalic__c, d.spacead__c, d.cust__c, d.art__c, d.Year__c, d.WP_Logo__c, d.WP_Logo_Header__c, d.Vendor__c, d.Under_Caption__c, d.UDAC__c,'+ 
                      'd.UDAC_Group__c, d.Type__c, d.Trademark_Sorting_NULL_Identifier__c, d.Trademark_Line__c, d.Trademark_Finding_Line__c, d.Trademark_Finding_Line_Record_ID__c,'+ 
                      'd.Trademark_Alternate_Call_Line__c, d.Trademark_2nd_Line_Indent_Level__c, d.Thumb_Letter__c, d.Telltale__c, d.Telco_Provider__c, d.Telco_Provided_Sort_Order__c,'+
                      'd.TCAP_Trademark_Line__c, d.TCAP_Text__c, d.SystemModstamp, d.Suppress__c, d.Source_Record_ID__c, d.Sort_As__c, d.Sort_As_Comment_Name__c,'+
                      'd.Sold_Indent_Order__c, d.Sold_Caption__c, d.Sold_Caption_Member__c, d.Sold_Caption_Header__c, d.Size__c, d.Sequence_in_Section__c, d.Sequence_Padded__c,'+
                      'd.Seniority_Date__c, d.Section_Type__c, d.Section_Heading_Mapping_ID__c, d.Section_Code__c, d.SectId__c, d.Secondary_Surname__c, d.Scoped_Indent_Order__c,'+
                      'd.Scoped_Caption_Member__c, d.Scoped_Caption_Header__c, d.Right_Telephone_Phrase__c, d.Reference_Listing__c, d.Publication_Company_Code__c,'+
                      'd.Provided_By_CLEC__c, d.Print_Phone__c, d.Print_Address__c, d.Phone_Override__c, d.Phone_Number__c, d.Phone_Number_Unformatted__c, d.Phone_Number_Line__c,'+
                      'd.Phone_Number_Exchange__c, d.Phone_Number_Area_Code__c, d.P4P_Number__c, d.OwnerId, d.Order_UDAC__c, d.Order_Of_Ouput__c, d.Order_Line_Item__c,'+ 
                      'd.Normalized_Listing_Name__c, d.Name, d.NCR_UDAC__c, d.NCR_UDAC_Font__c, d.NCR_UDAC_Color__c, d.NCR_Locality_Include__c, d.NCR_Listing_Include__c,'+
                      'd.NCR_Line__c, d.NCR_Line_Name__c, d.NCR_Heading_Code__c, d.NCR_Full_Name__c, d.NCR_Bus_Res_Include__c, d.NAT_CMR_Client__c, d.Miles_UDAC__c,'+
                      'd.Manual_City_Display_Rule__c, d.Locality__c, d.Listing__c, d.Listing_Street__c, d.Listing_Street_Number_ACR__c, d.Listing_Street_Name_ACR__c,'+
                      'd.Listing_State__c, d.Listing_Postal_Code__c, d.Listing_PO_Box__c, d.Listing_Name__c, d.Listing_City__c, d.Listing_City_Unformatted__c, d.Lineage_Title__c,'+
                      'd.Line_Indent_NCR__c, d.Line_Indent_ACR__c, d.Line_Break_for_Address_Phone__c, d.Left_Telephone_Phrase__c, d.Left_TN_Phrase_Indent_Level__c, d.Leader_Ad__c,'+
                      'd.LastViewedDate, d.LastReferencedDate, d.LastModifiedDate, d.LastModifiedById, d.ItemID__c, d.Is_Cross_Reference__c, d.IsDeleted, d.Internet_Bundle_Ad__c,'+
                      'd.Indent_Order__c, d.Indent_Level__c, d.Incorrect_Setup_Excluded_from_XML__c, d.Id, d.IBUN_Website_Two__c, d.IBUN_Website_One__c, d.IBUN_Type__c,'+
                      'd.Honorary_Title__c, d.Galley_Listing_Street__c, d.Full_Directory_Pagination_ID__c, d.Font_Zip_Code__c, d.Font_Street__c, d.Font_State__c, d.Font_Phone__c,'+
                      'd.Font_Name__c, d.Font_City__c, d.Font_Additional_Text__c, d.First_Name__c, d.Extra_Line_Type_Product__c, d.Display_Ad__c, d.Display_Ad_Bat__c,'+
                      'd.Directory_Section__c, d.Directory_Listing_Last_Business_Name__c, d.Directory_Heading__c, d.Directory_Heading_Normalized_Sort_Name__c,'+
                      'd.Directory_Heading_ID__c, d.Directory_Heading_Code__c, d.Directory_Edition__c, d.Directory_Edition_Name__c,'+
                      'd.Directory_Edition_Code__c, d.Directory_Code__c, d.Dir_Page_Number__c, d.Designation__c, d.DP_Print_Specialty_Product_Type__c, d.DP_Listing_Information__c,'+
                      'd.DP_IsTriggerExecuted__c, d.DP_Edition_Year__c, d.Customer_Type__c, d.Cust_Phone_Number__c, d.Cross_Reference_Text__c, d.Cross_Reference_Indent__c,'+
                      'd.CreatedDate, d.CreatedById, d.Coupon_Back_UDAC__c, d.Coupon_Back_Phone__c, d.Coupon_Back_Order_Number__c, d.Coupon_Back_Cust_Number__c,'+
                      'd.Coupon_Back_Artwork__c, d.Coupon_Ad__c, d.Contract_Ref__c, d.Continuous_Service_Order_Appearance__c, d.Color_Zip_Code__c, d.Color_Tab__c, d.Color_Street__c,'+
                      'd.Color_State__c, d.Color_Phone__c, d.Color_Name__c, d.Color_City__c, d.Color_Additional_Text__c, d.City_Name_Unedited__c, d.City_Name_Suppress_Override__c,'+
                      'd.City_Name_Section_Suppressed__c, d.City_Name_Section_Abbreviation__c, d.City_Name_Abbrv_Override__c, d.City_Abbreviation_Suppression__c, d.Caption_Related__c,'+
                      'd.Caption_Phone__c, d.Caption_Member__c, d.Caption_Header__c, d.Caption_Display_Text__c, d.CLEC_Provider__c, d.Billboard__c, d.Banner__c, d.Anchor__c,'+
                      'd.Anchor_Text__c, d.Anchor_Ref__c, d.Anchor_Product_Type__c From Directory_Pagination__c d '+filtercondition+' ORDER BY d.Directory_Section__c';
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext bc, List<Directory_Pagination__c> listDP) {
        Savepoint sp = Database.setSavepoint();
        try{
            DirectoryPagination_AIUDTrigger.splitDPAfterInsert(listDP); 
            for(Directory_Pagination__c dirPag:listDP){
                dirPag.DP_IsTriggerExecuted__c=true;
            }
        }catch(Exception e){
            System.debug('The exception is'+e);
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Batch trigger run for Directory Pagination records');
        }
        
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }

}