@isTest
public class MediaTraxServiceForUpdateHeadingTest{
    public static testmethod void MediaTraxUpdateHeadingBatchTest(){
        List<MediaTrax_API_Configuration__c> lstMedia = new list<MediaTrax_API_Configuration__c>();
        
        MediaTrax_API_Configuration__c objMedia = new MediaTrax_API_Configuration__c(NAME='GetAuthToken',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/authentication.cfc',HEADER_SOAPACTION__C='GetAuthentication',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='GetAuthenticationResponse',RETURN_METHOD__C='GetAuthenticationReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='',PREDICATEVALUE__C='',FIELDS_MAPPING__C='',OPERATION__C='');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='UpdateHeadings',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/headings.cfc',HEADER_SOAPACTION__C='UpdateHeadings',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='UpdateHeadingsResponse',RETURN_METHOD__C='UpdateHeadingsReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='headingId,headingName',OPERATION__C='Update');
        lstMedia.add(objMedia);
        insert lstMedia;
        
        Directory_Heading__c objDH = TestMethodsUtility.generateDirectoryHeading();
        objDH.Media_Trax_Heading_ID__c = '123';
        objDH.Is_Changed__c = true;
        insert objDH;
        MediaTraxServiceForUpdateHeadingBatch objUHCallout = new MediaTraxServiceForUpdateHeadingBatch();
        database.executebatch(objUHCallout , 1);
    }
}