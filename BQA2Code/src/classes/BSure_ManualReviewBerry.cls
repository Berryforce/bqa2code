global class BSure_ManualReviewBerry {

    public BSure_ManualReviewBerry(ApexPages.StandardSetController controller) {

    }

    public String piareviewId{get;set;}
    public list<PIA_Credit_Review_Opp__c> lstcreditapp{get;set;}
    public String creditappbsure{get;set;}
    public BSure_ManualReviewBerry(ApexPages.StandardController standardController)
    {
        piareviewId = Apexpages.currentPage().getParameters().get('Id');
        lstcreditapp = new list<PIA_Credit_Review_Opp__c>();
        String strQry = '';
        if(piareviewId != null && piareviewId !='')
        {
            strQry = ' Select Id,name,ExternalId__c from PIA_Credit_Review_Opp__c where Id = \''+piareviewId +'\'';
            lstcreditapp = Database.query(strQry);
        }   
        if(lstcreditapp != null && lstcreditapp.size() > 0)
        {
            if(lstcreditapp.get(0).ExternalId__c != null)
            {
                creditappbsure = lstcreditapp.get(0).ExternalId__c;
            }
        }
        
    }
    public Vertex_Berry__PIA_Credit_Review__c getPIAReviewDetails()
    {
        Vertex_Berry__PIA_Credit_Review__c piaObj;
        if(creditappbsure != null && creditappbsure != '')
        {
            piaObj = [Select Id,Name from Vertex_Berry__PIA_Credit_Review__c where Id =: creditappbsure];
        }
        return piaObj;
    }
}