@isTest
public class OrderSetDFFsControllerTest{
    public static testMethod void OSDmethod(){

        test.starttest();
        
        Product2 prdt = CommonUtility.createPrdtTest();
        insert prdt;
                
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;
        
        Account a = CommonUtility.createAccountTest(c);
        insert a;       
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;
        
        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;
        
        
        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;
        
        Order_Line_Items__c oln = CommonUtility.createOLITest(c, a, cnt, oppty, ord, og);
        insert oln;
        
        Modification_Order_Line_Item__c mOln = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        insert mOln;
        
        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.OrderLineItemID__c=oln.id; dff1.recordTypeId = Label.DFF_Spotzer_RT_Id; dff1.Talus_Subscription_Id__c=''; dff1.Talus_DFF_Id__c=''; dff1.DFF_Product__c=prdt.id; dff1.Bundle__c='Spotzer BASE'; dff1.business_url__c='http://www.google1.com';
        insert dff1;
               
        Digital_Product_Requirement__c dff2 = CommonUtility.createDffTest();
        dff2.OrderLineItemID__c=oln.id; dff2.RecordTypeId=Label.DFF_Boostability_RT_Id; dff2.Talus_Subscription_Id__c=''; dff2.Talus_DFF_Id__c=''; dff2.DFF_Product__c=prdt.id; dff2.Bundle__c='Spotzer SEO'; dff2.business_url__c='http://www.google2.com';
        insert dff2;
        
        Digital_Product_Requirement__c dff3 = CommonUtility.createDffTest();
        dff3.OrderLineItemID__c=oln.id; dff3.Talus_Subscription_Id__c=''; dff3.RecordTypeId=Label.DFF_Boostability_RT_Id; dff3.Talus_DFF_Id__c=''; dff3.DFF_Product__c=prdt.id; dff3.Bundle__c='Spotzer SEO'; dff3.business_url__c='http://www.google1.com';
        insert dff3;

        Digital_Product_Requirement__c dff4 = CommonUtility.createDffTest();
        dff4.ModificationOrderLineItem__c=mOln.id; dff4.recordTypeId = Label.DFF_Spotzer_RT_Id; dff4.Talus_Subscription_Id__c=''; dff4.Talus_DFF_Id__c=''; dff4.DFF_Product__c=prdt.id; dff4.Bundle__c='Spotzer BASE'; dff4.business_url__c='http://www.google1.com';
        insert dff4;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.Talus_Subscription_Id__c=''; dff.Talus_DFF_Id__c=''; dff.DFF_Product__c=prdt.id; dff.business_url__c='http://www.google1.com';
        insert dff;
                           
        ApexPages.StandardController controller1 = new ApexPages.StandardController(dff1); 
        OrderSetDFFsController ordStDffs1 = new OrderSetDFFsController(controller1);
        ordStDffs1.getDffs();
        
        ApexPages.StandardController controller2 = new ApexPages.StandardController(dff4);
        OrderSetDFFsController ordStDffs2 = new OrderSetDFFsController(controller2);
        ordStDffs2.getDffs();        

        ApexPages.StandardController controller3 = new ApexPages.StandardController(dff);
        OrderSetDFFsController ordStDffs3 = new OrderSetDFFsController(controller3);
        ordStDffs3.getDffs();
                
        OrderSetDFFsSummaryController smryDffs = new OrderSetDFFsSummaryController(controller1);
        smryDffs.getSmrryFlds();
        smryDffs.getAllDffsSmmry();
        smryDffs.submitfulfillment();

        test.stoptest();
    }
}