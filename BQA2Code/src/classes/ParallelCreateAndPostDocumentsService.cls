/**
 * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
 * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
 * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
 * result in criminal or other legal proceedings.
 *
 * Copyright FinancialForce.com, inc. All rights reserved.
 * Created by Agustina Garcia
 */

public with sharing class ParallelCreateAndPostDocumentsService
{
    public static final String MIN_VAL = 'min';
    public static final String MAX_VAL = 'max';

    public static final String SINV = 'Sales Invoices';
    public static final String SCRN = 'Sales Credit Notes';
    public static final String CE = 'Cash Entries';
    public static final String SINV_OBJECTNAME = 'c2g__codaInvoice__c ';
    public static final String SCRN_OBJECTNAME = 'c2g__codaCreditNote__c ';
    public static final String CE_OBJECTNAME = 'c2g__codaCashEntry__c';
    public static final String SINV_STATUS_FIELDNAME = 'c2g__InvoiceStatus__c';
    public static final String SCRN_STATUS_FIELDNAME = 'c2g__CreditNoteStatus__c';
    public static final String CE_STATUS_FIELDNAME = 'c2g__Status__c';

    public static final String SOURCEOBJECT_NAME = 'ffirule__SourceObject__c';
    public static final String SOURCEOBJECT_PROCESSEDFIELD_NAME = 'ffirule__SourceObjectProcessedField__c';

    /**
     * In order to run it from Developer console
     * Id ruleId = 'a0I2400000235BL'; //Integreation Rule Id
     * Integer scopeSize = 10; //number of records created on each chunck
     * String nameMask = 'OAR-{0000000000}'; //mask value comes from custom setting
     * ParallelCreateAndPostDocumentsService.createDocumentsInBatch(ruleId, scopeSize, nameMask);
     */
    public static void createDocumentsInBatch(Id ruleId, Integer scopeSize, String nameMask)
    {
        if(nameMask == null || nameMask == '')
        {
            throw new ParallelException('NameMask custom setting must be populated.');
        }

        List<SObject> rules = getClickLinkRuleById(ruleId);

        if(rules.isEmpty() || rules.size() != 1)
        {
            throw new ParallelException('ClickLink rule Id ' + ruleId + ' does not exist.');
        }

        Map<String,String> minAndMaxRange = getMinAndMaxRange(rules.get(0));

        Decimal minRange = minAndMaxRange.get(MIN_VAL) == '0' ? 0 : Decimal.valueOf(minAndMaxRange.get(MIN_VAL).split('-').get(1));
        Decimal maxRange = minAndMaxRange.get(MAX_VAL) == '0' ? 0 : Decimal.valueOf(minAndMaxRange.get(MAX_VAL).split('-').get(1));

        Integer numOfRecordsPerParallelJob = calculateNumberOfRecordsPerParallelJob(maxRange - minRange);

        Boolean canRun = canRunAJob(true,'ParallelCreateDocumentsBatch');

        if(canRun)
        {
            Integer intMinRange = Integer.valueOf(minRange);

            ParallelCreateDocumentsBatch createDocuments = new ParallelCreateDocumentsBatch(rules.get(0), scopeSize, nameMask, intMinRange, (intMinRange + numOfRecordsPerParallelJob)-1, 1);
            Database.executeBatch(createDocuments, scopeSize);

            canRun = canRunAJob(false,'ParallelCreateDocumentsBatch');
            if(canRun)
            {
                createDocuments = new ParallelCreateDocumentsBatch(rules.get(0), scopeSize, nameMask, (intMinRange + numOfRecordsPerParallelJob), Integer.valueOf(maxRange), 2);
                Database.executeBatch(createDocuments, scopeSize);
            }
        }
    }

    /**
     * In order to run it from Developer console
     * String docType = 'Sales Invoices';
     * //String docType = 'Sales Credit Notes';
     * //String docType = 'Cash Entries';
     * Integer scopeSize = 10; //number of records created on each chunck
     * String nameMask = 'SIN{000000}'; //mask value comes from custom setting
     * //String nameMask = 'SCR{000000}'; //mask value comes from custom setting
     * //String nameMask = 'CSH{000000}'; //mask value comes from custom setting
     * ParallelCreateAndPostDocumentsService.postDocumentsInBatch(docType,scopeSize,nameMask);
     */
    public static void postDocumentsInBatch(String docType, Integer scopeSize, String nameMask)
    {
        String objName;
        String statusFieldName;

        //Check docType and get document
        if(docType == SINV)
        {
            objName = SINV_OBJECTNAME;
            statusFieldName = SINV_STATUS_FIELDNAME;
        }
        else if(docType == SCRN)
        {
            objName = SCRN_OBJECTNAME;
            statusFieldName = SCRN_STATUS_FIELDNAME;
        }
        else if(docType == CE)
        {
            objName = CE_OBJECTNAME;
            statusFieldName = CE_STATUS_FIELDNAME;
        }
        else
        {
            throw new ParallelException('Not able to run Post. Your document type is ' + docType + ' and it should be ' + SINV + ', ' + SCRN + ' or ' + CE + '.');
        }

        if(nameMask == null || nameMask == '')
        {
            throw new ParallelException('NameMask custom setting must be populated.');
        }

        Map<String,String> minAndMaxRange = getMinAndMaxPostRange(objName,statusFieldName);

        Decimal minRange,maxRange;

        if(minAndMaxRange.get(MIN_VAL) == '0')
        {
            minRange = 0;
        }
        else
        {
            Integer minSize = minAndMaxRange.get(MIN_VAL).length() - 3;
            minRange = Decimal.valueOf(minAndMaxRange.get(MIN_VAL).right(minSize));
        }

        if(minAndMaxRange.get(MAX_VAL) == '0')
        {
            maxRange = 0;
        }
        else
        {
            Integer maxSize = minAndMaxRange.get(MAX_VAL).length() - 3;
            maxRange = Decimal.valueOf(minAndMaxRange.get(MAX_VAL).right(maxSize));
        }

        Integer numOfRecordsPerParallelJob = calculateNumberOfRecordsPerParallelJob(maxRange - minRange);

        Boolean canRun = canRunAJob(true,'ParallelPostDocumentsBatch');

        if(canRun)
        {
            Integer intMinRange = Integer.valueOf(minRange);

            ParallelPostDocumentsBatch postDocuments = new ParallelPostDocumentsBatch(docType, scopeSize, nameMask, intMinRange, (intMinRange + numOfRecordsPerParallelJob)-1, 1);
            Database.executeBatch(postDocuments, scopeSize);

            canRun = canRunAJob(false,'ParallelPostDocumentsBatch');

            if(canRun)
            {
                postDocuments = new ParallelPostDocumentsBatch(docType, scopeSize, nameMask, (intMinRange + numOfRecordsPerParallelJob), Integer.valueOf(maxRange), 2);
                Database.executeBatch(postDocuments, scopeSize);
            }
        }
    }

    public static void sendEmailAfterPost(String docymentType, Integer pos, List<String> errorMessages, List<Id> documentsPosted)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.saveAsActivity = false;
        mail.setTargetObjectId(UserInfo.getUserId());

        String body = 'Results of Bulk Post ' + docymentType;
        body += pos == 1 ? ' on 1st parallel job:' : pos == 2 ? ' on 2nd parallel job: ' : ' on remaining records: ';
        body += '\n\n';

        if( errorMessages.size() > 0 )
        {
            body += 'Errors occurred.\n';
            for( String error : errorMessages )
            {
                body += error +'\n';
            }
            body += '\n';
            mail.setSubject( 'Bulk Post ' + docymentType + ' completed with errors.');
        }
        else
        {
            if(documentsPosted.size() > 0)
            {
                body += documentsPosted.size() + ' ' + docymentType + ' have been posted.\n\n';
            }
            else
            {
                body += 'Although there is no error, no document has been posted.';
            }

            mail.setSubject( 'Bulk Post ' + docymentType + ' completed.');
        }

        mail.setPlainTextBody( body );

        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }

    //This method must go to a Selector class in order to follow SOC
    //however as we need to increase performance for this customer
    //we have decided to leave it here
    public static Map<String,String> getMinAndMaxRange(SObject clickLinkRule)
    {
        Map<String,String> minAndMaxRange = new Map<String,String>();

        Boolean processed = false;
        String sourceObjectName = (String)clickLinkRule.get(SOURCEOBJECT_NAME);
        String sourceProcessedFieldName = (String)clickLinkRule.get(SOURCEOBJECT_PROCESSEDFIELD_NAME);

        String qry = 'Select Min(Name) minRange, ';
        qry += 'Max(Name) maxRange ';
        qry += 'From ' + sourceObjectName + ' ';
        qry += 'WHERE ' + sourceProcessedFieldName + ' = :processed ';

        for(AggregateResult agR : Database.query(qry))
        {
            String minVal = (String)agr.get('minRange') == null
                                ? '0'
                                : (String)agr.get('minRange');

            String maxVal = (String)agr.get('maxRange') == null
                                ? '0'
                                : (String)agr.get('maxRange');

            minAndMaxRange.put(MIN_VAL, minVal);
            minAndMaxRange.put(MAX_VAL, maxVal);
        }

        return minAndMaxRange;
    }

    public static Integer calculateNumberOfRecordsPerParallelJob(Decimal numOfRecords)
    {
        if(numOfRecords == 0)
        {
            throw new ParallelException('There is no records to process.');
        }

        Decimal numOfRecordsPerParallelJob = numOfRecords.divide(2,0,System.RoundingMode.UP);

        return (Integer)numOfRecordsPerParallelJob;
    }

    public static Map<Integer, String> calculateMinAndMaxWithMask(List<String> maskValues, Integer minRange, Integer maxRange)
    {
        Map<Integer, String> minAndMaxWithMask = new Map<Integer, String>();

        //Having PRFN-0000000000
        //maskValues.get(0) --> PRFN-
        //maskValues.get(1) --> 00000000000

        String minWithMaskVal = '';
        String maxWithMaskVal = '';
        Integer minRangeSize = String.valueOf(minRange).length();
        Integer maxRangeSize = String.valueOf(maxRange).length();

        for(Integer i=0; i<maskValues.get(1).length() - minRangeSize; i++)
        {
            minWithMaskVal += '0';
        }

        for(Integer i=0; i<maskValues.get(1).length() - maxRangeSize; i++)
        {
            maxWithMaskVal += '0';
        }

        minAndMaxWithMask.put(1, maskValues.get(0) + minWithMaskVal + minRange);
        minAndMaxWithMask.put(2, maskValues.get(0) + maxWithMaskVal + maxRange);

        return minAndMaxWithMask;
    }

    //This method must go to a Selector class in order to follow SOC
    //however as we need to increase performance for this customer
    //we have decided to leave it here
    public static Integer calculateRemainingSourceRecords(String sourceObjName, String processedFieldName, String startCurrentRangeWithPrefix)
    {
        Boolean processed = false;

        String qry = 'SELECT count() ';
        qry += 'FROM ' + sourceObjName + ' ';
        qry += 'WHERE ' + processedFieldName + ' = :processed ';
        qry += 'AND Name >= :startCurrentRangeWithPrefix ';
        qry += 'LIMIT 50000';

        return Database.countQuery(qry);
    }

    //This method must go to a Selector class in order to follow SOC
    //however as we need to increase performance for this customer
    //we have decided to leave it here
    public static Integer calculateRemainingDocumentsToPost(String objectName, String statusFieldName, String startCurrentRangeWithPrefix)
    {
        String invStatus = 'In Progress';

        String qry = 'SELECT count() ';
        qry += 'FROM ' + objectName + ' ';
        qry += 'WHERE ' + statusFieldName  + ' = :invStatus ';
        qry += 'AND Name >= :startCurrentRangeWithPrefix ';
        qry += 'LIMIT 50000';

        return Database.countQuery(qry);
    }

    //This method must go to a Selector class in order to follow SOC
    //however as we need to increase performance for this customer
    //we have decided to leave it here
    public static List<SObject> getClickLinkRuleById(Id ruleId)
    {
        String qry = 'Select Id, ';
        qry += 'ffirule__SourceObject__c, ';
        qry += 'ffirule__SourceObjectProcessedField__c ';
        qry += 'From ffirule__IntegrationRule__c ';
        qry += 'Where Id = :ruleId';

        return Database.query(qry);
    }

    //This method must go to a Selector class in order to follow SOC
    //however as we need to increase performance for this customer
    //we have decided to leave it here
    public static Map<String,String> getMinAndMaxPostRange(String objectName, String statusFieldName)
    {
        Map<String,String> minAndMaxRange = new Map<String,String>();

        String invStatus = 'In Progress';

        String qry = 'Select Min(Name) minRange, ';
        qry += 'Max(Name) maxRange ';
        qry += 'FROM ' + objectName + ' ';
        qry += 'WHERE ' + statusFieldName  + ' = :invStatus ';

        for(AggregateResult agR : Database.query(qry))
        {
            String minVal = (String)agr.get('minRange') == null
                                ? '0'
                                : (String)agr.get('minRange');

            String maxVal = (String)agr.get('maxRange') == null
                                ? '0'
                                : (String)agr.get('maxRange');

            minAndMaxRange.put(MIN_VAL, minVal);
            minAndMaxRange.put(MAX_VAL, maxVal);
        }

        return minAndMaxRange;
    }

    //This method must go to a Selector class in order to follow SOC
    //however as we need to increase performance for this customer
    //we have decided to leave it here
    public static Boolean canRunAJob(Boolean fromScrach, String className)
    {
        Boolean canRun = false;
        String jobType = 'BatchApex';
        List<String> statusList = new List<String>{'Holding',
                                                   'Queued',
                                                   'Processing',
                                                   'Preparing'};

        String qry = 'Select count() From AsyncApexJob ';
        qry += 'Where JobType = :jobType ';
        qry += 'And Status IN :statusList ';
        qry += 'And MethodName = :className';

        Integer jobsSameClass = Database.countQuery(qry);

        Integer valueToCompare = fromScrach ? 1 : 2;
        Integer valueToTalToCompare = fromScrach ? 99 : 100;

        if(jobsSameClass < valueToCompare)
        {
            qry = 'Select count() From AsyncApexJob ';
            qry += 'Where JobType = :jobType ';
            qry += 'And Status IN :statusList';

            Integer numOfJobsRunning = Database.countQuery(qry);

            if(numOfJobsRunning >= valueToTalToCompare)
            {
                throw new ParallelException('You cannot run more than 100 jobs at the same time.');
            }
            else
            {
                canRun = true;
            }
        }
        else
        {
            throw new ParallelException('You cannot run more than 2 jobs related to the same process at the same time.');
        }

        return canRun;
    }

    public class ParallelException extends Exception {}
}