public class DirectoryPagination_AIUDTrigger {
    
    public static void onAfterInsert(list<Directory_Pagination__c> lstDP) {
        Boolean skipTrigger=CommonMethods.skipTriggerMethodLogic(CommonMessages.DPObjectName);
        if(!skipTrigger){
            splitDPAfterInsert(lstDP);
        }
        
    }
    
    public static void onBeforeInsert(list<Directory_Pagination__c> lstDP) {
        splitDPBeforeInsert(lstDP);
        
    }
    
    public static void onBeforeUpdate(list<Directory_Pagination__c> lstDP, Map<Id, Directory_Pagination__c> oldMap) {
      splitDPBeforeUpdate(lstDP, oldMap);
      
    }
    
    public static void splitDPAfterInsert(list<Directory_Pagination__c> lstDP) {
        set<Id> setDSID = new set<Id>();
        map<Id, Integer> mapDSDisplayAdCount = new map<Id, Integer>();
        map<Id, Integer> mapDSExposureCount = new map<Id, Integer>();
        boolean bFlag = false;
        for(Directory_Pagination__c iterator : lstDP) {
            if(iterator.Directory_Section__c != null){
                setDSID.add(iterator.Directory_Section__c);
                if(!mapDSDisplayAdCount.containsKey(iterator.Directory_Section__c)) {
                    mapDSDisplayAdCount.put(iterator.Directory_Section__c, 0);
                }
                
                if(!mapDSExposureCount.containsKey(iterator.Directory_Section__c)) {
                    mapDSExposureCount.put(iterator.Directory_Section__c, 0);
                }
                
                if(iterator.Display_Ad__c) {
                    bFlag = true;
                    if(iterator.Sequence_in_Section__c != null || iterator.Sequence_in_Section__c != 0) {
                        Integer iCount = mapDSDisplayAdCount.get(iterator.Directory_Section__c) + 1;
                        mapDSDisplayAdCount.put(iterator.Directory_Section__c, iCount);
                    }
                    Integer iCount = mapDSExposureCount.get(iterator.Directory_Section__c) + 1;
                    mapDSExposureCount.put(iterator.Directory_Section__c, iCount);
                }
                else if(iterator.Listing__c || iterator.Banner__c || iterator.Billboard__c || iterator.spacead__c){
                    bFlag = true;
                    Integer iCount = mapDSExposureCount.get(iterator.Directory_Section__c) + 1;
                    mapDSExposureCount.put(iterator.Directory_Section__c, iCount);
                }
            }
        }
        
        if(setDSID.size() > 0 && bFlag) {
            generateCounternew(setDSID, mapDSDisplayAdCount, mapDSExposureCount);
        }
    }
    
    private static void splitDPBeforeInsert(list<Directory_Pagination__c> lstDP) {
        Set<Id> dirEdIds = new Set<Id>();
        
        for(Directory_Pagination__c iterator : lstDP) {
            if(iterator.Directory_Edition__c != null) {
                dirEdIds.add(iterator.Directory_Edition__c);
            }
        }
        
        if(dirEdIds.size() > 0) {
            populatePubCode(lstDP, dirEdIds);
        }
    }
    
    private static void splitDPBeforeUpdate(list<Directory_Pagination__c> lstDP, Map<Id, Directory_Pagination__c> oldMap) {
        Set<Id> dirEdIds = new Set<Id>();
        
        for(Directory_Pagination__c iterator : lstDP) {
            if(iterator.Directory_Edition__c != null) {
                dirEdIds.add(iterator.Directory_Edition__c);
            }
        }
        
        if(dirEdIds.size() > 0) {
            populatePubCode(lstDP, dirEdIds);
        }
    }
    
    private static void generateCounternew(set<Id> setDSID, map<Id, Integer> mapDSDisplayAdCount, map<Id, Integer> mapDSExposureCount) {
        if(!setDSID.isEmpty()) {
            list<Directory_Section__c> dirSectionList = DirectorySectionSOQLMethods.getDirectorySectionById(setDSID);
            if(dirSectionList.size() > 0){
                for(Directory_Section__c DS : dirSectionList) {
                    if(mapDSDisplayAdCount.get(Ds.Id) != null) {
                        Integer iCount = mapDSDisplayAdCount.get(Ds.Id);
                        DS.Display_Ad_Count__c = (DS.Display_Ad_Count__c != null ? DS.Display_Ad_Count__c + iCount : iCount);
                    }
                    
                    if(mapDSExposureCount.get(Ds.Id) != null) {
                        Integer iCount = mapDSExposureCount.get(Ds.Id);
                        DS.Exposure_Count__c = (DS.Exposure_Count__c != null ? DS.Exposure_Count__c + iCount : iCount);
                    }
                }
                update dirSectionList;
            }
        }
    }
    
    /*public static void populateProductValues(List<Directory_Pagination__c> dirPaginationList) {
        List<Id> lstOLIn = new List<Id>();
        map<id,Directory_Edition__c> mapIdDE;
        map<id,Order_Line_Item__c> mapIdDE;
        for(Directory_Pagination__c iterator : dirPaginationList) {
            if(iterator.Directory_Edition__c != null) {
                lstDirEdition.add(iterator.Directory_Edition__c);
            }
            if(iterator.Order_Line_Item__c != null) {
                lstOLI.add(iterator.Order_Line_Item__c);
            }           
        }
        
        if(lstDirEdition != null && lstDirEdition.size() > 0) {
            mapIdDE = new Map<id, Directory_Edition__c>([select Year__c from Directory_Edition__c where id in :lstDirEdition]);
        }
        if(lstOLI != null && lstOLI.size() > 0) {
            mapIdDE = new Map<id, Order_Line_Item__c>([select Product2__r.Print_Product_Type__c from Order_Line_Items__c where id in :lstOLI]);
        }
        for(Directory_Pagination__c iterator : dirPaginationList) {
            if(iterator.Directory_Edition__c != null) {
                iterator.DP_Edition_Year__c = mapIdDE.get(iterator.Directory_Edition__c).Year__c;
            }
            if(iterator.Order_Line_Item__c != null) {
                iterator.DP_Print_Specialty_Product_Type__c = mapIdDE.get(iterator.Order_Line_Item__c).Product2__r.Print_Specialty_Product_Type__c;
            }
        }
    }*/
    
    /*public static void populateValues(List<Directory_Pagination__c> dirPaginationList) {
        List<Id> lstDirEdition = new List<Id>();
        for(Directory_Pagination__c iterator : dirPaginationList) {
            if(iterator.Directory_Edition__c != null) {
                lstDirEdition.add(iterator.Directory_Edition__c);
            }       
        }
        map<id,Directory_Edition__c> mapIdDE = new Map<id, Directory_Edition__c>([select Year__c from Directory_Edition__c where id in :lstDirEdition]);
        for(Directory_Pagination__c iterator : dirPaginationList) {
            if(iterator.Directory_Edition__c != null) {
                iterator.DP_Edition_Year__c = mapIdDE.get(iterator.Directory_Edition__c).Year__c;
            }
        }
    }
    
    public static void populateValues(List<Directory_Pagination__c> dirPaginationList, Map<Id, Directory_Pagination__c> oldMap) {
        Directory_Pagination__c objOldDP;
        System.debug('Testingg 1 ');
        List<Directory_Pagination__c> lstDPToUpdate = new List<Directory_Pagination__c>(); 
        for(Directory_Pagination__c iterator : dirPaginationList) {
            objOldDP = oldMap.get(iterator.Id);
            System.debug('Testingg 2 ');
            if(iterator.Directory_Edition__c != objOldDP.Directory_Edition__c) {
                System.debug('Testingg 3');
                if(iterator.Directory_Edition__c == null) {
                    iterator.DP_Edition_Year__c = null;
                    System.debug('Testingg 4');
                }
                else {
                    lstDPToUpdate.add(iterator);
                }
            }
        }
        if(lstDPToUpdate.size() > 0) {
            populateValues(lstDPToUpdate);
        }       
    }*/ 
    
    
    /*public static void generateCounter(List<Directory_Pagination__c> dirPaginationList) {
        Set<Id> DSIds = new Set<Id>();      
        List<Directory_Section__c> dirSectionList = new List<Directory_Section__c>();
        List<Directory_Section__c> dirSectionUpdateList = new List<Directory_Section__c>();
        Map<Id, List<Directory_Pagination__c>> DirSecDirPagMap = new Map<Id, List<Directory_Pagination__c>>();
        
        for(Directory_Pagination__c DP : dirPaginationList) {
            if(DP.Directory_Section__c != null){
                DSIds.add(DP.Directory_Section__c);
            }
        }
        
        if(!DSIds.isEmpty()) {
            system.debug('Directory Seciton ids are ' + DSIds);
            dirSectionList = DirectorySectionSOQLMethods.getDirectorySectionById(DSIds);
            
            for(Directory_Pagination__c DP : dirPaginationList) {
                if(!DirSecDirPagMap.containsKey(DP.Directory_Section__c)) {
                    DirSecDirPagMap.put(DP.Directory_Section__c, new List<Directory_Pagination__c>());
                }
                DirSecDirPagMap.get(DP.Directory_Section__c).add(DP);
            }
            
            if(dirSectionList.size() > 0){
                for(Directory_Section__c DS : dirSectionList) {
                    Integer countOfDisplayAds = 0;
                    Integer countOfExposureAds = 0;
                    List<Directory_Pagination__c> dirPagList = new List<Directory_Pagination__c>();
                    dirPagList = DirSecDirPagMap.get(DS.Id);
                    
                    for(Directory_Pagination__c DP : dirPagList) {
                        if(DP.Display_Ad__c) {
                            if(DP.Sequence_in_Section__c != null || DP.Sequence_in_Section__c != 0) {
                                countOfDisplayAds = countOfDisplayAds + 1;
                            }
                            countOfExposureAds = countOfExposureAds + 1;
                        } else if(DP.Listing__c || DP.Banner__c || DP.Billboard__c || DP.spacead__c){
                            countOfExposureAds = countOfExposureAds + 1;
                        }
                    }
                    
                    if(countOfDisplayAds != null) {
                        if(DS.Display_Ad_Count__c == null){
                            DS.Display_Ad_Count__c = countOfDisplayAds;     
                        } else {
                            DS.Display_Ad_Count__c += countOfDisplayAds;
                        }            
                    }
                    
                    if(countOfExposureAds != null) {                        
                        if(DS.Exposure_Count__c == null){
                            DS.Exposure_Count__c = countOfExposureAds;    
                        } else {
                            DS.Exposure_Count__c += countOfExposureAds;
                        }
                    }
                    
                    if(countOfDisplayAds != null || countOfExposureAds != null) {
                        dirSectionUpdateList.add(DS);
                    }
                }
                if(!dirSectionUpdateList.isEmpty()) {
                    System.debug('Testingg '+dirSectionUpdateList);
                    update dirSectionUpdateList;
                }
            }
        }
    }  */    
    
    public static void populatePubCode(List<Directory_Pagination__c> dirPaginationList, Set<Id> dirEdIds) {
        Map<Id, Directory_Edition__c> mapDirEdition = DirectoryEditionSOQLMethods.getDEMapByIds(dirEdIds); 
        for(Directory_Pagination__c iterator : dirPaginationList) {
            if(mapDirEdition.containsKey(iterator.Directory_Edition__c)) {
                if(mapDirEdition.get(iterator.Directory_Edition__c).Directory__r.Publication_Company_Code__c != null) {
                    iterator.Publication_Company_Code__c = mapDirEdition.get(iterator.Directory_Edition__c).Directory__r.Publication_Company_Code__c;
                }
            }
        }
    }
    //Method used for ApplyCityservice logic in 2 controllers(DirPaginationSortAndCityServiceBatch,ApplyCityServiceBatchClass)
    public static void ApplyCityServiceDPUpate(List<Directory_Pagination__c> listDP,set<string> setstrCity,map<string,Community_Section_Abbreviation__c> mapstrCSA,Boolean SortAsBol) {
        List<Directory_Pagination__c> lstUpdtDP = new List<Directory_Pagination__c>(); 
        Boolean bolUpdate = false;
        Boolean bolCityServiceUpdt = false;
        string strDslcCombo;
        if(listDP.size() > 0) {
            for(Directory_Pagination__c objDP : listDP) {
            strDslcCombo = null;
            if(SortAsBol) {
                if(objDP.Sort_As__c != null && objDP.DP_Sort_As__c != objDP.Sort_As__c) {
                    objDP.DP_Sort_As__c = objDP.Sort_As__c;
                    bolUpdate = true;
                }
            }
            if(setstrCity.size() > 0) {
                bolCityServiceUpdt = true;
            }
            if(bolCityServiceUpdt && setstrCity.contains(objDP.Listing_City_Unformatted__c)) {
                if(objDP.Directory_Section__c != null && objDP.Listing_City_Unformatted__c != null) {
                    strDslcCombo = objDP.Directory_Section__c+objDP.Listing_City_Unformatted__c;
                }
                if(mapstrCSA.get(strDslcCombo) != null) {
                    if(mapstrCSA.get(strDslcCombo).Suppress_Abbreviation_Rule__c == 'Suppress') {
                        if(String.isNotBlank(objDP.City_Name_Section_Abbreviation__c)){ 
                            objDP.City_Name_Section_Abbreviation__c = '';
                            bolUpdate = true;
                        }
                        if(objDP.City_Name_Section_Suppressed__c == false) {
                            objDP.City_Name_Section_Suppressed__c = true;
                            bolUpdate = true;    
                        }
                        if(objDP.City_Name_Unedited__c == null && mapstrCSA.get(strDslcCombo).Community_Name__c != null) {
                            objDP.City_Name_Unedited__c =  mapstrCSA.get(strDslcCombo).Community_Name__c;
                            bolUpdate = true;   
                        }                     
                }
                else if(mapstrCSA.get(strDslcCombo).Suppress_Abbreviation_Rule__c == 'Use Abbreviation') {
                        objDP.City_Name_Section_Abbreviation__c =  mapstrCSA.get(strDslcCombo).Community_Abbreviation__c;
                        bolUpdate = true;
                    if(objDP.City_Name_Section_Suppressed__c == true){
                        objDP.City_Name_Section_Suppressed__c = false;
                    }
                    if(objDP.City_Name_Unedited__c == null){
                        objDP.City_Name_Unedited__c =  mapstrCSA.get(strDslcCombo).Community_Name__c;  
                    } 
                }
                else if(mapstrCSA.get(strDslcCombo).Suppress_Abbreviation_Rule__c == 'Print in Full' && (String.isNotBlank(objDP.City_Name_Section_Abbreviation__c) || objDP.City_Name_Section_Suppressed__c == true)){
                        objDP.City_Name_Section_Abbreviation__c = '';
                        objDP.City_Name_Section_Suppressed__c = false;
                        bolUpdate = true;
                }
                } 
            }
            if(bolUpdate) {
                lstUpdtDP.add(objDP);
            }
            }               
            if(lstUpdtDP != null && lstUpdtDP.size() > 0) {
                update  lstUpdtDP;
            }
        }
    }
}