global class strmCntrlr1{
    @RemoteAction
    global static String getAllAccounts() //Need to define as a remote action as we will be calling this through javascript
    {
        List<Account> accounts = [SELECT Id, Name, Type, Phone FROM Account where Name like 'A%'];
        String JSONString = JSON.serialize(accounts);//This is how we can serialize our response into JSON format
        return '{\"Records\":' +JSONString+', \"error\": \"null\", \"status\":\"SUCCESS\", \"count\":\"' + accounts.size() + '\" }';
    }
    
    @RemoteAction
    global static String getStrmData() //Need to define as a remote action as we will be calling this through javascript
    {
        List<Account> accounts = [SELECT Id, Name, Type, Phone FROM Account where Name like 'A%'];
        String JSONString = JSON.serialize(accounts);//This is how we can serialize our response into JSON format
        return '{\"Records\":' +JSONString+', \"error\": \"null\", \"status\":\"SUCCESS\", \"count\":\"' + accounts.size() + '\" }';
    }
    
    @RemoteAction
    global static String getBkngData(String srchStrng) //Need to define as a remote action as we will be calling this through javascript
    {
        List<Booking__c> bookings = [SELECT Book_or_Cancel_Date__c, Customer_ID__c, Customer_Name__c, Directory_Code__c, Directory_Version__c, Directory_Name__c, Worker_ID__c, Heading_Code__c, ADP_Employee_ID__c, Employee_Name__c, Division_Name__c, Sales_Channel_Code__c, Media_Type__c, RGU__c, UDAC__c, New_or_Renew__c, Enterprise_Customer_ID__c, Previous_Close_Amount__c, Book_Increase_Amount__c, Increase_NI_Amount__c, NI_Amount__c, Term__c, Book_Amount__c, Through_Date__c  FROM Booking__c where Customer_Name__c like :srchStrng];
        String JSONString = JSON.serialize(bookings);//This is how we can serialize our response into JSON format
        return '{\"Records\":' +JSONString+', \"error\": \"null\", \"status\":\"SUCCESS\", \"count\":\"' + bookings.size() + '\" }';
    }
    
    @RemoteAction
    global static String srchBkngData(String divNmStrng, String mediaTypStrng, String dtFromStrng, String dtToStrng) //Need to define as a remote action as we will be calling this through javascript
    {
        String query1 = 'SELECT Book_or_Cancel_Date__c, Customer_ID__c, Customer_Name__c, Directory_Code__c, Directory_Version__c, Directory_Name__c, Worker_ID__c, Heading_Code__c, ADP_Employee_ID__c, Employee_Name__c, Division_Name__c, Sales_Channel_Code__c, Media_Type__c, RGU__c, UDAC__c, New_or_Renew__c, Enterprise_Customer_ID__c, Previous_Close_Amount__c, Book_Increase_Amount__c, Increase_NI_Amount__c, NI_Amount__c, Term__c, Book_Amount__c  FROM Booking__c where ';
                                    
        if(mediaTypStrng != null)
        {
            query1 += 'Media_Type__c = :mediaTypStrng and ';
        }
        
        if(divNmStrng != null)
        {
            query1 += 'Division_Name__c = :divNmStrng and ';
        }
        
        
    if(dtFromStrng != null)
        {
            query1 += 'Division_Name__c = :divNmStrng and ';         
          //query1 += 'Division_Name__c = :divNmStrng and ';
          //Date inputDate = Date.newInstance(2014,10,04);
          date inputDate = date.parse(dtFromStrng);
          //String dFrom = '2014-10-15';
          //DateTime refDateFrom = DateTime.newInstance(dtFrom.year(), (dtFrom.month() + 1), dtFrom.day(), 0, 0, 0);
          query1 += 'Book_or_Cancel_Date__c > :inputDate and '; 
          date toDate = date.parse(dtToStrng);
          query1 += 'Book_or_Cancel_Date__c < :toDate';
        }
        
        if(query1.endsWith(' and '))
        {
            query1 = query1.removeEnd(' and ');
        }
        
        System.debug('test'); 
        List<Booking__c> bookings = Database.query(query1);
               
        String JSONString = JSON.serialize(bookings);//This is how we can serialize our response into JSON format
        return '{\"Records\":' +JSONString+', \"error\": \"null\", \"status\":\"SUCCESS\", \"count\":\"' + bookings.size() + '\" }';
    }
    
    @RemoteAction
    global static String getDivCdData() //Need to define as a remote action as we will be calling this through javascript
    {
        List<String> divcds = new List<String>();
        //List <Booking__c> divcds = new List();
        for(AggregateResult ar : [select Division_Code__c from Booking__c group by Division_Code__c])
        {
            divcds.add(string.valueof(ar.get('Division_Code__c')));
        }
                
        //List<Booking__c> divcds = [SELECT  distinct Division_Code__c FROM Booking__c order by Division_Code__c];
        String JSONString = JSON.serialize(divcds);//This is how we can serialize our response into JSON format
        //console.log('JSONString: ', JSONString);
        return '{\"Records\":' +JSONString+', \"error\": \"null\", \"status\":\"SUCCESS\", \"count\":\"' + divcds.size() + '\" }';
    }
    
    @RemoteAction
    global static String getDivNmData() //Need to define as a remote action as we will be calling this through javascript
    {
        List<String> divnms = new List<String>();
        //List <Booking__c> divcds = new List();
        for(AggregateResult ar : [select Division_Name__c from Booking__c group by Division_Name__c])
        {
            //console.log('ar: ', ar);  
            //Object divcdObj;
            //divcdObj.Division_Name__c = string.valueof(ar.get('Division_Name__c'));
            //divnms[0].Division_Name__c = string.valueof(ar.get('Division_Name__c'));
            divnms.add(string.valueof(ar.get('Division_Name__c')));
            
        }
                
        //List<Booking__c> divcds = [SELECT  distinct Division_Code__c FROM Booking__c order by Division_Code__c];
        String JSONString = JSON.serialize(divnms);//This is how we can serialize our response into JSON format
        //console.log('JSONString: ', JSONString);
        return '{\"Records\":' +JSONString+', \"error\": \"null\", \"status\":\"SUCCESS\", \"count\":\"' + divnms.size() + '\" }';
    }
}