@isTest
public class createBulkOrderControllerTest{
    static testMethod void createBulkOrderControllerTest01() {
        System.debug('Testinggg createBulkOrderControllerTest01');
        Test.startTest();
        System.debug('Testinggg createBulkOrderControllerTest01 2');
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        newAccount.Billing_Anniversary_Date__c=system.Today();
        update newAccount;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        System.debug('Testinggg createBulkOrderControllerTest01 2');
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        insert objDirEd;
        
        //Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        //insert objDirEd1;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
        
        system.assertNotEquals(objDir.ID, null);
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = 'SEO';
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        if(x==0){
            newProduct.Vendor__c='yp';
            newProduct.Is_Anchor__c=true;
            newProduct.Print_Specialty_Product_Type__c=CommonMessages.ELProd;
            //newProduct.Print_Product_Type__c='Display';
        }else if(x==1){
            newProduct.Product_Type__c = 'MSA Website';
        }
        lstProduct.add(newProduct);
        }
        insert lstProduct;
        system.debug('ZZZZZZZZZZZ'+lstProduct);
        List<pricebookentry> lstinsertPBE = new List<pricebookentry>();
        //pricebook2 objPB = TestMethodsUtility.createpricebook();
        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        for(Product2 iterator : lstProduct) {
        
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
        lstinsertPBE.add(pbe);
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
        objDPM.Product2__c = iterator.Id;
        }
        insert lstinsertPBE;
        insert lstDPM;
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        
        //newOpportunity.modifyid__c = '12345';
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Billing_Frequency__c = 'Monthly';
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
        newOpportunity.CORE_Migration_ID__c  = '12345';
        //newOpportunity.isclosed=true;
        //newOpportunity.iswon=true;
        newOpportunity.StageName='Closed Won';
        insert newOpportunity;
        
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstinsertPBE) {
        OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
        objOLI.PricebookEntryId = iterator.Id;
        objOLI.Billing_Duration__c = 12;
        objOLI.Directory__c = objDir.Id;
        objOLI.Directory_Edition__c = objDirEd.Id;
        objOLI.Full_Rate__c = 30.00;
        objOLI.Renewals_action__c = 'New';
        objOLI.UnitPrice = 30.00;
        objOLI.Package_ID__c = '123456';
        objOLI.Billing_Partner__c = objOLI.Id;
        objOLI.OpportunityId = newOpportunity.Id;
        objOLI.Directory_Heading__c = objDH.Id;
        objOLI.Directory_Section__c = objDS.Id;
        lstOLI.add(objOLI);
        mapPBE.put(iterator.Id, iterator);
        }
        OpportunityLineItem objOLI1=lstOLI[0];
        objOLI1.Is_P4p__c=true;
         objOLI1.Billing_Duration__c=12;
         objOLI1.Effective_Date__c=system.Today();
        lstOLI.set(0,objOLI1);
        insert lstOLI;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        newOrLI.Parent_ID__c='pkg1';
        newOrLI.Parent_ID_of_Addon__c='pkg1';
        newOrLI.Product2__c =lstProduct[0].id;
        newOrLI.Media_Type__c = 'Digital';
        newOrLI.Addon_Type__c='Points';
       
        Order_Line_Items__c newOrLI1 = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        newOrLI1.Media_Type__c = 'Print';
        newOrLI1.Product2__c =lstProduct[1].id;
        newOrLI1.Addon_Type__c=CommonMessages.prdAddonTypeCoupon;
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        lstOrderLI.add(newOrLI);
         lstOrderLI.add(newOrLI1);
        if(lstOrderLI.size()>0)
        insert lstOrderLI;
        system.debug('WWWWWWWWWW'+lstOrderLI[0].Product2__r.Inventory_Tracking_Group__c+'---'+lstOrderLI[0].Product2__r.Is_Anchor__c+'----'+lstOrderLI[0].Product2__r.Product_Type__c);
        createBulkOrderController CTRL= new createBulkOrderController();
        createBulkOrderController.ValidateOpportunity(new List<opportunity>{newOpportunity});
        
        //createBulkOrderController.ValidateOpportunity(new List<opportunity>{newOpportunityValidate});
        
        createBulkOrderController.CheckAndCreateNewOrderIfNotExists(new List<opportunity>{newOpportunity});
        createBulkOrderController.newOrder(newOpportunity);
        createBulkOrderController.CreateNewOrderGroupNew(new List<opportunity>{newOpportunity},new List<Order__c>{newOrder});
        createBulkOrderController.newOrderSet(new Opportunity(), new Order__c(), newContact, new List<OpportunityLineItem>{new OpportunityLineItem()});
        createBulkOrderController.CreateOrderLineItem(new List<Opportunity>{newOpportunity}, new List<Order__c>{newOrder}, new List<Order_Group__c>{newOrderSet});
        //createBulkOrderController.getLeadRules(new Set<String>());
      //  createBulkOrderController.newOrderLineItem(new Order_Group__c(), new Order__c(),new OpportunityLineItem(),new Opportunity(),new  map<String, Lead_Assignment_Rules__c>(), '', '' );
        createBulkOrderController.LineItemHistoryProcess(new list<Order_Line_Items__c>());
        createBulkOrderController.newOrderLineItemHistory(new Order_Line_Items__c()); 
        createBulkOrderController.getDFFRecordTypeIDbyName(new Map<String,Schema.RecordTypeInfo>(), ''); 
        //createBulkOrderController.getDFFDescribes();
        //createBulkOrderController.newDigitalProductRequirement(new Order_Line_Items__c(), new Opportunity(), new Fulfillment_Profile__c(),0,'', '', createBulkOrderController.getDFFDescribes());
        map<id,Opportunity> mpOpp = new map<id,Opportunity>();
        mpOpp.put(newOpportunity.id,newOpportunity);
        List<Order_Line_Items__c> oliList=[Select Id, OL_Print_Specialty_Product_Type__c , Account__c, Order_Anniversary_Start_Date__c,Directory_Heading__r.Directory_Heading_Name__c, Account__r.phone,Account__r.BillingState,Account__r.BillingPostalCode,Account__r.BillingStreet,Account__r.BillingCity,
                Opportunity__r.StageName, Addon_Type__c, Billing_Contact__c, Billing_Contact__r.Name, Billing_Contact__r.Phone, Directory_Section__r.Section_Page_Type__c, P4P_Tracking_Number_ID__c,
                Billing_End_Date__c, Billing_Frequency__c,Opportunity__r.Billing_Contact__r.Email,Opportunity__r.Billing_Contact__r.HasOptedOutOfEmail, Billing_Partner__c, Anchor_Ad__c, Display_Ad__c,
                Billing_Start_Date__c, Canvass__c, Category__c, Continious_Billing__c, Opportunity__r.Billing_Contact__c,RecordTypeId, Core_Opportunity_Line_ID__c, 
                Description__c, Directory__c, Directory_Edition__c, Discount__c, Distribution_Area__c, Product2__r.Name, Product2__r.Inventory_Tracking_Group__c, Migration_Directory_Heading1__c, 
                Migration_Directory_Heading2__c, Migration_Directory_Heading3__c, Migration_Directory_Heading4__c, Product_Is_IBUN_Bundle_Product__c, Product2__r.Print_Product_Type__c,
                Effective_Date__c, FulfillmentDate__c, Geo_Code__c, Geo_Type__c, Is_Caption__c, Directory_Section__r.Section_Code__c,Media_Type__c,Cutomer_Cancel_Date__c, Product2__r.Is_Anchor__c,  
                Is_Child__c, Is_P4P__c, Line_Status__c, Linvio_Payment_Method_Id__c, Directory_Heading__c, Directory_Heading__r.Name, Directory_Heading__r.code__c,  
                Locality__c, UnitPrice__c, Name, Seniority_Date__c, Directory_Section__r.Columns__c,ListPrice__c,Last_Billing_Date__c,Account__r.OwnerId,Account__r.Owner.Email,Account__r.Owner.Manager.Email,
                Opportunity__c, Opportunity_line_item_id__c,Directory__r.Branch_Office__c, Order__c, Order_Group__c, Order_Line_Total__c, P4P_Billing__c,Is_Handled__c, 
                P4P_Price_Per_Click_Lead__c, Package_ID__c, Parent_ID__c, Parent_ID_of_Addon__c, Parent_Line_Item__c, Payment_Duration__c, Payment_Method__c,isCanceled__c,
                Payments_Remaining__c, Pricing_Program__c, Print_First_Bill_Date__c,Package_Name__c, Product_Type__c, Product2__c, Product2__r.Vendor__c, ProductCode__c, Quantity__c, 
                Quote_signing_method__c, Scope__c, Directory_Section__c, Statement_Suppression__c, Status__c, Product2__r.Family, Product2__r.RGU__c, Product2__r.Is_IBUN_Bundle_Product__c,
                Directory_Section__r.Name,Directory__r.Pub_Date__c, Section_Page_Type__c, strOLICombo__c, Product_Inventory_Tracking_Group__c, Opportunity_Close_Date__c,Listing__c,
                Account__r.Name,Account__r.Owner.Name,Directory_Edition__r.Name,Directory__r.Name,UDAC__c,Successful_Payments__c, Talus_Go_Live_Date__c, Telco_Invoice_Date__c, Directory_Code__c, 
                Edition_Code__c, Canvass__r.Canvass_Code__c,Product2__r.Product_Type__c, Listing__r.Phone__c, Listing__r.Area_Code__c, Opportunity__r.Name, Billing_Partner_Account__c, 
                Listing__r.Name, Listing__r.Listing_City__c, Listing__r.Listing_Country__c, Listing__r.Listing_PO_Box__c, Listing__r.Listing_Postal_Code__c, Listing__r.Listing_State__c, 
                Listing__r.Listing_Street__c, Listing__r.Listing_Street_Number__c,Directory_Edition__r.Book_Status__c,Directory_Edition__r.Pub_Date__c,Directory_Edition__r.Directory__c,
                Product2__r.Print_Specialty_Product_Type__c,Opportunity__r.Owner.Email, Opportunity__r.Owner.FirstName, Opportunity__r.Owner.LastName,
                Product2__r.ProductCode, Action_Code__c 
                from Order_Line_Items__c where Id IN:lstOrderLI];
        createBulkOrderController.FindAddOnProductForDFFAndCreateDFF(oliList,mpOpp,new map<Id, Fulfillment_Profile__c>()); 
        createBulkOrderController.setOpportunityToClosedWon(new list<Opportunity>{newOpportunity});
            
            

        Test.stopTest();
        List<Exception_Error__c> lstExp = [select id, name, Exception_Message__c, StackTraceString__c, Related_to__c from Exception_Error__c];
        System.debug('Testinggg Exceptions lstExp '+lstExp);
    }
    
       static testMethod void createBulkOrderControllerTest02() {
        Test.startTest();
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        newAccount.Billing_Anniversary_Date__c=system.Today();
        newAccount.Statement_Suppression__c=true;
        update newAccount;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.New_Print_Bill_Date__c=system.today();
        objDirEd.Bill_Prep__c=system.today();
        insert objDirEd;
        
        //Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        //insert objDirEd1;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
        
        system.assertNotEquals(objDir.ID, null);
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = 'Online Display';
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        newProduct.Vendor__c='yp';
        if(x==0){
            
            newProduct.Family='print';
        }
        if(x==1){
             newProduct.Product_Type__c = 'Spotzer Website';
        }
        if(x==2){
            newProduct.Product_Type__c = 'Register Domain';
        }
        lstProduct.add(newProduct);
        }
        insert lstProduct;
        List<pricebookentry> lstinsertPBE = new List<pricebookentry>();
        //pricebook2 objPB = TestMethodsUtility.createpricebook();
        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        for(Product2 iterator : lstProduct) {
        
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
        lstinsertPBE.add(pbe);
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
        objDPM.Product2__c = iterator.Id;
        }
        insert lstinsertPBE;
        insert lstDPM;
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        
        //newOpportunity.modifyid__c = '12345';
        newOpportunity.Billing_Partner__c = 'Berry';
        newOpportunity.Billing_Frequency__c = 'Single Payment';
        newOpportunity.Payment_Method__c = 'Credit Card';        
        newOpportunity.CORE_Migration_ID__c  = '12345';
        newOpportunity.modifyid__c='45765678';
        //newOpportunity.isclosed=true;
        //newOpportunity.iswon=true;
        newOpportunity.StageName='Closed Won';
        insert newOpportunity;
        
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstinsertPBE) {
        OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
        objOLI.PricebookEntryId = iterator.Id;
        objOLI.Billing_Duration__c = 12;
        objOLI.Directory__c = objDir.Id;
        objOLI.Directory_Edition__c = objDirEd.Id;
        objOLI.Full_Rate__c = 30.00;
        objOLI.Renewals_action__c = 'New';
        objOLI.UnitPrice = 30.00;
        objOLI.Package_ID__c = '123456';
        objOLI.Billing_Partner__c = objOLI.Id;
        objOLI.OpportunityId = newOpportunity.Id;
        objOLI.Directory_Heading__c = objDH.Id;
        objOLI.Directory_Section__c = objDS.Id;
        lstOLI.add(objOLI);
        mapPBE.put(iterator.Id, iterator);
        }
        OpportunityLineItem objOLI1=lstOLI[0];
        objOLI1.Is_P4p__c=true;
        // objOLI1.Billing_Duration__c=null;
         objOLI1.Effective_Date__c=system.Today()+2;
        lstOLI.set(0,objOLI1);
        insert lstOLI;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        newOrLI.Parent_ID__c='pkg1';
        newOrLI.Parent_ID_of_Addon__c='pkg1';
        newOrLI.Product2__c =lstProduct[0].id;
        newOrLI.Media_Type__c = 'Digital';
        newOrLI.Addon_Type__c=CommonMessages.prdAddonTypeCoupon;
        Order_Line_Items__c newOrLI1 = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        //newOrLI1.Media_Type__c = 'Print';
        newOrLI1.Product2__c =lstProduct[1].id;
        newOrLI1.Addon_Type__c=CommonMessages.prdAddonTypeAddOn;
        Order_Line_Items__c newOrLI2 = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        //newOrLI1.Media_Type__c = 'Print';
        newOrLI2.Product2__c =lstProduct[2].id;
        newOrLI2.Addon_Type__c=CommonMessages.prdAddonType5Heading;
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        lstOrderLI.add(newOrLI);
         lstOrderLI.add(newOrLI1);
          lstOrderLI.add(newOrLI2);
        if(lstOrderLI.size()>0)
        insert lstOrderLI;
        createBulkOrderController CTRL= new createBulkOrderController();
        createBulkOrderController.ValidateOpportunity(new List<opportunity>{newOpportunity});
        createBulkOrderController.CheckAndCreateNewOrderIfNotExists(new List<opportunity>{newOpportunity});
        createBulkOrderController.newOrder(newOpportunity);
        createBulkOrderController.CreateNewOrderGroupNew(new List<opportunity>{newOpportunity},new List<Order__c>{newOrder});
        createBulkOrderController.CreateOrderLineItem(new List<Opportunity>{newOpportunity}, new List<Order__c>{newOrder}, new List<Order_Group__c>{newOrderSet});
        map<id,Opportunity> mpOpp = new map<id,Opportunity>();
        mpOpp.put(newOpportunity.id,newOpportunity);
        List<Order_Line_Items__c> oliList=[Select Id, Account__c, Order_Anniversary_Start_Date__c,Directory_Heading__r.Directory_Heading_Name__c, Account__r.phone,Account__r.BillingState,Account__r.BillingPostalCode,Account__r.BillingStreet,Account__r.BillingCity,
                Opportunity__r.StageName, Addon_Type__c, Billing_Contact__c, Billing_Contact__r.Name, Billing_Contact__r.Phone, Directory_Section__r.Section_Page_Type__c, P4P_Tracking_Number_ID__c,
                Billing_End_Date__c, Billing_Frequency__c,Opportunity__r.Billing_Contact__r.Email,Opportunity__r.Billing_Contact__r.HasOptedOutOfEmail, Billing_Partner__c, Anchor_Ad__c, Display_Ad__c,
                Billing_Start_Date__c, Canvass__c, Category__c, Continious_Billing__c, Opportunity__r.Billing_Contact__c,RecordTypeId, Core_Opportunity_Line_ID__c, 
                Description__c, Directory__c, Directory_Edition__c, Discount__c, Distribution_Area__c, Product2__r.Name, Product2__r.Inventory_Tracking_Group__c, Migration_Directory_Heading1__c, 
                Migration_Directory_Heading2__c, Migration_Directory_Heading3__c, Migration_Directory_Heading4__c, Product_Is_IBUN_Bundle_Product__c, Product2__r.Print_Product_Type__c,
                Effective_Date__c, FulfillmentDate__c, Geo_Code__c, Geo_Type__c, Is_Caption__c, Directory_Section__r.Section_Code__c,Media_Type__c,Cutomer_Cancel_Date__c, Product2__r.Is_Anchor__c,  
                Is_Child__c, Is_P4P__c, Line_Status__c, Linvio_Payment_Method_Id__c, Directory_Heading__c, Directory_Heading__r.Name, Directory_Heading__r.code__c,  
                Locality__c, UnitPrice__c, Name, Seniority_Date__c, Directory_Section__r.Columns__c,ListPrice__c,Last_Billing_Date__c,Account__r.OwnerId,Account__r.Owner.Email,Account__r.Owner.Manager.Email,
                Opportunity__c, Opportunity_line_item_id__c,Directory__r.Branch_Office__c, Order__c, Order_Group__c, Order_Line_Total__c, P4P_Billing__c,Is_Handled__c, 
                P4P_Price_Per_Click_Lead__c, Package_ID__c, Parent_ID__c, Parent_ID_of_Addon__c, Parent_Line_Item__c, Payment_Duration__c, Payment_Method__c,isCanceled__c,
                Payments_Remaining__c, Pricing_Program__c, Print_First_Bill_Date__c,Package_Name__c, Product_Type__c, Product2__c, Product2__r.Vendor__c, ProductCode__c, Quantity__c, 
                Quote_signing_method__c, Scope__c, Directory_Section__c, Statement_Suppression__c, Status__c, Product2__r.Family, Product2__r.RGU__c, Product2__r.Is_IBUN_Bundle_Product__c,
                Directory_Section__r.Name,Directory__r.Pub_Date__c, Section_Page_Type__c, strOLICombo__c, Product_Inventory_Tracking_Group__c, Opportunity_Close_Date__c,Listing__c,
                Account__r.Name,Account__r.Owner.Name,Directory_Edition__r.Name,Directory__r.Name,UDAC__c,Successful_Payments__c, Talus_Go_Live_Date__c, Telco_Invoice_Date__c, Directory_Code__c, 
                Edition_Code__c, Canvass__r.Canvass_Code__c,Product2__r.Product_Type__c, Listing__r.Phone__c, Listing__r.Area_Code__c, Opportunity__r.Name, Billing_Partner_Account__c, 
                Listing__r.Name, Listing__r.Listing_City__c, Listing__r.Listing_Country__c, Listing__r.Listing_PO_Box__c, Listing__r.Listing_Postal_Code__c, Listing__r.Listing_State__c, 
                Listing__r.Listing_Street__c, Listing__r.Listing_Street_Number__c,Directory_Edition__r.Book_Status__c,Directory_Edition__r.Pub_Date__c,Directory_Edition__r.Directory__c,
                Product2__r.Print_Specialty_Product_Type__c,Opportunity__r.Owner.Email, Opportunity__r.Owner.FirstName, Opportunity__r.Owner.LastName,
                Product2__r.ProductCode , OL_Print_Specialty_Product_Type__c, Action_Code__c
                from Order_Line_Items__c where Id IN:lstOrderLI];
        createBulkOrderController.FindAddOnProductForDFFAndCreateDFF(oliList,mpOpp,new map<Id, Fulfillment_Profile__c>()); 
        Opportunity opp=[select id, isLocked__c from Opportunity where id=:newopportunity.id];
        
         createBulkOrderController.setOpportunityToClosedWon(new list<Opportunity>{opp});
        
        Test.stopTest();
        List<Exception_Error__c> lstExp = [select id, name, Exception_Message__c, StackTraceString__c, Related_to__c from Exception_Error__c];
        System.debug('Testinggg Exceptions lstExp '+lstExp);
    }
    
    static testMethod void createBulkOrderControllerTest03() {
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        newAccount.Billing_Anniversary_Date__c=system.Today();
        newAccount.Statement_Suppression__c=true;
        update newAccount;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.New_Print_Bill_Date__c=system.today();
        objDirEd.Bill_Prep__c=system.today();
        insert objDirEd;
        
        //Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        //insert objDirEd1;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
        
        system.assertNotEquals(objDir.ID, null);
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = 'Online Display';
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        newProduct.Vendor__c='yp';
        if(x==0){
            
            newProduct.Family='print';
        }
       
        lstProduct.add(newProduct);
        }
        insert lstProduct;
        List<pricebookentry> lstinsertPBE = new List<pricebookentry>();
        //pricebook2 objPB = TestMethodsUtility.createpricebook();
        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        for(Product2 iterator : lstProduct) {
        
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
        lstinsertPBE.add(pbe);
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
        objDPM.Product2__c = iterator.Id;
        }
        insert lstinsertPBE;
        insert lstDPM;
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        
        //newOpportunity.modifyid__c = '12345';
        newOpportunity.Billing_Partner__c = 'Berry';
        newOpportunity.Billing_Frequency__c = 'Single Payment';
        newOpportunity.Payment_Method__c = 'Credit Card';        
        newOpportunity.CORE_Migration_ID__c  = '12345';
        newOpportunity.modifyid__c='45765678';
        //newOpportunity.isclosed=true;
        //newOpportunity.iswon=true;
        newOpportunity.StageName='Closed Won';
        insert newOpportunity;
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstinsertPBE) {
        OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
        objOLI.PricebookEntryId = iterator.Id;
        objOLI.Billing_Duration__c = 12;
        objOLI.Directory__c = objDir.Id;
        objOLI.Directory_Edition__c = objDirEd.Id;
        objOLI.Full_Rate__c = 30.00;
        objOLI.Renewals_action__c = 'New';
        objOLI.UnitPrice = 30.00;
        objOLI.Package_ID__c = '123456';
        objOLI.Billing_Partner__c = objOLI.Id;
        objOLI.OpportunityId = newOpportunity.Id;
        objOLI.Directory_Heading__c = objDH.Id;
        objOLI.Directory_Section__c = objDS.Id;
        lstOLI.add(objOLI);
        mapPBE.put(iterator.Id, iterator);
        }
        OpportunityLineItem objOLI1=lstOLI[0];
        objOLI1.Is_P4p__c=true;
         objOLI1.Billing_Duration__c=12;
         objOLI1.Effective_Date__c=system.Today();
        lstOLI.set(0,objOLI1);
        insert lstOLI;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        newOrLI.Parent_ID__c='pkg1';
        newOrLI.Parent_ID_of_Addon__c='pkg1';
        newOrLI.Product2__c =lstProduct[0].id;
        newOrLI.Media_Type__c = 'Digital';
        newOrLI.Addon_Type__c='Points';
       
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        lstOrderLI.add(newOrLI);
        
        if(lstOrderLI.size()>0)
        insert lstOrderLI;
        Test.startTest();
        Opportunity opp=[select id, AccountId, Name from Opportunity where id=:newopportunity.id];
       //List<Order_Line_Items__c> oliList1=[Select Id from Order_Line_Items__c where Id IN:lstOrderLI];
       Order__c ordObj=[select id, Account__c from Order__c where id=:newOrder.Id];
       //list<Order_Line_Items__c> oliObjList=null;
       
            createBulkOrderController.CheckAndCreateNewOrderIfNotExists(new list<Opportunity>{opp});
            //createBulkOrderController.CreateNewOrderGroupNew(new list<Opportunity>{opp},new list<Order__c>{ordObj});
            createBulkOrderController.LineItemHistoryProcess(lstOrderLI);
        
        Test.stopTest();
        List<Exception_Error__c> lstExp = [select id, name, Exception_Message__c, StackTraceString__c, Related_to__c from Exception_Error__c];
        System.debug('Testinggg Exceptions lstExp '+lstExp);
    }
}