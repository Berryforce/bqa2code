global class SetCommissionReadyBatchScheduler implements Schedulable {
	public Interface SetCommissionReadyBatchSchedulerInterface {
		void execute(SchedulableContext sc);
	}
	
	global void execute(SchedulableContext sc) {
		Type targetType = Type.forName('SetCommissionReadyBatchSchedulerHndlr');
		if(targetType != null) {
			SetCommissionReadyBatchSchedulerInterface obj = (SetCommissionReadyBatchSchedulerInterface)targetType.newInstance();
			obj.execute(sc);
		}
	}
}