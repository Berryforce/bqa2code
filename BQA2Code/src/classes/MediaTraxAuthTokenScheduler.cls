global class MediaTraxAuthTokenScheduler implements Schedulable {
   public Interface MediaTraxAuthTokenSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('MediaTraxAuthTokenSchedulerHndlr');
        if(targetType != null) {
            MediaTraxAuthTokenSchedulerInterface obj = (MediaTraxAuthTokenSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}