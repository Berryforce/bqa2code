public without sharing class DirectoryGuides1
{
  
    public List<Directory_Guide__c> lstDirGuide
    {
        get {
        if(con != null)
        return (List<Directory_Guide__c>)con.getRecords();
        else
        return null ;
        }
        set;
    }
    
    public Directory_Guide__c DirGuideLst {get;set;}
    public DirectoryGuides1() 
    {
        lstDirGuide = new List<Directory_Guide__c> ();
        DirGuideLst = new Directory_Guide__c();
    }
    
    public PageReference Search()
    {   
        if(DirGuideLst.Name != null) {
            String finalSearchValue = '%' + DirGuideLst.Name.trim() + '%';
            con = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Id,Name FROM Directory_Guide__c where name LIKE :finalSearchValue ]));
            con.setPageSize(5);
        }
        else{
            con = null;
        }
        return null ;
    }
    
    public ApexPages.StandardSetController con{get; set;}
    
    public Boolean hasNext
    {
        get{return con.getHasNext();}
        set;
    }
    
    public Boolean hasPrevious
    {
        get{return con.getHasPrevious();}
        set;
    }
    
    public Integer pageNumber
    {
        get{return con.getPageNumber();}
        set;
    }
    
    public void previous()
    {
        con.previous();
    }
    
    public void next()
    {
        con.next();
    }
    
}