public class TearPagesController {
    public String pubCode {get;set;}
    public String editionCode {get;set;}
    public Id dirId {get;set;}
    public List<SelectOption> dirList {get;set;}
    public Id SACRACReportId {get;set;}
    public Boolean displayBool {get;set;}
    public Boolean tearPagesReadyBool {get;set;}
    public Boolean otherBool {get;set;}
    public boolean displayPopup {get; set;}
    public boolean goButtonBool {get; set;}
    public list<NationalInvoice__c> listNatInv {get;set;}
    public String strDirName{get;set;}
    public map<Id,string> mapDirIdName = new map<id,string>();
    map<String, Id> mapDirNameId = new map<String, Id>();
    list<Directory_Edition__c> dirEditionList = new list<Directory_Edition__c>();
    public Boolean sendAllBool {get; set;}
    public Boolean coversBool {get; set;}
    public Boolean indexBool {get; set;}
    public boolean YPSection1Bool {get; set;}
    public boolean YPSection2Bool {get; set;}
    public boolean WPSection1Bool {get; set;}
    public boolean WPSection2Bool {get; set;}
    public boolean tearPageComplBool {get; set;}
    public boolean pageSentRecvdBool {get; set;}
    public boolean pageSentRecvdFlagBool {get; set;}
    public boolean sendToEliteBool {get; set;}
    public boolean dirIndexBool {get; set;}
    public boolean dirIndexTransCompBool {get; set;}
    List<Directory_Section__c> listDirSec = new List<Directory_Section__c>(); 
    public List<Directory_Section__c> listYPDirSec {get; set;}
    public List<Directory_Section__c> listWPDirSec {get; set;}
    public List<Directory_Section__c> listOtherDirSec {get; set;}
    public String selectedDirCode {get;set;}
    public Map<Id, String> mapDirIdDirCode = new Map<Id, String>();
    public National_Billing_Status__c natBilling {get; set;}
    List<National_Billing_Status__c> listNatBill = new List<National_Billing_Status__c>();
    Public string notEqualMsg {get;set;}
    public Id natBillSACRACReportId{get;set;}
    
    public TearPagesController() {
        listYPDirSec = new List<Directory_Section__c>();
        listWPDirSec = new List<Directory_Section__c>();
        listOtherDirSec = new List<Directory_Section__c>();
        listNatInv = new list<NationalInvoice__c>();
        displayBool = false;
        otherBool = false;
        goButtonBool = true;
        sendToEliteBool = false;
        pageSentRecvdBool = false;
        dirIndexTransCompBool = false;
        dirList = new List<SelectOption>();
        pageSentRecvdFlagBool = false;
        SACRACReportId = National_Billing_Reports__c.getInstance('SACRACTransLog').Report_Id__c;
        natBillSACRACReportId = National_Billing_Reports__c.getInstance('natBillSACRACReport').Report_Id__c;
        natBilling = new National_Billing_Status__c();
        notEqualMsg = '(Pages not equal)';
    }
    
    public void fetchDirs() {
        if(String.isNotBlank(pubCode) && String.isNotBlank(editionCode)) {       
            dirList = new List<SelectOption>();     
            dirEditionList = new list<Directory_Edition__c>(); 
            dirEditionList = DirectoryEditionSOQLMethods.getDirEdByDirPubCodeAndEdCode(pubCode, editionCode);     
            system.debug('Directory Edition list is ' + dirEditionList);             
            if(dirEditionList.size() > 0) {         
                mapDirNameId = new map<String, Id>();
                mapDirIdName = new map<Id, String>();
                for(Directory_Edition__c DE : dirEditionList) {
                    mapDirNameId.put(DE.Directory__r.Name, DE.Directory__c);
                    mapDirIdName.put(DE.Directory__c, DE.Directory__r.Name);
                    mapDirIdDirCode.put(DE.Directory__c, DE.Directory__r.Directory_Code__c);
                }
                List<String> dirNames = new List<String>(mapDirNameId.keySet());
                dirNames.sort();
                for(String str : dirNames) {
                    dirList.add(new SelectOption(mapDirNameId.get(str), str));
                }
            }   
            goButtonCheck();         
        }
        displayBool = false;
        otherBool = false;
    }
    
    public void go() {
        listYPDirSec = new List<Directory_Section__c>();
        listWPDirSec = new List<Directory_Section__c>();
        listOtherDirSec = new List<Directory_Section__c>();
        listNatInv = new list<NationalInvoice__c>();
        displayBool = false;
        otherBool = false;
        goButtonCheck(); 
        sendToEliteBool = false;
        pageSentRecvdBool = false;
        dirIndexTransCompBool = false;
        selectedDirCode = mapDirIdDirCode.get(dirId);                    
        fetchNationalBilling();
        if(listNatBill.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No record found in Status Table'));
            displayBool = false;
        } else {   
            listDirSec = DirectorySectionSOQLMethods.getDirectorySectionsByDirIds(new Set<Id> {dirId});
            if(listDirSec.size() > 0) {
                for(Directory_Section__c DS : listDirSec) {
                    if(DS.Section_Page_Type__c == CommonMessages.ypSecPageType) {
                        listYPDirSec.add(DS);
                    } else if(DS.Section_Page_Type__c == CommonMessages.wpSecPageType) {
                        listWPDirSec.add(DS);
                    } else {
                    	listOtherDirSec.add(DS);
                    }
                }
            }
            strDirName = mapDirIdName.get(dirId);
            displayBool = true;
            assignValuesFromNatBill();
        }
    }
    
    public void tearPagesReady() {
        if(tearPagesReadyBool) {
            sendAllBool = true;
        } else {
            sendAllBool = false;
        }
    }
    
    public void showNatInv() {
        displayPopup = true;
        listNatInv = NationalInvoiceSOQLMethods.getNatInvByPubCodeEdtncodeDirId(pubCode, editionCode, dirId);
    }
    
    public void closePopup() {
        displayPopup = false;     
    }
    
    public void goButtonCheck() {
        if(String.isNotBlank(pubCode) && String.isNotBlank(editionCode) && dirEditionList.size() > 0) {
            goButtonBool = false;
        }
    }
    
    public void updateStatusTable() {
        natBilling.TP_ELITE_TEAR_PAGE_TRANSFER_COMPLETE__c = tearPageComplBool;
        natBilling.TP_Covers_Spines_And_Indexes__c = coversBool; 
        natBilling.DI_SEND_TO_ELITE_CHECK__c = dirIndexBool;
        natBilling.Directory_Index_Transfer_Completed__c = dirIndexTransCompBool;
        
        update natBilling;
    }
    
    public void fetchNationalBilling() {
        listNatBill = NationalBillingStatusSOQLMethods.getNatBillByPubEditionDirCode(pubCode, editionCode, selectedDirCode);
        system.debug('National Billing status size is ' + listNatBill.size());
        if(listNatBill.size() > 0) {
            natBilling = listNatBill.get(0);
            checkPageSentRecvd();
        }        
    }
    
    public void assignValuesFromNatBill() {
        tearPageComplBool = natBilling.TP_ELITE_TEAR_PAGE_TRANSFER_COMPLETE__c;
        coversBool = natBilling.TP_Covers_Spines_And_Indexes__c;
        dirIndexBool = natBilling.DI_SEND_TO_ELITE_CHECK__c;
        dirIndexTransCompBool = natBilling.Directory_Index_Transfer_Completed__c;
        sendToEliteBool = natBilling.TP_SEND_TO_ELITE_INITIATE_INFORMATICA__c;
    }
    
    public void updateSendToElite() {
        natBilling.TP_SEND_TO_ELITE_INITIATE_INFORMATICA__c = true;
        sendToEliteBool = true;
        update natBilling;
    }
    
    public void updateSendDirIndex() {
        natBilling.DI_SEND_TO_ELITE_INITIATE_INFORMATICA__c = true;
        update natBilling;
    }
    
    public void checkPageSentRecvd() {
        if(natBilling.Number_of_Tear_Pages_Received__c != null && natBilling.Number_of_Tear_Pages_Sent__c != null) {
            if(natBilling.Number_of_Tear_Pages_Received__c == natBilling.Number_of_Tear_Pages_Sent__c) {
                pageSentRecvdBool = true;
                pageSentRecvdFlagBool = false;
            } else {
                pageSentRecvdBool = false;
                pageSentRecvdFlagBool = true;
            }
        } else {
            pageSentRecvdFlagBool = false;
            pageSentRecvdBool = false;
        }
    }
    
    public void updateSendTearPagesOnDirSecs() {
        if(listDirSec.size() > 0) {
            update listDirSec;
        }
    }
}