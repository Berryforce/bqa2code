public without sharing class HeadingDisclaimer
{
  
    public List<Heading_Disclaimer__c> lstHeadingDisclaim
    {
        get {
        if(con != null)
        return (List<Heading_Disclaimer__c>)con.getRecords();
        else
        return null ;
        }
        set;
    }
    
    public Heading_Disclaimer__c HeadDisclmLst {get;set;}
    public HeadingDisclaimer() 
    {
        lstHeadingDisclaim = new List<Heading_Disclaimer__c> ();
        HeadDisclmLst = new Heading_Disclaimer__c();
    }
    
    public PageReference Search()
    {   
        if(HeadDisclmLst.Name != null)
        {
            String finalSearchValue = '%' + HeadDisclmLst.Name.trim() + '%';
            con = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Id,Name FROM Heading_Disclaimer__c where name LIKE :finalSearchValue ]));
 
            // sets the number of records in each page set
            con.setPageSize(5);
        }
        else
        {
             con = null;
        }
        return null ;
    }
    
    public ApexPages.StandardSetController con{get; set;}
    
    public Boolean hasNext
    {
        get
        {
            return con.getHasNext();
        }
        set;
    }
    
    public Boolean hasPrevious
    {
        get
        {
            return con.getHasPrevious();
        }
        set;
    }
    
    public Integer pageNumber
    {
        get
        {
            return con.getPageNumber();
        }
        set;
    }
    
    public void previous()
    {
        con.previous();
    }
    
    public void next()
    {
        con.next();
    }
    
}