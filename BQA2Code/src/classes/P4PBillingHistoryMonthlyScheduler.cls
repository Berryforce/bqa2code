/*
This should be scheduled end of every month. Not every day.
*/
global class P4PBillingHistoryMonthlyScheduler implements Schedulable {
   public Interface P4PBillingHistoryMonthlySchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('P4PBillingHistoryMonthlySchedulerHndlr');
        if(targetType != null) {
            P4PBillingHistoryMonthlySchedulerInterface obj = (P4PBillingHistoryMonthlySchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}