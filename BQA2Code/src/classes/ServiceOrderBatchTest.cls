@isTest
private class ServiceOrderBatchTest{    
    static testMethod void soNewDuplicateCheckBatch() {
        Database.BatchableContext bc;
        List<service_order_stage__c> objSOLst = new List<service_order_stage__c>();
        set<Id> setdirListId = new set<Id>();
        set<Id> setListId = new set<Id>();
        Recordtype rtId = TestMethodsUtility.getRecordType('Service_order_stage__c','Daily Service Orders');
        Recordtype rttelcoId = TestMethodsUtility.getRecordType('Telco__c','Telco Provider');
        Canvass__c objCanvss = TestMethodsUtility.createCanvass();
        Account objAcc = TestMethodsUtility.generateTelcoAccount();
        insert objAcc;
        Telco__c objTelco = TestMethodsUtility.generateTelco(objAcc.Id);
        objTelco.RecordTypeId = rttelcoId.id;
        insert objTelco;
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c=objTelco.Id;
        insert objDir;
        Directory__c objDir1 =  TestMethodsUtility.generateDirectory();
        objDir1.Telco_Provider__c=objTelco.Id;
        insert objDir1;
        Directory_Edition__c  objDirEditn =  TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEditn.Book_status__c = 'NI';
        objDirEditn.ALPHA_PAGES_DUE__c = System.today().adddays(-4);
        objDirEditn.PAGINATION_END_WP__c = System.today().adddays(-4);
        insert objDirEditn;
        Directory_Edition__c  objDirEditn1 = TestMethodsUtility.generateDirectoryEdition(objDir1);
        objDirEditn1.Book_status__c = 'BOTS';
        insert objDirEditn1;
        Directory_Section__c objDirSec = TestMethodsUtility.createDirectorySection(objDir);
        Directory_Section__c objDirSec1 = TestMethodsUtility.createDirectorySection(objDir1);
        Directory_Heading__c objDirH =TestMethodsUtility.createDirectoryHeading();
        Lead_Assignment_Rules__c objLAR = new Lead_Assignment_Rules__c(Name='Test LAR',Area_code__c=937,Exchange__c=856,Directory__c=objDir.id,Directory_Section__c=objDirSec.id);
        insert objLAR;
        listing__c objLst = new listing__c(Name ='TestLstName',Lst_Last_Name_Business_Name__c='TestLstName',Phone__c='(939)856-2222',Listing_State__c='TX',Listing_Street__c='Eq Dr',Listing_Street_Number__c='44',Listing_PO_Box__c='2244',Listing_City__c='Dallas',Listing_Postal_Code__c='45342',Telco_Provider__c=objTelco.Id,recordtypeId=system.label.TestListingMainRT);
        insert objLst;
        listing__c objLstBus = new listing__c(Name ='TestLstName',Lst_Last_Name_Business_Name__c='TestLstName',Phone__c='(939)856-2224',Listing_State__c='TX',Telco_Provider__c=objTelco.Id,recordtypeId=system.label.TestListingMainRT);
        insert objLstBus;
        listing__c objLstRes = new listing__c(Name ='TestLstName',First_Name__c='TstFirst',Lst_Last_Name_Business_Name__c='TestLstName',Phone__c='(939)856-2228',Listing_State__c='TX',Telco_Provider__c=objTelco.Id,recordtypeId=system.label.TestListingMainRT);
        insert objLstRes;
        directory_listing__c objdirLst = new directory_listing__c(Name ='TestLstName',SL_Last_Name_Business_Name__c='TestLstName',Phone_Number__c='(939)856-2224',Listing_State__c='TX',Listing_Street_Number__c='44',Listing_Street__c='Eq',Listing_PO_Box__c='2244',Listing_City__c='Dallas',Listing_Postal_Code__c='22',Telco_Provider__c=objTelco.Id,recordtypeId=system.label.TestScopedListingNonOverrideRT,
                                           Bus_Res_Gov_Indicator__c='Business',listing__c= objLstBus.Id,Directory__c = objDir.Id,Directory_Section__c = objDirSec.Id,Directory_Heading__c = objDirH.Id);
        insert objdirLst;
        directory_listing__c objdirLstres = new directory_listing__c(Name ='TestLstName',SL_Last_Name_Business_Name__c='TestLstName',First_Name__c='TstFirst',Phone_Number__c='(939)856-2228',Listing_State__c='TX',Listing_Street_Number__c='44',Listing_Street__c='Eq',Listing_PO_Box__c='2244',Listing_City__c='dallas',Listing_Postal_Code__c='22',Telco_Provider__c=objTelco.Id,recordtypeId=system.label.TestScopedListingNonOverrideRT,
                                           Bus_Res_Gov_Indicator__c='Residentail',listing__c= objLstRes.Id,Directory__c = objDir.Id,Directory_Section__c = objDirSec.Id,Directory_Heading__c = objDirH.Id);
        insert objdirLstres;
        Service_order_stage__c objSO = new Service_order_stage__c(Name = 'Test SO',SOS_LastName_BusinessName__c='Test SO',Action__c = 'New',Phone__c = '(937)856-4444',Listing_City__c='Texas',Listing_State__c='TX',Bus_Res_Gov_Indicator__c = 'Business',Directory__c = objDir.Id,Directory_Section__c = objDirSec.Id,Directory_Heading__c = objDirH.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',Scheduled_Process_Complete__c = false,Fallout__c= false);
        insert objSO;
        Service_order_stage__c objSO1 = new Service_order_stage__c(Name = 'Test SORes',SOS_LastName_BusinessName__c='Test SORes',First_Name__c='ResFirst',Action__c = 'New',Phone__c = '(937)856-4444',Listing_City__c='Texas',Listing_State__c='TX',Bus_Res_Gov_Indicator__c = 'Residential',Directory__c = objDir.Id,Directory_Section__c = objDirSec.Id,Directory_Heading__c = objDirH.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',Scheduled_Process_Complete__c = false,Fallout__c= false);
        insert objSO1;
        Service_order_stage__c objSONew = new Service_order_stage__c(Name = 'Test SONew',SOS_LastName_BusinessName__c='Test SONew',Action__c = 'New',Phone__c = '(937)856-4444',Listing_City__c='Texas',Listing_State__c='TX',Bus_Res_Gov_Indicator__c = 'Business',Directory__c = objDir1.Id,Directory_Section__c = objDirSec1.Id,Directory_Heading__c = objDirH.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',Scheduled_Process_Complete__c = false,Fallout__c= false);
        insert objSONew;
        Service_order_stage__c objSODupe = new Service_order_stage__c(Name = 'Test SO',SOS_LastName_BusinessName__c='Test SO',Action__c = 'New',Phone__c = '(937)856-4444',Listing_City__c='Texas',Listing_State__c='TX',Bus_Res_Gov_Indicator__c = 'Business',Directory__c = objDir.Id,Directory_Section__c = objDirSec.Id,Directory_Heading__c = objDirH.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',Scheduled_Process_Complete__c = false,Fallout__c= false);
        insert objSODupe;
        Service_order_stage__c objSOchange = new Service_order_stage__c(Name = 'Test SOChange',SOS_LastName_BusinessName__c='Test SOChange',Action__c = 'Change',Phone__c = '(937)856-4444',Listing_State__c='TX',Listing_Street_Number__c='8899',Listing_PO_Box__c='8888',Listing_Street__c='Foxhill Dr',Listing_Postal_Code__c='9999',Bus_Res_Gov_Indicator__c = 'Business',Directory__c = objDir.Id,
                                        Directory_Section__c = objDirSec.Id,Directory_Heading__c = objDirH.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',OUT_LAST_NAME_BUSINESS_NAME__c='TestLstName',OUT_PHONE_NUMBER__c='(939)856-2222',
                                        OUT_STATE__c='TX',OUT_STREET_NAME__c='Eq Dr',OUT_STREET_NUMBER__c='44',OUT_PO_BOX__c='2244',OUT_CITY__c='Dallas',OUT_POSTAL_CODE__c='45342',Scheduled_Process_Complete__c = false,Fallout__c= false);
        insert objSOchange;
        Service_order_stage__c objSOdisconnect = new Service_order_stage__c(Name = 'Test SODisconnect',SOS_LastName_BusinessName__c='Test SODisconnect',Action__c = 'Disconnect',Phone__c = '(937)856-4444',Bus_Res_Gov_Indicator__c = 'Business',Directory__c = objDir.Id,Service_Order__c='4444',disconnect_reason__c='disconnect',
                                        Directory_Section__c = objDirSec.Id,Directory_Heading__c = objDirH.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',OUT_LAST_NAME_BUSINESS_NAME__c='TestLstName',OUT_PHONE_NUMBER__c='(939)856-2224',
                                        OUT_STATE__c='TX',OUT_STREET_NUMBER__c='44',OUT_STREET_NAME__c='Eq',OUT_PO_BOX__c='2244',OUT_CITY__c='Dallas',OUT_POSTAL_CODE__c='22',Scheduled_Process_Complete__c = false,Fallout__c= false);
        insert objSOdisconnect;
        Service_order_stage__c objSOdisconnectRes = new Service_order_stage__c(Name = 'Test SODisconnect',SOS_LastName_BusinessName__c='Test SODisconnect',Action__c = 'Disconnect',Phone__c = '(937)856-4444',Bus_Res_Gov_Indicator__c = 'Residential',Directory__c = objDir.Id,Service_Order__c='4444',disconnect_reason__c='disconnect',
                                        Directory_Section__c = objDirSec.Id,Directory_Heading__c = objDirH.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',OUT_LAST_NAME_BUSINESS_NAME__c='TestLstName',OUT_FIRST_NAME__c='TstFirst',OUT_PHONE_NUMBER__c='(939)856-2228',
                                        OUT_STATE__c='TX',OUT_STREET_NUMBER__c='44',OUT_STREET_NAME__c='Eq',OUT_PO_BOX__c='2244',OUT_CITY__c='dallas',OUT_POSTAL_CODE__c='22',Scheduled_Process_Complete__c = false,Fallout__c= false);
        insert objSOdisconnectRes;
        
        set<Id> setSOId = new set<Id>();
        setSOId.add(objSO.Id);
        setSOId.add(objSO1.Id);
        setSOId.add(objSONew.Id);
        setSOId.add(objSODupe.Id);
        setSOId.add(objSOChange.Id);
        setSOId.add(objSOdisconnect.Id);
        setSOId.add(objSOdisconnectRes.Id);
        objSOLst= [SELECT strDLcombo__c,strSOSCombination__c,SOS_LastName_BusinessName__c,Action__c,Area_code__c,Associated_to_Dir_Listing__c,Associated_to_Dir_Listing__r.Disconnected__c,Associated_to_Listing__c,BEX__c,BOID__c,Bus_Res_Gov_Indicator__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,CLEC_Provider__c,Cross_Reference_Indicator__c,Cross_Reference_Text__c,CreatedDate,Data_Feed_Type__c,Designation__c,
                    Directory__r.BOTS_Edition_Publication_Date__c,Directory__r.SO_NI_Blackout_Period_Begin_Date__c,Directory__r.SO_NI_Blackout_Period_End_Date__c,Directory_Heading__c,Directory_Section__c,Directory_Section__r.Section_Page_Type__c,Directory__c,Disconnected__c,Disconnect_Reason__c,Effective_Date__c,Effective_Date_During_Blackout_Period__c,Exchange__c,Fallout__c,First_Name__c,Honorary_Title__c,
                    Id,Indent_Level__c,Indent_Order__c,Initial_Match_on_DL__c,Initial_Match_on_Listing__c,Is_Lead__c,Lead_Account_Source__c,Lead_Owner__c,Lead_Record_Type__c,Lead_Source__c,Lead_Status__c,Left_Telephone_Phrase__c,Lineage_Title__c,Line__c,Listing_City__c,Listing_Country__c,Listing_Postal_Code__c,Listing_PO_Box__c,Listing_State__c,Listing_Street_Number__c,Listing_Street__c,Listing_Type__c,
                    Manual_Sort_As_Override__c,Name,Normalized_Designation__c,Normalized_First_Name__c,Normalized_Honorary_Title__c,Normalized_Last_Name_Business_Name__c,Normalized_Lineage_Title__c,Normalized_Listing_City__c,Normalized_Listing_Postal_Code__c,Normalized_Listing_PO_Box__c,Normalized_Listing_Street_Name__c,Normalized_Listing_Street_Number__c,Normalized_Phone__c,Normalized_Secondary_Surname__c,
                    Omit_Address_OAD__c,On_Hold_Due_to_Blackout__c,Phone_Override_Indicator__c,Phone_Override__c,Phone_Type__c,Phone__c,Primary_Canvass__c,
                    RecordTypeId,Region__c,Right_Telephone_Phrase__c,Secondary_Surname__c,Service_Order__c,Telco_Provider__c,Under_Caption__c,
                    Under_Caption__r.Associated_to_Dir_Listing__c,Under_Sub_Caption__c,Under_Sub_Caption__r.Associated_to_Dir_Listing__c, Batch_ID__c,
                    Under_Caption__r.Associated_to_Listing__c,Under_Caption__r.Fallout__c,Year__c,Scheduled_Process_Complete__c,OUT_LAST_NAME_BUSINESS_NAME__c,OUT_FIRST_NAME__c,OUT_PHONE_NUMBER__c,OUT_STREET_NUMBER__c,OUT_STREET_NAME__c,OUT_PO_BOX__c,OUT_CITY__c,OUT_STATE__c,OUT_POSTAL_CODE__c,Telco_Sort_Order__c,StrBOCDisconnect__c,StrSOLstMatch__c,Do_Not_Create_YP__c,Data_Feed_Type_2__c 
                    FROM Service_Order_Stage__c where Id IN :setSOId];
        Test.startTest();
        ServiceOrderBatchController batchObj = new ServiceOrderBatchController();
        batchObj.start(bc);
        batchObj.execute(bc,objSOLst);
        Test.stopTest();
       
    }
    static testMethod void soMathcingBatch() {
        Database.BatchableContext bc;
        List<service_order_stage__c> objSOmatchingLst = new List<service_order_stage__c>();
        Recordtype rtId = TestMethodsUtility.getRecordType('Service_order_stage__c','Daily Service Orders');
        Recordtype rttelcoId = TestMethodsUtility.getRecordType('Telco__c','Telco Provider');
        Canvass__c objCanvss = TestMethodsUtility.createCanvass();
        Account objAcc = TestMethodsUtility.generateTelcoAccount();
        insert objAcc;
        Telco__c objTelco = TestMethodsUtility.generateTelco(objAcc.Id);
        objTelco.RecordTypeId = rttelcoId.id;
        insert objTelco;
        Directory__c objDir =  TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c=objTelco.Id;
        insert objDir;
        Directory_Edition__c  objDirEditn =  TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEditn.Book_status__c = 'NI';
        objDirEditn.ALPHA_PAGES_DUE__c = System.today().adddays(-4);
        objDirEditn.PAGINATION_END_WP__c = System.today().adddays(-4);
        insert objDirEditn;
        Directory_Section__c objDirSec = TestMethodsUtility.createDirectorySection(objDir);
        Directory_Section__c objDirSecYP = TestMethodsUtility.generateDirectorySection(objDir);
        objDirSecYP.Section_Page_Type__c = 'YP';
        objDirSecYP.Section_Code__c = TestMethodsUtility.generateRandomNumberForLen(8);
        insert objDirSecYP;
        Directory_Heading__c objDirH =TestMethodsUtility.createDirectoryHeading();
        listing__c objLst = new listing__c(Name ='TestLstNameExist',Lst_Last_Name_Business_Name__c='TestLstNameExist',Phone__c='(937)856-2244',Listing_State__c='TX',Telco_Provider__c=objTelco.Id,recordtypeId=system.label.TestListingMainRT);
        insert objLst;
        directory_listing__c objdirLst = new directory_listing__c(Name ='TestLstNameExist',SL_Last_Name_Business_Name__c='TestLstNameExist',Phone_Number__c='(937)856-2244',Listing_State__c='TX',Telco_Provider__c=objTelco.Id,recordtypeId=system.label.TestScopedListingNonOverrideRT,listing__c=objLst.Id,Directory__c = objDir.Id,Directory_Section__c = objDirSec.Id,Created_by_Batch__c=true);
        insert objdirLst;
        directory_listing__c objdirLstYP = new directory_listing__c(Name ='TestLstNameExist',SL_Last_Name_Business_Name__c='TestLstNameExist',Phone_Number__c='(937)856-2244',Listing_State__c='TX',Telco_Provider__c=objTelco.Id,recordtypeId=system.label.TestScopedListingNonOverrideRT,listing__c=objLst.Id,Directory__c = objDir.Id,Directory_Section__c = objDirSecYP.Id,Created_by_Batch__c=true);
        insert objdirLstYP;
        Service_order_stage__c objSO = new Service_order_stage__c(Name ='TestLstNameExist',SOS_LastName_BusinessName__c='TestLstNameExist',Action__c = 'New',Phone__c = '(937)856-2244',Listing_State__c='TX',Bus_Res_Gov_Indicator__c = 'Business',Directory__c = objDir.Id,
                                        Directory_Section__c = objDirSec.Id,Directory_Heading__c = objDirH.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',Scheduled_Process_Complete__c = false,Fallout__c= false);
        
        insert objSO;
        objSOmatchingLst = [SELECT strDLcombo__c,strSOSCombination__c,SOS_LastName_BusinessName__c,Action__c,Area_code__c,Associated_to_Dir_Listing__c,Associated_to_Dir_Listing__r.Disconnected__c,Associated_to_Listing__c,BEX__c,BOID__c,Bus_Res_Gov_Indicator__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,CLEC_Provider__c,Cross_Reference_Indicator__c,Cross_Reference_Text__c,CreatedDate,Data_Feed_Type__c,Designation__c,
                            Directory__r.BOTS_Edition_Publication_Date__c,Directory__r.SO_NI_Blackout_Period_Begin_Date__c,Directory__r.SO_NI_Blackout_Period_End_Date__c,Directory_Heading__c,Directory_Section__c,Directory_Section__r.Section_Page_Type__c,Directory__c,Disconnected__c,Disconnect_Reason__c,Effective_Date__c,Effective_Date_During_Blackout_Period__c,Exchange__c,Fallout__c,First_Name__c,Honorary_Title__c,
                            Id,Indent_Level__c,Indent_Order__c,Initial_Match_on_DL__c,Initial_Match_on_Listing__c,Is_Lead__c,Lead_Account_Source__c,Lead_Owner__c,Lead_Record_Type__c,Lead_Source__c,Lead_Status__c,Left_Telephone_Phrase__c,Lineage_Title__c,Line__c,Listing_City__c,Listing_Country__c,Listing_Postal_Code__c,Listing_PO_Box__c,Listing_State__c,Listing_Street_Number__c,Listing_Street__c,Listing_Type__c,
                            Manual_Sort_As_Override__c,Name,Normalized_Designation__c,Normalized_First_Name__c,Normalized_Honorary_Title__c,Normalized_Last_Name_Business_Name__c,Normalized_Lineage_Title__c,Normalized_Listing_City__c,Normalized_Listing_Postal_Code__c,Normalized_Listing_PO_Box__c,Normalized_Listing_Street_Name__c,Normalized_Listing_Street_Number__c,Normalized_Phone__c,Normalized_Secondary_Surname__c,
                            Omit_Address_OAD__c,On_Hold_Due_to_Blackout__c,Phone_Override_Indicator__c,Phone_Override__c,Phone_Type__c,Phone__c,
                            Primary_Canvass__c,RecordTypeId,Region__c,Right_Telephone_Phrase__c,Secondary_Surname__c,Service_Order__c,Telco_Provider__c,
                            Under_Caption__c,Under_Caption__r.Associated_to_Dir_Listing__c,Under_Sub_Caption__c,Under_Sub_Caption__r.Associated_to_Dir_Listing__c,
                            Under_Caption__r.Associated_to_Listing__c,Under_Caption__r.Fallout__c,Year__c,Scheduled_Process_Complete__c, Batch_ID__c,
                            OUT_LAST_NAME_BUSINESS_NAME__c,OUT_FIRST_NAME__c,OUT_PHONE_NUMBER__c,OUT_STREET_NUMBER__c,OUT_STREET_NAME__c,OUT_PO_BOX__c,OUT_CITY__c,OUT_STATE__c,OUT_POSTAL_CODE__c,Telco_Sort_Order__c,StrBOCDisconnect__c,StrSOLstMatch__c,Do_Not_Create_YP__c,Data_Feed_Type_2__c 
                            FROM Service_Order_Stage__c where Id=: objSO.id];
                            

        Test.startTest();
        ServiceOrderBatchController batchObj = new ServiceOrderBatchController();
        //database.executebatch(batchObj);
        batchObj.start(bc);
        batchObj.execute(bc,objSOmatchingLst);
        Test.stopTest();
    }
}