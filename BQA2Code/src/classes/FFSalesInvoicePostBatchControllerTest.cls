@isTest(SeeAllData=true)
private Class FFSalesInvoicePostBatchControllerTest
{

  static testMethod void TestFFSalesInvoicePostBatchController()
  {
     Account objacc = TestMethodsUtility.generateAccount();
     insert objacc;
     
     Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
     newOpportunity.AccountId = objacc.Id;
     insert newOpportunity;
     
     c2g__codaInvoice__c objSalesInvoice= TestMethodsUtility.generateSalesInvoice(objacc,newOpportunity);
     objSalesInvoice.Customer_Name__c=objacc.Id;
     List<c2g__codaInvoice__c> objlstSalesInvoice = new List<c2g__codaInvoice__c>();
     objlstSalesInvoice.add(objSalesInvoice);
     insert objlstSalesInvoice;
     
     c2g__codaCreditNote__c objCreditNote = TestMethodsUtility.createSalesCreditNote(objlstSalesInvoice[0],objacc);
     
     
     Set<Id>  ids = new Set<Id>();
     ids.add(objlstSalesInvoice[0].id);
     
     Test.startTest();
     FFSalesInvoicePostBatchController objFFSalesInvoice = new FFSalesInvoicePostBatchController(ids);
     Id id = Database.executebatch(objFFSalesInvoice);
     
     Test.StopTest();
  
  }


}