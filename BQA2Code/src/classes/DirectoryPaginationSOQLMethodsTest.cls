@IsTest(SeeAllData=True)
public class DirectoryPaginationSOQLMethodsTest {
    @isTest static void DirectoryPaginationSOQLMethodsCoverage() {
       set<Id> setSLId = new set<Id>();
       Account newAccount = TestMethodsUtility.createAccount('customer');
       Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
       Product2 newProduct = TestMethodsUtility.createproduct();
       Opportunity newOpportunity = TestMethodsUtility.createOpportunity('new', newAccount);
       Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
       Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
       Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
       Directory__c directory = TestMethodsUtility.createDirectory();
       Directory_Section__c dirSection = TestMethodsUtility.createDirectorySection(directory);
       Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(directory);
       objDirEd.Pub_Date__c=System.today().addMOnths(1);
       insert objDirEd;
       Listing__c objLst = TestMethodsUtility.generateMainListing();
       insert objLst;
       Directory_listing__c objDLst = TestMethodsUtility.generateNonOverrideRT();
       objDLst.Listing__c=objLst.Id;
       objDLst.Follows_Listing_Record2__c='true';
       objDLst.Disconnected__c = false;
       insert objDLst;
       setSLId.add(objDLst.Id);
       Directory_pagination__c objDP = TestMethodsUtility.generateDirectoryPagination(dirSection.Id,newOrLI.Id,objDirEd.Id);
       objDP.Reference_Listing__c =objDLst.Id;
       objDP.DP_Listing__c = objLst.Id;
       objDP.DP_OLI_Anchor_Caption_Header__c = 'Test';
       insert objDP;
       set<string> setstr = new set<string>{'Test'};
       Test.startTest();
       DirectoryPaginationSOQLMethods.getDirPaginationByUnderCaptions(setstr);
       DirectoryPaginationSOQLMethods.getDirPaginationByUnderCaption(setstr);
       DirectoryPaginationSOQLMethods.getDirPaginationByUnderCaptionId(setSLId);
       DirectoryPaginationSOQLMethods.getDirPaginationBySLId(setSLId);
       Test.stoptest();
       
    }
}