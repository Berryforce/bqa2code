global class DirectoryFlipBookUpdatesScheduleHndlr implements DirectoryFlipBookUpdatesSchedule.DirectoryFlipBookUpdatesScheduleInterface {
     global void execute(schedulableContext sc) {        
        List<Directory__c> lstDir = [select id,name,sales_lockout__c,EBD__c,Ship_date__c,DCR_Close__c,BOC__c,Final_Service_Order_Due_to_Berry__c,
                                    Book_Extract_YP__c,Pub_Date__c,Coup_Prelim_Filler__c,Delivery_Start_Date__c,Sales_Start__c from Directory__c where sales_Lockout__c =: System.today()-1 AND CANCELLED_DIRECTORIES_INDICATOR__c = False];  
        if(lstDir != null) {
            DirectoryFlipBookCommonMethods.doDirectoryFlipBooKProcess(lstDir);
        }
    }
}