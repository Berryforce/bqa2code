global class BillingFirstMonthPrintScheduler implements Schedulable {
   public Interface BillingFirstMonthPrintSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('BillingFirstMonthPrintSchedulerHndlr');
        if(targetType != null) {
            BillingFirstMonthPrintSchedulerInterface obj = (BillingFirstMonthPrintSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}