public with sharing class AccountWithDirectories {
    public String AcctId;
    public List<Line_Item_History__c> LIHList = new List<Line_Item_History__c>();
    public List<Line_Item_History__c> CurrentLIHList = new List<Line_Item_History__c>();
    public List<Line_Item_History__c> PreviousLIHList = new List<Line_Item_History__c>();
    Map<Id, String> DirIdName = new Map<Id, String>();
    Map<Id, List<Line_Item_History__c>> dirIdLIHListMap  = new Map<Id, List<Line_Item_History__c>>();
    
    Set<Id> DirIds = new Set<Id>();
    
    public Map<Id, List<Line_Item_History__c>> dirIdCurrentLIHListMap = new Map<Id, List<Line_Item_History__c>>();
    public Map<Id, List<Line_Item_History__c>> dirIdPreviousLIHListMap = new Map<Id, List<Line_Item_History__c>>();
    
    public List<DirectoryWrapper> DirectoryWrapperList {get;set;}
    
    public AccountWithDirectories(ApexPages.StandardController controller) {        
        DirectoryWrapperList = new List<DirectoryWrapper>();
        AcctId = controller.getId();
        LIHList = [SELECT Id, CreatedDate, Order_Line_Total__c, Order_Line_Item__c, Order_Line_Item__r.UnitPrice__c, Directory_Edition__c, Directory_Edition__r.Book_Status__c, Directory_Edition__r.Directory__c FROM Line_Item_History__c WHERE Order_Line_Item__r.Account__c = : AcctId Order By CreatedDate DESC];
        system.debug('LIH list is ' + LIHList);
        
        for(Line_Item_History__c LIH : LIHList) {
            if(!dirIdLIHListMap.containsKey(LIH.Directory_Edition__r.Directory__c)) {
                dirIdLIHListMap.put(LIH.Directory_Edition__r.Directory__c, new List<Line_Item_History__c>());
            }
            dirIdLIHListMap.get(LIH.Directory_Edition__r.Directory__c).add(LIH);            
            
            if(LIH.Directory_Edition__r.Book_Status__c == 'NI') {
                CurrentLIHList.add(LIH);
            } else if(LIH.Directory_Edition__r.Book_Status__c == 'BOTS') {
                PreviousLIHList.add(LIH);
            }
        }
        
        for(Line_Item_History__c LIH : CurrentLIHList) {
            if(!dirIdCurrentLIHListMap.containsKey(LIH.Directory_Edition__r.Directory__c)) {
                dirIdCurrentLIHListMap.put(LIH.Directory_Edition__r.Directory__c, new List<Line_Item_History__c>());
            }
                dirIdCurrentLIHListMap.get(LIH.Directory_Edition__r.Directory__c).add(LIH);
        }
        
        for(Line_Item_History__c LIH : PreviousLIHList) {
            if(!dirIdPreviousLIHListMap.containsKey(LIH.Directory_Edition__r.Directory__c)) {
                dirIdPreviousLIHListMap.put(LIH.Directory_Edition__r.Directory__c, new List<Line_Item_History__c>());
            }
                dirIdPreviousLIHListMap.get(LIH.Directory_Edition__r.Directory__c).add(LIH);            
        }   
        
        DirIds = dirIdLIHListMap.keySet();
        system.debug('Directory Ids are ' + DirIds);
        
        List<Directory__c> DirList = new List<Directory__c>();
        DirList = [SELECT Id, Name FROM Directory__c];
        
        for(Directory__c Dir : DirList) {
            DirIdName.put(Dir.Id, Dir.Name);
        }
        
        for(Id DirId : DirIds) {
            String DirName = DirIdName.get(DirId);
            Double NI = 0;
            Double PI = 0;
            List<Line_Item_History__c> TempCurrentLIHList = new List<Line_Item_History__c>();
            List<Line_Item_History__c> TempPreviousLIHList = new List<Line_Item_History__c>();
            Map<Id, List<Line_Item_History__c>> OLIIdLIHListMap =  new Map<Id, List<Line_Item_History__c>>();
            Set<Id> OLIIds = new Set<Id>();
            
            if(dirIdCurrentLIHListMap.containsKey(DirId)){
                TempCurrentLIHList = dirIdCurrentLIHListMap.get(DirId);
                for(Line_Item_History__c LIH : TempCurrentLIHList) {
                    OLIIds.add(LIH.Order_Line_Item__c);
                    if(!OLIIdLIHListMap.containsKey(LIH.Order_Line_Item__c)) {
                        OLIIdLIHListMap.put(LIH.Order_Line_Item__c, new List<Line_Item_History__c>());
                    }
                    OLIIdLIHListMap.get(LIH.Order_Line_Item__c).add(LIH);
                    for(Id OLIId : OLIIds) {
                        List<Line_Item_History__c> TempCurrentLIHListForNI = new List<Line_Item_History__c>();
                        if(OLIIdLIHListMap.containsKey(OLIId)) {
                            TempCurrentLIHListForNI = OLIIdLIHListMap.get(OLIId);
                            if(TempCurrentLIHListForNI.get(0).Order_Line_Total__c != null) {
                                NI = NI + TempCurrentLIHListForNI.get(0).Order_Line_Total__c;
                            }
                        }
                    }
                }           
                OLIIds.clear();
                OLIIdLIHListMap.clear();    
            }
            if(dirIdPreviousLIHListMap.containsKey(DirId)) {
                TempPreviousLIHList = dirIdPreviousLIHListMap.get(DirId);
                for(Line_Item_History__c LIH : TempPreviousLIHList) {
                    OLIIds.add(LIH.Order_Line_Item__c);
                    if(!OLIIdLIHListMap.containsKey(LIH.Order_Line_Item__c)) {
                        OLIIdLIHListMap.put(LIH.Order_Line_Item__c, new List<Line_Item_History__c>());
                    }
                    OLIIdLIHListMap.get(LIH.Order_Line_Item__c).add(LIH);
                    for(Id OLIId : OLIIds) {
                        List<Line_Item_History__c> TempPreviousLIHListForPI = new List<Line_Item_History__c>();
                        if(OLIIdLIHListMap.containsKey(OLIId)) {
                            TempPreviousLIHListForPI = OLIIdLIHListMap.get(OLIId);
                            if(TempPreviousLIHListForPI.get(0).Order_Line_Total__c != null) {
                                PI = PI + TempPreviousLIHListForPI.get(0).Order_Line_Total__c;
                            }
                        }
                    }
                }               
            }
            
            if(DirName != null) {
                DirectoryWrapperList.add(new DirectoryWrapper(DirName, NI, PI));  
            }      
        }
    }
    
    public class DirectoryWrapper {
        public String DirectoryName {get;set;}
        public Double NI {get;set;}
        public Double PI {get;set;}
        
        public DirectoryWrapper(String DirectoryName, Double NI, Double PI) {
            this.DirectoryName = DirectoryName;
            this.NI = NI;
            this.PI = PI;
        }
    }
}