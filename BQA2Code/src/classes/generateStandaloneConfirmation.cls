public class generateStandaloneConfirmation
{
public Id thisOpportunity_Id {get;set;}
public Opportunity Opp=new Opportunity();
Set<ID> OrderGroupSet=new Set<ID>();
public List<Order_Line_Items__c> OliItem {get;set;}
public void generateStandaloneConfirmation()
{

}

public Opportunity getOpp()
{
OliItem=new List<Order_Line_Items__c>();
Opp=[Select Id,Billing_Contact__c,Amount, (SELECT BillingChangeNoofDays__c,BillingChangeProrateCreditDays__c,BillingChangesPayment__c,
    Billing_Change_Prorate_Credit__c,Billing_Close_Date__c,Billing_Contact__c,Billing_End_Date__c,Billing_Frequency__c,Billing_Partner_Account__c,Billing_Partner__c,
    Billing_Start_Date__c,Cancelation_Fee__c,Cancellation__c,Canvass__c,checked__c,Continious_Billing__c,CreatedById,CreatedDate,
    Current_Billing_Period_Days__c,Current_Daily_Prorate__c,Current_Rate__c,Cutomer_Cancel_Date__c,Date_of_Transfer__c,Days_B_W__c,Description__c,
    Digital_Product_Requirement__c,Directory__c,Discount__c,Effective_Date__c,Fulfilled_on__c,FulfillmentDate__c,Directory_Heading__c,
    Id,isCanceled__c, Directory_Heading__r.Name, Directory_Heading__r.Directory_Heading_Name__c,
    IsDeleted,Is_Billing_Cycle__c,LastActivityDate,LastModifiedById,LastModifiedDate,Last_successful_settlement__c,Line_Status__c,ListPrice__c,Name,Next_Billing_Date__c,
    Opportunity__c,Order_Anniversary_Start_Date__c,Order_Billing_Date_Changed__c,Order_Group__c,Order__c,OriginalBillingDate__c,Package_ID__c,Payments_Remaining__c,
    Payment_Duration__c,Payment_Method__c,Product2__c,ProductCode__c,Product_Type__c,Prorate_Credit_Days__c,Prorate_Credit__c,Prorate_Stored_Value__c,Quantity__c,
    Quote_Signed_Date__c,Quote_signing_method__c,Reason_for_Transfer__c,Scope__c,Directory_Section__c,Sent_to_fulfillment_on__c,Service_End_Date__c,Service_Start_Date__c,
    Statement_Suppression__c,Status__c,Subscription_ID__c,Successful_Payments__c,SystemModstamp,Talus_Cancel_Date__c,Talus_Fulfillment_Date__c,Talus_Go_Live_Date__c,
    Talus_OrderLineItem__c,Telco__c,Total_Prorated_Days_for_contract__c,Total_Prorate__c,UnitPrice__c,User_who_initiated_the_transfer__c,
    Zero_Cancel_Fee_Approved__c,zero_Cancel_Fee_Requested__c FROM Order_Line_Items__r),(SELECT id,CreatedById,CreatedDate,IsDeleted,LastModifiedById,LastModifiedDate,
    Name,oli_count__c,Opportunity__c,Order_Account__c,Order__c,selected__c FROM Order_Sets__r),    Billing_Contact__r.Name,Billing_Contact__r.MailingStreet,
    Billing_Contact__r.MailingCity,Billing_Contact__r.MailingState,Billing_Contact__r.MailingPostalCode,Billing_Contact__r.Phone  
    FROM Opportunity  where id=:thisOpportunity_Id];

for(Order_Group__c ordergroup:Opp.Order_Sets__r)
{
          OrderGroupSet.add(ordergroup.id);

}

for(Order_Line_Items__c OrdrLi:Opp.Order_Line_Items__r)
{
       OliItem.add(OrdrLi);     
}
return Opp;

}

}