public class DirectoryHeadingSOQLMethods {    
    public static list<Directory_Heading__c> getDirectoryHeadingGetMediaTrax() {
        return [Select Id, Name, Media_Trax_Heading_ID__c from Directory_Heading__c where Media_Trax_Heading_ID__c = null limit 500];
    }
    public static list<Directory_Heading__c> getDirectoryHeadingUpdateMediaTrax() {
        return [Select Id, Name, Media_Trax_Heading_ID__c from Directory_Heading__c where Media_Trax_Heading_ID__c != null and Is_Changed__c = true limit 500];
    }    
}