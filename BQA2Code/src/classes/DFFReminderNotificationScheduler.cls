global class DFFReminderNotificationScheduler implements Schedulable {
	public Interface DFFReminderNotificationSchedulerInterface {
		void execute(SchedulableContext sc);
	}
	
	global void execute(SchedulableContext sc) {
		Type targetType = Type.forName('DFFReminderNotificationSchedulerHndlr');
		if(targetType != null) {
			DFFReminderNotificationSchedulerInterface obj = (DFFReminderNotificationSchedulerInterface)targetType.newInstance();
			obj.execute(sc);
		}
	}
}