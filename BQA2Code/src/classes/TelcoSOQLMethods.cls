public class TelcoSOQLMethods {
	public static map<Id, Telco__c> getTelco(set<Id> setCLECIds){
        return new map<Id, Telco__c>([SELECT Id,RecordtypeId FROM Telco__c WHERE Id IN : setCLECIds and RecordtypeId=:Label.TestCLECProviderRT]);
   } 
}