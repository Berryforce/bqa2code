public class CommonVariables {
    public CommonVariables(parentController controller) {  }
    
    public static boolean bScoping = false;
        
    public CommonVariables() {}
    
    public static string strId {set; get;}
    
    public static boolean bOpportunityProductBundleRecursive = false;
    
    public static boolean bAccountBulkBillRecursive = false;
    
    public static boolean LeadListingAdrRecursive=false;
    
    public static boolean ServiceOrderDirListingRecursive=false;
    
    public static boolean inFutureContext = false;
    
    public static boolean inrollup=false;
    
    public static boolean inrollup1=false;
    
    public static boolean InternetBundleLogic = true;
    
    public static boolean AccountRepeatCheck = true;
    
    public static boolean PaymentRepeatCheck = true;
    
    public static boolean multiAdRepeatCheck = true;
    
    //public static boolean ISSCommissionRepeatCheck = true;
    
    public static boolean NSLISaleRateRepeatCheck = true;
    
    public static boolean OLIGoLiveRepeatCheck = true;
    
    public static boolean ListingsDisconnectCheck = false;
    
    public static boolean AppearanceLogic = true;
    
    public static boolean ListingABDCheck = false;
    
    public static boolean ScopedListingUpdateCheck = true;
    
    public static boolean listingtriggeroff = false;
    
    public static List<Directory_Listing__c> LstDirListing {get;set;}
    
    public static void setLstDirListing(List<Directory_Listing__c> value) {
        
        LstDirListing = value;
    }
    
    public static List<Directory_Listing__c> getLstDirListing() {
        return LstDirListing;
    }
    
    public static void setBScopting() {
        bScoping = true;
    }
    
    public static boolean getBScoping() {      
        return bScoping;
    }
    
    public static void reSetBScoping() {       
        bScoping = false;
    }
    
    public static void setBOpportunityProductBundleRecursive() {
        bOpportunityProductBundleRecursive = true;
    }
    
    public static boolean getBOpportunityProductBundleRecursive() {      
        return bOpportunityProductBundleRecursive;
    }
    
    public static void reSetBOpportunityProductBundleRecursive() {       
        bOpportunityProductBundleRecursive = false;
    }
    
    public static void setBAccountBulkBillRecursive() {
        bAccountBulkBillRecursive = true;
    }
    
    public static boolean getBAccountBulkBillRecursive() {      
        return bAccountBulkBillRecursive;
    }
    
    public static void reSetBAccountBulkBillRecursive() {       
        bAccountBulkBillRecursive = false;
    }
    
    /*Order line Item Handler Controller recurcive varibales*/
    public static boolean bOLIRecursive = false;
    public static void setOLIRecursive() {
        bOLIRecursive = true;
    }
    
    public static boolean getOLIRecursive() {
        return bOLIRecursive;
    }
    
    public static void reSetOLIRecursive() {
        bOLIRecursive = false;
    }
    
    /*Order line Item Handler Controller DoAccountPartner Method recurcive varibales*/
    public static boolean bOLIDoAccountPartnerRecursive = false;
    public static void setOLIDoAccountPartnerRecursive() {
        bOLIDoAccountPartnerRecursive = true;
    }
    
    public static boolean getOLIDoAccountPartnerRecursive() {
        return bOLIDoAccountPartnerRecursive;
    }
    
    public static void reSetOLIDoAccountPartnerRecursive() {
        bOLIDoAccountPartnerRecursive = false;
    }
    
    /*Order line Item Handler Controller accountCanvassLogic Method recurcive varibales*/
    public static boolean bOLIAccountCanvassLogicRecursive = false;
    public static void setOLIaccountCanvassLogicRecursive() {
        bOLIAccountCanvassLogicRecursive = true;
    }
    
    public static boolean getOLIaccountCanvassLogicRecursive() {
        return bOLIAccountCanvassLogicRecursive;
    }
    
    public static void reSetOLIaccountCanvassLogicRecursive() {
        bOLIAccountCanvassLogicRecursive = false;
    }
    
    /*Order line Item Handler Controller NextBillingDate/Proration Method recurcive varibales*/
    public static boolean bOLINextBillingAndProrationRecursive = false;
    public static void setOLINextBillingAndProrationRecursive() {
        bOLINextBillingAndProrationRecursive = true;
    }
    
    public static boolean getOLINextBillingAndProrationRecursive() {
        return bOLINextBillingAndProrationRecursive;
    }
    
    public static void reSetOLINextBillingAndProrationRecursive() {
        bOLINextBillingAndProrationRecursive = false;
    }
    
    /*Order line Item Handler Controller IncreaseDecreaseIQQuantity Method recurcive varibales*/
    public static boolean bOLIInventoryTracking = false;
    public static void setOLIInventoryTrackingRecursive() {
        bOLIInventoryTracking = true;
    }
    
    public static boolean getOLIInventoryTrackingRecursive() {
        return bOLIInventoryTracking;
    }
    
    public static void reSetOLIInventoryTrackingRecursive() {
        bOLIInventoryTracking = false;
    }
    
     /*ListingToScopedListingSync syncListingToScopedListing Method recurcive varibales*/
    public static boolean syncListingToScopedListing = false;
    public static void setsyncListingToScopedListing() {
        syncListingToScopedListing = true;
    }
    
    public static boolean getsyncListingToScopedListing() {
        return syncListingToScopedListing;
    }
    
    public static void reSetsyncListingToScopedListing() {
        syncListingToScopedListing = false;
    }
    
    //@IsTest


}