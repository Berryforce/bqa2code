public class CommonMessages {
    public CommonMessages(){    }
    
    /****************Record Type name start**********************/
    //Telco Record types
    public static string TelcoProviderRT = 'Telco Provider';
    
    //Opportunity RT names
    public static string opportunityLockRTName = 'Opportunity Won';
    public static string opportunityAssignedRT = 'Opportunity Ready For Order';
    public static string opportunityNewRT = 'Opportunity New';
    public static string opportunityNationalRT = 'National Opportunity';
    
    //Quote RT names
    public static string quoteNewRT = 'New Quote RT';
    public static string quoteSignedRT = 'Signed Quote RT'; 
    
    //Account RT names
    public static string accountCustomerRT = 'Customer Account';
    public static string accountBulkBillRT = 'Bulk Billing Customer';
    public static string accountTelcoRT = 'Telco Partner';
    public static string accountNationalClientRT = 'National Client';
    public static string accountNationalActRT = 'National Account';
    public static string accountCMRRT = 'CMR';
    public static string accountCMRNet = 'CMR Network';
    public static string accountFistPartyBulk = '1st Party Bulk Bill';
    public static String accountthirdPartyWriteOff = '3rd Party Write-off';
    
    //Case RT names,
    public static string caseNonClaimRT = 'CS_Non_Claim_RT'; 
    public static String caseInternal = 'CS_Internal_Berry_Team_RT';
    public static string caseClaimRT = 'CS_Claim_RT'; 
    public static string NationalRT = 'National Claim';
    
    //Media type
    public static string printMediaType = 'Print'; 
    public static String digitalMediaType = 'Digital'; 
    
    //Media type for Monthly Cron
    public static String MCP4PPrintCancelMediaType = 'P4P Print Canceled';
    public static String MCP4PDigitalCancelMediaType = 'P4P Digital Canceled';
    public static String MCFMPMediaType = 'FMP';
    
    //Directory Listing RT Names
    public static string dlNonOverRT = 'Non Override RT';
    public static string dlOverRT = 'Address Override RT';
    public static string dlCaptionMemb = 'Caption Member';
    
    //Order line Item
    public static string nationaOLIRT = 'National Order Line Item';
    public static string beryyOLIRT = 'New Order Line Item';
    
    //YPC AddOn
    public static string YPCObjectName = 'YPC_AddOn__c';
    public static string couponYPCRT = 'Coupon';
    public static string CustomLinkYPCRT = 'Custom Link';
    
    //National Staging RT Names
    public static string eliteNSRT = 'Elite RT';
    public static string manualNSRT = 'Manual NS RT';
    
    //Fulfillment Profile RT
    public static string fProfileRT = 'Initial Fulfillment Profile';
    
    //Case RT Names
    public static string NationalCase = 'National Claim - Hide Button';
    public static string CSManager = 'Customer Service Manager';
    /****************Record Type name end**********************/
    
    /****************Object name start**********************/
    //Account Object name
    public static string accountObjectName = 'Account';
    
    //Contact Object name
    public static string contactObjectName = 'Contact';
    
    //Opportunity Object name
    public static string opportunityObjectName = 'Opportunity';
    
    //Lead Object name
    public static string leadObjectName = 'Lead';
    
    //Quote Object Name
    public static string quoteObjectName = 'Quote';
    
    //Case Object Name
    public static string caseObjectName = 'Case';
    
    //Opportunity Line Item Object Name
    public static string OpportunityLIObjectName ='OpportunityLineItem';
    
    //National Staging Object Name
    public static string NSObjectName = 'Staging_Order__c';
    
    
    //Order Object Name
    public static string orderObjectName ='Order__c';
    
    //Order Line Item Object Name
    public static string OLIObjectName = 'Order_Line_Items__c';
    
    //Modification Order Line Item Object Name
    public static string MOLIObjectName = 'Modification_Order_Line_Item__c';
    
    //Fulfillment Profile Object Name
    public static string FPObjectName = 'Fulfillment_Profile__c';
    
    //Listing Object Name 
    public static string LObjectName = 'Listing__c';
    
    //Directory Listing Object Name
    public static string DLObjectName = 'Directory_Listing__c';
    
    //Directory Listing Object Name
    public static string DPObjectName = 'Directory_Pagination__c';
    
    //Directory Edition Object Name
    public static string DEObjectName = 'Directory_Edition__c';
    
    //DFF Object Name
    public static string dffObjectName = 'Digital_Product_Requirement__c';
    
    //YPC Graphics Object Name
    public static string ypcGraphicsObjectName = 'YPC_Graphics__c';
    
    //National Staging Order Set Object Name
    public static string nationalStagingOrderSetObjectName = 'National_Staging_Order_Set__c';
    
    //BMI Quote Object Name
    public static string quoteBMIObjectName = 'BigMachines__Quote__c';
    
    //Processor Connetction name
    public static string processorConnectionName = System.Label.ProcessorConnectionName;
    public static string processorConnectionId = System.Label.ProcessorConnectionId;
    
    //Product name for prorate
    public static string prorateProductName = 'Product Proration';
    
    //Accounting Currency Name
    public static string accountingCurrencyName = 'USD';
    public static string accountingCurrencyId = System.Label.AccountingCurrencyId;    
    
    //Product name for ELAL Mapping
    public static string ELProd = 'Extra Line Type Product';
    public static string ALProd = 'Alternate Call Type Product';
    public static String CrossRefListProd = 'Cross Reference Listing';
    
    /****************Object name end**********************/
    
    /****************Account Client Tier Name Start**********************/
    public static string PlatinumPartner = 'Platinum';
    public static string DiamondPartner = 'Diamond';
    public static string FrontierTelcoPartner = 'Frontier Communications';
    /****************Account Client Tier Name Start**********************/
    
    /****************BCC Name Start**********************/
    public static string FrontierCaresElite = 'Frontier Cares Elite';
    public static string FrontierCares = 'Frontier Cares';
    public static string BerryCares = 'Berry Cares';
    public static string BerryCaresElite = 'Berry Cares Elite';
    /****************BCC Name Start**********************/
    
    /****************Product2 variables Start**********************/
    public static string prdAddonTypeAddOn = 'Add On';
    public static string prdAddonTypePoints = 'Points';
    public static string prdAddonTypeVideo = 'Video';
    public static string prdAddonTypeCoupon = 'Coupon';
    public static string prdAddonType5Heading = 'Points 5 Heading';    
    public static string prdVendorYP = 'yp';
    public static string prdVendorWebCom = 'web.com';
    /****************Product2 variables End**********************/
    
    /****************DFF variables Start**********************/
    public static string dffLockRT = 'Multi Ad';
    public static string dffMultiAdRT = 'Multi Ad';
    /****************DFF variables End**********************/
    
    /****************FF SI variables Start**********************/
    public static string billingTransferTD = 'TD - Billing Transfer Invoice';
    public static string scnBillingEngine = 'CC - Sales Credit Note - CS';
    public static string scnBillingEngineNational = 'NCB - Sales Credit Note - National Billing';
    /****************FF SI variables End**********************/
    
    /****************Publication Account variables Start**********************/
    public static string publicationCode = '0921';
    /****************Publication Account variables End**********************/
    
    /****************Order Group variables Start**********************/
    public static string osNational = 'National';
    public static string osNonNational = 'Berry';
    /****************Order Group variables End**********************/
    
    /****************OLI variables Start**********************/
    public static string oliPrintProductType = 'Print';
    public static string oliLineStatusContinuous = 'Continuous Billing';
    public static string oliLineStatusInitial = 'Initial Payment Made';
    public static string oliLineStatusMonthly = 'Monthly Billing';
    public static string oliLineStatusFinished = 'Billing Finished';
    public static string oliLineStatusCancel = 'Cancelation Requested';
    /****************OLI variables End**********************/
    
    
    /****************cash entry variables Start**********************/
    public static string cashEntryAccpaymentMethod = 'Electronic';
    /****************OLI variables End**********************/
    
    /****************Bank_Account variables Start**********************/
    public static string bankAccountName = 'Berry National Collections';
    /****************OLI variables End**********************/
    
    /****************Product variables Start**********************/
    public static string printProductFamily = 'Print';
    /****************Product variables End**********************/
    
    /****************Opportunity variables Start**********************/
    public static string ccPaymentMethod = 'Credit Card';
    public static string ACHPaymentMethod = 'ACH';
    public static string telcoPaymentMethod = 'Telco Billing';
    public static string StatementPaymentMethod = 'Statement';
    public static string singlePayment = 'Single Payment';
    public static string monthlyPayment = 'Monthly';
    public static string setupFeesProductName = 'Setup Fees';
    public static string closedOwnStageOpp = 'Closed Won';
    public static string proposalStageOpp = 'proposal created';
    public static string nationalStageOpp = 'National Quote';
    public static string oppStatus = 'Continuous Billing';
    public static string berryTelcoName = 'THE BERRY COMPANY';    
    public static string berryBillingPartner = 'berry';
    public static string berryBillPartner = 'BERRY';
    /****************Opportunity variables end**********************/
    
    /****************PaymentX variables Start**********************/
    public static string ccPaymentType = 'Credit Card';
    public static string ACHPaymentType = 'ECheck';
    public static Date systemDate = system.today();
    public static DateTime systemDateTime = system.now();
    public static String pymtTransactionType = 'Payment';
    public static String pymtStatus = 'Scheduled';
    public static String pymtStatusComplete = 'Completed';
    public static String pymtStatusDecline = 'Declined';
    public static String pymtCurrencyISOCode = 'USD';
    public static String initialPymt = 'Initial Payment';
    public static String pymtBatchProcessingAction = 'Process using assigned Payment Method';
    /****************PaymentX variables end**********************/
    
    /****************Payment Method variables Start**********************/
    public static string pymtMthdBank = 'Bank Account';
    /****************Payment Method variables End**********************/
    
    /****************Custom Messages**********************/
    //National Order Temp Prodcut
    public static string nationalProductID = System.Label.NationalProductID;
    public static string cmrAccountRecordTypeID = System.Label.TestAccountCMRRT;
    public static string clientAccountRecordTypeID = System.Label.TestAccountNationalRT;
    
    //Group Name
    public static string queueBilling = 'Billing';
    
    //common message
    public static string requiredField = 'You must enter a value';
    public static string successMessage = 'Record saved successfully';
    
    //Account Message
    public static string accountDelinquencyIndicator = 'Can not create a opportunity for Delinquency Account';
    
    //Opportunity Message
    public static string opportunityModifyorDelete = 'Opportunity is closed Won and Cannot be Edited or Deleted';
    public static string opportunityAccountStreet = 'Account main listed street should not empty';
    public static string opportunityAccountCity = 'Account main listed city should not empty';
    public static string opportunityAccountState = 'Account main listed state should not empty';
    public static string opportunityAccountPostalCode = 'Account main listed postal code should not empty';
    public static string opportunityAccountCountry = 'Account main listed country should not empty';
    public static string opportunityContactEmail = 'Opportunity contact email address should not empty';
    public static string opportunityContactContactRole = 'Opporunity contact billing/contact role should not empty';
    public static string opportunityContactPhone = 'Opportunity contact phone number should not empty';
    public static string opportunityContactStreet = 'Opportunity contact street should not empty';
    public static string opportunityContactCity = 'Opportunity contact City should not empty';
    public static string opportunityContactState = 'Opportunity contact state should not empty';
    public static string opportunityContactPostalCode = 'Opportunity contact postal code should not empty';
    public static string opportunityContactCountry = 'Opportunity contact country should not empty';
    public static string opportunityQuoteContact = 'You must enter the contact information on synced quote';
    public static string opportunityBillingContact = 'Billing Contact is required on opportunity';
    public static string opportunityContactPrimaryBilling = 'Opportunity billing contact is not a primary billing contact';
    public static string opportunityQuoteDate = 'Quote signed date should not empty';
    public static string opportunityQuoteSync = 'Quote is not synced';
    public static string opportunityQuoteLineItemRequired = 'Atlease one quote line item required';
    public static string opportunityAccountDelinquency = 'Delinquency account opportunity can not close';
    public static string opportunityDocuSign = 'Quote signed document not recived';
    public static string opportunitySigningMethodRAS = 'RAS';
    public static string opportunitySigningMethodDocuSign = 'DocuSign';
    public static String opptyTelcoAcctError = 'Telco Billing is Prohibited on this Account. Please update your Billing Partner and Payment method.';
    public static string opptyLinvioMissingMsg = 'Linvio Payment Method is missing on the Original Order Line Item';
    public static string opptyLinvioMissingInBCMsg = 'There is no default Linvio Payment Method for opportunity billing contact';
    
    //Pending Item to Close Opportunity
    public static string opportunityBMIQuote = 'BMI Quote is required';
    public static string opportunityBMIQuoteLineItem = 'Atleast one BMI Quote Product is required';
    public static string opportunityPrimaryBMIQuote = 'BMI Quote is not synced with opportunity';
    public static string opportunitySigningDocument = 'DocuSign document is not yet received';
    public static string opportunitySigningMethod = 'Signing Method is required.';
    public static string opportunitySigningMethodRSADateID = 'RSA ID and Date is missing';
    public static string opportunitySigningContact = 'Signing Contact is missing';
    
    
    //Opportunity Product Messages
    public static string opportunityProductModifyOrDelete = 'Opportunity is closed Won Opportunity Products can not be modified or deleted';
    public static string opportunityProductAdd = 'Opportunity is closed Won New Opportunity Products can not be added';
    public static string opportunityProductMOLI = 'Original Order Line Item is missing on modified opportunity product : ';
    
    //Quote messages
    public static string opportunityQuoteModifyOrDelete = 'Opportunity is closed Won Quote can not be modified or deleted';
    public static string opportunityQuoteAdd = 'Opportunity is closed Won new Quote can not be added';
    public static string opportunityLineItemEmpty = 'Can not create a quote without any product on opportunity.';
    
    //Quote line Item messages
    public static string opportunityQuoteLineItemModifyOrDelete = 'Opportunity is closed Won Quote Line Item can not be modified or deleted';
    public static string opportunityQuoteLineItemAdd = 'Opportunity is closed Won new Quote Line Item can not be added';
    
    // Contact messages   
    public static string contactBillingPrimary = 'Primary Billing Contact has already been activated for this account.';
    public static string mainContact = 'Already primary contact has been activated for this account.';
    public static string proofContact = 'Already proof contact has been activated for this account.';
    
    //Sales credit note messages
    public static string salesCreditNoteAccount = 'Without account you can not create a sales credit note for a case. Please go back to case and fill account information and try again.';
    public static string salesCreditNoteInvoice = 'Without invoice you can not create a sales credit note for a case. Please go back to case and fill invoice information and try again.';
    public static string salesCreditNoteCheckbox = 'Please select any one invoice line item';
    public static string invoiceCheckbox = 'Please select atleast one item to proceed next.';
    public static string invoiceLineItemCheckbox = 'There is no line item for selected invoice.';
    public static string salesCreditNoteFieldValidation = 'You must enter a value';
    public static string salesCreditNoteMatchInvoiceAccount = 'Account does not match with selected invoice.';
    public static string salesCreditNoteExists = 'Sales credit note already exists for this case.';
    public static string salesCreditNoteInvoiceForAccount = 'There is no invoice associated on account';
    public static string salesCreditNoteUnitPrice = 'Item Total Credit value should be less than or equal to Invoice Line Item value';
    public static string salesCreditNoteUnitPriceCheck = 'Item Total Credit value should be less than or equal to Remainig Credit Note';
    public static string salesCreditNoteRemainingBalanace = 'Can not create a credit note more than Invoice Net value';
    
    //Task messages
    public static string preventAccountTaskDelete = 'You do not have rights to delete Account related Notes : ';
    
    /* Case object messages */
    public static string newCase = 'New';
    public static string creditCardIssueReason = 'Credit Card Issue';
    public static string creditCardProcessorRecdFrom = 'Credit Card Processor';
    public static string receivedByOther = 'Other';   
    public static string billingCaseType = 'Billing Case';
    
    //Opportunity Pending Item message
    public static string commonMessage = 'These items are required in order to Close the Opportunity and Create the Order';
    public static string opportunityProductRequired = 'Opportunity product is required';
    public static string opportunityQuoteRequired = 'Quote is required';
    public static string opportunityStage = 'Opportunity Stage should be "Closed Won" or "Closed Decrease';
    public static string opportunityQuoteNeedtoSync = 'Quote needs to sync with Opportunity';
    
    //DFF messages
    public static string radioSelectionMessage = 'Please select one fulfillment profile record.';
    public static string dffAccountIsEmpty = 'Account is not associated with DFF.';
    public static string fulfillmentProfileisEmpty = 'No record found.';
    public static string printGraphic = 'Print Graphic';
    public static string artOrAdContactRole = 'Art/Ad Approval';
    public static string decisionMakerContactRole = 'Decision Maker';
    public static string ownerContactRole = 'Owner';
    public static string billingContactRole = 'Billing';
    public static string email = 'Email';
    public static string paper = 'Paper';
    
    //Division Message
    public static string divisionQueueCheck = 'Division name alreay exists';
    
    /* National Order */
        public static string NSDAT_A = 'A';
        public static string NSType1_A = 'Values of Listed Address';
        public static string NSDAT_C = 'C';
        public static string NSType1_C = 'Caption';
        public static string NSDAT_D = 'D';
        public static string NSType1_D = 'Specialized Dialing Instructions';
        public static string NSDAT_E = 'E';
        public static string NSType1_E = 'E-mail';
        public static string NSDAT_F = 'F';
        public static string NSType1_F = 'Finding Line';
        public static string NSDAT_H = 'H';
        public static string NSType1_H = 'Classified Heading';
        public static string NSDAT_I = 'I';
        public static string NSType1_I = 'Indented Listing Captions';
        public static string NSDAT_L = 'L';
        public static string NSType1_L = 'Line Overflow';
        public static string NSDAT_N = 'N';
        public static string NSType1_N = 'Listed Name';
        public static string NSDAT_O = 'O';
        public static string NSType1_O = 'Other/Guides/Sections';
        public static string NSDAT_S = 'S';
        public static string NSType1_S = 'Subcaption';
        public static string NSDAT_T = 'T';
        public static string NSType1_T = 'Listed Telephone Number';
        public static string NSDAT_U = 'U';
        public static string NSType1_U = 'URL';
        public static string NSDAT_V = 'V';
        public static string NSType1_V = 'Vanity Telephone Number';
        public static string NSDAT_X = 'X';
        public static string NSType1_X = 'Sub Subcaption';    
    
    //SPEC Art Request
    public static string SPECArtReqObjId = 'a2e';
    
    /* Dimensions */
    public static string DimensionPrint = 'DimensionPrint';
    public static string DimensionDigital = 'DimensionDigital';
    public static string DimensionProductType = 'DimensionProductType';
    public static string DimensionCMR = 'DimensionCMR';
    public static string DimensionP4P = 'DimensionP4P';
    public static string DimensionRGUType = 'DimensionRGUType';
    
    public static string P4P = 'P4P';
    
    public static string Berry = 'The Berry Company';
    public static string CMR = 'CMR';
    public static string Telco = 'Telco';    
    
    public static string BerryForDimension = 'THE BERRY COMPANY';
    public static string CMRForDimension = 'CMR (N)';
    public static string TelcoForDimension = 'Telco (L)';
    
    public static String billBoard = 'Billboard';
    public static String banner = 'Banner';
    public static String fcban = 'FCBAN';
    public static String ban = 'BAN';
    public static String bnrb = 'BNRB';
    public static String wbmm =  'WBMM';
    public static String wbmmh = 'WBMMH';
    public static String wbmmp = 'WBMMP';
    public static String Specialty = 'Specialty';
    public static String printRGU = 'Print';
    public static String menuPrintSpecialty = 'Menu';
    
    public static String billingTranInv = 'TD - Billing Transfer Invoice';
    
    /* Berry Cares Certificate*/
    public static String berryCare = 'Berry Cares';
    public static String berryCareElite = 'Berry Cares Elite';
    public static String frontierCare = 'Frontier Cares';
    public static String frontierCareElite = 'Frontier Cares Elite';
    public static String frontierCareSign = 'Frontier Cares Signature';
    
    /* National Trans */
    public static String oldCMR = 'OLD_CMR';
    public static String newCMR = 'NEW_CMR';
    public static String equalTo = '=';
    public static String oldClientNumber = 'OLD_CLIENT_NUMBER';
    public static String newClientNumber = 'NEW_CLIENT_NUMBER';
    public static String underScore = '_';
    public static String semiColon = ';';
    
    public static String bots = 'BOTS';
    public static String botsMinus1 = 'BOTS-1';
    
    /* Renew Actions */
    public static String newRenewalAction = 'New';
    public static String cancelRenewalAction = 'Cancel';
    public static String renewRenewalAction = 'Renew';
    public static String modifyRenewalAction = 'Modify';
    
    /* Directory Product Mapping */
    public static string wpBillBoards = 'WP Billboards';
    public static string wpBanners = 'WP Banners';
    public static string ypLeaderAd = 'YP Leader Ad';
    
    /* Section Page Type */
    public static String ypSecPageType = 'YP';
    public static String wpSecPageType = 'WP';
    public static String tradMarkFindLine = 'Trademark Finding Line';
    
    /* Directory Edition Mapping */
    public static string editionBookStatus = 'BOTS-1';
    
    /* Canvass Billing Entities */
    public static String ACSBillEntity = 'ACS';
    
    //Product Codes for Adding a second to seniority date
    public static set<string>setProdCode =new set<string>{'DTKR','DTR2','DTR3','DTRC','DTRP','DTRW','DTRW1','DTRWR','MDTRW','MDTRP'}; 
    
    
    //Lead RT
    public static string leadRT = 'Lead_RT';
    /*****************************************************/
    
    @IsTest(SeeAllData=true)
    public static void CommonMessagesTest(){
        CommonMessages msg = new CommonMessages();
    }
}