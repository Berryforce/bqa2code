global class LetterRenewalReportSchedulerHndlr implements LetterRenewalReportScheduler.LetterRenewalReportSchedulerInterface {
    global void execute(SchedulableContext sc) {
      LetterRenewalBatchController  lrBatch = new LetterRenewalBatchController (); 
      database.executebatch(lrBatch);
   }   
}