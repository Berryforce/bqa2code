public class SalesInvoiceSOQLMethods {
    
    
    //Adding a Method to fetch Invoice,InvoiceLineItems and Payments
    public static map<Id, c2g__codaInvoice__c> getMapSalesInvoiceLineItemsPaymentsbyInvoiceId(set<Id> InvoiceId){
        return new map<ID, c2g__codaInvoice__c>([Select c2g__TaxTotal__c, c2g__DueDate__c, c2g__Account__c, c.Name, Id, c2g__Opportunity__c,c2g__NetTotal__c,
                                      (Select Id, Name, pymt__Amount__c, pymt__Authorization_Id__c,pymt__Check_Number__c, pymt__Contact__r.Name, pymt__Date__c, pymt__Last_4_Digits__c, pymt__Payment_Type__c, pymt__Status__c,pymt__Card_Type__c From p2f__Payments__r), 
                                      (Select Id,Order_Line_Item__r.Account__c,c2g__Invoice__c, c2g__LineDescription__c, c2g__LineNumber__c, c2g__UnitPrice__c,c2g__Product__r.Name,c2g__codaInvoiceLineItem__c.c2g__NetValue__c 
                                       From c2g__InvoiceLineItems__r) 
                                      From c2g__codaInvoice__c c where Id IN: InvoiceId]);
    }
    
    public static List<c2g__codaInvoice__c> getMapSalesInvoiceLineItemsPaymentsbyInvoiceIds(set<Id> salesInvoiceIds){
        return new List<c2g__codaInvoice__c>([SELECT c2g__Account__r.Name, c2g__Account__r.BillingStreet, c2g__Account__r.BillingCity, c2g__Account__r.BillingState, 
                                              c2g__Account__r.BillingCountry, c2g__Account__r.BillingPostalCode, c2g__Account__r.AccountNumber, c2g__Account__r.Phone,
                                              Service_Start_Date__c, Service_End_Date__c, c2g__DueDate__c,
                                              (Select Id, Name, pymt__Amount__c, pymt__Authorization_Id__c,pymt__Check_Number__c, pymt__Contact__r.Name, pymt__Date__c, 
                                              pymt__Last_4_Digits__c, pymt__Payment_Type__c, pymt__Status__c,pymt__Card_Type__c From p2f__Payments__r), 
                                              (Select Id,Order_Line_Item__r.Account__c,c2g__Invoice__c, c2g__LineDescription__c, c2g__LineNumber__c, c2g__UnitPrice__c,c2g__Product__r.Name,
                                              Order_Line_Item__r.Package_ID__c, c2g__codaInvoiceLineItem__c.c2g__NetValue__c From c2g__InvoiceLineItems__r) 
                                              From c2g__codaInvoice__c c where Id IN: salesInvoiceIds]);
    } 
    public static list<c2g__codaInvoice__c> getSalesInvoiceByInvoiceID(set<Id> InvoiceId) {
        return [Select SI_Payment_Method__c, c2g__InvoiceStatus__c, c2g__PaymentStatus__c,c2g__InvoiceDate__c, c2g__Opportunity__c, c2g__NetTotal__c, c2g__InvoiceTotal__c, c2g__Interval__c, c2g__Account__c, Order_Set__c,c2g__Period__c,c2g__DueDate__c ,c2g__InvoiceCurrency__c, c2g__PrintStatus__c,Name, Id, Customer_Name__c, c2g__Dimension1__c,c2g__Dimension2__c,c2g__Dimension3__c,Transaction_Type__c From c2g__codaInvoice__c where Id IN :InvoiceId];
    }
    
    public static list<c2g__codaInvoice__c> getSalesInvoiceByInvoice(list<c2g__codaInvoice__c> lstFFInvoice) {
        return [select Total_Commission__c, Total_Gross_Amnt__c, c2g__TaxTotal__c, c2g__NetTotal__c from c2g__codaInvoice__c where id in :lstFFInvoice];
    }
    
    public static list<c2g__codaInvoice__c> getSalesInvoiceByISSIds(set<Id> ISSIds) {
        return [SELECT c2g__TaxTotal__c, c2g__NetTotal__c, Total_Gross_Amnt__c, Total_Commission__c FROM c2g__codaInvoice__c 
                WHERE ISS_Number__c IN :ISSIds];
    }
}