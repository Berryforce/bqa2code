public with sharing class MediaTraxHeadingManualCallOutController {
    
    public MediaTraxHeadingManualCallOutController(Apexpages.Standardcontroller controller) {
        
    }
    
    public void AddHeading() {
        Batch_Size__c objBatchSize = Batch_Size__c.getInstance('MediaTraxHeading');
        MediaTraxServiceForAddHeadingBatch objCallout = new MediaTraxServiceForAddHeadingBatch();
        database.executebatch(objCallout, Integer.valueOf(objBatchSize.Size__c));
    }
    
    public void AddDirectory() {
        Batch_Size__c objBatchSize = Batch_Size__c.getInstance('MediaTraxDirectory');
        MediaTraxServiceForAddDirectoryBatch objCallout = new MediaTraxServiceForAddDirectoryBatch();
        database.executebatch(objCallout, Integer.valueOf(objBatchSize.Size__c));
    }
    
    public void AddClient() {
        Batch_Size__c objBatchSize = Batch_Size__c.getInstance('MediaTraxClient');
        MediaTraxServiceForGetAddClientBatch objCallout = new MediaTraxServiceForGetAddClientBatch();
        database.executebatch(objCallout, Integer.valueOf(objBatchSize.Size__c));
    }
    
    public void GetAddHeading() {
        Batch_Size__c objBatchSize = Batch_Size__c.getInstance('MediaTraxHeading');
        MediaTraxServiceForGetAddHeadingBatch objCallout = new MediaTraxServiceForGetAddHeadingBatch();
        database.executebatch(objCallout, Integer.valueOf(objBatchSize.Size__c));
    }
    
    public void GetAddDirectory() {
        Batch_Size__c objBatchSize = Batch_Size__c.getInstance('MediaTraxDirectory');
        MediaTraxServiceForGetAddDirectoryBatch objCallout = new MediaTraxServiceForGetAddDirectoryBatch();
        database.executebatch(objCallout, Integer.valueOf(objBatchSize.Size__c));
    }
    
    public void GetAddClient() {
        Batch_Size__c objBatchSize = Batch_Size__c.getInstance('MediaTraxClient');
        MediaTraxServiceForGetAddClientBatch objCallout = new MediaTraxServiceForGetAddClientBatch();
        database.executebatch(objCallout, Integer.valueOf(objBatchSize.Size__c));
    }
    
    public void UpdateHeading() {
        Batch_Size__c objBatchSize = Batch_Size__c.getInstance('MediaTraxHeading');
        MediaTraxServiceForUpdateHeadingBatch objCallout = new MediaTraxServiceForUpdateHeadingBatch();
        database.executebatch(objCallout, Integer.valueOf(objBatchSize.Size__c));
    }
    
    public void UpdateDirectory() {
        Batch_Size__c objBatchSize = Batch_Size__c.getInstance('MediaTraxDirectory');
        MediaTraxServiceForUpdateDirectoryBatch objCallout = new MediaTraxServiceForUpdateDirectoryBatch();
        database.executebatch(objCallout, Integer.valueOf(objBatchSize.Size__c));
    }
    
    public void UpdateClient() {
        Batch_Size__c objBatchSize = Batch_Size__c.getInstance('MediaTraxClient');
        MediaTraxServiceForUpdateClientBatch objCallout = new MediaTraxServiceForUpdateClientBatch();
        database.executebatch(objCallout, Integer.valueOf(objBatchSize.Size__c));
    }    
    
    public void AuthendationSutureCallout() {
        try{
            MediaTraxHeadingCalloutController_V1 objCallout = new MediaTraxHeadingCalloutController_V1();
            objCallout.AuthendationSutureCallout();
        }catch(Exception objExp){
            System.debug('Exception occured on getting Authentication token - Message '+objExp.getMessage());
            futureCreateErrorLog.createErrorRecord(objExp.getMessage(), objExp.getStackTraceString(),'MediaTrax');
        }
    }
}