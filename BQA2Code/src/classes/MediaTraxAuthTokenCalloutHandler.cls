public class MediaTraxAuthTokenCalloutHandler {
    
    public static DOM.Document getCalloutRespose(String endPointURl, DOM.Document objDoc, String headerSOAPAction, String calloutState, String contentType) {
        System.debug('endPointURl'+endPointURl);
        System.debug('objDoc'+objDoc);
        System.debug('headerSOAPAction'+headerSOAPAction);
        System.debug('calloutState'+calloutState);
        System.debug('contentType'+contentType);
        System.debug('objDoc.toXmlString()'+objDoc.toXmlString());
        System.debug('calloutState'+calloutState);
        HttpRequest req = new HttpRequest();
        req.setMethod(calloutState);
        req.setEndpoint(endPointURl);
        req.setHeader('Content-Type', contentType);
        req.setHeader('SOAPAction', headerSOAPAction);
        req.setBodyDocument(objDoc);
        req.setTimeOut(10000);
        Http http = new Http();
        HttpResponse res;
        
        
        if(!Test.isRunningTest()){
            res = http.send(req);
        }
        else {
            res = new HttpResponse();
            res.setHeader('Content-Type', 'application/xml');
            res.setBody('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><Response><Return><GetHeadings><action>GetHeadings</action><headings><heading><headingid>93</headingid><headingname>TestName</headingname></heading></headings><total>1</total></GetHeadings></Return></Response><Response xmlns="https://ct.mediatrax.com/api/"><Return xsi:type="ns1:Document"xmlns:ns1="http://xml.apache.org/xml-soap"><GetDirectories xmlns="http://calltraks.dev.local/api/"><directories><directory><directorynumber>101753</directorynumber><directoryid>1974</directoryid></directory></directories><total>2</total></GetDirectories></Return></Response></soapenv:Body></soapenv:Envelope>');
            res.setStatusCode(200);
        }
            
            
        system.debug('Service name : '+ headerSOAPAction);
        system.debug('Status Code : '+ res.getStatusCode());
        if(res.getStatusCode() == 200) {
            return res.getBodyDocument();
        }
        return null;
    }
    
    public static DOM.Document generateDOMObjectForAuth(String userName, String password, String headerSOAPAction, String soapNS, String xsi) {
        DOM.Document doc = new DOM.Document();  
        
        dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv'); 
        envelope.setNamespace('api', xsi);
        dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
        dom.XmlNode body1 = body.addChildElement(headerSOAPAction, xsi, 'api');
        
        body1.addChildElement('username', xsi, null).addTextNode(userName);
        body1.addChildElement('password', xsi, null).addTextNode(password);
        return doc;
    }
    
    public static String getAuthTokenFromMediaTrax(String userName, String password, String endPointURL, String headerSOAPAction, String calloutState, 
                                                    String contentType, String strResponse, String strReturn, String soapNS, String xsi) {
        DOM.Document objDoc = generateDOMObjectForAuth(userName, password, headerSOAPAction, soapNS, xsi);
        Dom.Document doc2 = getCalloutRespose(endPointURL, objDoc, headerSOAPAction, calloutState, contentType);
        String authCode;
        if(doc2 != null) {
            Dom.XMLNode address = doc2.getRootElement();
            dom.XmlNode header = address.getChildElement('Body', soapNS).getChildElement(strResponse, xsi).getChildElement(strReturn, xsi);
            
            for(dom.XmlNode iterator: header.getChildElements()) {
                for(dom.XmlNode iterator1: iterator.getChildElements()) {
                    //system.debug('Child : '+ iterator1);
                    if(iterator1.getName() == 'authtoken') {
                        system.debug('Child : '+ iterator1.getName());
                        system.debug('Child : '+ iterator1.getText()); 
                        authCode = iterator1.getText();
                        break;
                    }
                }
            }
        }
        return authCode;
    }
    
    @future(callout=true)
    public static void AuthendationSutureCallout() {
        map<String, MediaTrax_API_Configuration__c> mapMediaTraxConf = new map<String, MediaTrax_API_Configuration__c>();
        list<MediaTrax_API_Configuration__c> lstMediTracConf = [Select Content_Type__c, UserName__c, SOAP_URL__c, Return_Method__c, Response_Method__c, Password__c, Name, Id, 
                                    Header_SOAPAction__c, Endpoint_URL__c, Callout_Method__c, AuthToken__c, API_URL__c From MediaTrax_API_Configuration__c];
        
        for(MediaTrax_API_Configuration__c iterator : lstMediTracConf) {
            mapMediaTraxConf.put(iterator.Name, iterator);
        }
        if(mapMediaTraxConf.size() > 0) {
            MediaTrax_API_Configuration__c objMediaTeax = mapMediaTraxConf.get('GetAuthToken');
            if(objMediaTeax != null) {
                String authToken = MediaTraxAuthTokenCalloutHandler.getAuthTokenFromMediaTrax(objMediaTeax.UserName__c, objMediaTeax.Password__c, objMediaTeax.Endpoint_URL__c, objMediaTeax.Header_SOAPAction__c, objMediaTeax.Callout_Method__c, 
                                                                            objMediaTeax.Content_Type__c, objMediaTeax.Response_Method__c, objMediaTeax.Return_Method__c, objMediaTeax.SOAP_URL__c, objMediaTeax.API_URL__c);
                if(String.isNotBlank(authToken)) {
                    for(MediaTrax_API_Configuration__c iterator : mapMediaTraxConf.values()) {
                        iterator.AuthToken__c = authToken;
                    }
                    
                    update mapMediaTraxConf.values();
                }
            }
        }
    }

}