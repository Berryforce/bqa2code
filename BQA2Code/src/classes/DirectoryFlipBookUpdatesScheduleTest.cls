@isTest(SeeAllData=False)
public class DirectoryFlipBookUpdatesScheduleTest{
    public static testmethod void scheduletest1(){
        Test.startTest();
        Account acct = TestMethodsUtility.createAccount('cmr');
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }               
         Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        DirectoryFlipBookUpdatesSchedule sh1 = new DirectoryFlipBookUpdatesSchedule();  
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        objDir.sales_Lockout__c =System.today();
        insert objDir;    
        String schDigitalScheduler = '0  00 1 3 * ?';
       
        system.schedule('Test1DigitalScheduler', schDigitalScheduler, sh1);
       
        Test.stopTest();
    }
}