global class ScheduleYpcAddonPatch implements Schedulable {
   public Interface ScheduleYpcAddonPatchInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('ScheduleYpcAddonPatchHndlr');
        if(targetType != null) {
            ScheduleYpcAddonPatchInterface obj = (ScheduleYpcAddonPatchInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}