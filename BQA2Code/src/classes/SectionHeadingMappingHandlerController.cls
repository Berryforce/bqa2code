public class SectionHeadingMappingHandlerController {
    public static void onBeforeInsert(list<Section_Heading_Mapping__c> lstSHM) {
        combineDirectorySecAndDirectoryHeading(lstSHM, null);
    }
    
    public static void onBeforeUpdate(list<Section_Heading_Mapping__c> lstSHM, map<ID, Section_Heading_Mapping__c> mapOldSHM) {
        combineDirectorySecAndDirectoryHeading(lstSHM, mapOldSHM);
    }
    
    
    private static void combineDirectorySecAndDirectoryHeading(list<Section_Heading_Mapping__c> lstSHM, map<ID, Section_Heading_Mapping__c> mapOldSHM) {
        Set<Id> dirSectionIdSet=new Set<Id>();
        Map<Id,Directory_Section__c> dirSectionMap=null;
        for(Section_Heading_Mapping__c iterator : lstSHM){
            if(mapOldSHM==null && iterator.Directory_Section__c!=null){
                dirSectionIdSet.add(iterator.Directory_Section__c);
            }else if(mapOldSHM!=null) { // && iterator.Directory_Section__c != mapOldSHM.get(iterator.Id).Directory_Section__c){
                dirSectionIdSet.add(iterator.Directory_Section__c);
            }
        }
        
        if(dirSectionIdSet!=null && dirSectionIdSet.size()>0){
            dirSectionMap=new Map<Id,Directory_Section__c>([select Id,Directory_Code__c from Directory_Section__c where id IN :dirSectionIdSet]);
        }
        
        for(Section_Heading_Mapping__c iterator : lstSHM) {
            if(iterator.Directory_Heading__c != null && iterator.Directory_Section__c != null) {
                if(mapOldSHM != null) {
                    Section_Heading_Mapping__c objOldSHM = mapOldSHM.get(iterator.Id);
                   if(objOldSHM.Directory_Heading__c != iterator.Directory_Heading__c || objOldSHM.Directory_Section__c != iterator.Directory_Section__c) {
                        iterator.SHM_strDirectorySecAndHeading__c = iterator.Directory_Section__c +''+ iterator.Directory_Heading__c;
                   }
                }
                else {
                    iterator.SHM_strDirectorySecAndHeading__c = iterator.Directory_Section__c +''+ iterator.Directory_Heading__c;
                }
            }
            if(iterator.Directory_Section__c != null && dirSectionMap != null) {
                if(dirSectionMap.get(iterator.Directory_Section__c) != null) {
                    if(mapOldSHM != null) {
                       Section_Heading_Mapping__c objOldSHM = mapOldSHM.get(iterator.Id);
                       //if(objOldSHM.Directory_Section__c != iterator.Directory_Section__c) {
                            iterator.Directory_Code__c = dirSectionMap.get(iterator.Directory_Section__c).Directory_Code__c;
                       //}
                    }
                    else {
                        iterator.Directory_Code__c =dirSectionMap.get(iterator.Directory_Section__c).Directory_Code__c;
                    }
                }
            }
        }
    }
}