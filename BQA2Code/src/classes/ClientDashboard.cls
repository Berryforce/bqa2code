global with sharing class ClientDashboard {

    //Getter and Setter
    public String accName {
        get;
        set;
    }
    public Account acc {
        get;
        set;
    }
    public List < Account > lstAcc {
        get;
        set;
    }
    public Boolean inPutFlg {
        get;
        set;
    }

    public String selectedAccount {
        get;
        set;
    }

    //Constructor
    public ClientDashboard() {
        acc = new Account();
    }

    //Method to redirect user to Dashboard Page
    public pageReference clntDsbd() {
        System.debug('************SelectedAccountId************' + selectedAccount);
        if (String.isNotBlank(selectedAccount)) {
            PageReference page = new PageReference('/apex/SingleSignOnDashboard?id=' + selectedAccount);
            page.setRedirect(true);
            
            //Assign AccountId to Null
            selectedAccount = '';
          
            return page;
            /*            
            try {
                acc = [SELECT Id, Name from Account where Name = : accName or Enterprise_Customer_ID__c =: accName limit 1];
            } catch (exception e) {
                //CommonUtility.msgError('No Account exists with this Name or Enterprise Id: ' + accName);
            }
            if (acc.Id != null) {
                accName = '';
                PageReference page = new PageReference('/apex/SingleSignOnDashboard?id=' + acc.Id);
                //page.setRedirect(true);
                return page;
            } else {
                CommonUtility.msgError('No Account exists with this Name or Enterprise Id: ' + accName);
                accName = '';
            }
            */
        } else {
            CommonUtility.msgError('Please enter an Account Name to view Dashboard');
        }
        return null;
    }

    //JS Remoting action called when searching for specific Account
    @RemoteAction
    global static List < Account > searchAccount(String searchTerm) {
        //System.debug('Movie Name is: '+searchTerm );
        List < Account > movies = Database.query('Select Id, Name from Account where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\' limit 50');
        return movies;
    }

}