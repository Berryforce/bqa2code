public with sharing class BMIQuoteSOQLMethods {
    
    public static list<BigMachines__Quote__c> getPrimaryBMIQuoteByOpportunityID(set<Id> opportunityID) {
        return [Select BigMachines__Process_Sync__c, BigMachines__Opportunity__c, BigMachines__Is_Primary__c, Approval_Triggers__c, Approval_Status__c, BigMachines__Is_Forced_Sync__c, BigMachines__Amount__c, BigMachines__Account__c, Submitted_for_Approval__c,(Select Id From BigMachines__BigMachines_Quote_Products__r) From BigMachines__Quote__c where BigMachines__Is_Primary__c = true and BigMachines__Opportunity__c IN :opportunityID];
    }
    
    public static list<BigMachines__Quote__c> getBMIQuoteByOpportunityID(set<Id> opportunityID) {
        return [Select BigMachines__Process_Sync__c, BigMachines__Opportunity__c, BigMachines__Is_Primary__c, BigMachines__Is_Forced_Sync__c, BigMachines__Amount__c, BigMachines__Account__c, Submitted_for_Approval__c,(Select Id From BigMachines__BigMachines_Quote_Products__r) From BigMachines__Quote__c where BigMachines__Opportunity__c IN :opportunityID and BigMachines__Is_Primary__c = true];
    }
}