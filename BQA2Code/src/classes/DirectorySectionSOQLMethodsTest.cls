@IsTest(seeAllData=True)
public class DirectorySectionSOQLMethodsTest {
    @isTest(seeAllData=True) 
    static void DirectorySectionSOQLMethodsCoverage() {
        User u = TestMethodsUtility.generateStandardUser();
        System.runAs(u) {
            list<Account> lstAccount = new list<Account>();
            lstAccount.add(TestMethodsUtility.generateAccount('telco'));
            lstAccount.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount.add(TestMethodsUtility.generateAccount('publication'));
            insert lstAccount;  
            Account newAccount = new Account();
            Account newPubAccount = new Account();
            Account newTelcoAccount = new Account();
            for(Account iterator : lstAccount) {
                if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                    newAccount = iterator;
                }
                else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                    newPubAccount = iterator;
                }
                else {
                    newTelcoAccount = iterator;
                }
            }
            system.assertNotEquals(newAccount.ID, null);
            system.assertNotEquals(newPubAccount.ID, null);
            system.assertNotEquals(newAccount.Primary_Canvass__c, null);
            system.assertNotEquals(newTelcoAccount.ID, null);
            Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
            objTelco.Telco_Code__c = 'Test';
            update objTelco;
            system.assertNotEquals(newTelcoAccount.ID, null);
            Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
            Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
            Division__c objDiv = TestMethodsUtility.createDivision();
            Directory__c objDir = TestMethodsUtility.generateDirectory();
            objDir.Telco_Provider__c = objTelco.Id;
            objDir.Canvass__c = newAccount.Primary_Canvass__c;        
            objDir.Publication_Company__c = newPubAccount.Id;
            objDir.Division__c = objDiv.Id;
            insert objDir;
            System.assertNotEquals(null, objDir);
            Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
            objDS.Name = 'WPE' + objDS.Name;
            objDS.Section_Page_Type__c = CommonMessages.ypSecPageType;
            update objDS;
            System.assertNotEquals(null, objDS);
            Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
            insert objSHM;
            System.assertNotEquals(null, objSHM);
            set<id> DSIds = new set<id>();
            set<id> DirIds = new set<id>();
            
            DSIds.add(objDS.id);
            DirIds.add(objDir.id);            
            
            System.assertNotEquals(null,DirectorySectionSOQLMethods.getDirectorySectionById(DSIds));
            System.assertNotEquals(null,DirectorySectionSOQLMethods.getDirectorySectionByDirIdWithEAS(DirIds));
            System.assertNotEquals(null,DirectorySectionSOQLMethods.getDirectorySectionByDirIdAndDSId(DSIds, objDir.Id));
            System.assertNotEquals(null,DirectorySectionSOQLMethods.getYPDirectorySectionsByDirIds(DirIds));
            DirectorySectionSOQLMethods.getWPirectorySectionsByIds(DSIds);
            DirectorySectionSOQLMethods.getDirectorySectionsByDirIds(DirIds);
        }
    }
}