global class DFFReminderNotificationSchedulerHndlr implements DFFReminderNotificationScheduler.DFFReminderNotificationSchedulerInterface {
	global void execute(SchedulableContext sc) {
        DFFReminderNotification obj = new DFFReminderNotification();
        Database.executeBatch(obj);
    }
}