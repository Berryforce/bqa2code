global class DFFMultiAddSequenceLogic implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String userID  = '005G0000005kHfk';
        String soql = 'Select Id from Order_Group__c where createdbyId =:userID';
        return Database.getQueryLocator(soql);
    }

    global void execute(Database.BatchableContext bc, List<Order_Group__c> objOSList){
        set<Id> setOS = new set<Id>();
        for(Order_Group__c iterator : objOSList) {
            setOS.add(iterator.Id);
        }
        Id MutiRTId = '012G0000000WYJb';
        Id PrintGraphicRTId = '012G0000000WYJf';
        list<Digital_Product_Requirement__c> objDFFList = [Select Id,Name,Order_URN__c,Sequence__c,OrderLineItemID__r.Order_Group__c,OrderLineItemID__r.Edition_Code__c,RecordtypeId from Digital_Product_Requirement__c 
                                                            where (RecordtypeId=:MutiRTId OR RecordtypeId=:PrintGraphicRTId) and Sequence__c != null and Order_URN__c != null 
                                                            and OrderLineItemID__r.Order_Group__c IN:setOS Order By Order_URN__c,Name];
        if(objDFFList.size() > 0) {
            map<Id, map<string,map<string,list<Digital_Product_Requirement__c>>>> mapUrnDff = new map<Id, map<string,map<string,list<Digital_Product_Requirement__c>>>>();
            list<Digital_Product_Requirement__c> dffSeqLst = new list<Digital_Product_Requirement__c>();
            for(Digital_Product_Requirement__c objDff : objDFFList){
                if(!mapUrnDff.containskey(objDff.OrderLineItemID__r.Order_Group__c)){
                    mapUrnDff.put(objDff.OrderLineItemID__r.Order_Group__c, new map<string,map<string,list<Digital_Product_Requirement__c>>>());
                }
            
                if(!mapUrnDff.get(objDff.OrderLineItemID__r.Order_Group__c).containskey(objDff.Order_URN__c)){
                    mapUrnDff.get(objDff.OrderLineItemID__r.Order_Group__c).put(objDff.Order_URN__c, new map<string,list<Digital_Product_Requirement__c>>());
                }
                if(!mapUrnDff.get(objDff.OrderLineItemID__r.Order_Group__c).get(objDff.Order_URN__c).containskey(objDff.OrderLineItemID__r.Edition_Code__c)){
                    mapUrnDff.get(objDff.OrderLineItemID__r.Order_Group__c).get(objDff.Order_URN__c).put(objDff.OrderLineItemID__r.Edition_Code__c, new list<Digital_Product_Requirement__c>());
                }
                mapUrnDff.get(objDff.OrderLineItemID__r.Order_Group__c).get(objDff.Order_URN__c).get(objDff.OrderLineItemID__r.Edition_Code__c).add(objDff);
            }
            if(mapUrnDff.size()> 0){
                for(Id isOS : mapUrnDff.keyset()){
                    for(string strURN : mapUrnDff.get(isOS).keyset()){
                        Integer Count=1;
                        if(mapUrnDff.get(isOS).get(strURN) != null){
                            for(string dirEid : mapUrnDff.get(isOS).get(strURN).keyset()){
                                if(mapUrnDff.get(isOS).get(strURN).get(dirEid) != null) {
                                    boolean bFlag = false;
                                    if(mapUrnDff.get(isOS).get(strURN).get(dirEid).size() > 1) {
                                        bFlag = true;
                                    }
                                    for(Digital_Product_Requirement__c dffSeq : mapUrnDff.get(isOS).get(strURN).get(dirEid)){
                                        dffSeq.Sequence__c = count;
                                        dffSeq.Is_Multied__c = bFlag;
                                        if(count > 1) {
                                            dffSeq.RecordtypeId = MutiRTId;
                                        }
                                        dffSeqLst.add(dffSeq);
                                        count++;  
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(dffSeqLst.size()> 0){
                update dffSeqLst;
            }
        }
    }

    global void finish(Database.BatchableContext bc){

    }

}