public with sharing class PriceBookEntrySOQLMethods {
    
    
    public static List<PricebookEntry> getPriceBookEntryByProductID(set<String> productIDs) {
        return [Select Product2Id, Product2.product_type__c,   Pricebook2.Description, Pricebook2.IsStandard, Pricebook2.IsActive, Pricebook2.Name, Pricebook2Id, Name, IsActive, Id From PricebookEntry where Product2Id IN :productIDs and Pricebook2.IsActive = true order by Pricebook2.Name asc];
    }
    public static map<ID, PricebookEntry> getProductIdByPriceBookEntryIDs(set<ID> priceBookEntryIDs) {
        return new map<ID, PricebookEntry>([Select Pricebook2Id, Product2.IsBundle__c, Product2.IsActive, Product2.product_type__c,  Product2.Name, Product2Id, Id From PricebookEntry where id IN :priceBookEntryIDs and Product2.IsBundle__c = true]);
    }
    public static List<PricebookEntry> getPriceBookEntryByProductID(set<ID> productIds) {
        return [Select  Product2.product_type__c,  UnitPrice, Product2.IsBundle__c,  Product2.Family, Product2.IsActive, Product2.Description, 
                Product2.ProductCode, Product2.Name, Product2Id, Pricebook2.Description, Pricebook2.IsStandard, Pricebook2.IsActive, 
                Pricebook2.Name, Pricebook2Id, Name, IsActive, Id From PricebookEntry 
                where Product2.IsActive = true and Product2Id NOT IN :productIds and Product2.IsBundle__c = false order by Product2.createddate desc];
    }
    
    public static list<PricebookEntry> getPriceBookEntryByProductIDandIsActive(set<Id> productId) {
        return [Select UnitPrice,  Product2.product_type__c,  Product2.IsBundle__c,  Product2.Family, Product2.IsActive, Product2.Description, Product2.ProductCode, Product2.Name, Product2.Id, Product2Id, IsActive, Id From PricebookEntry Where Product2Id IN :productID and Product2.IsActive = true];
    }
    
    public static list<PricebookEntry> getPriceBookEntryByUDAC(set<String> setUDAC) {
        return [Select ProductCode, Product2Id, Pricebook2Id, IsActive, Id From PricebookEntry where ProductCode IN :setUDAC and IsActive = true];
    }
}