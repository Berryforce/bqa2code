global class UnlockNationaLockOrdersBatch Implements Database.Batchable <sObject> {
  global Database.queryLocator start(Database.BatchableContext bc){
    String SOQL = 'SELECT Id, Is_Locked__c FROM Locked_National_Order__c WHERE Is_Locked__c = true';
    return Database.getQueryLocator(SOQL);
  } 
 
  global void execute(Database.BatchableContext bc, list<Locked_National_Order__c> listLockedNO) {
    for(Locked_National_Order__c LNO : listLockedNO) {
      LNO.Is_Locked__c = false;
    }   
    update listLockedNO;
  }
   
  global void finish(Database.BatchableContext bc) {
  }
}