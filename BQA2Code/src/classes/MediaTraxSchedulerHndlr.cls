global class MediaTraxSchedulerHndlr implements MediaTraxScheduler.MediaTraxSchedulerInterface {
    global void execute(SchedulableContext sc) {
        Batch_Size__c objBatchSize = Batch_Size__c.getInstance('MediaTraxHeading');
        MediaTraxServiceForGetAddHeadingBatch objCallout = new MediaTraxServiceForGetAddHeadingBatch(true);
        database.executebatch(objCallout, Integer.valueOf(objBatchSize.Size__c));
    } 
}