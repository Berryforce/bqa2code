@IsTest
public class SetCommissionReadyBatchControllerTest {
    static testMethod void SetCommissionReadyTest() {
        //Database.BatchableContext bc;       
        Account objAccount = new Account();
        objAccount.Name = 'Test Account 1';
        objAccount.Phone = '(456)789-3456';
        objAccount.Open_claim__c = false;
       
        objAccount.Delinquency_Indicator__c= false;
        objAccount.Letter_Renewal_Sequence__c = '1';
        insert objAccount;
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Account2';
        objAccount1.Phone = '(456)789-3478';
        objAccount1.Open_claim__c = false;

        objAccount1.Delinquency_Indicator__c=false;
        objAccount1.Letter_Renewal_Sequence__c = '1';
        insert objAccount1;
        
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = objAccount.Id;
        insert objContact;
        
        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = objAccount.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=objAccount.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        insert objOpportunity;
        
        Directory__c objDir = TestMethodsUtility.createDirectory();
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c =Date.today().addMonths(2);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE2';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS';
        objDirE1.Pub_Date__c =Date.today().addMonths(3);
        insert objDirE1;    
        
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print Graphic';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Family='Print';                
        insert  objProd;
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Digital';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        
        insert objProd1;
        
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder ;
    
        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=objAccount.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id);
        insert objOrderGroup ;
    
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup.id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id);
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup.id,Order__c=objOrder .id,Product2__c=objProd1.Id,is_p4p__c=true,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id);
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Account__c=objAccount1.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup.id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id);
        oliLst.add(objOrderLineItem);
        oliLst.add(objOrderLineItem1);
        oliLst.add(objOrderLineItem2);
        insert oliLst;
        Digital_Product_Requirement__c dff=TestMethodsUtility.generateDataFulfillmentForm();
        dff.OrderLineItemID__c=oliLst[0].id;
        dff.Miles_Status__c='Change';
        dff.status__c = 'Complete';
        insert dff;
        Digital_Product_Requirement__c dff1=TestMethodsUtility.generateDataFulfillmentForm();
        dff1.OrderLineItemID__c=oliLst[1].id;
        dff1.Fulfillment_Submit_Status__c='Complete';
        dff1.status__c = 'Complete';
        insert dff1;              
       Test.startTest(); 
       SetCommissionReadyBatchController sh1 = new SetCommissionReadyBatchController();
       Database.executeBatch(sh1);
       Test.stopTest();
    }
    
    static testMethod void SetCommissionReadyTest1() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account 1';
        objAccount.Phone = '(456)789-3456';
        objAccount.Open_claim__c = false;
       
        objAccount.Delinquency_Indicator__c= false;
        objAccount.Letter_Renewal_Sequence__c = '1';
        insert objAccount;
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Account2';
        objAccount1.Phone = '(456)789-3478';
        objAccount1.Open_claim__c = false;

        objAccount1.Delinquency_Indicator__c=false;
        objAccount1.Letter_Renewal_Sequence__c = '1';
        insert objAccount1;
        
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = objAccount.Id;
        insert objContact;
        
        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = objAccount.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=objAccount.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        insert objOpportunity;        
        
        Directory__c objDir = TestMethodsUtility.createDirectory();
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c =Date.today().addMonths(4);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE2';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS';
        objDirE1.Pub_Date__c =Date.today().addMonths(5);
        insert objDirE1;           
        
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print Graphic';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Family='Print';  
        insert  objProd;
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Digital';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        objProd.Family='SEM-Talus';
        insert objProd1;
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder ;
    
        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=objAccount.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id);
        insert objOrderGroup ;
    
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id);
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd1.Id,is_p4p__c=true,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id);
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Account__c=objAccount1.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id);
        oliLst.add(objOrderLineItem);
        oliLst.add(objOrderLineItem1);
        oliLst.add(objOrderLineItem2);
        insert oliLst;
        Digital_Product_Requirement__c dff=TestMethodsUtility.generateDataFulfillmentForm();
        dff.OrderLineItemID__c=oliLst[0].id;
        dff.status__c = 'Complete';
        insert dff;
        Digital_Product_Requirement__c dff1=TestMethodsUtility.generateDataFulfillmentForm();
        dff1.OrderLineItemID__c=oliLst[1].id;
        //dff1.status__c = 'Complete';
        dff1.Fulfillment_Submit_Status__c=null;
        insert dff1; 
        Digital_Product_Requirement__c dff2=TestMethodsUtility.generateDataFulfillmentForm();
        dff2.OrderLineItemID__c=oliLst[2].id;
        //dff2.status__c = 'Complete';
        dff2.Fulfillment_Submit_Status__c=null;
        insert dff2;                     
       Test.startTest(); 
       String sch = '0 0 23 * * ?'; 
       system.schedule('TestSetCommissionReadyBatchScheduler', sch, new SetCommissionReadyBatchScheduler());
       Test.stopTest();              
    } 
    
    static testMethod void SetCommissionReadyTest2() {
        //Database.BatchableContext bc;       
        Account objAccount = new Account();
        objAccount.Name = 'Test Account 1';
        objAccount.Phone = '(456)789-3456';
        objAccount.Open_claim__c = false;
       
        objAccount.Delinquency_Indicator__c= false;
        objAccount.Letter_Renewal_Sequence__c = '1';
        insert objAccount;
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Account2';
        objAccount1.Phone = '(456)789-3478';
        objAccount1.Open_claim__c = false;

        objAccount1.Delinquency_Indicator__c=false;
        objAccount1.Letter_Renewal_Sequence__c = '1';
        insert objAccount1;
        
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = objAccount.Id;
        insert objContact;
        
        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = objAccount.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=objAccount.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        insert objOpportunity;
        
        Directory__c objDir = TestMethodsUtility.createDirectory();
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c =Date.today().addMonths(6);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE2';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS';
        objDirE1.Pub_Date__c =Date.today().addMonths(7);
        insert objDirE1;          
        
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Family='SEM-Talus'; 
        insert  objProd;
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Digital';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        objProd.Family='SEM-Talus';
        insert objProd1;
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder ;    
        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=objAccount.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id);
        insert objOrderGroup ;
    
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id);
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd1.Id,is_p4p__c=true,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id);
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Account__c=objAccount1.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id);
        oliLst.add(objOrderLineItem);
        oliLst.add(objOrderLineItem1);
        oliLst.add(objOrderLineItem2);
        insert oliLst;
        Digital_Product_Requirement__c dff=TestMethodsUtility.generateDataFulfillmentForm();
        dff.OrderLineItemID__c=oliLst[0].id;
        dff.status__c = 'Complete';
        insert dff;
        Digital_Product_Requirement__c dff1=TestMethodsUtility.generateDataFulfillmentForm();
        dff1.OrderLineItemID__c=oliLst[1].id;
        dff1.status__c = 'Complete';
        insert dff1; 
        Digital_Product_Requirement__c dff2=TestMethodsUtility.generateDataFulfillmentForm();
        dff2.OrderLineItemID__c=oliLst[2].id;
        dff2.status__c = 'Complete';
        insert dff2;                     
       Test.startTest(); 
       SetCommissionReadyBatchController sh1 = new SetCommissionReadyBatchController();
       Database.executeBatch(sh1);
       Test.stopTest();
    }          
}