global class TBCscUnSupressionBatchScheduler implements Schedulable {
	public Interface TBCscUnSupressionBatchSchedulerInterface {
		void execute(SchedulableContext sc);
	}
	global void execute(SchedulableContext sc) {
		Type targetType = Type.forName('TBChUnSuppressionbatchHandler');
        if(targetType != null) {
            TBCscUnSupressionBatchSchedulerInterface obj = (TBCscUnSupressionBatchSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
	}
}