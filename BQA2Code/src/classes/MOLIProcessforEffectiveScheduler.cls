global class MOLIProcessforEffectiveScheduler implements Schedulable {
   public Interface MOLIProcessforEffectiveSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('MOLIProcessforEffectiveSchedulerHndlr');
        if(targetType != null) {
            MOLIProcessforEffectiveSchedulerInterface obj = (MOLIProcessforEffectiveSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}