global class SensitiveHeadingUpdateBatchScheduleHndlr implements SensitiveHeadingUpdateBatchSchedule.SensitiveHeadingUpdateBatchScheduleInterface {
      global void execute(SchedulableContext sc) {
        SensitiveHeadingUpdateBatch obj = new SensitiveHeadingUpdateBatch();
        ID batchprocessid = database.executebatch(obj, 5);
    }  
}