public class ApplyCityforListing{
    public string listId;
    public Listing__c listing {get;set;}
    public String listingCity;
    public String manualAbbreviate {get;set;}
    public list<WrapobjDP> wrapobjDPLst {get;set;}
    public List<Directory_Pagination__c> SelectedDPForUpdateLst{get;set;}
    
    public ApplyCityforListing(ApexPages.StandardController controller){
        listId= Apexpages.currentPage().getParameters().get('Id');
        //listId=System.currentPageReference().getParameters().get('id');
        system.debug('**********Listing Id***********'+listId);
        listing = new Listing__c();
        listing = [Select Id,Name,Phone__c,Listing_State__c,Listing_Street__c,Listing_PO_Box__c,Listing_City__c from Listing__c where Id=:listId limit 1];
        listingCity = listing.Listing_City__c;
        wrapobjDPLst = new list<WrapobjDP> ();
       
        listingDPLst();
       
    }
    
    public list<WrapobjDP> listingDPLst(){
         List<Directory_Pagination__c > objDPLst = new List<Directory_Pagination__c>();
         objDPLst = [Select Id,Directory_Section__c,Directory_Section__r.Name,Directory_Heading__c,UDAC__c,Caption_Header__c,Caption_Member__c,
                     Caption_Display_Text__c,Listing_City__c,City_Name_Section_Abbreviation__c,City_Name_Section_Suppressed__c,Order_Line_Item__r.Listing__c,
                     Reference_Listing__r.Listing__c,Manual_City_Display_Rule__c from Directory_Pagination__c where (Reference_Listing__r.Listing__c =:listId or Order_Line_Item__r.Listing__c=:listId)];
         
         if(objDPLst.size()>0){    
             for(Directory_Pagination__c objDirPg : objDPLst){
                 WrapobjDP objWrDP = new WrapobjDP();
                 objWrDP.objDP = objDirPg;
                 wrapobjDPLst.add(objWrDP);    
             }
         }
         system.debug('************WrapDirectoryPagination List*********'+wrapobjDPLst);
         return wrapobjDPLst; 
        
    }
   
    public pagereference ApplyRule(){
     system.debug('******Manual Entered Text**********'+manualAbbreviate);
        SelectedDPForUpdateLst = new list<Directory_Pagination__c>();
        system.debug('******Manual Entered Text**********'+manualAbbreviate);
        for(WrapobjDP Wrdp : wrapobjDPLst){
            if(Wrdp.ischecked){
            system.debug('***********Enter here checked************');
                if(Wrdp.objDP.Manual_City_Display_Rule__c =='Abbreviate'){
                    Wrdp.objDP.City_Name_Abbrv_Override__c = Wrdp.manualAbbreviate;
                    Wrdp.objDP.City_Name_Suppress_Override__c = false;     
                }
                if(Wrdp.objDP.Manual_City_Display_Rule__c =='Suppress'){
                    Wrdp.objDP.City_Name_Suppress_Override__c = true;
                    Wrdp.objDP.City_Name_Abbrv_Override__c = '';     
                }
                if(Wrdp.objDP.Manual_City_Display_Rule__c =='Print In Full'){
                    Wrdp.objDP.City_Name_Abbrv_Override__c = listingCity;
                    Wrdp.objDP.City_Name_Suppress_Override__c = false;     
                }
                if(Wrdp.objDP.Manual_City_Display_Rule__c =='Clear Config'){
                    Wrdp.objDP.City_Name_Abbrv_Override__c = '';
                    Wrdp.objDP.City_Name_Suppress_Override__c = false;     
                }
                SelectedDPForUpdateLst.add(Wrdp.objDP);
            }
        }
        system.debug('************WrapDirectoryPagination updateList*********'+SelectedDPForUpdateLst);
        if(SelectedDPForUpdateLst.size()>0){
            update SelectedDPForUpdateLst;
        }
        return(new pagereference('/apex/ApplyCityforListing?id='+listId).setredirect(true)) ;
        //return null;
    }
    public PageReference redirect()
    {
      PageReference listPage = new ApexPages.StandardController(listing).view();
      listPage.setRedirect(true); 
      return listPage;
    }
    public class WrapobjDP{
        public boolean ischecked{get;set;}
        public String manualAbbreviate {get;set;}
        public Directory_Pagination__c objDP{get;set;}
    }
}