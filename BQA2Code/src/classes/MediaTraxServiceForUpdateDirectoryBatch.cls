global class MediaTraxServiceForUpdateDirectoryBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    
    global String SOQL = '';
    global Boolean bolChainBatch = false;
    
    global MediaTraxServiceForUpdateDirectoryBatch(){
    }
    
    global MediaTraxServiceForUpdateDirectoryBatch(Boolean bolChainBtc){
        bolChainBatch = bolChainBtc;
    }
    global Database.QueryLocator start(Database.BatchableContext bc){
        SOQL = 'Select Is_Changed__c, Id, Directory_Code__c, Name, Media_Trax_Publisher_ID__c, Media_Trax_Directory_ID__c, Telco_Provider_Code__c from Directory__c where Directory_Code__c != null and Media_Trax_Publisher_ID__c != null and Telco_Provider_Code__c != null and Media_Trax_Directory_ID__c != null and Is_Changed__c = true';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, list<Directory__c> lstDirectory){
        
        MediaTraxHeadingCalloutController_V1 objMediaTraxService = new MediaTraxHeadingCalloutController_V1();
        objMediaTraxService.ProcessMediaTraxForDirectoryUpdateScope(lstDirectory);
    }
    
    global void finish(Database.BatchableContext bc){
        Batch_Size__c objBatchSize = Batch_Size__c.getInstance('MediaTraxClient');
        MediaTraxServiceForGetAddClientBatch objCallout = new MediaTraxServiceForGetAddClientBatch(true);
        database.executebatch(objCallout, Integer.valueOf(objBatchSize.Size__c));
    }
}