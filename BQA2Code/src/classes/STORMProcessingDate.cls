global class STORMProcessingDate implements Schedulable {
	global void execute(SchedulableContext sc) {
		list<STORMProcessing__c> processDate = new list<STORMProcessing__c>();
		processDate = [SELECT Id, Processing_Date__c FROM STORMProcessing__c];
		for(STORMProcessing__c sp:processDate)
			{
			sp.Processing_Date__c = Date.today();
			}
		update processDate;
	}
}