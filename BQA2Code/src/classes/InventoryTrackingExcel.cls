public with sharing class InventoryTrackingExcel {
    String Cid;
    String dirCode;
    public Canvass__c Canvass {get;set;}
    public String newLine {get;set;}
    public List<Directory_Product_Mapping__c> DPMList {get;set;}
    
    public InventoryTrackingExcel() {
        newLine = '\n';
        DPMList = new List<Directory_Product_Mapping__c>();
        Cid = ApexPages.currentPage().getParameters().get('Cid');
        dirCode = ApexPages.currentPage().getParameters().get('dirCode');
        Canvass = [SELECT Name FROM Canvass__c WHERE Id = : Cid];
        DPMList = DirectoryProductMappingSOQLMethods.getDirProdMapByDirCode(dirCode);
    }
}