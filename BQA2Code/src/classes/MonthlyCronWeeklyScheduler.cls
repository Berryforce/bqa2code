global class MonthlyCronWeeklyScheduler implements Schedulable {
   public Interface MonthlyCronWeeklySchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('MonthlyCronWeeklySchedulerHndlr');
        if(targetType != null) {
            MonthlyCronWeeklySchedulerInterface obj = (MonthlyCronWeeklySchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}