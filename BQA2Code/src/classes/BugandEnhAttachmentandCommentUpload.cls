public with sharing class BugandEnhAttachmentandCommentUpload {
 
  public Case cs{get;set;}
  
   public BugandEnhAttachmentandCommentUpload(ApexPages.StandardController stdController)
     {
        this.cs = ( Case ) stdController.getRecord();
        cs = [SELECT Id FROM Case WHERE Id = :cs.Id];
     }
  
  public Attachment attachment {
  get {
      if (attachment == null)
        attachment = new Attachment();
      return attachment;
    }
  set;
  }
 
  public PageReference upload() {
 
    attachment.OwnerId = UserInfo.getUserId();
    attachment.ParentId = cs.Id; // the record the file is attached to
    attachment.IsPrivate = false;
 
    try {
      insert attachment;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      attachment = new Attachment(); 
    }
 
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
  }
  
  public CaseComment cComment {
  get {
      if (cComment == null)
        cComment = new CaseComment();
      return cComment;
    }
  set;
  }
 
  public PageReference upload2() {
 
    cComment.ParentId = cs.Id; // the record the file is attached to
 
    try {
      insert cComment;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating Case Comment'));
      return null;
    } finally {
      cComment = new CaseComment(); 
    }
 
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Case Comment uploaded successfully'));
    return null;
  }
 
}