global class MediaTraxScheduler implements Schedulable {
   public Interface MediaTraxSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('MediaTraxSchedulerHndlr');
        if(targetType != null) {
            MediaTraxSchedulerInterface obj = (MediaTraxSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}