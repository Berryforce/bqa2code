global class NationalOrderAutomationSchedulerHndlr implements NationalOrderAutomationScheduler.NationalOrderAutomationSchedulerInterface {
	global void execute(SchedulableContext sc) {
        NationalOrderAutomationProcessBatch obj = new NationalOrderAutomationProcessBatch();
        Database.executeBatch(obj);
    }
}