global class DataLoadBatch implements Database.Batchable<sObject>{
    global DataLoadBatch(){
    }
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        String SOQL = 'select Caption_Print_Phone_Number__c,Caption_Print_Address__c,id,DFF_Product__r.Print_Specialty_Product_Type__c from Digital_Product_Requirement__c';
        
        //String SOQL = 'SELECT id FROM Listing__c';
        
        //String SOQL = 'SELECT id, DP_Caption_Member_F__c,DP_Caption_Header_F__c,DP_Continuous_Service_Order_Appearance_F__c,DP_Is_Cross_Reference_F__c,DP_Trademark_Finding_Line_F__c,DP_IBUN_Type_F__c,DP_IBUN_Website_One_F__c,DP_IBUN_Website_Two_F__c,DP_Internet_Bundle_Ad_F__c,DP_Section_Code_F__c,DP_Directory_Code_F__c,DP_Banner_F__c,DP_Billboard_F__c,DP_Year_F__c,DP_Directory_Edition_Code_F__c,DP_Directory_Edition_Name_F__c,DP_Section_Type_F__c,DP_Type_F__c,DP_Scoped_Caption_Header_F__c,DP_DataLoad__c, Order_Line_Item__r.Digital_Product_Requirement__c FROM Directory_Pagination__c where DP_DataLoad__c = false limit 1000000';
        
        //String SOQL = 'select id, Canvass__r.Primary_Telco__c from Modification_Order_Line_Item__c where Canvass__r.Primary_Telco__c != null and Telco__c = null and completed__c = false and inactive__c = false';
        
        return Database.getQueryLocator(SOQL);
    }
    global void execute(Database.BatchableContext bc, List<Digital_Product_Requirement__c> scope){
    
        //delete scope;
    
        List<Digital_Product_Requirement__c> lstUpdt = new List<Digital_Product_Requirement__c>();
        for(Digital_Product_Requirement__c objOLI : scope) {
            //Directory_Pagination__c objDFFNew = new Directory_Pagination__c(id = objDP.id);
            //lstUpdt.add(new Directory_Pagination__c(id = objDP.id, Scoped_Caption_Header__c = objDP.DP_Scoped_Caption_Header_F__c, Caption_Member__c = objDP.DP_Caption_Member_F__c, Caption_Header__c = objDP.DP_Caption_Header_F__c, Continuous_Service_Order_Appearance__c = objDP.DP_Continuous_Service_Order_Appearance_F__c, Is_Cross_Reference__c = objDP.DP_Is_Cross_Reference_F__c, Trademark_Finding_Line__c = objDP.DP_Trademark_Finding_Line_F__c, IBUN_Type__c = objDP.DP_IBUN_Type_F__c, IBUN_Website_One__c = objDP.DP_IBUN_Website_One_F__c, IBUN_Website_Two__c = objDP.DP_IBUN_Website_Two_F__c, Internet_Bundle_Ad__c = objDP.DP_Internet_Bundle_Ad_F__c, Section_Code__c = objDP.DP_Section_Code_F__c, Directory_Code__c = objDP.DP_Directory_Code_F__c, Banner__c = objDP.DP_Banner_F__c, Billboard__c = objDP.DP_Billboard_F__c, Year__c = objDP.DP_Year_F__c, Directory_Edition_Code__c = objDP.DP_Directory_Edition_Code_F__c, Directory_Edition_Name__c = objDP.DP_Directory_Edition_Name_F__c, Section_Type__c = objDP.DP_Section_Type_F__c, Type__c = objDP.DP_Type_F__c, DP_DataLoad__c = true, Digital_Product_Requirement__c = objDP.Order_Line_Item__r.Digital_Product_Requirement__c));
            //lstUpdt.add(new Modification_Order_Line_Item__c(id = objOLI.id, Telco__c = objOLI.Canvass__r.Primary_Telco__c));
            if(objOLI.DFF_Product__r.Print_Specialty_Product_Type__c == 'Extra Line Type Product' || objOLI.DFF_Product__r.Print_Specialty_Product_Type__c == 'Cross Reference Listing') {
                if(objOLI.Caption_Print_Phone_Number__c == true || objOLI.Caption_Print_Address__c == true) {
                    objOLI.Caption_Print_Phone_Number__c = false;
                    objOLI.Caption_Print_Address__c = false;
                    lstUpdt.add(objOLI);
                }
            }
            else {
                if(objOLI.DFF_Product__r.Print_Specialty_Product_Type__c == 'Alternate Call Type Product') {
                    if(objOLI.Caption_Print_Address__c == true || objOLI.Caption_Print_Phone_Number__c == false) {
                        objOLI.Caption_Print_Address__c = false;
                        objOLI.Caption_Print_Phone_Number__c = true;
                        lstUpdt.add(objOLI);
                    }
                }
                else if(objOLI.Caption_Print_Address__c == false || objOLI.Caption_Print_Phone_Number__c == false) {
                    objOLI.Caption_Print_Address__c = true;
                    objOLI.Caption_Print_Phone_Number__c = true;
                    lstUpdt.add(objOLI);
                }
            }
            
        }
        if(lstUpdt.size() > 0) {
            update lstUpdt;
        }        
    }
    
    global void finish(Database.BatchableContext bc){
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com'};
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Batch Process Status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures.');
    }

}