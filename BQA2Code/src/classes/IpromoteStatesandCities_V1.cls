public with sharing class IpromoteStatesandCities_V1 {
	public list<I_Promote_Audience_Targeting__c> lstIP {get;set;}
	public list<I_Promote_Audience_Targeting__c> lstIPSelected {get;set;}
	private set<String> setSelectedValues;
	private set<String> setSelectedIds;
	public list<SelectOption> options{get;set;}
	
	private String strIpId;
	public boolean errorFlag {get;set;}
	public string userInputSearch {get;set;}
	public String selectedState{get;set;}
	public Digital_Product_Requirement__c objDFF {get;set;}
		
	public Integer OffsetSize = 0;
    private Integer QueryLimit = 5;
    public Integer CountTotalRecords{get;set;}
	
	public IpromoteStatesandCities_V1() {
		strIpId = Apexpages.currentPage().getParameters().get('dffid');
		lstIP = new list<I_Promote_Audience_Targeting__c>();
		lstIPSelected = new list<I_Promote_Audience_Targeting__c>();
		objDFF = new Digital_Product_Requirement__c();
		setSelectedIds = new set<String>();
		CountTotalRecords = 0;
		errorFlag = true;
	}
	
	public Pagereference onLoad() {
		objDFF = getDFFByID();
		setSelectedValues = new set<String>();
		if(objDFF != null) {
			if(String.isNotBlank(objDFF.audience_scope__c) && String.isNotBlank(objDFF.audience_targets__c)) {
				system.debug(objDFF.audience_targets__c.split(';'));
				setSelectedValues.addall(objDFF.audience_targets__c.split(';'));
				system.debug(setSelectedValues);
				getIpromoteStateByScope();
			}
			else if(String.isNotBlank(objDFF.audience_scope__c)) {
				getIpromoteStateByScope();
			}
		}
		
		if(setSelectedValues.size() > 0) {
			lstIP = getIPromoteTargeting('notin');
			lstIPSelected = getIPromoteTargeting('in');
			for(I_Promote_Audience_Targeting__c iterator : lstIPSelected) {
				setSelectedIds.add(iterator.Id);
			}
		}
		return null;
	}
	
	private Digital_Product_Requirement__c getDFFByID() {
		Digital_Product_Requirement__c objDFF = new Digital_Product_Requirement__c();
		for(Digital_Product_Requirement__c iterator : [select Id,audience_targets__c, audience_scope__c from Digital_Product_Requirement__c where id =: strIpId limit 1]) {
			objDFF = iterator;
		}
		return objDFF;
	}
	
	private list<I_Promote_Audience_Targeting__c> getIPromoteTargeting(String strQuery) {
		String soqlquery='Select Is_Checked__c, Scope__c, State__c, External_Name__c, Targeting_Name__c, Name, Id from I_Promote_Audience_Targeting__c';
        set<String> setState = new set<String>{objDFF.audience_scope__c};
        if(strQuery.equalsIgnoreCase('in')) {
            soqlquery = soqlquery + ' where External_Name__c IN:setSelectedValues and Scope__c IN: setState order by Targeting_Name__c';
        }
        else if(strQuery.equalsIgnoreCase('notin')) {
        	soqlquery = soqlquery + ' where External_Name__c NOT IN:setSelectedValues and Scope__c IN: setState order by Targeting_Name__c limit '+QueryLimit+' offset '+ OffsetSize;        	
        }
        system.debug(soqlquery);
        list<I_Promote_Audience_Targeting__c> lstTem = database.query(soqlquery);
        return lstTem;
	}
	
	public void getIpromoteStateByScope() {
		options = new list<SelectOption>();
		set<String> setStatesTemp = new set<String>();
		list<I_Promote_Audience_Targeting__c> lstIPTemp = [select id, State__c from I_Promote_Audience_Targeting__c where state__c != null and Scope__c =:objDFF.audience_scope__c];
		CountTotalRecords = lstIPTemp.size();
		for(I_Promote_Audience_Targeting__c objI : lstIPTemp) {
			system.debug('**********State data **********'+objI.State__c);
			if(!setStatesTemp.contains(objI.State__c)) {
				options.add(new selectOption(objI.state__c, objI.state__c));
				setStatesTemp.add(objI.state__c);
			}
		}
    }
    
    public void changeAudienceScope() {
    	getIpromoteStateByScope();
    	selectedState = 'none';
    	userInputSearch = '';
    	OffsetSize = 0;
    	QueryLimit = 5;	
    	getIpromoteByState();
    	lstIPSelected = new list<I_Promote_Audience_Targeting__c>();
    	setSelectedIds = new set<String>();
    }
    
    private void totalRecordCount() {
    	String soqlqueryCount ='Select Id from I_Promote_Audience_Targeting__c';
    	String whereCond = '';
    	set<String> setScope = new set<String>{objDFF.audience_scope__c};
    	String cityInput = '\'' + userInputSearch + '%\'';
		set<String> setState = new set<String>{selectedState};
    	if(selectedState.equals('none') && String.isBlank(userInputSearch)) {
    		whereCond += ' where Scope__c IN:setScope';    		 
    	}
    	else if(!selectedState.equals('none') && String.isNotBlank(userInputSearch)) {
    		whereCond += ' where State__c IN:setState and Scope__c IN:setScope and Targeting_Name__c like '+cityInput;
    	}
    	else if(!selectedState.equals('none') && String.isBlank(userInputSearch)) {
    		whereCond += ' where State__c IN:setState and Scope__c IN:setScope';
    	}
    	else if(selectedState.equals('none') && String.isNotBlank(userInputSearch)) {
    		whereCond += ' where Scope__c IN:setScope and Targeting_Name__c like '+cityInput;
    	}
    	
    	if(setSelectedIds.size() > 0) {
    		whereCond += ' and Id NOT IN:setSelectedIds';
    	}
    	
    	CountTotalRecords = database.query(soqlqueryCount +''+ whereCond).size();
    }
    
    public void searchData() {
    	OffsetSize = 0;
    	QueryLimit = 5;
    	totalRecordCount();
    	getIpromoteByState();
    }
    
    public void getIpromoteByState() {
    	String soqlquery ='Select Is_Checked__c, Scope__c, State__c, External_Name__c, Targeting_Name__c, Name, Id from I_Promote_Audience_Targeting__c';
    	String whereCond = '';
    	set<String> setScope = new set<String>{objDFF.audience_scope__c};
    	String cityInput = '\'' + userInputSearch + '%\'';
		set<String> setState = new set<String>{selectedState};
    	if(selectedState.equals('none') && String.isBlank(userInputSearch)) {
    		whereCond += ' where Scope__c IN:setScope';    		 
    	}
    	else if(!selectedState.equals('none') && String.isNotBlank(userInputSearch)) {
    		whereCond += ' where State__c IN:setState and Scope__c IN:setScope and Targeting_Name__c like '+cityInput;
    	}
    	else if(!selectedState.equals('none') && String.isBlank(userInputSearch)) {
    		whereCond += ' where State__c IN:setState and Scope__c IN:setScope';
    	}
    	else if(selectedState.equals('none') && String.isNotBlank(userInputSearch)) {
    		whereCond += ' where Scope__c IN:setScope and Targeting_Name__c like '+cityInput;
    	}
    	
    	if(setSelectedIds.size() > 0) {
    		whereCond += ' and Id NOT IN:setSelectedIds';
    	}
    	
    	whereCond += ' order by Targeting_Name__c limit '+QueryLimit+' offset '+OffsetSize;
    	
    	lstIP = database.query(soqlquery + whereCond);
    }
    
    public PageReference addValues() {
    	if(lstIP.size() > 0) {
    		boolean bFlag = false;
    		for(I_Promote_Audience_Targeting__c iterator : lstIP) {
    			if(iterator.Is_Checked__c) {
    				lstIPSelected.add(iterator);
    				setSelectedIds.add(iterator.Id);
    				bFlag = true;
    			}
    		}
    		if(bFlag) {
    			totalRecordCount();
    			getIpromoteByState();    			
    		}
    		else {
    			CommonMethods.addError('Please selected atleast one record.');
    		}
    	}
        return null;
    }
    
    public Pagereference removeSelected() {
    	String strID = ApexPages.currentPage().getParameters().get('delId');
    	system.debug('Delete Id : ' +strID);
    	setSelectedIds.remove(strID);
    	totalRecordCount();
    	getIpromoteByState();
    	if(setSelectedIds.size() > 0) {
	    	String soqlquery='Select Is_Checked__c, Scope__c, State__c, External_Name__c, Targeting_Name__c, Name, Id from I_Promote_Audience_Targeting__c where Scope__c IN:setScope ';
	    	set<String> setScope = new set<String>{objDFF.audience_scope__c};
	    	
	    	if(setSelectedIds.size() > 0) {
	    		soqlquery += ' and Id IN:setSelectedIds';
	    	}
	    	lstIPSelected = database.query(soqlquery);
    	}
    	else {
    		lstIPSelected = new list<I_Promote_Audience_Targeting__c>();
    	}
    	return null;
    }
    
    public Pagereference finish() {
    	errorFlag = true;
    	if(lstIPSelected.size() > 0) {
    		String strTargeting = '';
    		for(I_Promote_Audience_Targeting__c iterator : lstIPSelected) {
    			//strTargeting += (String.isBlank(strTargeting) ? iterator.External_Name__c : iterator.External_Name__c +'; ');
    			strTargeting += iterator.External_Name__c + '; ';
    		}
    		objDFF.audience_targets__c = strTargeting.subString(0, strTargeting.length()-2);
    		update objDFF;
    		CommonMethods.addINFO('Record updated successfully.');
    	}
    	else {
    		errorFlag = false;
    		CommonMethods.addError('Please selected atleast one record to proceed.');
    	}
    	return null;
    }
    
    public Boolean getDisableAddButton(){
        if(lstIP.size() > 0){
            return false;
        }
        else return true;
    }
    
    public Boolean getDisablePrevious(){
        if(OffsetSize>0){
            return false;
        }
        else return true;
    }
 
    public Boolean getDisableNext() {
        if (OffsetSize + QueryLimit < countTotalRecords){
            return false;
        }
        else return true;
    }
 
    public PageReference Next() {
        OffsetSize += QueryLimit;
        getIpromoteByState();
        return null;
    }
 
    public PageReference Previous() {
        OffsetSize -= QueryLimit;
        getIpromoteByState();
        return null;
    }
}