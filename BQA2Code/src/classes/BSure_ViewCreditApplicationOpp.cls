global class BSure_ViewCreditApplicationOpp {
    public String caId{get;set;}
    public boolean visibilityEquifaxbtn{get;set;}
    public list<BsureCreditApplication__c> lstcamainQ{get;set;}
    public  BSure_ViewCreditApplicationOpp(ApexPages.StandardController controller)
    {
        caId = Apexpages.currentPage().getParameters().get('Id');
        visibilityEquifaxbtn = false;
        if(caId != null && caId !='')
        {
            String strstatus ='Completed';
            String strcaQuery = 'Select Id,Status__c,BCS__c,PCS__c from BsureCreditApplication__c where Id=\''+caId+'\'' + ' AND Status__c = \''+strstatus+'\'';
            lstcamainQ = Database.query(strcaQuery);
        }
        if(lstcamainQ != null && lstcamainQ.size() > 0)
        {
            if(lstcamainQ.get(0).Status__c == 'Completed' && (lstcamainQ.get(0).BCS__c == null ||  lstcamainQ.get(0).BCS__c == '') && (lstcamainQ.get(0).PCS__c == null ||  lstcamainQ.get(0).PCS__c == ''))
            {
                visibilityEquifaxbtn = true;
            }
        }   
    }
    public BsureCreditApplication__c getCreditApplicationOppDetails()
    {
        BsureCreditApplication__c caobj;
        if(caId != null && caId != '')
        {
            caobj = [Select id,name from BsureCreditApplication__c where Id =: caId]; 
        }
        return caobj;
    }
     webservice Static String validatePermissionCA(String strUserId)
     {
        String strreturn = 'false';
        strreturn = AdminUserInfo();
        
        String strPermsetname = getConfigurationValues('CACreateAccess').get(0);
        System.debug('strPermsetname=========='+strPermsetname);
        String strQry = 'SELECT AssigneeId,Id,PermissionSetId FROM PermissionSetAssignment where AssigneeId =\''+string.valueOf(strUserId)+'\'';
        list<PermissionSetAssignment> lstPermissionset = new list<PermissionSetAssignment>();
        lstPermissionset = Database.Query(strQry);
        map<String,String> maplist = new map<String,String>();
        system.debug('lstPermissionset============'+lstPermissionset);
        if(lstPermissionset != null && lstPermissionset.size() > 0)
        {
            for(PermissionSetAssignment pobj : lstPermissionset){
                String strpermmsid = pobj.PermissionSetId;
                maplist.put(strpermmsid.substring(0,15),strpermmsid.substring(0,15));
            }   
        }
        if(maplist != null && maplist.size() > 0)
        {
            if(strPermsetname != null)
            {
                if(strPermsetname.contains(','))
                {
                    list<String> lstdata = strPermsetname.split(',');
                    for(String s: lstdata){
                        if(maplist.containskey(s))
                        {
                            strreturn = 'true';
                        }
                    }
                }
                else
                {
                    if(maplist.containskey(strPermsetname))
                    {
                        strreturn = 'true';
                    }
                }
            }
        }   
        return strreturn;
     }
      webservice Static String validatePermissionPIA(String strUserId)
     {
        String strreturn = 'false';
        strreturn = AdminUserInfo();
        
        String strPermsetname = getConfigurationValues('PIACreateAccess').get(0);
        System.debug('strPermsetname=========='+strPermsetname);
        String strQry = 'SELECT AssigneeId,Id,PermissionSetId FROM PermissionSetAssignment where AssigneeId =\''+string.valueOf(strUserId)+'\'';
        list<PermissionSetAssignment> lstPermissionset = new list<PermissionSetAssignment>();
        lstPermissionset = Database.Query(strQry);
        map<String,String> maplist = new map<String,String>();
        system.debug('lstPermissionset============'+lstPermissionset);
        if(lstPermissionset != null && lstPermissionset.size() > 0)
        {
            for(PermissionSetAssignment pobj : lstPermissionset){
                String strpermmsid = pobj.PermissionSetId;
                maplist.put(strpermmsid.substring(0,15),strpermmsid.substring(0,15));
            }   
        }
        if(maplist != null && maplist.size() > 0)
        {
            if(strPermsetname != null)
            {
                if(strPermsetname.contains(','))
                {
                    list<String> lstdata = strPermsetname.split(',');
                    for(String s: lstdata){
                        if(maplist.containskey(s))
                        {
                            strreturn = 'true';
                        }
                    }
                }
                else
                {
                    if(maplist.containskey(strPermsetname))
                    {
                        strreturn = 'true';
                    }
                }
            }
        }   
        return strreturn;
     }
     
      webservice Static String validatePermissionPaymentplan(String strUserId)
     {
        String strreturn = 'false';
        strreturn = AdminUserInfo();
        
        String strPermsetname = getConfigurationValues('PaymentPlanCreateAccess').get(0);
        System.debug('strPermsetname=====****====='+strPermsetname);
        String strQry = 'SELECT AssigneeId,Id,PermissionSetId FROM PermissionSetAssignment where AssigneeId =\''+string.valueOf(strUserId)+'\'';
        list<PermissionSetAssignment> lstPermissionset = new list<PermissionSetAssignment>();
        lstPermissionset = Database.Query(strQry);
        map<String,String> maplist = new map<String,String>();
        system.debug('lstPermissionset=====**======='+lstPermissionset);
        if(lstPermissionset != null && lstPermissionset.size() > 0)
        {
            for(PermissionSetAssignment pobj : lstPermissionset){
                String strpermmsid = pobj.PermissionSetId;
                maplist.put(strpermmsid.substring(0,15),strpermmsid.substring(0,15));
            }   
        }
        if(maplist != null && maplist.size() > 0)
        {
            if(strPermsetname != null)
            {
                if(strPermsetname.contains(','))
                {
                    list<String> lstdata = strPermsetname.split(',');
                    for(String s: lstdata){
                        if(maplist.containskey(s))
                        {
                            strreturn = 'true';
                        }
                    }
                }
                else
                {
                    if(maplist.containskey(strPermsetname))
                    {
                        strreturn = 'true';
                    }
                }
            }
        }   
        return strreturn;
     }
     
     public static String AdminUserInfo()
     {
        String strStatus = 'false';
        List<user> lstuser = new list<user>([select Id from user where Profile.Name = 'System Administrator' or Profile.Name = 'Sales Rep' or Profile.Name = 'Revenue Assurance']);
        if(lstuser != null && lstuser.size() > 0)
        {
            for(User u:lstuser)
            {
                if(u.Id == Userinfo.getUserId())
                {
                    strStatus = 'true';
                }
            }
        }
        return strStatus;
     }
      public static Map<String, List<String>> configSettingsMap {
        get
        {
            if (configSettingsMap == null || configSettingsMap.size() ==0)
            {
                configSettingsMap  = new Map<String, List<String>>();
                for( Vertex_Berry__BSure_Configuration_Settings__c configSetting : Vertex_Berry__BSure_Configuration_Settings__c.getAll().values() )
                {
                    if(configSetting.Vertex_Berry__Parameter_Key__c != null)
                        {
                            List<String> valuesList = configSettingsMap.get(configSetting.Vertex_Berry__Parameter_Key__c.toLowerCase());
                            
                            if(valuesList == null) 
                            {
                                valuesList = new List<String>();                
                            } 
                            valuesList.add(configSetting.Vertex_Berry__Parameter_Value__c);
                            configSettingsMap.put(configSetting.Vertex_Berry__Parameter_Key__c.toLowerCase(), valuesList);
                        }
                }
            }
            return configSettingsMap;
        }        
    }
    
    public static List<String> getConfigurationValues(String ParameterName)
    {        
        if (ParameterName == null)
        {
            return new List<string>();
        }
        if (configSettingsMap.get(ParameterName.tolowercase()) != null)
        {
            return configSettingsMap.get(ParameterName.tolowercase());
        }
        return new List<string>();
    }
}