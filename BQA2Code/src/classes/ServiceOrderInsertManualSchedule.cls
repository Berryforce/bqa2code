public with sharing class ServiceOrderInsertManualSchedule {

    public pagereference doRun() {
        database.executebatch(new ServiceOrderBatchController ());
        return (new pagereference('/apex/ServiceOrderInsertManualSchedule').setredirect(true));
    }
}