@IsTest(SeeAllData=true)
public class SubmitToYodleaAndGeneralTest{

    public static testMethod void sbmtYdlGnrlTest(){
        
        List < Digital_Product_Requirement__c > lstDffs = new List < Digital_Product_Requirement__c > ();
        
        Test.startTest();
 
        Digital_Product_Requirement__c Dff = CommonUtility.createDffTest();
        Dff.Status__c = 'Complete';
        insert DFF;
        
        lstDffs.add(Dff);  
        
        SubmitToYodleaAndGeneral.updtDffforYodle(lstDffs);
        SubmitToYodleaAndGeneral.updtDffforGeneral(lstDffs);
        
        Test.stopTest();
    } 
    
}