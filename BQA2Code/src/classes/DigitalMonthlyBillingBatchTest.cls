@isTest
public class DigitalMonthlyBillingBatchTest {
	@testSetup static void setup() {
		User_Ids_For_Email__c userId = TestMethodsUtility.createUserIdForEmail('LocalBilling');
		Batch_Size__c DigitalBatchSize = TestMethodsUtility.createBatchSize('DigitalMonthly', 5); 
		TBC_Cron__c DigitalPrintCron = TestMethodsUtility.createTBCCron('DigitalPrintCron');
		Batch_Size__c PMBBatchSize = TestMethodsUtility.createBatchSize('PMB', 5); 
		TBC_Cron__c CleanUpCron = TestMethodsUtility.createTBCCron('CleanUpCron');
		TBC_Cron__c FMPCron = TestMethodsUtility.createTBCCron('FMPCron');
		Batch_Size__c FMPBatchSize = TestMethodsUtility.createBatchSize('FMP', 5); 
	}
	
    static testMethod void DigitalMonthlyBillingTest() {
        Test.startTest();        
        Account acct = TestMethodsUtility.createAccount('cmr');
        Account childAcct = TestMethodsUtility.generateAccount();
        childAcct.ParentId = acct.Id;
        insert childAcct;
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.createOrderSet(acct, ord, oppty);
        Order_Line_Items__c oln = TestMethodsUtility.createOrderLineItem(acct, cnt, oppty, ord, og);
        Monthly_Cron__c monCron = TestMethodsUtility.createMonthlyCron(CommonMessages.DigitalMediaType, system.today(), og.Id);               
        DigitalMonthlyBilling obj = new DigitalMonthlyBilling(system.today());
        ID batchprocessid = Database.executeBatch(obj);
        Test.stopTest();
    }   
    
    static testMethod void DigitalMonthlyBillingTest1() {
        Test.startTest();        
        Account acct = TestMethodsUtility.createAccount('cmr');
        Account childAcct = TestMethodsUtility.generateAccount();
        childAcct.ParentId = acct.Id;
        insert childAcct;
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.createOrderSet(acct, ord, oppty);
        Order_Line_Items__c oln = TestMethodsUtility.createOrderLineItem(acct, cnt, oppty, ord, og);
        Monthly_Cron__c monCron = TestMethodsUtility.createMonthlyCron(CommonMessages.DigitalMediaType, system.today(), og.Id);        
        DigitalMonthlyBilling obj = new DigitalMonthlyBilling(system.today(), CommonMessages.PrintMediaType);
        ID batchprocessid = Database.executeBatch(obj);
        Test.stopTest();
    }  
    
    static testMethod void DigitalMonthlyBillingTest2() {
        Test.startTest();        
        Account acct = TestMethodsUtility.createAccount('cmr');
        Account childAcct = TestMethodsUtility.generateAccount();
        childAcct.ParentId = acct.Id;
        insert childAcct;
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.createOrderSet(acct, ord, oppty);
        Order_Line_Items__c oln = TestMethodsUtility.createOrderLineItem(acct, cnt, oppty, ord, og);
        Monthly_Cron__c monCron = TestMethodsUtility.createMonthlyCron(CommonMessages.DigitalMediaType, system.today(), og.Id);        
        DigitalMonthlyBilling obj = new DigitalMonthlyBilling(system.today(), CommonMessages.PrintMediaType, true);
        ID batchprocessid = Database.executeBatch(obj);
        Test.stopTest();
    }  
}