global class PushLeadstoQueueSchedulerHndlr implements PushLeadstoQueueScheduler.PushLeadstoQueueSchedulerInterface {
   global void execute(SchedulableContext sc) 
   {
        PushLeadstoQueue pushleads = new PushLeadstoQueue();
        database.executebatch(pushleads, 2000);
   }  
}