/**********************************************************
Apex Class to submit account manually to Fulfillment Vendor
Created By: Reddy(pyellanki2@csc.com)
Updated Date: 03/30/2015
$Id$
***********************************************************/
public with sharing class CreateTalusAccount1Controller {

    //getter and setters
    public String accId {
        get;
        set;
    }
    public String prflId {
        get;
        set;
    }
    public String prflNm {
        get;
        set;
    }
    public String newResponsestring {
        get;
        set;
    }

    //Get nigel endpoint url from custom label
    public static final string furl = Label.Talus_Nigel_Url;

    public static String dt;
    public static Map < String, Object > newResps = new Map < String, Object > ();
    public static Map < String, Object > accmngr = new Map < String, Object > ();
    public static Map < String, Object > phn1 = new Map < String, Object > ();
    public static List < Object > phnbrs = new List < Object > ();

    //Constructor
    public CreateTalusAccount1Controller(ApexPages.StandardController controller) {

        accId = controller.getId();
        Id prflId = userInfo.getProfileId();
        Profile prfl = [SELECT Name from Profile where id = : prflId];
        prflNm = prfl.Name;

    }

    public pageReference crtAccnt() {

        if (prflNm != 'System Administrator') {

            CommonUtility.msgWarning(CommonUtility.notSAPrfl);

        } else {

            //Query for account information 
            Account acc = [Select TalusAccountId__c, Talus_Updated_JSON_Body__c, TalusPhoneId__c, Phone, Name, Id, Berry_ID__c, Sensitive_Heading__c,
                Account_Manager__r.FirstName, Account_Manager__r.LastName, Account_Manager__r.Phone, Account_Manager__c, Account_Manager__r.Name,
                Account_Manager__r.Email, Account_Manager__r.ManagerId, Enterprise_Customer_ID__c, CreatedBy.FirstName, CreatedBy.LastName, CreatedBy.Username, CreatedBy.Email, CreatedBy.phone, (SELECT Id, Name, Enterprise_Customer_ID__c, TalusAccountId__c, TalusPhoneId__c, TalusCreatedDate__c, TalusModifiedDate__c, Original_Enterprise_Customer_Id__c from childAccounts)
                From Account where Id = : accId limit 1
            ];

            //System.debug('************AccountInfo************' + acc);

            if (String.isBlank(acc.TalusAccountId__c)) {

                String extIdUrl = furl + '?external_id=' + acc.Enterprise_Customer_ID__c;
                newResponsestring = TalusRequestUtilityGET.initiateRequest(extIdUrl);

                if (Test.isRunningTest() && acc.Name == 'TestingHttp') {
                    newResponsestring = CommonUtility.mockGetRspns();
                }

                if (newResponsestring.contains('external_id')) {

                    //Deserialize JSON response body
                    newResps = (Map < String, Object > ) JSON.deserializeUntyped(newResponsestring);
                    List < Object > lstObj = (List < Object > ) newResps.get('objects');
                    Map < String, Object > vals = (Map < String, Object > ) lstObj[0];

                    System.debug('************KeySet************' + vals.keySet() + '************Values************' + vals.Values());

                    //Update fulfillment values on account record
                    acc.TalusAccountId__c = String.valueof(vals.get('id'));
                    dt = String.valueof(vals.get('created'));
                    acc.TalusCreatedDate__c = datetime.valueof(dt.replace('T', ' '));
                    dt = String.valueof(vals.get('last_updated'));
                    acc.TalusModifiedDate__c = datetime.valueof(dt.replace('T', ' '));
                    acc.Original_Enterprise_Customer_Id__c = acc.Enterprise_Customer_ID__c;

                    //Parse account_manager information for phone id value
                    accmngr = (Map < String, Object > ) vals.get('account_manager');
                    phnbrs = (List < Object > ) accmngr.get('phone_numbers');

                    if (phnbrs.size() > 0) {
                        phn1 = (Map < String, Object > ) phnbrs[0];
                        //System.debug('************PhoneId************' + phn1.get('id'));
                        acc.TalusPhoneId__c = String.valueof(phn1.get('id'));
                    }

                    update acc;
                    CommonUtility.msgInfo(String.valueof('Account with this ' + acc.Enterprise_Customer_ID__c + 'Enterprise Customer Id already exist, so updating current account with existing Fulfillment Ids'));

                } else {

                    //Instantiate JSON Generator method to form JSON data
                    JSONGenerator jsonGen = JSON.createGenerator(true);
                    String newJsonString;

                    //Starting JSON Generator
                    jsonGen.writeStartObject();

                    jsonGen.writeStringField('status', 'active');
                    jsonGen.writeBooleanField('privacy_flag', acc.Sensitive_Heading__c);
                    jsonGen.writeStringField('name', acc.Name);
                    jsonGen.writeStringField('external_id', acc.Enterprise_Customer_ID__c);

                    jsonGen.writeFieldName('account_manager');

                    jsonGen.writeStartObject();

                    if (acc.Account_Manager__c == null) {
                        jsonGen.writeStringField('name', acc.CreatedBy.UserName);
                        jsonGen.writeStringField('first_name', acc.CreatedBy.FirstName);
                        jsonGen.writeStringField('last_name', acc.CreatedBy.LastName);
                        jsonGen.writeStringField('email', acc.CreatedBy.Email);
                        if (acc.CreatedBy.Phone != null) {
                            jsonGen.writeFieldName('phone_numbers');
                            jsonGen.writeStartArray();
                            jsonGen.writeStartObject();
                            jsonGen.writeStringField('phone_type', 'Office');
                            jsonGen.writeStringField('phone_number', acc.CreatedBy.Phone);
                            jsonGen.writeEndObject();
                            jsonGen.writeEndArray();
                        }
                    } else {
                        jsonGen.writeStringField('name', acc.Account_Manager__r.Name);
                        jsonGen.writeStringField('first_name', acc.Account_Manager__r.FirstName);
                        jsonGen.writeStringField('last_name', acc.Account_Manager__r.LastName);
                        jsonGen.writeStringField('email', acc.Account_Manager__r.Email);
                        if (acc.Account_Manager__r.Phone != null) {
                            jsonGen.writeFieldName('phone_numbers');
                            jsonGen.writeStartArray();
                            jsonGen.writeStartObject();
                            jsonGen.writeStringField('phone_type', 'Office');
                            jsonGen.writeStringField('phone_number', acc.Account_Manager__r.Phone);
                            jsonGen.writeEndObject();
                            jsonGen.writeEndArray();
                        }
                    }

                    jsonGen.writeEndObject();

                    //Ending JSON Generator
                    jsonGen.writeEndObject();

                    //Retrieve JSON body as a string
                    newJsonString = jsonGen.getAsString();

                    System.debug('************JSON Body************' + newJsonString);

                    if (String.isNotBlank(newJsonString)) {
                        try {

                            HttpResponse newRes = CommonUtility.postUtil(newJsonString, fUrl, String.valueof('POST'));

                            System.debug('************Response************' + newRes);

                            if (Test.isRunningTest()) {
                                newResponsestring = CommonUtility.mockGetRspns();
                                newRes.setStatusCode(201);
                            }

                            newResponsestring = newRes.getBody();

                            system.debug('************Response************' + newResponsestring + '----------' + newRes.getStatusCode() + '------------' + newRes.getStatus());

                            if (newRes != NULL && newRes.getStatusCode() == 201) {

                                //Deserializing response body
                                if (!Test.isRunningTest()) {
                                    newResps = (Map < String, Object > ) JSON.deserializeUntyped(newResponsestring);
                                }

                                acc.TalusAccountId__c = String.valueof(newResps.get('id'));
                                dt = String.valueof(newResps.get('created'));
                                acc.TalusCreatedDate__c = datetime.valueof(dt.replace('T', ' '));
                                dt = String.valueof(newResps.get('last_updated'));
                                acc.TalusModifiedDate__c = datetime.valueof(dt.replace('T', ' '));
                                acc.Original_Enterprise_Customer_Id__c = acc.Enterprise_Customer_ID__c;

                                //Parse account_manager information for phone id value
                                accmngr = (Map < String, Object > ) newResps.get('account_manager');
                                phnbrs = (List < Object > ) accmngr.get('phone_numbers');

                                if (phnbrs.size() > 0) {
                                    phn1 = (Map < String, Object > ) phnbrs[0];
                                    System.debug('************Phone Id************' + phn1.get('id'));
                                    acc.TalusPhoneId__c = String.valueof(phn1.get('id'));
                                }

                                update acc;
                                CommonUtility.msgConfirm(CommonUtility.accSflySbmtd);

                            } else {
                                CommonUtility.msgFatal('Exception: ' + newResponsestring);
                            }
                        } catch (exception e) {
                            CommonUtility.msgFatal('Exception: ' + String.valueof(e.getMessage()));
                        }
                    }
                }

            } else {
                CommonUtility.msgInfo(CommonUtility.accntEstablished);
            }
        }

        return null;
    }

}