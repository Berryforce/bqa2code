public with sharing class DirectoryManualSchedule {

    public Directory__c objDir {get;set;}
    
    public DirectoryManualSchedule(){
            objDir = new Directory__c(); 
    }
    
    public pagereference doRun() {
        List<directory__c> lstDir = new List<Directory__c>();       
        lstDir = [select id,Name,Sales_Lockout__c,EBD__c,Ship_Date__c,DCR_Close__c,BOC__c,Final_Service_Order_Due_to_Berry__c,
                    Book_Extract_YP__c,Pub_Date__c,Sales_Start__c,Delivery_Start_Date__c,Coup_Prelim_Filler__c from Directory__c 
                    where Sales_Lockout__c =:objDir.Sales_Lockout__c and EBD__c =:objdir.EBD__c and Ship_Date__c =:objDir.Ship_Date__c and 
                     DCR_Close__c =:objDir.DCR_Close__c and BOC__c =:objdir.BOC__c and Final_Service_Order_Due_to_Berry__c =:objdir.Final_Service_Order_Due_to_Berry__c 
                     and Book_Extract_YP__c  =:objDir.Book_Extract_YP__c and Pub_Date__c =:objDir.Pub_Date__c and Sales_Start__c=:objDir.Sales_Start__c and Delivery_Start_Date__c =:objDir.Delivery_Start_Date__c and Coup_Prelim_Filler__c =:objDir.Coup_Prelim_Filler__c];
        
        system.debug('---lstdir from Page-->'+lstDir); 
        if(lstDir.size() > 0) {
            DirectoryFlipBookCommonMethods.doDirectoryFlipBookProcess(lstDir);
        }
        return (new pagereference('/apex/DirectoryFlipBookTest').setredirect(true));
    }
}