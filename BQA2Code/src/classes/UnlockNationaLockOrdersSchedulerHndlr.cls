global class UnlockNationaLockOrdersSchedulerHndlr implements UnlockNationaLockOrdersScheduler.UnlockNationaLockOrdersSchedulerInterface {
	global void execute(SchedulableContext sc) {
        UnlockNationaLockOrdersBatch obj = new UnlockNationaLockOrdersBatch();
        Database.executeBatch(obj);
    }
}