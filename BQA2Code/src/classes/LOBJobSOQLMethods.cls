public with sharing class LOBJobSOQLMethods {
	public static List<List_Of_Business_Job__c> getLOBJobsByDirIdDirEdId(Id dirEdId, Id dirId) {
		return [SELECT Id FROM List_Of_Business_Job__c WHERE LBJ_Directory_Edition_Id__c =: dirEdId AND LBJ_Directory_Id__c =: dirId];
	}
}