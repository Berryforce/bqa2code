public with sharing class NationalInvoiceView_V1 {
        
    Order_Group__c objOS {get;set;}
    public HeaderWrapperClass objHead {get;set;}
    
    public NationalInvoiceView_V1(ApexPages.StandardController controller) {
        objOS = (Order_Group__c)controller.getRecord();        
    }
    
    public Pagereference onLoad() {
        
        list<NationalInvoice__c> lstNI = NationalInvoiceSOQLMethods.getNationalInvoiceForNationalViewByOrderSet(new set<Id>{objOS.Id});
        objHead = new HeaderWrapperClass();
        if(lstNI!=null && lstNI.size() > 0) {
            NationalInvoice__c objNi = lstNI[0];
            objHead.objNi = objNI;
            objHead.totalGrossAmt = objNI.TotalGrossAmount__c;
            objHead.totalADJAmt = objNI.TotalADJAmount__c;
            objHead.taxAmt = objNI.Tax_Amount_New__c;
            objHead.totalAmt = objNI.Total_Amount__c;
            objHead.netAmount = objNI.Net_Amount__c;
            objHead.ISSNumber = objNI.ISS__r.Name;
            objHead.ISSDate = objNI.Invoice_Date__c;
            objHead.bolNi = true;
            
            set<Id> lstOliNiLi = new set<Id>();
            Map<id,ChildWrapperClass> mpOliIli = new Map<id,ChildWrapperClass>();
            Map<id,List<ChildWrapperClass>> mpOliNcl = new Map<id,List<ChildWrapperClass>>();
            for(NationalInvoiceLineItem__c iterator : objNi.National_Invoice_Line_Items__r) {
                lstOliNiLi.add(iterator.OrderLineItem__c);
                ChildWrapperClass objChild = new ChildWrapperClass();
                objChild.oliID = iterator.OrderLineItem__C;
                objChild.lineNumber = iterator.Line_Number__c;
                objChild.PGNumber = iterator.Page_Number__c;
                objChild.UDAC = iterator.UDAC__c;
                objChild.DAT = iterator.DAT__c;
                objChild.BAS = iterator.BAS__c;
                objChild.SPINS = iterator.SPINS__c;
                objChild.fullRate = iterator.OrderLineItem__r.ListPrice__c;
                //objHead.totalGrossAmt = objHead.totalGrossAmt + iterator.OrderLineItem__r.ListPrice__c;
                objChild.code = '';
                objChild.ADJAmount = 0;
                objChild.AdvertisingData = iterator.Advertising_Data__c;
                mpOliIli.put(iterator.OrderLineItem__c, objChild);
            }
            
            list<National_Child_Lines__c> lstNcl = NationalChildLinesSOQLMethods.getNCLWithOutOmitByOrderLineItemIds(lstOliNiLi);            
            
                List<ChildWrapperClass> lstCh;
                for(National_Child_Lines__c iterator : lstNcl) {  
                ChildWrapperClass objChildLine = new ChildWrapperClass();
                objChildLine.oliID = iterator.Order_Line_Item__c;
                objChildLine.nroID = iterator.Id;
                objChildLine.lineNumber = String.valueOf(iterator.Line_Number__c);
                objChildLine.PGNumber = null;
                objChildLine.BAS = iterator.BAS__c;
                objChildLine.DAT = iterator.DAT__c;
                objChildLine.SPINS = iterator.SPINS__c;
                objChildLine.code = '';
                objChildLine.AdvertisingData = iterator.Advertising_Data__c;
                if(mpOliNcl.get(iterator.Order_Line_Item__c)!=null)
                {
                    lstCh = mpOliNcl.get(iterator.Order_Line_Item__c);
                    lstCh.add(objChildLine);
                    mpOliNcl.put(iterator.Order_Line_Item__c, lstCh);
                    //mpOliNcl.put(iterator.Order_Line_Item__c, mpOliNcl.get(iterator.Order_Line_Item__c).add(objChildLine));
                }
                else
                {
                    lstCh = new List<ChildWrapperClass>();
                    lstCh.add(objChildLine);
                    mpOliNcl.put(iterator.Order_Line_Item__c, lstCh);
                    //mpOliNcl.put(iterator.Order_Line_Item__c, new List<ChildWrapperClass>().add(objChildLine));
                }
            }
            
            //objHead.netAmount = objHead.totalGrossAmt - objHead.totalADJAmt;
            //objHead.taxAmt = 0;
            //objHead.totalAmt = objHead.netAmount + objHead.taxAmt;
            
            list<ChildWrapperClass> lstTempChild = new list<ChildWrapperClass>();
            for(Id IdRecord : mpOliIli.keySet()) {
                if(mpOliIli.get(IdRecord)!=null)
                {
                    lstTempChild.add(mpOliIli.get(IdRecord));
                }
                if(mpOliNcl.get(IdRecord)!=null)
                {
                    lstTempChild.addall(mpOliNcl.get(IdRecord));
                }
                 
             }
            objHead.lstChild = lstTempChild;
        }        
        else
        {
            objHead.bolNi = false;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No National Invoices are associated with this Order Set');
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
    
    public class HeaderWrapperClass {
        public NationalInvoice__c objNi {get;set;}
        public Boolean bolNi {get;set;}
        public String ISSNumber {get;set;}
        public Date ISSDate {get;set;}
        public Decimal totalGrossAmt {get;set;}
        public Decimal totalADJAmt {get;set;}
        public Decimal netAmount {get;set;}
        public Decimal taxAmt {get;set;}
        public Decimal totalAmt {get;set;}
        public list<ChildWrapperClass> lstChild {get;set;}
        
        public HeaderWrapperClass() {
            lstChild = new list<ChildWrapperClass>();
        }
    }
    
    public class ChildWrapperClass {
        public Id oliID {get;set;}
        public Id nroID {get;set;}
        public Decimal PGNumber {get;set;}
        public String lineNumber {get;set;}
        public String UDAC {get;set;}
        public String BAS {get;set;}
        public String DAT {get;set;}
        public String SPINS {get;set;}
        public Decimal fullRate {get;set;}
        public String code {get;set;}
        public Decimal ADJAmount {get;set;}
        public String AdvertisingData {get;set;}
    }
}