public with sharing class GalleyReportController_V1 {
    String s = '';
    public String selectedHeader {get;set;}
    public List<SelectOption> listGalleyHeaders {get;set;}
    public Id headerId {get;set;}
    Map<Id, Galley_Report_Header__c> mapGalleyReportHeader = new Map<Id, Galley_Report_Header__c>();
    public Galley_Report_Header__c galleyRepHead {get;set;}
    public List<SelectOption> listTemplateURLs {get;set;}
    
    public GalleyReportController_V1(ApexPages.StandardController stdController) {
        galleyRepHead = new Galley_Report_Header__c();
        listGalleyHeaders = new List<SelectOption>();
        List<Galley_Report_Header__c> listGalleyReportHeader = new List<Galley_Report_Header__c>();
        listGalleyReportHeader = [SELECT Id, Name, GR_Directroy_Name__c, GR_Directory_Code__c, GR_Selected_Year__c FROM Galley_Report_Header__c 
                                 WHERE GR_User_Name__c =: UserInfo.getUserId() ORDER BY Name];
        listGalleyHeaders.add(new SelectOption('None', 'None'));
        
        if(listGalleyReportHeader.size() > 0) {
            for(Galley_Report_Header__c GRH : listGalleyReportHeader) {
                String optionName = GRH.Name + ' - ' + GRH.GR_Directroy_Name__c + ' - ' + GRH.GR_Directory_Code__c + ' / ' + GRH.GR_Selected_Year__c;
                listGalleyHeaders.add(new SelectOption(GRH.Id, optionName));
                mapGalleyReportHeader.put(GRH.Id, GRH);
            }
            
        	Id idParam = ApexPages.CurrentPage().getParameters().get('id');
        	if(mapGalleyReportHeader.containsKey(idParam)) {
        		headerId = idParam;
        		selectedHeader = mapGalleyReportHeader.get(idParam).Name;
        		galleyRepHead = mapGalleyReportHeader.get(idParam);
        	}
        	else {
        		headerId = listGalleyReportHeader.get(0).Id;
        		selectedHeader = listGalleyReportHeader.get(0).Name;
        		galleyRepHead = listGalleyReportHeader.get(0);
        	}
        	fetchTemplateURLs();
        }
    }
    
    public void fetchHeaderName() {
        selectedHeader = mapGalleyReportHeader.get(headerId).Name;
        galleyRepHead = mapGalleyReportHeader.get(headerId);
        fetchTemplateURLs();
    }
 
    public String getString() {
        return s;
    }
            
    public void setString(String s) {
        this.s = s;
    }
    
    public void fetchTemplateURLs() {
    	List<GalleyReportTemplate__c> listTemplates = GalleyReportTemplate__c.getAll().values();
    	listTemplates.sort();
    	listTemplateURLs = new List<SelectOption>();
    	if(listTemplates.size() > 0) {
    		for(GalleyReportTemplate__c GRT : listTemplates) {
    			listTemplateURLs.add(new SelectOption(GRT.URL__c + selectedHeader, GRT.Name));
    		}
    	}
    }
}