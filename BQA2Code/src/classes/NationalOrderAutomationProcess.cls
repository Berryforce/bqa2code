public class NationalOrderAutomationProcess{

  public static void processNationalOrder(list<National_Staging_Order_Set__c> lstNSOS) {
        
        list<National_Staging_Order_Set__c> lstNSOSforUpdate=new list<National_Staging_Order_Set__c>();
        lstNSOSforUpdate=ValidateNationalOrder(lstNSOS);
        System.debug('&&&&&lstNSOSforUpdate===>'+lstNSOSforUpdate);
        if(lstNSOSforUpdate.size()>0) {
            set<Id> NSOSIds=new set<Id>();
            for(National_Staging_Order_Set__c iterator : lstNSOSforUpdate){
                NSOSIds.add(iterator.Id);
            }
            NationalOpportunityCreationHandler_v3.NationalOrderProcessByManually(NSOSIds);
        }
  }
  
  public static list<National_Staging_Order_Set__c> ValidateNationalOrder(list<National_Staging_Order_Set__c> lstNSOS){
       
       List<National_Staging_Order_Set__c> validNSOS=new List<National_Staging_Order_Set__c>(); 
       if(lstNSOS.size()>0){
           map<Id,boolean> mapNSOSValidate=new map<Id,boolean>();
           map<Id,National_Staging_Order_Set__c> mapNSOS=new map<Id,National_Staging_Order_Set__c>();
            map<String, Id> mapUDAC = new map<String, Id>();
            Set<id> setDirHeading = new Set<id>();
            Set<id> setDirSection = new Set<id>();
            set<String> setUDAC = new set<String>();
            list<Product2> lstUDAC = new list<Product2>();
            map<Id,set<Id>> MapSecHeadMappingsByDHDS=new map<Id,set<Id>>();
            map<Id,set<Id>> mapDirIdSections=new map<Id,set<Id>>();
            map<Id,set<Id>> mapOriginalDirIdSections=new map<Id,set<Id>>();
            map<Id,set<id>> mapOriginalDPM=new map<Id,set<id>>();
            map<Id,Directory_Product_Mapping__c> mapAllDPM=new map<Id,Directory_Product_Mapping__c>();
            for(National_Staging_Order_Set__c NatHdrObj : lstNSOS){
              
              mapNSOS.put(NatHdrObj.Id,NatHdrObj); 
              for(National_Staging_Line_Item__c NatLin : NatHdrObj.National_Staging_Line_Items__r) {
                     
                     if(NatLin.Directory_Section__c != null) {
                        setDirSection.add(NatLin.Directory_Section__c);
                        if(!mapDirIdSections.containskey(NatHdrObj.Directory__c)){
                                  mapDirIdSections.put(NatHdrObj.Directory__c,new Set<Id>());
                                }
                       mapDirIdSections.get(NatHdrObj.Directory__c).add(NatLin.Directory_Section__c);
                        if(NatLin.Directory_Heading__c != null) {
                            setDirHeading.add(NatLin.Directory_Heading__c);
                        }
                    }
                    
                    if(String.isNotBlank(NatLin.UDAC__c)){
                       setUDAC.add(NatLin.UDAC__c);
                    }
               }
           }
           
            if(setUDAC.size() > 0) {
                lstUDAC = Product2SOQLMethods.getProductByUDAC(setUDAC);
            }
            if(lstUDAC.size()>0){            
                for(Product2 iterator : lstUDAC){
                    mapUDAC.put(iterator.ProductCode, iterator.Id);
                }
            }
            
            if(mapDirIdSections.size()>0) {
               mapOriginalDirIdSections= fetchDSByDirIds(mapDirIdSections.keyset());
               mapOriginalDPM=fetchDPM(mapDirIdSections.keyset());
               mapAllDPM=fetchAllDPM(mapDirIdSections.keyset(),mapUDAC.values());
            }
            
            if(setDirHeading !=null && setDirHeading.size() > 0) {
               MapSecHeadMappingsByDHDS=SectionHeadingMappingSOQLMethods.getMapSecHeadMappingsByDHDS(setDirSection);
            }
            
           string str='';
           for(National_Staging_Order_Set__c NatHdrObj : lstNSOS){
           
            mapNSOSValidate.put(NatHdrObj.Id,true);
            
            System.debug('######NatHdrObj.Id#######'+NatHdrObj.Id);
              
            //Check for Ready for Processing at NSOS Header
            /*
            if(NatHdrObj.Late_Order__c==true && NatHdrObj.Approved_to_Process__c!=true){
                str+='LockOrder,';
                mapNSOSValidate.put(NatHdrObj.Id,false);
            }
            
            if(!NatHdrObj.Is_Ready__c) {
                str+='NotReady,';
                mapNSOSValidate.put(NatHdrObj.Id,false);
            }*/
            if(NatHdrObj.Client_Name__c == null && String.isBlank(NatHdrObj.Client_Name_Text__c)) {
                str+='ClientName,';
                mapNSOSValidate.put(NatHdrObj.Id,false);
            }
            if(NatHdrObj.CMR_Name__c == null){
                str+='CMRName,';
                mapNSOSValidate.put(NatHdrObj.Id,false);
            }
            else
            {
                 if(!NatHdrObj.CMR_Name__r.Is_Active__c){
                     str+='CMRNotActive,';
                     mapNSOSValidate.put(NatHdrObj.Id,false);
                 }
                 if(NatHdrObj.CMR_Name__r.National_Credit_Status__c!='Approved' && NatHdrObj.Override__c!=true){
                     str+='CMRNCSnotApproved,';
                     mapNSOSValidate.put(NatHdrObj.Id,false);
                 }
                
            }
            
            string childstr='';
            //Check for Ready for Processing at Child Level
             for(National_Staging_Line_Item__c NatLin : NatHdrObj.National_Staging_Line_Items__r) {
                 
                 System.debug('######NatLin.Id#######'+NatLin.Id);
                 
                    if(String.isNotBlank(NatLin.Line_Number__c)) {
                        if(NatLin.Line_Number__c.length() < 5){
                            childstr+='LineNumber,';
                            mapNSOSValidate.put(NatHdrObj.Id,false);
                        }
                    }
                    
                if(String.isNotBlank(NatLin.UDAC__c)) 
                {
                    if(NatLin.Directory_Section__c == null){
                        childstr+='DirectorySection,';
                        mapNSOSValidate.put(NatHdrObj.Id,false);
                    }
                    if(NatLin.Directory_Heading__c == null){
                        childstr+='DirectoryHeading,';
                        mapNSOSValidate.put(NatHdrObj.Id,false);
                    }
                    if(NatLin.Discount__c == null){
                         childstr+='Discount,';
                        mapNSOSValidate.put(NatHdrObj.Id,false);
                    }
                    if(NatLin.Listing__c==null){
                        childstr+='Listing,';
                        mapNSOSValidate.put(NatHdrObj.Id,false);
                    }
                    else{
                        if(NatLin.Listing__r.Disconnected__c){
                            childstr+='ListingDisconnected,';
                            mapNSOSValidate.put(NatHdrObj.Id,false);
                        }
                    }
                    
                    if(!NatLin.Ready_for_Processing__c){
                        childstr+='NotReady_for_Processing__c,';
                        mapNSOSValidate.put(NatHdrObj.Id,false);
                    }
                    
                    if(!mapUDAC.containskey(NatLin.UDAC__c)){
                       childstr+='UDACNotValid,';
                       mapNSOSValidate.put(NatHdrObj.Id,false);
                    }
                    if(!mapOriginalDirIdSections.containskey(NatHdrObj.Directory__c)){
                        childstr+='DirectoryNotValid,';
                        mapNSOSValidate.put(NatHdrObj.Id,false);
                    }
                    else{
                        if(!mapOriginalDirIdSections.get(NatHdrObj.Directory__c).contains(NatLin.Directory_Section__c)){
                            childstr+='DirectorySectionNotValid,';
                            mapNSOSValidate.put(NatHdrObj.Id,false);
                        }
                    }
                    if(NatLin.Directory_Heading__c==null){
                        childstr+='DirectoryHeadingNotValid,';
                        mapNSOSValidate.put(NatHdrObj.Id,false);
                    }
                    else{
                        
                        if(!MapSecHeadMappingsByDHDS.containskey(NatLin.Directory_Section__c)){
                            childstr+='DirectorySectionHeadingNotValid,';
                            mapNSOSValidate.put(NatHdrObj.Id,false);
                        }
                        else{
                            if(!MapSecHeadMappingsByDHDS.get(NatLin.Directory_Section__c).contains(NatLin.Directory_Heading__c)){
                                 childstr+='DirectorySectionHeadingNotValidHeading,';
                                 mapNSOSValidate.put(NatHdrObj.Id,false);
                            }
                        }            
                   }
                   
                   if(!mapOriginalDPM.containskey(NatHdrObj.Directory__c)){
                     childstr+='DirectoryNotValidforDPM,';
                     mapNSOSValidate.put(NatHdrObj.Id,false);
                   }
                   else{
                       if(!mapOriginalDPM.get(NatHdrObj.Directory__c).contains(NatLin.Product2__c)){
                            childstr+='ProductNotValidforDPM,';
                            mapNSOSValidate.put(NatHdrObj.Id,false);
                        }
                   }
                   
                   if(NatLin.National_Pricing__c == null) {
                       childstr+='NationalPriceNotValid,';
                       mapNSOSValidate.put(NatHdrObj.Id,false);
                   }
                   else{
                       if(!mapAllDPM.containskey(NatLin.National_Pricing__c)){
                        childstr+='NationalPriceNotValidforDPM,';
                        mapNSOSValidate.put(NatHdrObj.Id,false);
                       }
                   }
                   
                System.debug('@@@@@@@@@@@@@NSLI=='+NatLin.Id+'######NSLI==='+childstr);
                }//UDAC condition
             
             } //for loop on NSLI's closed
            System.debug('@@@@@@@@@@@@@NSOS=='+NatHdrObj.id+'#####NSOS'+str);
           } //for loop on NSOS closed
          validNSOS=new List<National_Staging_Order_Set__c>(); 
          for(Id iterator : mapNSOSValidate.keyset()){
             if(mapNSOSValidate.get(iterator)){
                 validNSOS.add(mapNSOS.get(iterator));
             }
          }
          
       } //if closed
       return validNSOS;
  }
  
  public static map<Id,set<id>> fetchDSByDirIds(Set<id> dirIds){
      map<Id,set<id>> mapDirIdSections=new map<Id,set<id>>();
       if(dirIds.size()>0){
          
          for(Directory_Section__c dirSec :[SELECT Id, Directory__c FROM Directory_Section__c WHERE Directory__c IN : dirIds]){
             if(!mapDirIdSections.containskey(dirSec.Directory__c)){
                 mapDirIdSections.put(dirSec.Directory__c ,new Set<id>());
             }
             mapDirIdSections.get(dirSec.Directory__c).add(dirSec.Id);
          }
    }
    return mapDirIdSections;
  }
  
  public static map<Id,set<id>> fetchDPM(Set<id> dirIds){
      map<Id,set<id>> mapDPM=new map<Id,set<id>>();
       if(dirIds.size()>0){
          
          for(Directory_Product_Mapping__c dpm :[SELECT Id, Directory__c,Product2__c FROM Directory_Product_Mapping__c WHERE Directory__c IN : dirIds]){
             if(!mapDPM.containskey(dpm.Directory__c)){
                 mapDPM.put(dpm.Directory__c ,new Set<id>());
             }
             mapDPM.get(dpm.Directory__c).add(dpm.Product2__c);
          }
    }
    return mapDPM;
  }
  
  public static map<Id,Directory_Product_Mapping__c> fetchAllDPM(Set<id> dirIds,List<Id> ProductIds){
       map<Id,Directory_Product_Mapping__c> mapDPM=new map<Id,Directory_Product_Mapping__c>();
       if(dirIds.size()>0){
          for(Directory_Product_Mapping__c dpm :[SELECT Id, Directory__c,Product2__c FROM Directory_Product_Mapping__c WHERE Directory__c IN : dirIds AND Product2__c in :ProductIds]){
             mapDPM.put(dpm.Id,dpm);
          }
    }
    return mapDPM;
  }
  
}