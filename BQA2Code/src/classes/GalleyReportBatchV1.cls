global class GalleyReportBatchV1 Implements Database.Batchable <sObject>, Database.Stateful {
    public integer batchcount = 0 ;
    set<Id> dirEdIds = new set<Id>();
    set<Id> dirSecIds = new set<Id>();
    set<String> setBusTyp = new set<String>();
    set<String> telcoNames = new set<String>();
    set<String> setDirCodes = new set<String>();
    Integer iCount = 0;
    String gallRepHeadNo;
    Id gallRepHeadId;
    String years = '';
    String DirEds = '';
    String DirSecs = '';
    String DirCodes = '';
    String BusTypes = '';
    String Telcos = '';
    Integer intDPSize;
    set<Id> galleyHeaderIds = new set<Id>();
    String directoryName = '';
    Map<String, Id> mapGalleyHeadNameId = new Map<String, Id>();
    List<id> lstDPIds = new List<Id>();
    Directory_Edition__c dirEdForGRS = new Directory_Edition__c();
    map<String, list<String>> mapSelectedValues = new map<String, list<String>>();
    Map<Integer,Galley_Report_Header__c> mapHeader = new Map<Integer,Galley_Report_Header__c>(); 
    
    global GalleyReportBatchV1(set<Id> dirEdIds, set<Id> dirSecIds, set<String> telcoNames, set<String> setBusTyp, map<String, list<String>> mapSelectedValues, 
                            Directory_Edition__c dirEdForGRS, List<Id> lstDPIds, Integer intDPSize, Map<Integer,Galley_Report_Header__c> mapHeader) {
        this.dirEdIds = dirEdIds;
        this.dirSecIds = dirSecIds;
        this.telcoNames = telcoNames;
        this.setBusTyp = setBusTyp;
        this.dirEdForGRS = dirEdForGRS;
        this.mapSelectedValues = mapSelectedValues;
        this.lstDPIds = lstDPIds;
        this.intDPSize = intDPSize;
        this.mapHeader = mapHeader;

        if(mapSelectedValues.containsKey('Year')) {
            for(String str : mapSelectedValues.get('Year')) {
                years += str + ';';
            }
        }
        
        if(mapSelectedValues.containsKey('DirEd')) {
            for(String str : mapSelectedValues.get('DirEd')) {
                DirEds += str + ';';
            }
        }
        
        if(mapSelectedValues.containsKey('DirSec')) {
            for(String str : mapSelectedValues.get('DirSec')) {
                DirSecs += str + ';';
            }
        }
        
        if(mapSelectedValues.containsKey('Telcos')) {
            for(String str : mapSelectedValues.get('Telcos')) {
                Telcos += str + ';';
            }
        }
        
        if(mapSelectedValues.containsKey('BusType')) {
            for(String str : mapSelectedValues.get('BusType')) {
                BusTypes += str + ';';
            }
        }
        
        if(mapSelectedValues.containsKey('DirCode')) {
            for(String str : mapSelectedValues.get('DirCode')) {
                DirCodes += str + ';';
            }
        }
        
        if(mapSelectedValues.containsKey('DirName')) {
            directoryName = mapSelectedValues.get('DirName').get(0);
        }        
        
        setDirCodes.addAll(mapSelectedValues.get('DirCode'));
    }
        
    global Database.queryLocator start(Database.BatchableContext bc) {
        String SOQLQuery;
        SOQLQuery = 'SELECT Id FROM Directory_Pagination__c WHERE Directory_Code__c IN: setDirCodes AND';                    
        
        if(dirSecIds.size() > 0) {
            SOQLQuery += ' Directory_Section__c IN :dirSecIds AND';
        }
        
        /*if(dirEdIds.size() > 0) {
            SOQLQuery += ' (Directory_Edition__c IN :dirEdIds OR (Continuous_Service_Order_Appearance__c = true AND Directory_Edition__c = null)) AND';
        }
        
        if(telcoNames.size() > 0) {
            SOQLQuery += ' Telco_Provider__c IN :telcoNames AND';
        }
        
        if(setBusTyp.size() > 0) {
            SOQLQuery += ' Customer_Type__c IN :setBusTyp AND'; 
        }*/
        
        if(mapSelectedValues.get('localOrNational').get(0) == 'national') {
            SOQLQuery += ' Order_Line_Item__r.RecordTypeId = \'' + System.Label.TestOLIRTNational + '\' AND';
        }
        
        SOQLQuery = SOQLQuery.removeEnd('AND');
        
        SOQLQuery += 'ORDER BY Directory_Heading__c, Sort_As__c';
        
        return Database.getQueryLocator(SOQLQuery);
    }
    
    global void execute(Database.BatchableContext bc, list<Directory_Pagination__c> listDirPagId) {
    	integer icount2 = 0;
    	Integer intHeader = 0;
    	for(ID objDP : lstDPIds) {
    		icount2++;
    		if(objDP == listDirPagId[0].ID) {
    			intHeader = icount2+1;
    		}
    		else {
    			
    		}
    	}
    	List<Directory_Pagination__c> lstDPNew = new List<Directory_Pagination__c>();
    	lstDPNew.add(listDirPagId[0]);
    	lstDPNew.add(listDirPagId[199]);
    	
    	for(Id idDP : lstDPIds) {
    		lstDPNew.add(new Directory_Pagination__c(id=idDP));
    	}
    	
    	List<Directory_Pagination__c> lstDPCheck = [select id from Directory_Pagination__c where id in :lstDPNew order by sort_as__c];
    	Integer icount3 = 0;
    	
    	if(icount2 == 0) {
	    	for(Directory_Pagination__c objDP : lstDPCheck) {
	    		icount3++;
	    		if(objDP.id == listDirPagId[0].ID) {
	    			intHeader = icount3;
	    		}
	    	}
    	}
    	else {
    		intHeader=icount2;
    	}
		String strCond;
		if(telcoNames.contains('-NullValues-')) {
            strCond = ' and (Telco_Provider__c IN :telcoNames or Telco_Provider__c = null or CLEC_Provider__c IN :telcoNames or CLEC_Provider__c = null)';
            System.debug('Testing Galley strCond '+strCond);
        }
        else {
            strCond = ' and (Telco_Provider__c IN :telcoNames or CLEC_Provider__c IN :telcoNames)';
            System.debug('Testing Galley strCond 2 '+strCond);
        }
		
		String strSOQLQuery = 'SELECT Id, Customer_Type__c, Directory_Code__c, Directory_Edition__c, Directory_Section__c, Telco_Provider__c, Listing_Street_Name_ACR__c, ' + 
                    'Reference_Listing__r.First_Name__c, Order_Line_Item__r.Listing__r.First_Name__c, Order_Line_Item__c, Directory_Heading_Normalized_Sort_Name__c, ' +
                    'CLEC_Provider__c, Cross_Reference_Text__c, Directory_Edition_Code__c, Directory_Edition_Name__c, Caption_Member__c,' + 
                    'Phone_Number_Line__c, Phone_Number_Exchange__c, Phone_Number_Area_Code__c, Indent_Order__c, Listing_Street_Number_ACR__c, ' +
                    'Directory_Heading__c, Directory_Heading_Code__c, Directory_Listing_Last_Business_Name__c, Order_Line_Item__r.Name, Print_Phone__c,' + 
                    'Galley_Listing_Street__c, Indent_Level__c, Left_Telephone_Phrase__c, Listing_City__c, Directory_Edition__r.Directory__r.Name, ' +
                    'Listing_City_Unformatted__c, Listing_Name__c, Listing_PO_Box__c, Listing_Postal_Code__c, Listing_State__c, Listing_Street__c, '+
                    'Order_UDAC__c, Phone_Number__c, Right_Telephone_Phrase__c, Section_Code__c, Sort_As__c, Telco_Provided_Sort_Order__c, '+ 
                    'Type__c, UDAC__c, Year__c, Caption_Display_Text__c, cust__c, Directory_Section__r.Name, Order_Line_Item__r.RecordTypeId, '+ 
                    'Directory_Section__r.Telco_Drives_WP_Sorting__c, First_Name__c, Designation__c, Extra_Line_Type_Product__c, Print_Address__c,' + 
                    'DP_Listing__r.LST_Listing_Number__c, Reference_Listing__r.SL_Scoped_Listing_Number__c, Reference_Listing__r.Suppressed__c,' +
                    'Continuous_Service_Order_Appearance__c, ' +
                    'NAT_CMR_Client__c, Directory_Section__r.Section_Page_Type__c FROM Directory_Pagination__c WHERE id in : listDirPagId'+strCond+ ' ORDER BY Directory_Heading__c, Sort_As__c';
                    
    	list<Directory_Pagination__c> listDirPag = Database.query(strSOQLQuery);
        list<Directory_Pagination__c> listDirPagination = new list<Directory_Pagination__c>();
        list<Galley_Report_Stage__c> listGRS = new list<Galley_Report_Stage__c>();
        
        for(Directory_Pagination__c DP : listDirPag) {
        	if((dirEdIds.contains(DP.Directory_Edition__c) || (DP.Continuous_Service_Order_Appearance__c && DP.Directory_Edition__c == null)) &&
        	   setBusTyp.contains(DP.Customer_Type__c)) {
        		listDirPagination.add(DP);
        	}
        }
        
        Galley_Report_Header__c gallRepHead;
        //insert gallRepHead;
        gallRepHead = mapHeader.get(intHeader);
        gallRepHeadNo = gallRepHead.Name;
        gallRepHeadId = gallRepHead.Id;
        galleyHeaderIds.add(gallRepHead.Id);
        Set<id> setId = new Set<Id>();
        setId.addAll(lstDPIds);
        for(Directory_Pagination__c DP : listDirPagination) {
            if(setId.contains(DP.id)) {
            	intHeader++;
            	gallRepHead = mapHeader.get(intHeader);
                gallRepHeadNo = gallRepHead.Name;
                gallRepHeadId = gallRepHead.Id;
                iCount = 0;
                galleyHeaderIds.add(gallRepHead.Id);
            }
            //iCount += 1;
            listGRS.add(generateGalleyReportStage(DP));
        }
        
        insert listGRS;
        
        batchcount += listDirPag.size();
    }
    
    global void finish(Database.BatchableContext bc) {
        String strErrorMessage = '';
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        //String[] toAddresses = new String[] {a.CreatedBy.Email};
        
        if(a.NumberOfErrors > 0){
            strErrorMessage = a.ExtendedStatus;
        }
        
        String body = 'The batch job processed ' + a.TotalJobItems + ' batch(es).<br/>Total number of records processed is ' + batchcount + 
                      ' with '+ a.NumberOfErrors + ' failures.<br/> ' + strErrorMessage;
        
        if(galleyHeaderIds.size() > 0) {
            body  += '<br/>Use the below URL(s) to review Galley Header records. <br/>';
            List<Galley_Report_Header__c> listGalleyReportHead = [SELECT Id, Name FROM Galley_Report_Header__c WHERE Id IN: galleyHeaderIds]; 
            
            for(Galley_Report_Header__c GRH : listGalleyReportHead) {
                mapGalleyHeadNameId.put(GRH.Name, GRH.Id);
            }
            
            body += '<br/>The Galley Report Header record(s) <br/>';
            
            List<String> listGalleyHeadrNames = new List<String>();
            listGalleyHeadrNames.addAll(mapGalleyHeadNameId.keySet()); 
            listGalleyHeadrNames.sort();
            
            for(String headerName : listGalleyHeadrNames) {
                 body += headerName + ' - ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + mapGalleyHeadNameId.get(headerName) + '<br/>';
            }
        }   
        
        CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'Galley Report Record Selection Batch Processing Status: ' + a.Status, body);
    }
    
    private Galley_Report_Header__c generateGalleyReportHead(Directory_Pagination__c dirPag) {
        return new Galley_Report_Header__c(GR_User_Name__c = UserInfo.getUserId(),
                                           GR_Business_Type__c = BusTypes,
                                           GR_Directory_Code__c = DirCodes,
                                           GR_Directory_Edition__c = DirEds,
                                           GR_Directory_Section__c = DirSecs,
                                           GR_Telco__c = Telcos,
                                           GR_Selected_Year__c = years,
                                           GR_Directroy_Name__c = directoryName
                                           );
    }
    
    private Galley_Report_Stage__c generateGalleyReportStage(Directory_Pagination__c dirPag) {
        String addrss = '';
        String tempPrintName = '';
        String phoneNo = '';
        String indentLevel = '';
        Galley_Report_Stage__c galleyRepStg = new Galley_Report_Stage__c(Caption_Display_Text__c = dirPag.Caption_Display_Text__c,
                                            CLEC_Provider__c = dirPag.CLEC_Provider__c,                                            
                                            cust__c = dirPag.cust__c,
                                            Cross_Reference_Text__c = dirPag.Cross_Reference_Text__c,
                                            Customer_Type__c = dirPag.Customer_Type__c,
                                            GRS_Designation__c = dirPag.Designation__c,
                                            Directory_Code__c = dirPag.Directory_Code__c,
                                            Directory_Heading__c = dirPag.Directory_Heading__c,
                                            Directory_Heading_Code__c = dirPag.Directory_Heading_Code__c,
                                            Directory_Heading_Normalized_Sort_Name__c = dirPag.Directory_Heading_Normalized_Sort_Name__c,
                                            Directory_Listing_Last_Business_Name__c = dirPag.Directory_Listing_Last_Business_Name__c,
                                            GRS_Directory_Pagination__c = dirPag.Id,
                                            Directory_Section_Name__c = dirPag.Directory_Section__r.Name,
                                            GRS_Extra_Line_Product__c = dirPag.Extra_Line_Type_Product__c,
                                            GRS_First_Name__c = dirPag.First_Name__c,
                                            GSR_Galley_Report_Header__c = gallRepHeadId,
                                            GRS_UniqueID__c = gallRepHeadNo,
                                            Indent_Level__c = dirPag.Indent_Level__c,      
                                            GRS_Indent_Order__c = dirPag.Indent_Order__c,
                                            GRS_Listing_Street_Name_ACR__c = dirPag.Listing_Street_Name_ACR__c,
                                            GRS_Listing_Street_Number_ACR__c = dirPag.Listing_Street_Number_ACR__c,                                                                              
                                            Listing_City_Unformatted__c = dirPag.Listing_City_Unformatted__c,
                                            Listing_Name__c = dirPag.Listing_Name__c,
                                            Listing_State__c = dirPag.Listing_State__c,
                                            Listing_Street__c = dirPag.Listing_Street__c,
                                            NAT_CMR_Client__c = dirPag.NAT_CMR_Client__c,
                                            Order_UDAC__c = dirPag.Order_UDAC__c, 
                                            GRS_Phone_Number_Line__c = dirPag.Phone_Number_Line__c,
                                            GRS_Phone_Number_Area_Code__c  = dirPag.Phone_Number_Area_Code__c ,
                                            GRS_Phone_Number_Exchange__c = dirPag.Phone_Number_Exchange__c,
                                            GRS_Print_Address__c = dirPag.Print_Address__c,  
                                            GRS_Print_Phone__c = dirPag.Print_Phone__c,  
                                            Section_Code__c = dirPag.Section_Code__c,                                                                                  
                                            Section_Page_Type__c = dirPag.Directory_Section__r.Section_Page_Type__c,
                                            Sort_As__c = dirPag.Sort_As__c,
                                            Telco_Provider__c = dirPag.Telco_Provider__c,
                                            Type__c = dirPag.Type__c,
                                            UDAC__c = dirPag.UDAC__c,
                                            Caption_Member__c = dirPag.Caption_Member__c,
                                            Order_Line_Item__c = dirPag.Order_Line_Item__r.Name                                            
                                            );
        galleyRepStg.PrintName__c = '';
        galleyRepStg.GRS_Print_Name__c = '';     
        
        if(dirPag.Reference_Listing__r.Suppressed__c) {
            galleyRepStg.GRS_Listing_Number__c = dirPag.DP_Listing__r.LST_Listing_Number__c;
        } else {
            galleyRepStg.GRS_Listing_Number__c = dirPag.DP_Listing__r.LST_Listing_Number__c;
            galleyRepStg.GRS_Scoped_Listing_Number__c = dirPag.Reference_Listing__r.SL_Scoped_Listing_Number__c;
        }
        
        if(String.isBlank(dirPag.Order_Line_Item__c)) {
            galleyRepStg.GRS_First_Name_Only__c = dirPag.Reference_Listing__r.First_Name__c;
        } else {
            galleyRepStg.GRS_First_Name_Only__c = dirPag.Order_Line_Item__r.Listing__r.First_Name__c;
        }
                                     
        if(String.isBlank(dirPag.Listing_Street__c) && String.isBlank(dirPag.Listing_City__c) && String.isBlank(dirPag.Listing_State__c)) {
            galleyRepStg.X0Addr__c = true;                 
        } else {
            galleyRepStg.X0Addr__c = false; 
        }        
              
        if(String.isNotBlank(dirPag.Listing_Street__c)) {
            addrss += dirPag.Listing_Street__c + ' ';
        }        
        if(String.isNotBlank(dirPag.Listing_City__c)) {
            addrss += dirPag.Listing_City__c + ' ';
        }        
        if(String.isNotBlank(dirPag.Listing_State__c)) {
            addrss += dirPag.Listing_State__c + ' ';
            if(String.isNotBlank(dirPag.Listing_Postal_Code__c)) {
                addrss += dirPag.Listing_Postal_Code__c + ' ';
            }
        }        
        
        if(dirPag.Print_Address__c) {
            galleyRepStg.Galley_Listing_Street__c = dirPag.Galley_Listing_Street__c;
            galleyRepStg.Listing_PO_Box__c = dirPag.Listing_PO_Box__c;
            galleyRepStg.Listing_City__c = dirPag.Listing_City__c;
            galleyRepStg.Listing_Postal_Code__c = dirPag.Listing_Postal_Code__c;
            galleyRepStg.PrintAddr__c = addrss.trim();
        } else {
            galleyRepStg.Galley_Listing_Street__c = ' ';
            galleyRepStg.Listing_PO_Box__c = ' ';
            galleyRepStg.Listing_City__c = ' ';
            galleyRepStg.Listing_Postal_Code__c = ' ';
            galleyRepStg.PrintAddr__c = ' ';
        }
        
        if(dirPag.Print_Phone__c) {
            galleyRepStg.Left_Telephone_Phrase__c = dirPag.Left_Telephone_Phrase__c;
            galleyRepStg.Right_Telephone_Phrase__c = dirPag.Right_Telephone_Phrase__c;
        } else {
            galleyRepStg.Left_Telephone_Phrase__c = ' ';
            galleyRepStg.Right_Telephone_Phrase__c = ' '; 
        }
        
        if(String.isNotBlank(dirPag.Directory_Edition__c)) {
            galleyRepStg.Directory_Edition_Code__c = dirPag.Directory_Edition_Code__c;
            galleyRepStg.Directory_Edition_Name__c = dirPag.Directory_Edition_Name__c;
            galleyRepStg.Year__c = dirPag.Year__c;
            galleyRepStg.Directory_Edition_Directory_Name__c = dirPag.Directory_Edition__r.Directory__r.Name;
        } else {
            galleyRepStg.Directory_Edition_Code__c = dirEdForGRS.Edition_Code__c;
            galleyRepStg.Directory_Edition_Name__c = dirEdForGRS.Name;
            galleyRepStg.Year__c = dirEdForGRS.Year__c;     
            galleyRepStg.Directory_Edition_Directory_Name__c = dirEdForGRS.Directory__r.Name;       
        }
        
        if(String.isNotBlank(dirPag.Phone_Number__c)) {                     
            phoneNo = dirPag.Phone_Number__c.remove(' ').remove('(').remove(')').remove('-');
            if(phoneNo.length() == 10) {
                phoneNo = phoneNo.left(3) + '-' + phoneNo.subString(3, 6) + '-' + phoneNo.right(4);
            } else {
                phoneNo = phoneNo.left(3) + '-' + phoneNo.right(4);
            }
            galleyRepStg.Phone_Number__c = phoneNo;
        }
        
        if(dirPag.Directory_Section__r.Telco_Drives_WP_Sorting__c) {
            galleyRepStg.Telco_Sort_Order__c = dirPag.Telco_Provided_Sort_Order__c;
        }
        
        if(dirPag.Indent_Level__c != null) {
            for(Integer i = 0; i < Integer.valueOf(dirPag.Indent_Level__c); i++) {
                indentLevel += '%';
            }
        }
        
        if(String.isNotBlank(dirPag.Caption_Display_Text__c)) {
            if(String.isNotBlank(dirPag.Directory_Listing_Last_Business_Name__c)) {
                galleyRepStg.PrintName__c += dirPag.Directory_Listing_Last_Business_Name__c + ' ';
                if(String.isNotBlank(dirPag.First_Name__c)) {
                    galleyRepStg.PrintName__c += dirPag.First_Name__c + ' ';
                }
                if(String.isNotBlank(dirPag.Designation__c)) {
                    galleyRepStg.PrintName__c += dirPag.Designation__c + ' ';
                }
                galleyRepStg.PrintName__c += ' |' + dirPag.Caption_Display_Text__c + ' |' + indentLevel + ' ';
                tempPrintName += indentLevel;
            } else {
                galleyRepStg.PrintName__c += dirPag.Caption_Display_Text__c + ' |' + indentLevel;
                tempPrintName += indentLevel;
            }
            //galleyRepStg.GRS_Print_Name__c += dirPag.Caption_Display_Text__c + ' ';
        } else {        
            if(String.isNotBlank(dirPag.Cross_Reference_Text__c)) {
                if(String.isNotBlank(dirPag.Directory_Listing_Last_Business_Name__c)) {
                    galleyRepStg.PrintName__c += dirPag.Directory_Listing_Last_Business_Name__c + ' ';
                    if(String.isNotBlank(dirPag.First_Name__c)) {
                        galleyRepStg.PrintName__c += dirPag.First_Name__c + ' ';
                    }
                    if(String.isNotBlank(dirPag.Designation__c)) {
                        galleyRepStg.PrintName__c += dirPag.Designation__c + ' ';
                    }
                    galleyRepStg.PrintName__c += ' |' + dirPag.Cross_Reference_Text__c + ' ';
                    if(String.isNotBlank(indentLevel)) {
                        galleyRepStg.PrintName__c += + indentLevel + ' ';
                        tempPrintName += indentLevel + ' ';
                    }
                } else {
                    galleyRepStg.PrintName__c += dirPag.Cross_Reference_Text__c + ' ';
                    if(String.isNotBlank(indentLevel)) {
                        galleyRepStg.PrintName__c += + indentLevel + ' ';
                        tempPrintName += indentLevel + ' ';
                    }
                }               
            } else {
                if(String.isNotBlank(dirPag.Directory_Listing_Last_Business_Name__c)) {
                    galleyRepStg.PrintName__c += indentLevel + dirPag.Directory_Listing_Last_Business_Name__c + ' ';
                    tempPrintName += indentLevel + dirPag.Directory_Listing_Last_Business_Name__c + ' ';
                    if(String.isNotBlank(dirPag.First_Name__c)) {
                        galleyRepStg.PrintName__c += dirPag.First_Name__c + ' ';
                        tempPrintName += dirPag.First_Name__c + ' ';
                    }
                    if(String.isNotBlank(dirPag.Designation__c)) {
                        galleyRepStg.PrintName__c += dirPag.Designation__c + ' ';
                        tempPrintName += dirPag.Designation__c + ' ';
                    }
                    if(String.isNotBlank(dirPag.Left_Telephone_Phrase__c) && String.isBlank(dirPag.Caption_Display_Text__c) && !dirPag.Extra_Line_Type_Product__c && String.isBlank(dirPag.Cross_Reference_Text__c)) {
                        tempPrintName += '%' + dirPag.Left_Telephone_Phrase__c;
                    }
                } else {
                    galleyRepStg.PrintName__c += indentLevel + ' ';
                    tempPrintName += indentLevel + ' ';
                    if(String.isNotBlank(dirPag.Left_Telephone_Phrase__c) && String.isBlank(dirPag.Caption_Display_Text__c) && !dirPag.Extra_Line_Type_Product__c && String.isBlank(dirPag.Cross_Reference_Text__c)) {
                        tempPrintName += '%' + dirPag.Left_Telephone_Phrase__c;
                    }
                }
            }
            if(String.isNotBlank(addrss)) {
                tempPrintName += ' ' + addrss.trim() + ' ';
                if(String.isNotBlank(dirPag.Left_Telephone_Phrase__c)) {
                    tempPrintName += '%' + dirPag.Left_Telephone_Phrase__c;
                }
            }
        }
        
        if(String.isNotBlank(dirPag.Designation__c)) {
            if(String.isNotBlank(dirPag.First_Name__c)) {
                galleyRepStg.GRS_Print_Name__c += String.isNotBlank(dirPag.Directory_Listing_Last_Business_Name__c) ? dirPag.Directory_Listing_Last_Business_Name__c : '';
                galleyRepStg.GRS_Print_Name__c += ' ' + dirPag.First_Name__c + ' ' + dirPag.Designation__c;
            } else {
                galleyRepStg.GRS_Print_Name__c += String.isNotBlank(dirPag.Directory_Listing_Last_Business_Name__c) ? dirPag.Directory_Listing_Last_Business_Name__c : '';
                galleyRepStg.GRS_Print_Name__c += ' ' + dirPag.Designation__c;
            }
        } else if(String.isNotBlank(dirPag.First_Name__c)) {
            galleyRepStg.GRS_Print_Name__c += String.isNotBlank(dirPag.Directory_Listing_Last_Business_Name__c) ? dirPag.Directory_Listing_Last_Business_Name__c : '';
            galleyRepStg.GRS_Print_Name__c += ' ' + dirPag.First_Name__c;
        }else {
            galleyRepStg.GRS_Print_Name__c += String.isNotBlank(dirPag.Directory_Listing_Last_Business_Name__c) ? dirPag.Directory_Listing_Last_Business_Name__c : '';
        }
        
        if(tempPrintName.length() > 43) {
            tempPrintName = indentLevel + ' ' + addrss.trim() + ' '; 
            if(tempPrintName.length() > 45) {
                if(dirPag.Print_Address__c) {
                    galleyRepStg.PrintName__c += addrss.trim();
                }
                if(String.isNotBlank(dirPag.Left_Telephone_Phrase__c) && String.isBlank(dirPag.Caption_Display_Text__c) && !dirPag.Extra_Line_Type_Product__c && String.isBlank(dirPag.Cross_Reference_Text__c)) {
                    galleyRepStg.PrintName__c += '%' + dirPag.Left_Telephone_Phrase__c;
                }
                if(String.isNotBlank(phoneNo)) {
                    galleyRepStg.PrintName__c += ' |';
                    tempPrintName = '';
                }
            } else {
                if(dirPag.Print_Address__c) {
                    galleyRepStg.PrintName__c += addrss.trim();
                }
                if(String.isNotBlank(dirPag.Left_Telephone_Phrase__c) && String.isBlank(dirPag.Caption_Display_Text__c) && !dirPag.Extra_Line_Type_Product__c && String.isBlank(dirPag.Cross_Reference_Text__c)) {
                    galleyRepStg.PrintName__c += '%' + dirPag.Left_Telephone_Phrase__c;
                }
            }
        } else {
            if(dirPag.Print_Address__c) {
                galleyRepStg.PrintName__c += addrss.trim() + ' ';       
            } 
            if(String.isNotBlank(phoneNo)) {
                if(String.isNotBlank(dirPag.Left_Telephone_Phrase__c) && String.isBlank(dirPag.Caption_Display_Text__c) && !dirPag.Extra_Line_Type_Product__c && String.isBlank(dirPag.Cross_Reference_Text__c)) {
                    galleyRepStg.PrintName__c += '%' + dirPag.Left_Telephone_Phrase__c;
                }
            }
        }
        
        if(String.isNotBlank(phoneNo) && String.isBlank(dirPag.Cross_Reference_Text__c) && String.isBlank(dirPag.Caption_Display_Text__c)) {
             galleyRepStg.PrintName__c += generatePhoneWithDots(phoneNo, tempPrintName.length());
        }
        
        if(String.isNotBlank(dirPag.Right_Telephone_Phrase__c) && String.isBlank(dirPag.Caption_Display_Text__c) && !dirPag.Extra_Line_Type_Product__c && String.isBlank(dirPag.Cross_Reference_Text__c)) {
            galleyRepStg.PrintName__c += '%' + dirPag.Right_Telephone_Phrase__c + '%';
        }
        
        return galleyRepStg;
    }
    
    private string generatePhoneWithDots(String phoneNo, Integer len) {
        String phoneNoWithDots = '';
        Integer dotsLen = 58 - len - phoneNo.length();
        for(Integer i = 0; i < 10; i++) {
            phoneNoWithDots += '.';
        }
        //phoneNoWithDots += ' ' + phoneNo  + ' ';
        return phoneNoWithDots;
    }
}