@IsTest(seeAllData=True)
public class Contact_BIUAIUTest {
    static testMethod void ContactTest () {
        //User u = TestMethodsUtility.generateUser();
        //Profile p = TestMethodsUtility.getSysAdminProfile();
        //User u = new User(Alias = 'stadt', Email='standardur@testorg.com', 
      //                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
       //                   LocaleSidKey='en_US', ProfileId = p.Id, 
        //                  TimeZoneSidKey='America/Los_Angeles', UserName=TestMethodsUtility.generateRandomEmail());
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'aakash', Email='asumanth@salesforce.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName=TestMethodsUtility.generateRandomEmail('theberrycompany.com'));
        
        System.runAs(u) {
            Account objAccount = TestMethodsUtility.createAccount('customer');
            system.assertNotEquals(null, objAccount);
            system.assert(objAccount != null, 'Account ID : ' + objAccount.Id);
            Contact objContact = TestMethodsUtility.createContact(objAccount.Id);           
            system.assertNotEquals(null, objContact);
            system.assert(objContact != null, 'Contact ID : ' + objContact.Id);
            update objContact;
        }
    }
    
    static testMethod void ContactTestPrimaryBilling () {
        //User u = TestMethodsUtility.generateUser();
        //Profile p = TestMethodsUtility.getSysAdminProfile();
        //User u = new User(Alias = 'stadt', Email='standardur@testorg.com', 
      //                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
       //                   LocaleSidKey='en_US', ProfileId = p.Id, 
        //                  TimeZoneSidKey='America/Los_Angeles', UserName=TestMethodsUtility.generateRandomEmail());
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'aakash', Email='asumanth@salesforce.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName=TestMethodsUtility.generateRandomEmail('theberrycompany.com'));
        
        System.runAs(u) {
            Account objAccount = TestMethodsUtility.createAccount('customer');
            system.assertNotEquals(null, objAccount);
            system.assert(objAccount != null, 'Account ID : ' + objAccount.Id);
            Contact objContact = TestMethodsUtility.createContact(objAccount.Id);           
            system.assertNotEquals(null, objContact);
            system.assert(objContact != null, 'Contact ID : ' + objContact.Id);
            objContact.Primary_Contact__c = false;
            objContact.PrimaryBilling__c = false;
            update objContact;            
            objContact.Primary_Contact__c = true;
            objContact.PrimaryBilling__c = true;
            update objContact;
        }
    }
}