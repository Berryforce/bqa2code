@isTest(seeAllData=true)
public with sharing class updtDffWithTalusIdTest{

	static testMethod void updtDff() {
		test.starttest();
	
	//User u= [Select id, phone, Email from user where isActive=true and phone!=null and Email!=null and userRole.Name = 'Account Manager' limit 1];
	
		Account telcoAcc = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcc.Id);
		Canvass__c c = TestMethodsUtility.createCanvass(telco);
	
		Account a = new Account(Name='Test Account1',  Primary_Canvass__c=c.id, Phone='(422) 552-4422', TalusAccountId__c='12');
		insert a;       
		
		Opportunity oppty = new Opportunity(accountId=a.id, Name='TestOppty', StageName='Test Stage', CloseDate=system.today());
		insert oppty;
		
		Contact cnt = new Contact(AccountId=a.id, FirstName='Test FName', Primary_Contact__c=false,
		                          MailingCity='Test Mailing City', MailingState='CA',
		                          MailingStreet='Test Mailing Street', MailingPostalCode='94538',
		                          LastName='Test LName', Email='test.test@test.com', Phone='(422)552-4422');
		insert cnt;
		
		Order__c ord = new Order__c(Account__c=a.id,Billing_Anniversary_Date__c = null);
		insert ord;
		
		
		Order_Group__c og = new Order_Group__c(Order_Account__c=a.id, Order__c=ord.id, Opportunity__c=oppty.id);
		insert og;
		
		Order_Line_Items__c oln= new Order_Line_Items__c(Billing_Contact__c=cnt.id, Canvass__c=c.id, Opportunity__c=oppty.id, Order_Group__c=og.id,Order__c=ord.id,
		                                                 Quote_Signed_Date__c=system.today()+1, Order_Anniversary_Start_Date__c=system.today()-5, Talus_Go_Live_Date__c=system.today()+30,
		                                                 Account__c = a.Id, UnitPrice__c = 300, Talus_Subscription_ID__c = '', Talus_DFF_Id__c = '');
		insert oln;
		
		Modification_Order_Line_Item__c mOln = new Modification_Order_Line_Item__c(Billing_Contact__c=cnt.id, Canvass__c=c.id, Opportunity__c=oppty.id, Order_Group__c=og.id,Order__c=ord.id,
		                                                 Quote_Signed_Date__c=system.today()+1, Order_Anniversary_Start_Date__c=system.today()-5, Talus_Go_Live_Date__c=system.today()+30,
		                                                 Account__c = a.Id, UnitPrice__c = 300, Talus_Subscription_ID__c = '', Talus_DFF_Id__c = '');
		insert mOln;
		
		Digital_Product_Requirement__c dff = new Digital_Product_Requirement__c(OrderLineItemID__c = oln.id, business_name__c = 'Test Name', Talus_Subscription_Id__c = '', Talus_DFF_Id__c = '');
		
		insert dff;
		
		Digital_Product_Requirement__c dff1 = new Digital_Product_Requirement__c(ModificationOrderLineItem__c = mOln.id, business_name__c = 'Test Name', Talus_Subscription_Id__c = '', Talus_DFF_Id__c = '');
		
		insert dff1;
		
		Map<String, String> dmp = new Map<String, String>();
		Map<String, String> dmp1 = new Map<String, String>();
		
		if(dff.OrderLineItemID__c != null){
		dmp.put(dff.id, '12345');
		dmp1.put(dff.id, '12345');
		}else{
		dmp.put(dff.id, '12345');
		dmp1.put(dff.id, '12345');
		}
		
		updtDffWithTalusId.updtDffTId(dmp, dmp1);
		
		test.stoptest();
	}
}