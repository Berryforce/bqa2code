@isTest
public with sharing class HeadingDisclaimerTest {
	static testMethod void headDiscTest(){
        Test.StartTest();
        Heading_Disclaimer__c objHeadDisc = new Heading_Disclaimer__c();
        objHeadDisc.Name = 'Test Disc';
        objHeadDisc.Disclaimer_Text__c = 'Test Heading Disclaimer Text';        
        insert objHeadDisc;
		HeadingDisclaimer HD = new HeadingDisclaimer();		
		HD.HeadDisclmLst = objHeadDisc;
		HD.Search();
		HD.next();
		HD.previous();
        Test.StopTest();  		
	}
}