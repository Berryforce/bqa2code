global class BoostRenewalAccountsBatchController implements Database.Batchable<sObject>,Database.Stateful{
   
   public set<Id> setOptyId = new set<Id>();
   
   global BoostRenewalAccountsBatchController(set<Id> setOpptyId) {
        if(setOpptyId.size()>0 && setOpptyId != null){
            setOptyId = setOpptyId;
        }  
   }
   global Database.QueryLocator start(Database.BatchableContext bc) {
        String soql = 'Select Id from Opportunity where Id IN : setOptyId';
        return Database.getQueryLocator(soql);
    }
   
    global void execute(Database.BatchableContext bc, List<opportunity> OpptyLst) {
        set<Id> setobjOptyId = new set<Id>();
        for(Opportunity objopty : OpptyLst){
            setobjOptyId.add(objopty.Id);
        }
        if(setobjOptyId.size()>0) {
            list<Order_Line_Items__c> oliLst = [Select Id,Name,Account__r.Delinquency_Indicator__c,Account__r.OwnerId,Product_Type__c,Product2__r.Print_Product_Type__c,RecordtypeId,UnitPrice__c,Is_P4P__c,Directory__c,Directory_Edition__c,Directory_Edition__r.Name,Directory_Edition__r.Book_Status__c,Directory_Edition__r.Final_Auto_Renew_Job__c,Account__c,Account__r.Name,Account__r.Open_or_Closed_Claim_Past_18_Months__c,Account__r.Boost_Fallout_Flag__c,Product2__r.Name,Is_Handled__c from Order_Line_Items__c where Opportunity__c IN: setobjOptyId AND Is_Handled__c = false AND isCanceled__c = false and Directory__c != null];
            if(oliLst.size()>0) {
                BoostAccountRenewalController.BoostAccountRenewal(oliLst);
            }
        }
    }
    global void finish(Database.BatchableContext bc) {
    	LetterRenewalBatchController lrBC = new LetterRenewalBatchController();
    	database.executeBatch(lrBC);
    }
}