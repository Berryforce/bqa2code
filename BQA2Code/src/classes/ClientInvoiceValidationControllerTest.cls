@isTest(seealldata=true)
public class ClientInvoiceValidationControllerTest {
    public static testMethod void clientInvTest() {
        Test.StartTest();
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.createAccount('cmr');
        Account pubAcct = TestMethodsUtility.generatePublicationAccount();
        insert pubAcct;
          Telco__c newTelco = TestMethodsUtility.createTelco(acct.Id);
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Directory__c dir = TestMethodsUtility.createDirectory();
      
        dir.Publication_Company__c = pubAcct.Id;
        insert dir;
        Directory_Edition__c DE = TestMethodsUtility.generateDirectoryEdition(dir);
        DE.Pub_Date__c = system.today();
        insert DE;
        DE = [SELECT Id, Edition_Code__c, Directory__c, Directory__r.Directory_Code__c, Directory__r.Name FROM Directory_Edition__c WHERE Id = : DE.Id];
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        National_Staging_Order_Set__c NSOS = TestMethodsUtility.generateNationalStagingOrderSet();
        NSOS.Opportunity__c = oppty.Id;
        NSOS.Directory__c = dir.Id;
        NSOS.Directory_Edition__c = DE.Id;
        insert NSOS;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(acct, ord, oppty);
        og.selected__c = true;
        og.InvoiceGenerationStatus__c = false;
        og.National_Staging_Order_Set__c = NSOS.Id;
        insert og;
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);              
        oln.UnitPrice__c = 300;
        oln.Action__c = 'I';
        oln.Publication_Company__c = pubAcct.Id;
        oln.Publication_Code__c = pubAcct.Publication_Code__c;
        oln.Directory__c = dir.Id;
        oln.Directory_Edition__c = DE.Id;
        insert oln; 
        National_Billing_Status__c natBillStatus = TestMethodsUtility.createNatBillStatus(pubAcct.Id, DE.Id);  
        National_Tax__c NT = TestMethodsUtility.createNatTaxWithCMR(acct.Id);
        National_Tax__c NT1 = TestMethodsUtility.createNatTaxWithDir(dir.Id);      
        
        ClientInvoiceValidationController CIVC = new ClientInvoiceValidationController();
        CIVC.pubCode = pubAcct.Publication_Code__c;
        CIVC.editionCode = DE.Edition_Code__c;
        CIVC.dirId = dir.Id;
        CIVC.fetchDirs();
        CIVC.calculateOrderTotal();
        CIVC.createNationalInv();
        CIVC.closePopup();
        CIVC.showNatInv();
        CIVC.updateStatusTable();
        CIVC.enableCreateInv();
        Test.StopTest();
    }
}