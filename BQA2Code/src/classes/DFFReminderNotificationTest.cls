@isTest
private class DFFReminderNotificationTest {
	static testmethod void  DFFReminderV1TestMethod() { 
		List<Digital_Product_Requirement__c> lstDFF = new List<Digital_Product_Requirement__c >();
		Account acct = TestMethodsUtility.createAccount('cmr');
		Contact cnt = TestMethodsUtility.createContact(acct.Id);
		Order__c ord = TestMethodsUtility.createOrder(acct.Id);
		Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
		insert oppty;
		Order_Group__c og = TestMethodsUtility.generateOrderSet(acct, ord, oppty);
		og.selected__c = true;
		insert og;
		Order_Line_Items__c oln = TestMethodsUtility.createOrderLineItem(acct, cnt, oppty, ord, og);
		Fulfillment_Profile__c FFP = TestMethodsUtility.createFulfillmentProfile('Initial Fulfillment Profile', acct);
		Directory_Heading__c dirHead = TestMethodsUtility.createDirectoryHeading();  
		Digital_Product_Requirement__c DFF = TestMethodsUtility.generateDataFulfillmentForm('Print Graphic');
		DFF.Account__c = acct.Id;
		DFF.Contact__c = cnt.Id;
		DFF.Proof_Contact__c = cnt.Id;
		DFF.Fulfillment_Profile__c = FFP.Id;
		DFF.Heading_1__c = dirHead.Id;
		DFF.Heading_2__c = dirHead.Id;
		DFF.Heading_3__c = dirHead.Id;
		DFF.Heading_4__c = dirHead.Id;
		DFF.Heading_5__c = dirHead.Id;
		DFF.OrderLineItemID__c=oln.Id;
		DFF.OpportunityID__c=oppty.Id;
		DFF.Competitor_1__c = 'test'; 
		DFF.Competitor_2__c = 'test';
		DFF.Competitor_3__c = 'test';   
		DFF.Employee_1__c = 'test';
		DFF.Employee_2__c = 'test';
		DFF.Employee_3__c = 'test';
		DFF.Affiliation_1__c = 'test';
		DFF.Affiliation_2__c = 'test';   
		DFF.Affiliation_3__c = 'test';
		DFF.Affiliation_4__c = 'test';         
		DFF.Also_Known_As_aka_1__c = 'test';
		DFF.Also_Known_As_aka_2__c = 'test';
		DFF.Also_Known_As_aka_3__c = 'test';   
		DFF.Also_Known_As_aka_4__c = 'test';
		DFF.Also_Known_As_aka_5__c = 'test'; 
		DFF.CpnDesc_CstLnkUrl_CstLnkTxt__c = true;
		DFF.add_ons__c = 'YPCP';
		DFF.coupon_description__c = 'test';
		DFF.Account_Manager__c = UserInfo.getUserId();
		lstDFF.add(DFF);
		insert lstDFF;
		
		Test.StartTest();
		Datetime dt = Datetime.now().addMinutes(1);
		String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
		String jobId = System.schedule('TestDFFReminderNotificationScheduler', CRON_EXP, new DFFReminderNotificationScheduler() );   
		DFFNotification.DFFRemainder(lstDFF);
		Test.StopTest();
	}
}