@isTest
public class OAuthTest 
{
  static OAuth_Service__c testService;
  static final string TEST_QUERY_PARAMETER = 'param'; 
  static final string TEST_QUERY_VALUE = 'horses';
  static OAuth_Service__c createOauthService(String Name, string URL, string consumerKey, string consumerSecret, string token, string tokenSecret,boolean isInsert)
     {
        OAuth_Service__c toReturn = new OAuth_Service__c();
        toReturn.Consumer_Key__c = consumerKey;
        toReturn.Consumer_Secret__c = consumerSecret;
        toReturn.Token__c = token;
        toReturn.Token_Secret__c = tokenSecret;
        toReturn.URL__c = URL;

        toReturn.name = Name;

        if (isInsert)
        {
            insert toReturn;
        }

        return toReturn;
     }
  static void setup()
  {
    testService = createOauthService('whocares','http://test.com','testKey', 'testSecret','testToken', 'testTokenSecret',true);
  }
  
  static testMethod void testConstruction()
  {
    setup();
    OAuth testOAuth;
    
    test.startTest();
      try
      {
        testOAuth = new OAuth(testService);
      }
      catch(exception e)
      {
        system.assert(false,'no exceptions should be thrown in construction');
      }
    test.stopTest();
    
    system.assertNotEquals(null,testOAuth,'an oauth instance should be created');
  }
  
  static testMethod void testGenerateOauthParameters()
  {  
    
    setup();
    
    string testTimestamp = String.valueOf(DateTime.now().getTime()/1000);
    string testNonce =  String.valueOf(Crypto.getRandomLong());
    
    OAuth testOAuth = new OAuth(testService);
    
    test.startTest();
      Map<String,String> testParams = testOAuth.generateOauthParams(testTimestamp,testNonce);
    test.stopTest();
    
    Map<String,String> expectedParams = new Map<String,String>();
    expectedParams.put(OAuth.SIGNATURE_METHOD_PARAM,OAuth.DEFUALT_SIGNATURE_METHOD);
    expectedParams.put(OAuth.TIMESTAMP_PARAM,testTimestamp);
    expectedParams.put(OAuth.NONCE_PARAM,testNonce);
    expectedParams.put(OAuth.VERSION_PARAM,OAuth.DEFAULT_VERSION);
    
    system.assertEquals(expectedParams.get(OAuth.SIGNATURE_METHOD_PARAM),testParams.get(OAuth.SIGNATURE_METHOD_PARAM),'the signature method is incorrect');
    system.assertEquals(expectedParams.get(OAuth.TIMESTAMP_PARAM),testParams.get(OAuth.TIMESTAMP_PARAM),'the timestamp is incorrect');
    system.assertEquals(expectedParams.get(OAuth.NONCE_PARAM),testParams.get(OAuth.NONCE_PARAM),'the nonce is incorrect');
    system.assertEquals(expectedParams.get(OAuth.VERSION_PARAM),testParams.get(OAuth.VERSION_PARAM));
    system.assertEquals(testService.Consumer_Key__c,testParams.get(OAuth.CONSUMER_KEY_PARAM),'the consumer key is incorrect');
    system.assertEquals(testService.Token__c,testParams.get(OAuth.TOKEN_PARAM),'the toekn is incorrect');
  }
  
  static testMethod void testCreateBaseString()
  {
    setup();
    
    string testTimestamp = String.valueOf(DateTime.now().getTime()/1000);
    string testNonce =  String.valueOf(Crypto.getRandomLong());
    
    OAuth testOAuth = new OAuth(testService);
    Map<String,String> parameters = testOAuth.generateOauthParams(testTimestamp,testNonce);
    
    string testUrl = testService.URL__c + '?' + TEST_QUERY_PARAMETER + '=' + TEST_QUERY_VALUE;
    
    HTTPRequest testRequest = new HTTPRequest();
    testRequest.setEndpoint(testUrl);
    testRequest.setMethod('GET');
    
    string testBaseString = testRequest.getMethod();
    testBaseString += '&' + EncodingUtil.urlEncode(testService.URL__c, 'UTF-8');
    
    string queryString = '';
    
    queryString += OAuth.CONSUMER_KEY_PARAM + '=' + parameters.get(OAuth.CONSUMER_KEY_PARAM);
    queryString += '&' + OAuth.NONCE_PARAM + '=' + parameters.get(OAuth.NONCE_PARAM);
    queryString += '&' + OAuth.SIGNATURE_METHOD_PARAM + '=' + parameters.get(OAuth.SIGNATURE_METHOD_PARAM);
    queryString += '&' + OAuth.TIMESTAMP_PARAM + '=' + parameters.get(OAuth.TIMESTAMP_PARAM);
    queryString += '&' + OAuth.TOKEN_PARAM + '=' + parameters.get(OAuth.TOKEN_PARAM);
    queryString += '&' + OAuth.VERSION_PARAM + '=' + parameters.get(OAuth.VERSION_PARAM);
    //inserting here for lexical ordering
    queryString += '&' + TEST_QUERY_PARAMETER + '=' + TEST_QUERY_VALUE;
    
    
    testBaseString +='&' + EncodingUtil.urlEncode(queryString, 'UTF-8');
    
    test.startTest();
      string baseString = testOAuth.createBaseString(parameters, testRequest);
    test.stopTest();
    
    system.assertEquals(testBaseString, baseString,'the base string was generated incorrectly');
  }
  
  static testMethod void testGenerateSignatureString()
  {
    setup();
    
    string testTimestamp = String.valueOf(DateTime.now().getTime()/1000);
    string testNonce =  String.valueOf(Crypto.getRandomLong());
    
    OAuth testOAuth = new OAuth(testService);
    Map<String,String> parameters = testOAuth.generateOauthParams(testTimestamp,testNonce);
    
    string testUrl = testService.URL__c + '?' + TEST_QUERY_PARAMETER + '=' + TEST_QUERY_VALUE;
    
    HTTPRequest testRequest = new HTTPRequest();
    testRequest.setEndpoint(testUrl);
    testRequest.setMethod('GET');
    
    string testBaseString = testRequest.getMethod();
    testBaseString += '&' + EncodingUtil.urlEncode(testService.URL__c, 'UTF-8');
    
    string queryString = '';
    
    queryString += OAuth.CONSUMER_KEY_PARAM + '=' + parameters.get(OAuth.CONSUMER_KEY_PARAM);
    queryString += '&' + OAuth.NONCE_PARAM + '=' + parameters.get(OAuth.NONCE_PARAM);
    queryString += '&' + OAuth.SIGNATURE_METHOD_PARAM + '=' + parameters.get(OAuth.SIGNATURE_METHOD_PARAM);
    queryString += '&' + OAuth.TIMESTAMP_PARAM + '=' + parameters.get(OAuth.TIMESTAMP_PARAM);
    queryString += '&' + OAuth.TOKEN_PARAM + '=' + parameters.get(OAuth.TOKEN_PARAM);
    //inserting here for lexical ordering
    queryString += '&' + TEST_QUERY_PARAMETER + '=' + TEST_QUERY_VALUE;
    
    testBaseString +='&' + EncodingUtil.urlEncode(queryString, 'UTF-8');
    
    string testSig = EncodingUtil.urlEncode(EncodingUtil.base64encode(Crypto.generateMac('HmacSHA1', Blob.valueOf(testBaseString), Blob.valueOf(testService.Consumer_Secret__c+'&'+ testService.Token_Secret__c))), 'UTF-8');
    
    test.startTest();
      String signature = testOAuth.generateSignatureString(testBaseString);
    test.stopTest();
    
    system.assertEquals(testSig, signature,'the signature string was generated incorrectly');
  }
  
  static testMethod void testCreateHeader()
  {
    final string test1 = 'test';
    
    setup();
    
    OAuth testOAuth = new OAuth(testService);
    
    Map<String,String> testParams = new Map<String,String>();
    
    testParams.put(OAuth.CONSUMER_KEY_PARAM,test1);
    
    string testSig = 'SIGNATURE';
    
    test.startTest();
      string header =testOAuth.createHeader(testParams, testSig); 
    test.stopTest();
    
    string expectedHeader = OAuth.OAUTH_HEADER_PARAM + ' ' + OAuth.CONSUMER_KEY_PARAM + '="' + test1 + '", ' + OAuth.OAUTH_SIGNATURE_PARAM + '="'+testSig+'"';
  }
  
  static testMethod void testGetURL()
  {
    setup();
    
    OAuth testOAuth = new OAuth(testService);
    
    test.startTest();
      string URL = testOAUTH.URL;
    test.stopTest();
    
    system.assertEquals(testService.URL__c,URL,'the URL should match the given one');
  }
  
  static testMethod void testSign()
  {
    setup();
    
    string testUrl = testService.URL__c + '?' + TEST_QUERY_PARAMETER + '=' + TEST_QUERY_VALUE;
    
    HTTPRequest testRequest = new HTTPRequest();
    testRequest.setEndpoint(testUrl);
    testRequest.setMethod('GET');
    
    OAuth testOAuth = new OAuth(testService);
    
    test.startTest();
      testOauth.sign(testRequest);
    test.stopTest();
    
    System.AssertNotEquals(null,testRequest.getHeader(OAuth.OAUTH_HEADER_KEY),'the service should have added an authorization header');
    
  }
}