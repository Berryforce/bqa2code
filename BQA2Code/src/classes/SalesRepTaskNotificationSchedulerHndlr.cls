global class SalesRepTaskNotificationSchedulerHndlr implements SalesRepTaskNotificationScheduler.SalesRepTaskNotificationSchedulerInterface {
	global void execute(SchedulableContext sc) {
        SalesRepTaskNotificationBatchController obj = new SalesRepTaskNotificationBatchController();
        Database.executeBatch(obj);
    }
}