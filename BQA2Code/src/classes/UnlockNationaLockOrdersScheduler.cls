global class UnlockNationaLockOrdersScheduler implements Schedulable {
	public Interface UnlockNationaLockOrdersSchedulerInterface {
		void execute(SchedulableContext sc);
	}
	
	global void execute(SchedulableContext sc) {
		Type targetType = Type.forName('UnlockNationaLockOrdersSchedulerHndlr');
		if(targetType != null) {
			UnlockNationaLockOrdersSchedulerInterface obj = (UnlockNationaLockOrdersSchedulerInterface)targetType.newInstance();
			obj.execute(sc);
		}
	}
}