public class DirectoryFlipBookCommonMethods {

    public static void doDirectoryFlipBooKProcess(List<Directory__c> lstD) {
        list<Directory_Edition__c> insertDE = new list<Directory_Edition__c>();
        map<date, datetime> mapFinalHoliday = DirectoryEditionDatesUpdate.getHolidayList();
        // Existing Directory Editons...(lstDEExist)
        map<Id,String> DirLSAVersionMap=new map<Id,String>();
        for(Directory__c objD : [Select id,(Select Id,LSA_Directory_Version__c from Directory_Editions__r Order by Year__c desc Limit 1)  from Directory__c where id in:lstD]) {
          if(objD.Directory_Editions__r.size()>0 && objD.Directory_Editions__r[0].LSA_Directory_Version__c != null)
          DirLSAVersionMap.put(objD.Id,objD.Directory_Editions__r[0].LSA_Directory_Version__c);
        }
        for(Directory__c objD : lstD) {
            Directory_Edition__c objDE = new Directory_Edition__c();

            objDE.Name = 'Directory Edition';
            objDE.Directory__c = objD.Id;
            if(DirLSAVersionMap.containskey(objD.id))
            objDE.LSA_Directory_Version__c='000'+String.valueof(Integer.valueof(DirLSAVersionMap.get(objD.id))+1);

            //objDE.Book_Status__c = 'NI';
            if(objD.Sales_Lockout__c != null) {
                objDE.sales_Lockout__c = DirectoryEditionDatesUpdate.DateCalculate(objD.Sales_Lockout__c, mapFinalHoliday, true);
                objD.Sales_Lockout__c = objD.Sales_Lockout__c.addYears(1);
            }

            if(objD.EBD__c != null) {
                objDE.EBD__c = DirectoryEditionDatesUpdate.DateCalculate(objD.EBD__c, mapFinalHoliday, true);
                objD.EBD__c = objD.EBD__c.addYears(1);
            }

            if(objD.Ship_Date__c != null) {
                objDE.Ship_Date__c = DirectoryEditionDatesUpdate.ShipDateCalculate(objD.Ship_Date__c, mapFinalHoliday, true);
                objD.Ship_Date__c = objD.Ship_Date__c.addYears(1);
            }

            /*
            if(objD.DCR_Close__c != null) {
                objDE.DCR_Close__c = DirectoryEditionDatesUpdate.DateCalculate(objD.DCR_Close__c, mapFinalHoliday, true);
                objD.DCR_Close__c = objD.Ship_Date__c.addDays(-102);
            }

            if(objD.BOC__c != null) {
                objDE.BOC__c = DirectoryEditionDatesUpdate.DateCalculate(objD.BOC__c, mapFinalHoliday, true);
                objD.BOC__c = objD.Ship_Date__c.addDays(-98);
            }

            if(objD.Final_Service_Order_Due_to_Berry__c != null) {
                objDE.Final_Service_Order_Due_to_Berry__c = DirectoryEditionDatesUpdate.DateCalculate(objD.Final_Service_Order_Due_to_Berry__c, mapFinalHoliday, true);
                objD.Final_Service_Order_Due_to_Berry__c = objD.Ship_Date__c.addDays(-93);
            }

            if(objD.Book_Extract_YP__c != null) {
                objDE.Book_Extract_YP__c = DirectoryEditionDatesUpdate.DateCalculate(objD.Book_Extract_YP__c, mapFinalHoliday, true);
                objD.Book_Extract_YP__c = objD.Ship_Date__c.addDays(-52);
            }
            */

            if(objD.Pub_Date__c != null) {
                objDE.Pub_Date__c = DirectoryEditionDatesUpdate.DateCalculate(objD.Pub_Date__c, null, true);
                if(objDE.Pub_Date__c != null) {
                        integer month = objDE.Pub_Date__c.month();
                        integer year = objDE.Pub_Date__c.year();
                        integer day = 10;
                        integer supUnsupDay = 14;
                        integer supUnsupmonth = month+1;
                        objDE.New_Print_Bill_Date__c = date.newInstance(year,month,day);
                        objDE.DE_Suppress_UnSuppress_OLI__c = date.newInstance(year,supUnsupmonth,supUnsupDay);
                }
                objD.Pub_Date__c = objD.Pub_Date__c.addYears(1);
            }

            if(objD.Sales_Start__c != null) {
                objDE.Sales_Start__c = DirectoryEditionDatesUpdate.DateCalculate(objD.Sales_Start__c, mapFinalHoliday, true);
                objD.Sales_Start__c = objD.Sales_Start__c.addYears(1);
            }

            if(objD.Delivery_Start_Date__c != null) {
                objDE.Delivery_Start_Date__c = DirectoryEditionDatesUpdate.DateCalculate(objD.Delivery_Start_Date__c, mapFinalHoliday, true);
                objD.Delivery_Start_Date__c = objD.Delivery_Start_Date__c.addYears(1);
            }

            /*
            if(objD.Coup_Prelim_Filler__c != null) {
                objDE.Coup_Prelim_Filler__c = DirectoryEditionDatesUpdate.DateCalculate(objD.Coup_Prelim_Filler__c, mapFinalHoliday, true);
                objD.Coup_Prelim_Filler__c = objD.Ship_Date__c.addDays(-15);
            }
            */

            if(objD.Distribution_Method__c != null)
            {
                objDE.Distribution_Method__c = objD.Distribution_Method__c;
            }

            if(objD.Printer__c != null) {
                objDE.Printer__c = objD.Printer__c;
            }

            if(objD.Printer_Location__c != null) {
                objDE.Printer_Location__c = objD.Printer_Location__c;
            }

            if(objD.WP_PROD_MGR__c != null) {
                objDE.WP_PROD_MGR__c = objD.WP_PROD_MGR__c;
            }

            if(objD.YP_PROD_MGR__c != null) {
                objDE.YP_PROD_MGR__c = objD.YP_PROD_MGR__c;
            }

            if(objD.NCR_INDICATOR__c != null) {
                objDE.NCR_INDICATOR__c = objD.NCR_INDICATOR__c;
            }

            if(objD.Branch_Manager__c != null) {
                objDE.Branch_Manager__c = objD.Branch_Manager__c;
            }

            if(objD.ALPHA_COLOR_LEVEL__c != null) {
                objDE.ALPHA_COLOR_LEVEL__c = objD.ALPHA_COLOR_LEVEL__c;
            }

            if(objD.CLASS_COLOR_LEVEL__c != null) {
                objDE.CLASS_COLOR_LEVEL__c = objD.CLASS_COLOR_LEVEL__c;
            }

            if(objD.Directory_Coordinator__c != null) {
                objDE.Directory_Coordinator__c = objD.Directory_Coordinator__c;
            }

            insertDE.add(objDE);
        }

        if(insertDE.size() > 0) {
            insert insertDE;
        }

        if(lstD.size() > 0) {
            update lstD;
        }
        updateBookstatus(insertDE);
    }

    public static void updateBookstatus(list<Directory_Edition__c> insertDE ) {
        system.debug('**********BOOK STATUS********');
        set<ID> dirId = new set<Id>();
        system.debug('--insertDE--->'+insertDE);
        //map<Id,Directory_Edition__c> mapDE = new map<Id,Directory_Edition__c>();
        for(Directory_Edition__c objDE : insertDE) {
            dirId.add(objDE.Directory__c);
            //mapDE.put(objDE.Id,objDE);
        }
        system.debug('==dirID=='+dirId);
        //system.debug('==mapDE=='+mapDE);
        list<Directory__c> lstDir = new List<Directory__c>();
        lstDir = [Select Id,Name,(Select Id,Name,Directory__c,Year__c,Book_Status__c from Directory_Editions__r Order by Year__c desc) from Directory__c where Id IN : dirId];
        list<Directory_Edition__c> newDELst = new List<Directory_Edition__c>();
        for(Directory__c objdir : lstDir) {
            Integer iCount = 0;
            for(Directory_Edition__c objDirE : objdir.Directory_Editions__r) {
                if(iCount == 0) {
                    objDirE.Book_Status__c = 'NI';
                }
                else if(iCount == 1) {
                    objDirE.Book_Status__c = 'BOTS';
                }
                else {
                    objDirE.Book_Status__c = 'BOTS'+'-'+(iCount-1);
                }
                icount++;
                newDELst.add(objDirE);
            }
        }

        if(newDELst.size()>0 ){
            upsert newDELst;
        }
    }
}