public class SelectNonOLIForDiscountController {
    public Order_Line_Item_Discount__c objOLID {get;set;}
    public Case objCase {get;set;}
    public Case objTempCase {get;set;}
    public Decimal totalAmount {get;set;}
    public boolean bFlag {get;set;}
    
    public list<SelectOption> getTwelveItems() {
        list<SelectOption> options = new list<SelectOption>();
        Integer MAXi =13;
        for(Integer i = 0; i < MAXi; i++) {
            options.add(new SelectOption(''+i,''+i));
        }          
        return options;
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        Integer iMax =13;
        Integer iNAT = 2;
        if(CommonMethods.getRedordTypeIdByName(CommonMessages.NationalRT, CommonMessages.caseObjectName) == objCase.RecordTypeId) {
            for(Integer i = 1; i < iNAT; i++) {
                options.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
            }
        }
        else{
            for(Integer i = 0; i < iMax; i++) {
                options.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
            }
        }
        return options;
    }
    
    public SelectNonOLIForDiscountController(ApexPages.StandardController controller) {
        bFlag = true;
        objTempCase = new Case();
        objCase = new Case();
        objOLID = new Order_Line_Item_Discount__c();
        objOLID.Number_of_Billing_Periods_to_discount__c = 0;
        objOLID.BCC_Duraton__c = 0;
        objOLID.total_Credit_amount__c = '0.00';
        objOLID.BCC_Amount__c = 0.00;
        objOLID.Discount_Per_period__c = 0.00;
        objOLID.Total_BCC__c = '0.00';
        totalAmount = 0.00;
        String caseId = controller.getId();
        objTempCase=[select id, case_source__c,case_reason__c from case where id=:caseId];
        string objOLIDiscountId = objOLID.Id;
        if(String.isNotBlank(caseId)) {
            if(ApexPages.currentPage().getParameters().get('mode') == 'true') {
                bFlag = false;
            }
            fetchOLIDValues(caseId);
            fetchCaseValues(caseId);
        } 

    }
    
    public void calculateBCCTotal() {
        system.debug(objOLID.BCC_Amount__c);
        system.debug(objOLID.BCC_Duraton__c);
        objOLID.Total_BCC__c =  String.valueOf(roundValues(objOLID.BCC_Amount__c * objOLID.BCC_Duraton__c));
        totalAmount = Double.valueOf(objOLID.Total_BCC__c) + Double.valueOf(objOLID.total_Credit_amount__c);
    }
    
    public void calculateTotalCredit() {
        system.debug(objOLID.Discount_Per_period__c);
        system.debug(objOLID.Number_of_Billing_Periods_to_discount__c);
        objOLID.total_Credit_amount__c =  String.valueOf(roundValues(Double.valueOf(objOLID.Discount_Per_period__c) * Double.valueOf(objOLID.Number_of_Billing_Periods_to_discount__c)));
        totalAmount = Double.valueOf(objOLID.Total_BCC__c) + Double.valueOf(objOLID.total_Credit_amount__c);
    }
    
    private Decimal roundValues(Decimal fullRate) {
        fullRate = fullRate.setScale(2, RoundingMode.HALF_UP);
        system.debug('rate : '+ fullRate);
        return fullRate;
    }
    
    private boolean validateBeforeSave() {
        boolean bFlag = true;       
        
        if(!CheckValidValue(objTempCase.Case_Reason__c)){
            objTempCase.Case_Reason__c.addError('Select a Case Reason');
            bFlag = false;
        }
        else{
            objOLID.reason__c = objTempCase.Case_Reason__c;
        }
        
        if(!CheckValidValue(objTempCase.Case_Source__c)){
            objTempCase.Case_Source__c.addError('Select a Case Source');
            bFlag = false;
        }
        else{
            objOLID.Source__c = objTempCase.Case_Source__c;  
        }
        
        if(objTempCase.Case_Source__c != null) {
            if(objTempCase.Case_Source__c == 'SALES' || objTempCase.Case_Source__c == 'Sales AM') {
                if(objOLID.User_Responsible__c == null) {
                    objOLID.User_Responsible__c.addError('Select a User Responsible');
                    bFlag = false;
                }
            }
        }
        
        if(totalAmount <= 0) {
            CommonMethods.addError('You can not save a zero Total Credit / Total BCC  value');
            bFlag = false;
        }

        if(Decimal.valueOf(objOLID.total_Credit_amount__c) > 0) {
            if(objOLID.Product_Non_OLI__c == null) {
                objOLID.Product_Non_OLI__c.addError('You must select value');
                bFlag = false;
            }
            
            if(objOLID.Dimension_1__c == null) {
                objOLID.Product_Non_OLI__c.addError('You must select value');
                bFlag = false;
            }
            
            if(objOLID.Dimension_2__c == null) {
                objOLID.Dimension_2__c.addError('You must select value');
                bFlag = false;
            }
            
            if(objOLID.Dimension_3__c == null) {
                objOLID.Dimension_3__c.addError('You must select value');
                bFlag = false;
            }
            
            if(objOLID.Dimension_3__c != null) {
                list<c2g__codaDimension3__c> lstDim3 = [Select Id, Name from c2g__codaDimension3__c where ID =:objOLID.Dimension_3__c];
                String strDim3Name = '';
                for(c2g__codaDimension3__c iterator : lstDim3) {
                    strDim3Name = iterator.Name;
                }
                if(String.isNotBlank(strDim3Name)) {
                    if(strDim3Name.equals('Local')) {
                        if(objOLID.Telco__c == null) {
                            CommonMethods.addError('Telco is mandaroty');
                            objOLID.Telco__c.addError('You must select value');
                            bFlag = false;
                        }//ATP-4231
                    }else if(strDim3Name.equals('Foreign') && String.isNotBlank(objOLID.Telco__c))
                    {
                        CommonMethods.addError('If Billing(LFN) (DIM3) = "Foreign" no Telco should be selected. To issue a Sales Credit Note to a Telco please update Billing(LFN) (DIM3) to Local.');
                        bFlag = false;
                    }
                }
                
            }
            
            if(objOLID.Dimension_2__r.Name == 'Print') {
                if(objOLID.Directory_Edition__c == null) {
                    CommonMethods.addError('Directory Edition is mandatory if Directory Type equals Print ');
                    objOLID.Directory_Edition__c.addError('You must select value');
                    bFlag = false;
                }
            }
        }
        
        return bFlag;
    }
    
    public static boolean CheckValidValue(object TheValue) {
        boolean toReturn = false;
        if(TheValue!=null) {
            string sval=String.valueOf(TheValue);            
            toReturn=!String.isBlank(sval);
        }
        return toReturn;
    }
    
    public pageReference SaveNONAndReturn() {
       //ATP-3914 
        //objCase.Total_Credit__c = 0;
        //system.debug('Before Save' + objOLID.total_Credit_amount__c);
        //system.debug('Before Save' + objCase.Total_Credit__c);
        List<Order_Line_Item_Discount__c> Ordl = [SELECT Additional_BCC__c,BCC_Amount__c,BCC_Duraton__c,Canvass_Primary_Telco__c,Case__c,checked__c,claim_reversed__c,Client_Tier__c,Dimension_1__c,Dimension_2__c,Dimension_3__c,Dimension_4__c,Dimension_2__r.Name,Directory_Edition__c,Discount_Percent__c,Discount_Per_period__c,Discount_Type__c,Id,Maxbcc_Num__c,Max_BCC__c,Max_Credit__c,Name,Number_of_Billing_Periods_to_discount__c,Order_Group__c,Order_Line_Item__c,Product__c,reason__c,SCNTelco__c,Source__c,Telco_Partner_Name__c,Telco__c,Total_BCC__c,total_Credit_amount__c,Total_Credit_num__c,User_Responsible__c,Product_Non_OLI__c,Net_Commission__c,Total_Net_Commission__c FROM Order_Line_Item_Discount__c WHERE Case__c =: objCase.Id AND Order_Line_Item__c = null LIMIT 1];
        List<Order_Line_Item_Discount__c> OLIDList=[SELECT Additional_BCC__c,BCC_Amount__c,BCC_Duraton__c,Canvass_Primary_Telco__c,Case__c,checked__c,claim_reversed__c,Client_Tier__c,Dimension_1__c,Dimension_2__c,Dimension_3__c,Dimension_4__c,Dimension_2__r.Name,Directory_Edition__c,Discount_Percent__c,Discount_Per_period__c,Discount_Type__c,Id,Maxbcc_Num__c,Max_BCC__c,Max_Credit__c,Name,Number_of_Billing_Periods_to_discount__c,Order_Group__c,Order_Line_Item__c,Product__c,reason__c,SCNTelco__c,Source__c,Telco_Partner_Name__c,Telco__c,Total_BCC__c,total_Credit_amount__c,Total_Credit_num__c,User_Responsible__c,Product_Non_OLI__c,Net_Commission__c,Total_Net_Commission__c FROM Order_Line_Item_Discount__c WHERE Case__c =: objCase.Id AND Order_Line_Item__c != null];
        Double existingMonthlyBCCAmt=0.0;
        Double existingTotalBCC=0.0;
        Double existingTotalCredit=0.0;
        for(Order_Line_Item_Discount__c iterator: OLIDList){
            existingMonthlyBCCAmt+=iterator.BCC_Amount__c;
            existingTotalBCC+=Decimal.valueof(iterator.Total_BCC__c);
            existingTotalCredit+=Decimal.valueof(iterator.total_Credit_amount__c);
        }
        calculateBCCTotal();
        calculateTotalCredit();
        
        if(validateBeforeSave()) {
            if(objOLID.Discount_Per_period__c != null) {
                objCase.Sub_Credit__c += objOLID.Discount_Per_period__c;
            }
            if(objOLID.total_Credit_amount__c != null) {
                objCase.Total_Credit__c = existingTotalCredit+Decimal.valueof(objOLID.total_Credit_amount__c);
            }
            if(objOLID.BCC_Amount__c != null) {
                objCase.Total_Monthly_BCC__c = existingMonthlyBCCAmt+objOLID.BCC_Amount__c; 
            }
            
            system.debug('Flag value----' + bFlag);
            system.debug('OLID Total BCC ----' + objOLID.Total_BCC__c);
           // if(bFlag) {
                if(objOLID.Total_BCC__c != null) {
                    objCase.Total_BCC__c = existingTotalBCC+Decimal.valueof(objOLID.Total_BCC__c);
                }
            //}
            
            //For QA-1915, Whenever NON-OLI tool is used, It must route for approval
            
            if(objOLID.Total_BCC__c != null || objOLID.total_Credit_amount__c != null){
                objCase.Needs_SOA_Approval__c = true;
            }
            
            if(objOLID.BCC_Duraton__c != null){
                objCase.BCC_Duration__c = String.valueOf(objOLID.BCC_Duraton__c);
            }
            objOLID.case__c = objCase.Id;
            map<String, Id> mapProduct = new Map<String, Id>();    
            list<Product2> lstProduct = [SELECT Id,Name FROM Product2 WHERE Name IN ('Converted Digital UDAC','Converted Print UDAC')];
            for (Product2 iterator : lstProduct) {
                mapProduct.put(iterator.Name, iterator.Id);
            }
            
            if(Double.valueOf(objOLID.total_Credit_amount__c) > 0 && objCase.Net_Commission__c != null){
                objOLID.Net_Commission__c = true;
                objOLID.Total_Net_Commission__c = Double.valueOf(objOLID.total_Credit_amount__c) * Double.valueOf(objCase.Net_Commission__c) / 100;
                objOLID.Total_Net_Commission__c = roundValues(objOLID.Total_Net_Commission__c);
            }
            
            if(mapProduct.size() > 0) {
                if(objOLID.Product_Non_OLI__c == 'Print' && objOLID.Product_Non_OLI__c != '')
                {
                    objOLID.Product__c = mapProduct.get('Converted Print UDAC');
                }
                else if(objOLID.Product_Non_OLI__c == 'Digital' && objOLID.Product_Non_OLI__c != '')
                {
                    objOLID.Product__c = mapProduct.get('Converted Digital UDAC');
                }
            }
            if(Ordl.size()>0){
                // for(Order_Line_Item_Discount__c tempOLIDIterator : Ordl){
                    // system.debug('Iterator Olid ----' + tempOLIDIterator);
                    // if(tempOLIDIterator.BCC_Amount__c > 0 || tempOLIDIterator.Discount_Per_period__c == 0.00){
                        // objOLID.Number_of_Billing_Periods_to_discount__c = 0;
                        // objOLID.Product_Non_OLI__c = '';
                        // objOLID.Dimension_1__c = null;
                        // objOLID.Dimension_2__c = null;
                        // objOLID.Dimension_3__c = null;
                        // objOLID.Dimension_4__c = null;
                        // objOLID.Product__c = null;                       
                    // }
                // }
                objOLID.id = Ordl[0].id;
                update objOLID;
            }
            else{
                insert objOLID;
            }
            update objCase;
        }
        else {
            return null;
        }
        pageReference pr;
        pr = new pageReference('/'+objCase.Id);
        pr.setRedirect(true);
        return pr;
    }
    
    private void fetchOLIDValues(String CaseIdsstr){
        
        List<Order_Line_Item_Discount__c> tempOLID=[Select Id, Name, Order_Line_Item__c, BCC_Amount__c, BCC_Duraton__c, Case__c, Discount_Per_period__c, Discount_Percent__c, Discount_Type__c, Effective_Date__c, Issue_as_Single_Credit__c, Max_BCC__c, Max_Credit__c, Number_of_Billing_Periods_to_discount__c, Source__c, Total_BCC__c, User_Responsible__c, checked__c, date_of_issuance__c, reason__c, total_Credit_amount__c, Line_item_total_value__c, Order_Group__c, Credit_Amount_Reversed__c, Order_Line_Item_Canvass__c, Canvass_Primary_Telco__c, Overcompensated__c, Maxbcc_Num__c, Total_Credit_num__c, Client_Tier__c, OLI_Billing_Partner_Code__c, Directory_Edition__c, Line_Item_History__c, Product__c, claim_reversed__c, Issue_Month__c, Pub_Year__c, Total_Adjustment_For_Ind_Telco_Report__c, Contract_Credit_For_Ind_Telco_Report__c, Date_of_Reversal__c, Date_Created_Reversed__c, Product_Non_OLI__c, Dimension_1__c, Dimension_2__c, Dimension_3__c, Dimension_4__c, Dimension_2__r.Name,Telco__c, Net_Commission__c, Additional_BCC__c, Telco_Partner_Name__c, Credit_Issued_To__c, OLI_Billing_Frequency__c, Total_Net_Commission__c, Credit_Issued_To1__c From Order_Line_Item_Discount__c WHERE Case__c =: CaseIdsstr AND Order_Line_Item__c = null LIMIT 1];
        
        if(tempOLID.size() > 0){
            objOLID = tempOLID[0];
        }
            for(Order_Line_Item_Discount__c iterator: tempOLID){
                objTempCase.Case_Source__c = iterator.Source__c;
                objTempCase.Case_Reason__c = iterator.reason__c;
            }
    }   
    private void fetchCaseValues(String strCaseId) {
    
        objCase = [SELECT Id, CaseNumber, ContactId, AccountId, SuppliedName, SuppliedEmail, SuppliedPhone, SuppliedCompany, Type, RecordTypeId,
            Status, Reason, Origin, Subject, Priority, Description, IsClosed, ClosedDate, HasCommentsUnreadByOwner, HasSelfServiceComments,  
            CreatedDate, CreatedById, Case_Category__c, Case_Source__c, Customer_Satisfied__c, Main_Listed_Phone_Number__c, Received_From__c, 
            Resolution_Note__c, Total_Credit_Note__c, User_Responsible_del__c, Digital_Product_Requirement__c, Account.Name, 
            account.account_manager__c,account.Account_Manager__r.Name, account.account_number__c, account.phone, account.primary_canvass__c, 
            Additional_BCC__c, Contracted_Total_Value__c, Risk_Dollars_Adjusted__c, Sub_Credit__c, Total_BCC__c, Total_Credit__c, Total_Max_BCC__c,
            Total_Monthly_BCC__c, recordType.developername, Total_Contract_Value__c, Adjustment_Against_Contract__c, of_Contract_Adjusted__c,RecordType.Name, account.Primary_Canvass__r.Name,Net_Commission__c  
            FROM Case WHERE Id = :strCaseId Limit 1];   
        if(objCase.Total_BCC__c == null) {
            objCase.Total_BCC__c = 0.00;
        }
        if(objCase.Total_Max_BCC__c == null) {
            objCase.Total_Max_BCC__c = 0.00;
        }
        if(objCase.Sub_Credit__c == null) {
            objCase.Sub_Credit__c = 0.00;
        }
        if(objCase.Total_Credit__c == null) {
            objCase.Total_Credit__c = 0.00;
        }
        if(objCase.Total_Monthly_BCC__c == null) {
            objCase.Total_Monthly_BCC__c = 0.00;
        }
    }  
}