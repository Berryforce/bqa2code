@isTest(SeeAllData=true)
   private class SiteBalancePay_Test {




    public static testmethod void testSiteBalancePayController1(){
        // --------------- Set up records for test scenarios ------------------
        
        // Check to make sure settings record has been created, and PC is in Simulation Mode (to
        // prevent API callouts from being made during test method execution.
        
        pymt__Settings__c settings = new pymt__Settings__c();
        List<pymt__Settings__c> settingsList = [ select name, pymt__Gateway_Simulation_Mode__c from pymt__Settings__c ];
        if (settingsList.size() > 0) {
            settings = settingsList[0];
            settings.pymt__Gateway_Simulation_Mode__c = true;    
            update settings;   
        }
        else {
            
            settings.name = 'ApexTestSettings';
            settings.pymt__Gateway_Simulation_Mode__c = true;                    
            insert settings;
        }
        
        // Add processor connections
        pymt__Processor_Connection__c connection1 = new pymt__Processor_Connection__c(
                name = 'ApexTestProcessorConnection1',
                pymt__PaymentConnect_Setup__c = settings.id,
                pymt__Assigned_To_Terminal__c = true,
                pymt__Default_Connection__c = true,
                pymt__Processor_Id__c = 'Authorize.Net',
                pymt__Enabled_Card_Types__c = 'Visa;Mastercard',
                pymt__Authnet_Enable_Echecks__c = true);
            Database.SaveResult sr =    Database.insert(connection1);
            System.assert(sr.isSuccess(), 'Error inserting test processor connection object. '+sr);     
     
        Contact[] con = [select Id, AccountId,Email, Account.X3l_External_ID__c, Account.Account_Number__c from Contact where Account.Account_Number__c != null limit 1];
        System.Debug('Size of con : ' + con.size());
        Account[] acct = [ select Id, X3l_External_ID__c, Account_Number__c from Account where Id =: con[0].AccountId limit 1]; 
        //system.assertEquals(acct.size() > 0, True);
        Account[] acctLegacy = [ select Id, Account_Number__c, X3l_External_ID__c from Account where X3l_External_ID__c != null limit 1];
        //system.assertEquals(acctLegacy.size() > 0, true);
        system.debug(acctLegacy);
        PageReference pageRef = Page.SiteBalancePay;
        Test.setCurrentPage(pageRef);   
    
        //less than 5 in length
        SiteBalancePayController controller = new SiteBalancePayController();

        //search for an account with query criteria less than 5
        controller.accountNumber = '5';
        controller.searchBalance();
        //search for account number that is empty
        controller.accountNumber = '';
        controller.searchBalance();
        controller.accountNumber = '8888888';
        controller.searchBalance();
        controller.accountNumber = '18888888';
        controller.searchBalance();
        controller.accountNumber = acctLegacy[0].X3l_External_ID__c;
        controller.searchBalance();
        controller.payNow();
        controller.accountNumber = acct[0].Account_Number__c;
        controller.searchBalance();
        List<selectOption> so = controller.contactOptionList;
        controller.cancelCreateContact();
        //pay without an amount
        controller.payNow();
        controller.createContact();
        controller.contactEmail = 'dd@apex.com';
        controller.contactlastname = 'Apex';
        controller.payNow();
       //invalid phone and no first name
        controller.contactPhone = '222-222-22224444';
        controller.findContact();
        controller.payNow();
        //validate first name
        controller.contactfirstname = 'DD';
        controller.findContact();
        controller.payNow();
        //validate phone
        controller.contactPhone = '222-222-2222';
        controller.findContact();
        controller.payAmount = 4;
        controller.payNow();
        //test with zero amount
        controller.contactPhone = '222-222-2222';
        controller.findContact();
        controller.payAmount = 0;
        controller.payNow();
        //find with existing email
        controller.contactPhone = '222-222-2222';
        controller.contactEmail = con[0].email;
        controller.findContact();
        controller.payAmount = 4;
        controller.payNow();


    }
    
    public static testmethod void testSiteBalancePayController2() {
        
        // --------------- Set up records for test scenarios ------------------
        
        // Check to make sure settings record has been created, and PC is in Simulation Mode (to
        // prevent API callouts from being made during test method execution.
        
        pymt__Settings__c settings = new pymt__Settings__c();
        List<pymt__Settings__c> settingsList = [ select name, pymt__Gateway_Simulation_Mode__c from pymt__Settings__c ];
        if (settingsList.size() > 0) {
            settings = settingsList[0];
            settings.pymt__Gateway_Simulation_Mode__c = true;    
            update settings;   
        }
        else {
            
            settings.name = 'ApexTestSettings';
            settings.pymt__Gateway_Simulation_Mode__c = true;                    
            insert settings;
        }
        
        // Add processor connections
        pymt__Processor_Connection__c connection1 = new pymt__Processor_Connection__c(
                name = 'ApexTestProcessorConnection1',
                pymt__PaymentConnect_Setup__c = settings.id,
                pymt__Assigned_To_Terminal__c = true,
                pymt__Default_Connection__c = true,
                pymt__Processor_Id__c = 'Authorize.Net',
                pymt__Enabled_Card_Types__c = 'Visa;Mastercard',
                pymt__Authnet_Enable_Echecks__c = true);
            Database.SaveResult sr =    Database.insert(connection1);
            System.assert(sr.isSuccess(), 'Error inserting test processor connection object. '+sr);     
     
        Account[] acct = [ select Id, X3l_External_ID__c, Account_Number__c from Account where Account_Number__c != null limit 1]; 
        //system.assertEquals(acct.size() > 0, True);
        Account[] acctLegacy = [ select Id, Account_Number__c, X3l_External_ID__c from Account where X3l_External_ID__c != null limit 1];
        //system.assertEquals(acctLegacy.size() > 0, true);
        system.debug(acctLegacy);
        PageReference pageRef = Page.SiteBalancePay;
        Test.setCurrentPage(pageRef);   
    
        //less than 5 in length
        SiteBalancePayController controller = new SiteBalancePayController();
        //search for an account with query criteria less than 5
        controller.accountNumber = '5';
        controller.searchBalance();
        //search for account number that is empty
        controller.accountNumber = '';
        controller.searchBalance();
        controller.accountNumber = '8888888';
        controller.searchBalance();
        controller.accountNumber = '18888888';
        controller.searchBalance();
        controller.accountNumber = acctLegacy[0].X3l_External_ID__c;
        controller.searchBalance();
        controller.accountNumber = acctLegacy[0].Account_Number__c;
        controller.searchBalance();
        List<selectOption> so = controller.contactOptionList;
        controller.cancelCreateContact();
        //pay without an amount
        controller.payNow();
        controller.createContact();
        controller.contactEmail = 'dd@apex.com';
        controller.contactlastname = 'Apex';
       //invalid phone and no first name
        controller.contactPhone = '222-222-22224444';
        controller.findContact();
        //validate first name
        controller.contactfirstname = 'DD';
        controller.findContact();
        //validate phone
        controller.contactPhone = '222-222-2222';
        controller.findContact();
        controller.payAmount = 4;
        controller.payNow();
    
        
    }

}