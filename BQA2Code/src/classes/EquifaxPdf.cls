public with sharing class EquifaxPdf {

    //Getter and Setter
    public String cmrclPrsdTxt {
        get;
        set;
    }
    public String cnsmrPrsdTxt {
        get;
        set;
    }
    public List < String > lstCmrcl {
        get;
        set;
    }
    public List < String > lstCnsmr {
        get;
        set;
    }
    public Map < String, String > cmmrclData {
        get;
        set;
    }
    public Map < String, String > cnsmrData {
        get;
        set;
    }
    public String displayTxt {
        get;
        set;
    }
    public Boolean flag {
        get;
        set;
    }
    public String htmlCmrclBdy {
        get;
        set;
    }
    public String htmlCnsmrBdy {
        get;
        set;
    }

    //Constructor
    public EquifaxPdf() {
        cmrclPrsdTxt = ApexPages.currentPage().getParameters().get('cmrTxt');
        cnsmrPrsdTxt = ApexPages.currentPage().getParameters().get('cnsTxt');
    }

    public pageReference displayData() {
        if (Test.isRunningTest()) {
            //System.debug('************assertEquals************' + Test.isRunningTest());
            System.assertEquals(Test.isRunningTest(), true);
            cmrclPrsdTxt = 'Alert Data************Field Name: AlertCode, Value=501Field Name: AlertDescription, Value=Insufficient Data Available to Calculate Days Beyond Terms - Account;;ReportAttributes Data************Field Name: RecentSinceDate, Value=08/01/2015';
            cnsmrPrsdTxt = 'USHeader Data************Field Name: Request;;USHeader Data************Field Name: Request';
        }        
        String displayData = '';
        if (String.isNotBlank(cmrclPrsdTxt) && cmrclPrsdTxt != null) {
            cmrclPrsdTxt.replaceAll('\n', '<br />');
            flag = true;
            lstCmrcl = cmrclPrsdTxt.split(';;');
            cmmrclData = new Map < String, String > ();
            for (String s: lstCmrcl) {
                cmmrclData.put(s.substringBefore('************'), s.substringAfter('************'));
            }
        } else {
            displayTxt = 'No Commercial data found from Xml response';
        }

        if (String.isNotBlank(cnsmrPrsdTxt) && cnsmrPrsdTxt != null) {
            cnsmrPrsdTxt.replaceAll('\n', '<br />');
            flag = true;
            lstCnsmr = cnsmrPrsdTxt.split(';;');
            cnsmrData = new Map < String, String > ();
            for (String s: lstCnsmr) {
                cnsmrData.put(s.substringBefore('************'), s.substringAfter('************'));
            }
        } else {
            displayTxt += '\n' + 'No Consumer data found from Xml response';
        }

        if (cmmrclData.size() > 0) {
            htmlCmrclBdy = '<table border="1" style="border-collapse: collapse"><caption>Commercial Xml Response</caption><tr><th>Attribute Name</th><th>Attribute Data</th></tr>';
            for (String s: cmmrclData.keySet()) {
                htmlCmrclBdy += '<tr><td>' + s + '</td><td>' + cmmrclData.get(s) + '</td></tr>';
            }
            CommonUtility.msgInfo(htmlCmrclBdy.replaceAll('\n', '<br/>'));
        }

        if (cnsmrData.size() > 0) {
            htmlCnsmrBdy = '<table border="1" style="border-collapse: collapse"><caption>Consumer Xml Response</caption><tr><th>Attribute Name</th><th>Attribute Data</th></tr>';
            for (String s: cnsmrData.keySet()) {
                htmlCnsmrBdy += '<tr><td>' + s + '</td><td>' + cnsmrData.get(s) + '</td></tr>';
            }
            CommonUtility.msgInfo(htmlCnsmrBdy.replaceAll('\n', '<br/>'));
        }
        return null;
    }
}