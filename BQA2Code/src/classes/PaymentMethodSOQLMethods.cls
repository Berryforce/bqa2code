public with sharing class PaymentMethodSOQLMethods {
    public static List<pymt__Payment_Method__c> getExpireSoonCreditCardDetails(set<Id> setContactId, string month, string year) {
        return [SELECT Id,  Name,  pymt__Contact__c, pymt__Billing_City__c, pymt__Billing_Country__c, 
                pymt__Billing_Email__c, pymt__Billing_First_Name__c, pymt__Billing_Last_Name__c, pymt__Billing_Phone__c, 
                pymt__Billing_Postal_Code__c, pymt__Billing_Salutation__c, pymt__Billing_State__c, pymt__Billing_Street__c, 
                pymt__Card_Type__c, pymt__Customer_Profile_Id__c, pymt__Default__c, pymt__Expiration_Month__c, 
                pymt__Expiration_Status__c, pymt__Expiration_Year__c, pymt__Last_4_Digits__c, pymt__Payment_Type__c, 
                pymt__Processor_Connection__c, pymt__Profile_Id__c, pymt__Type__c, 
                Expiration_Status__c, pymt__Contact__r.Email , pymt__Contact__r.Accountid , pymt__Contact__r.Name 
                FROM pymt__Payment_Method__c  
                WHERE pymt__Contact__c in :setContactId 
                AND pymt__Expiration_Year__c = :year 
                AND pymt__Expiration_Month__c = :month 
                AND pymt__Default__c = True];
    }
    
    public static List<pymt__Payment_Method__c> getExpireSoonCreditCardDetailsByExipiryDate(set<Id> setContactId, Date expiryDate) {
        return [SELECT Id,  Name,  pymt__Contact__c, pymt__Billing_City__c, pymt__Billing_Country__c, 
                pymt__Billing_Email__c, pymt__Billing_First_Name__c, pymt__Billing_Last_Name__c, pymt__Billing_Phone__c, 
                pymt__Billing_Postal_Code__c, pymt__Billing_Salutation__c, pymt__Billing_State__c, pymt__Billing_Street__c, 
                pymt__Card_Type__c, pymt__Customer_Profile_Id__c, pymt__Default__c, pymt__Expiration_Month__c, 
                pymt__Expiration_Status__c, pymt__Expiration_Year__c, pymt__Last_4_Digits__c, pymt__Payment_Type__c, 
                pymt__Processor_Connection__c, pymt__Profile_Id__c, pymt__Type__c,  
                Expiration_Status__c, pymt__Contact__r.Email , pymt__Contact__r.Accountid , pymt__Contact__r.Name 
                FROM pymt__Payment_Method__c  
                WHERE pymt__Contact__c in :setContactId 
                AND Expiration_Date__c = :expiryDate 
                AND pymt__Default__c = True];
    }
    
    public static List<pymt__Payment_Method__c> getDefaultPymtMethodsByContactIds(set<Id> setContactIds) {
        return [SELECT Id, Name FROM pymt__Payment_Method__c WHERE pymt__Contact__c in :setContactIds and pymt__Default__c = true];
    }
}