@IsTest
public class ModificationOrderLineItemSOQLMethodsTest {
    @isTest static void ModificationOrderLineItemSOQLMethodsCoverage() {
		Account telcoAcct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcct.Id);
        Canvass__c objCanvss = TestMethodsUtility.generateCanvass(telco);
	    Date effectiveDate;
	    set<Id> accountIds = new set<id>();
	    Account newAccount = TestMethodsUtility.createAccount('customer');
	    Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Product2 newProduct = TestMethodsUtility.createproduct();
        Opportunity newOpportunity = TestMethodsUtility.createOpportunity('new', newAccount);
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        Directory__c directory = TestMethodsUtility.createDirectory();
       // Directory_Section__c dirSection = TestMethodsUtility.createDirectorySection(directory);

        Modification_Order_Line_Item__c mol = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI.id);
        mol.Order_Group__c = newOrderSet.Id;
        insert mol;
	    accountIds.add(newAccount.Id);
	    
	    ModificationOrderLineItemSOQLMethods.getMOLIBYEffectiveDate(system.today());
	    ModificationOrderLineItemSOQLMethods.getMOLIBYAccountIds(accountIds);
	    ModificationOrderLineItemSOQLMethods.getMOLIBYOLI(new Set<ID> {newOrLI.Id}, system.today());
	    ModificationOrderLineItemSOQLMethods.getMOLIBasedOnCanvass(newAccount.Id, objCanvss.Id);
	    ModificationOrderLineItemSOQLMethods.getMOLIBasedOnID(new Set<ID> {mol.Id});
	    ModificationOrderLineItemSOQLMethods.getMOLIBasedOnList(new List <Modification_Order_Line_Item__c> {mol});
    }    
}