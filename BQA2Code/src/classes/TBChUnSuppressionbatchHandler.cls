global class TBChUnSuppressionbatchHandler implements TBCscUnSupressionBatchScheduler.TBCscUnSupressionBatchSchedulerInterface {
	global void execute(SchedulableContext sc) {  
	 	list<Directory__c> lstDirectory = DirectorySOQLMethods.fetchDirectoryandBOTSDE();
	 	set<Id> setDirId = new set<Id>();
	 	if(lstDirectory.size()> 0) {
	 		for(Directory__c iterator : lstDirectory) {
 				if(system.today() >= iterator.Dir_BOTS_Suppress_UnSuppress_OLI__c) {
 					setDirId.add(iterator.Id);
 				}
	 		}
	 	}
	 	if(setDirId.size()> 0) {
	 		TBCcUnsuppressionScopedListingBatch unsuppressBatch = new TBCcUnsuppressionScopedListingBatch(setDirId);
	 		database.executebatch(unsuppressBatch);
	 	}
	 }
}