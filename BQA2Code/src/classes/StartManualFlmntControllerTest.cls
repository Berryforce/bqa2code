@isTest(seeAllData = true)
public class StartManualFlmntControllerTest {

    public static testmethod void startMnlFlmnt() {

        Test.StartTest();

        String rTypId = Label.Video_RT_Id;
        Id accMngrId = System.Label.TestUserPerformanceAdvisorId;
         
        Canvass__c c = CommonUtility.createCanvasTest();
        insert c;

        Account a = CommonUtility.createAccountTest(c);
        a.Account_Manager__c = accMngrId;
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;

        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;

        Order_Line_Items__c oln = CommonUtility.createOLITest(c, a, cnt, oppty, ord, og);
        oln.Status__c = 'new';
        insert oln;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.recordTypeId = rTypId;
        dff.OrderLineItemID__c = oln.Id;
        insert dff;

        PageReference pgRf = Page.StartManualFlmnt;
        Test.setCurrentPage(pgRf);

        ApexPages.StandardController sc = new ApexPages.standardController(dff);
        StartManualFlmntController controller = new StartManualFlmntController(new ApexPages.StandardController(dff));
        ApexPages.currentPage().getParameters().put('id', dff.Id);

        controller.stFlmnt();

        Modification_Order_Line_Item__c mOLI = CommonUtility.createmOLITest(c, a, cnt, oppty, ord, og);
        mOLI.Status__c = 'New';
        insert mOLI;

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.recordTypeId = rTypId;
        dff1.ModificationOrderLineItem__c = MOLI.Id;
        insert dff1;        

        User u = [SELECT Id, Name FROM User WHERE ProfileId = : [SELECT Id from Profile where Name = 'ICR'] and IsActive = true limit 1];

        System.runAs(u) {

            PageReference pgRf1 = Page.StartManualFlmnt;
            Test.setCurrentPage(pgRf1);

            ApexPages.StandardController sc1 = new ApexPages.standardController(dff);
            StartManualFlmntController controller1 = new StartManualFlmntController(new ApexPages.StandardController(dff));
            ApexPages.currentPage().getParameters().put('id', dff.Id);

            controller1.stFlmnt();

            PageReference pgRf2 = Page.StartManualFlmnt;
            Test.setCurrentPage(pgRf2);

            ApexPages.StandardController sc2 = new ApexPages.standardController(dff1);
            StartManualFlmntController controller2 = new StartManualFlmntController(new ApexPages.StandardController(dff1));
            ApexPages.currentPage().getParameters().put('id', dff1.Id);

            controller2.stFlmnt();

        }

        Test.StopTest();

    }

}