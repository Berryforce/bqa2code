@isTest
private class advancedLookUpTest {
    static testMethod void myUnitTest() {
        Test.startTest();
   advancedLookUp Advlook = new advancedLookUp();
         
         Account acc = new Account();
         acc.Name = 'Test';
         //acc.Phone='1234567890';
         insert acc;
         
         Advlook.searchValue  = 'Test';
         Advlook.objectType= 'Account';
         Advlook.search();
         
         Advlook.maxHeight1 = 10;
         Advlook.height=3;
                  
         
         Advlook.search();
         Advlook.selectValue();
         Advlook.hideResults();
         
         Advlook.searchValue  = 'Test Account';
         Advlook.search();
         
         Advlook.searchValue  = 'Test Account';
         Advlook.search();
         
         Advlook.maxHeight1 = 0;
         Advlook.searchValue  = 'Test';
         Advlook.search();
         
         Advlook.searchValue  = '';
         Advlook.search();
         
        Test.stopTest();
    }
   }