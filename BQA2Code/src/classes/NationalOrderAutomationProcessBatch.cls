global class NationalOrderAutomationProcessBatch implements Database.Batchable <sObject> {
  global Database.queryLocator start(Database.BatchableContext bc){
   String SOQL='Select Id FROM National_Staging_Order_Set__c where IsMigratedXtrans__c=true AND Is_Converted__c=false';
    return Database.getQueryLocator(SOQL);
  }
 
  global void execute(Database.BatchableContext bc, list<National_Staging_Order_Set__c> listNO) {
        List<National_Staging_Order_Set__c> NSOSProcess=new List<National_Staging_Order_Set__c>();
        if(listNO.size()>0){
           for(National_Staging_Order_Set__c iterator:[SELECT CMR_Name__r.Is_Active__c,Auto_Number__c,Publication_Code__c, Publication_Company__c, ChildReadyForProcess__c, Directory__r.Name,Directory_Edition__r.National_Billing_Lockout_Date__c,Directory_Edition__r.Name,CMR_Name__r.Name, Client_Name__r.Name,Child_Total__c, Client_Name_Text__c, Client_Name__c, Client_Number__c,CMRClientCombo__c,CMR_Name__c,CMR_Notice__c,CMR_Number__c,CORE_Migration_ID__c,Late_Order__c, Approved_to_Process__c, CreatedById, CreatedDate, Date__c, Directory_Edition_Number__c,Directory_Edition__c,Directory_Number__c,Directory_Version__c,Directory__c, From_Number__c,From_Type__c,Header_Error_Description__c,Id,Is_Converted__c,Is_Ready__c,Name,NAT_Client_Id__c,NAT__c,Non_Elite_CMR__c,OwnerId, To_Type__c,Transaction_ID__c,Publication_Date__c,RAC_Date__c,Ready_for_Process__c,RecordTypeId,Reference_Date__c,SAC_Date__c,State__c,To_Number__c,Transaction_Unique_ID__c,Transaction_Version_ID__c,Transaction_Version__c,TRANS_Code__c,Override__c,CMR_Name__r.National_Credit_Status__c,(SELECT Needs_Review__c,Action__c,Standing_Order__c,Advertising_Data__c, National_Staging_Header__r.CreatedDate,Prior_Directory_Heading_SFDC_ID__c,Prior_UDAC_Group__c,Auto_Number__c,BAS__c,CORE_Migration_ID__c,DAT__c,Delete__c,Listing__r.account__r.recordtype.name,Directory_Heading__c,Discount__c,Full_Rate_f__c,Id,Line_Error_Description__c,Line_Number__c,Listing__c,Seniority_Date__c ,Listing__r.Disconnected__c,Directory_Section__c,Is_Changed__c,Seniority_Date_Reset__c,Name,National_Discount__c,National_Staging_Header__c,Parent_ID__c,Product2__c,Ready_for_Processing__c,National_Graphics_ID__c,Row_Type__c,Sales_Rate__c,Scoped_Listing__c,SEQ__c,SPINS__c,Caption_Header__c,National_Pricing__c,Processed_Order_Line_Item__c,Transaction_Id__c,Transaction_Line_Id__c,Transaction_Unique_ID__c,Transaction_Version_ID__c,Type__c,UDAC__c,Is_Processed__c FROM National_Staging_Line_Items__r) FROM National_Staging_Order_Set__c where Id in: listNO]){
               NSOSProcess.add(iterator);
           }
        }
         if(NSOSProcess.size()>0)
             NationalOrderAutomationProcess.processNationalOrder(NSOSProcess);
  }
   
  global void finish(Database.BatchableContext bc) {
      AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id =:BC.getJobId()];
      /*Map<String, NationalOrderAutomationBatchEmails__c> allemails = NationalOrderAutomationBatchEmails__c.getAll();
      Set<String> emailaddrSet = new Set<String>();
      emailaddrSet.add(a.CreatedBy.Email);
      String emailaddr='';
      for(NationalOrderAutomationBatchEmails__c iter : allemails.values()) {
            emailaddrSet.add(iter.email__c);
        }
     List<String> emailaddrLst=new List<String>();
     emailaddrLst.addall(emailaddrSet);*/
     Set<String> recipientIds = new Set<String>();
     recipientIds.addAll(User_Ids_For_Email__c.getInstance('NationalOrder').User_Ids__c.split(';'));
     recipientIds.add(UserInfo.getUserId());
     String strErrorMessage = '';
      if(a.NumberOfErrors > 0){
          strErrorMessage = a.ExtendedStatus;
        }
        for(String str : recipientIds) {
	        CommonEmailUtils.sendHTMLEmailForTargetObject(str, 'NationalOrderAutomationProcess is ' + a.Status, 'The batch Apex job processed ' + a.TotalJobItems +
          	' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage+'.');
        }  
  }
}