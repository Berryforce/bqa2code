public class SuppressingScopedListingsController {
    public pagereference RunBatch() {
        //database.executebatch(new  SuppressingScopedListingsBatchController(),2000);
        list<Directory__c> lstDirectory = DirectorySOQLMethods.fetchDirectoryandBOTSDE();
        set<Id> setDEId = new set<Id>();
        if(lstDirectory.size()> 0) {
            for(Directory__c iterator : lstDirectory) {
                if(iterator.Directory_Editions__r.size()> 0) {
                    for(Directory_Edition__c iteratorDE : iterator.Directory_Editions__r) {
                        if(system.today() < iterator.Dir_BOTS_Suppress_UnSuppress_OLI__c && iteratorDE.Book_Status__c == 'BOTS') {
                            setDEId.add(iteratorDE.Id);
                        }
                        else if(system.today() > iterator.Dir_BOTS_Suppress_UnSuppress_OLI__c && iteratorDE.Book_Status__c == 'NI') {
                            setDEId.add(iteratorDE.Id);
                        }
                    }
                }
            }
        }
        system.debug('Set of DE ID'+setDEId);
        if(setDEId.size()> 0) {
            TBCbSuppressingScopedListingsBatch suppressBatch = new TBCbSuppressingScopedListingsBatch(setDEId);
            database.executebatch(suppressBatch);
            return (new pagereference('/apex/SuppressingScopedListings').setredirect(true));
        }
        return null;
    }
    public pagereference RunUnsuppressBatch() {
        //database.executebatch(new  SuppressingScopedListingsBatchController(),2000);
        //return (new pagereference('/apex/SuppressingScopedListings').setredirect(true));
        list<Directory__c> lstDirectory = DirectorySOQLMethods.fetchDirectoryandBOTSDE();
        set<Id> setDirId = new set<Id>();
        if(lstDirectory.size()> 0) {
            for(Directory__c iterator : lstDirectory) {
                //if(iterator.Directory_Editions__r.size()> 0) {
                    //for(Directory_Edition__c iteratorDE : iterator.Directory_Editions__r) {
                        if(system.today() >= iterator.Dir_BOTS_Suppress_UnSuppress_OLI__c) {
                            setDirId.add(iterator.Id);
                        }
                    //}
                //}
            }
        }
        if(setDirId.size()> 0) {
            TBCcUnsuppressionScopedListingBatch unsuppressBatch = new TBCcUnsuppressionScopedListingBatch(setDirId);
            database.executebatch(unsuppressBatch);
            return (new pagereference('/apex/SuppressingScopedListings').setredirect(true));
        }
        return null;
    }
}