@isTest
private class DirectoryListingSOQLMethodsTest {

    static testMethod void DirectoryListingSOQLMethodsCoverage() {
        List<String> customerids = new List<String>();
        Set<Id> canvasId = new Set<Id>();
        set<Id> setSLId = new set<Id>(); 
        set<Id> setLstId = new set<Id>(); 
        set<Id> setDirId = new set<Id>();
        set<Id> setDirSecId = new set<Id>();
        set<string> setBusGovIndcs = new set<string>{'Business'};
        set<string> setAreaCodes = new set<string>{'999'};
        set<string> setExchangeCodes = new set<string>{'888'};
        set<string> setMatchedDL = new set<string>{'TestMatching'};
        set<string> setPhone = new set<string>{'(999)888-0112'};
        Account newAccountCustomer = TestMethodsUtility.createAccount('customer');
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccountCustomer.Id);
        Directory_Mapping__c newDirectoryMapping = TestMethodsUtility.createDirectoryMapping(newTelco);
        customerids.add(newAccountCustomer.Id);     
        canvasId.add(newAccountCustomer.Primary_Canvass__c);
        User u = TestMethodsUtility.generateStandardUser();
        Directory__c objDir = TestMethodsUtility.createDirectory();
        setDirId.add(objDir.Id);
        Directory_Section__c objDirSec = TestMethodsUtility.createDirectorySection(objDir);
        setDirSecId.add(objDirSec.Id);
        Listing__c objLst = TestMethodsUtility.generateMainListing();
        insert objLst;
        setLstId.add(objLst.Id);
        Directory_listing__c objDLst = TestMethodsUtility.generateNonOverrideRT();
        objDLst.Name='TestMatching';
        objDLst.SL_strScopedlistingMatched__c ='TestMatching';
        objDLst.Listing__c=objLst.Id;
        objDLst.Follows_Listing_Record2__c='true';
        objDLst.Directory__c = objDir.Id;
        objDLst.Telco_Provider__c=null;
        objDLst.Directory_section__c = objDirSec.Id;
        objDLst.Phone_Number__c = '(999)888-0000';
        objDLst.Bus_Res_Gov_Indicator__c = 'Business';
        objDLst.Disconnected__c = false;
        insert objDLst;
        Directory_listing__c objDLstYp = TestMethodsUtility.generateNonOverrideRT();
        objDLstYp.Listing__c=objLst.Id;
        objDLstYp.Follows_Listing_Record2__c='true';
        objDLstYp.Telco_Provider__c=null;
        objDLstYp.Directory_section__c = objDirSec.Id;
        objDLstYp.Phone_Number__c = '(999)888-0112';
        objDLstYp.Disconnected__c = false;
        insert objDLstYp;
        Directory_listing__c objDLst1 = TestMethodsUtility.generateCaptionMember();
        objDLst1.Listing__c=objLst.Id;
        objDLst1.Follows_Listing_Record2__c='true';
        objDLst1.Caption_header__c=true;
        insert objDLst1;
        Directory_listing__c objDLst2 = TestMethodsUtility.generateCaptionMember();
        objDLst2.Listing__c=objLst.Id;
        objDLst2.Follows_Listing_Record2__c='true';
        objDLst2.Caption_Member__c=true;
        objDLst2.Under_Caption__c=objDLst1.Id;
        insert objDLst2;
        setSLId.add(objDLst.Id);
        setSLId.add(objDLst2.Id);
        System.runAs(u) {
            Test.startTest();
            System.assertNotEquals(null, DirectoryListingSOQLMethods.getDirectoryListingByCanvassID(canvasId));
            DirectoryListingSOQLMethods.getYPCMscopedlistings(setSLId);
            DirectoryListingSOQLMethods.getDirListingByIds(setSLId);
            DirectoryListingSOQLMethods.getScopedListingsByListingIds(setLstId);
            DirectoryListingSOQLMethods.getCMSlsByListingIdsforNormalization(setLstId);
            DirectoryListingSOQLMethods.getMatchedDirectoryListings(setMatchedDL,setDirId);
            DirectoryListingSOQLMethods.getDlsbyUndercaption(setSLId);
            DirectoryListingSOQLMethods.getDirListingMapByIds(setSLId);
            DirectoryListingSOQLMethods.getScopedListingsByAreaExchDirDirSec(setAreaCodes,setExchangeCodes,setDirId,setDirSecId,setBusGovIndcs);
            DirectoryListingSOQLMethods.getYPSL(setPhone,setDirId);
            Test.stopTest(); 
        }
    }
}