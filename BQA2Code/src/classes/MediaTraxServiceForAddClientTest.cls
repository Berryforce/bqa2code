@isTest
public class MediaTraxServiceForAddClientTest{
    public static testmethod void MediaTraxAddClientBatchTest(){
        List<MediaTrax_API_Configuration__c> lstMedia = new list<MediaTrax_API_Configuration__c>();
        MediaTrax_API_Configuration__c objMedia = new MediaTrax_API_Configuration__c(NAME='GetAuthToken',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/authentication.cfc',HEADER_SOAPACTION__C='GetAuthentication',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='GetAuthenticationResponse',RETURN_METHOD__C='GetAuthenticationReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='',PREDICATEVALUE__C='',FIELDS_MAPPING__C='',OPERATION__C='');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='AddClients',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/clients.cfc',HEADER_SOAPACTION__C='AddClients',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='AddClientsResponse',RETURN_METHOD__C='AddClientsReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='',PREDICATEVALUE__C='',FIELDS_MAPPING__C='clientNameList,clientNumberList,address1List,address2List,cityList,stateCodeList,zipCodeList,countryCodeList,languageList,phoneList,faxList,isYellowPageList,priceOverrideList,callRecordingList,mailMasterList,callTransferList,voiceMailList,whisperModeList,costPerCallList,callEmployeeCodeList,maxAdSpendTypeList,maxAdSpendList',OPERATION__C='');
        lstMedia.add(objMedia);
        insert lstMedia;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        Account newAccount = TestMethodsUtility.generateAccount('customer');
        newAccount.P4P_Spending_Cap__c = '12345';
        newAccount.P4P_Spending_Cap_Type__c = 'Y';
        
        insert newAccount;  
        
        system.assertNotEquals(newAccount.ID, null);
        
        MediaTraxServiceForAddClientBatch objACCallout = new MediaTraxServiceForAddClientBatch();
        database.executebatch(objACCallout, 5);
    }
}