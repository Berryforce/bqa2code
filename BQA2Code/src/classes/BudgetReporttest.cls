@isTest(SeeAllData=true)
public class BudgetReporttest
{
    public static testMethod void testBudgetReport() 
    {
        Test.StartTest();
        Canvass__c c=TestMethodsUtility.createCanvass();
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
        //Telco__c objTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id Limit 1];
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Recives_Electronice_File__c=true;
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        objDir.Pub_Date__c=date.today();
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd .New_Print_Bill_Date__c=date.today();
        objDirEd .Bill_Prep__c=date.parse('01/01/2013');
        objDirEd .Pub_Date__c=date.today();
        objDirEd.XML_Output_Total_Amount__c=100;
        insert objDirEd;
        







        Budget__c ObjBudget=TestMethodsUtility.createBudget(objDir,objDirEd);
        insert ObjBudget;
         
        PageReference pageRef = Page.BudgetReport;
        Test.setCurrentPage(pageRef);
        Budget__c bgt=new Budget__c();
        ApexPages.StandardController bgtctrl = new ApexPages.standardController(bgt);
        //Negative Test Case
        BudgetReport controllernew =new BudgetReport(bgtctrl);
        Apexpages.currentpage().getparameters().put('dir',null);
        Apexpages.currentpage().getparameters().put('dirE',null);
        controllernew.showdirectoryLevelReport();
        BudgetReport.MainCompanyReportWrapper mcrw=new BudgetReport.MainCompanyReportWrapper('Jan', null,null,10.00,null,null,null,20.00,null,null,null,20.00,null);
        



          
        //Positive Test Case
        Apexpages.currentpage().getparameters().put('dir',String.valueof(objDir.id));
        Apexpages.currentpage().getparameters().put('dirE',String.valueof(objDirEd.id));
        BudgetReport controller =new BudgetReport(bgtctrl);
        controller.URLdir=objDir.id;
        controller.URLdEdition=objDirEd.id;
        controller.showReport();
        controller.showcompanypanel();
        controller.exportReport();
        controller.getreportyear();
        controller.setyearforreport('2013');
        controller.getyearforreport();
        controller.showdirectoryLevelReport();
        controller.showCompanyLevelReport();
        controller.getdirectories();
        controller.getdirectory();
        controller.setdirectories(objDir.id);
        controller.getdirectoryEdition();
        controller.getdirectoriesEdition();
        controller.setdirectoriesEdition(objDirEd.id);
        controller.populateEdition();
        controller.generateBudgetReport();
        
        controller.yearforreport='2013';
        controller.FetchCompanyData();
        controller.GenerateComapanyLevelReport('2013');
        
        controller.yearforreport='2014';
        controller.FetchCompanyData();
        controller.GenerateComapanyLevelReport('2014');
        
        controller.yearforreport='2015';
        controller.FetchCompanyData();
        controller.GenerateComapanyLevelReport('2015');
        
        controller.yearforreport='0';
        controller.FetchCompanyData();
        
        controller.saveforecast();
    }
}