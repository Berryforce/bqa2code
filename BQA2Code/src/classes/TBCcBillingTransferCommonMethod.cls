public class TBCcBillingTransferCommonMethod{
    public static map<String, Id> getMapOFPeriod() {
        list<c2g__codaPeriod__c> lstPeriord = PeriodSOQLMethods.getCurrentPeriodByName(new set<String>{CommonMethods.getCurrentPeriod()});
        map<String, Id> mapPeriod = new map<String, Id>();
        for(c2g__codaPeriod__c iterator : lstPeriord) {
            mapPeriod.put(iterator.Name, iterator.Id);
        }
        return mapPeriod;
    } 
  /*  public static boolean checkOfZeroLineItems(list<c2g__codaInvoiceLineItem__c> lstILI){
        Integer jcount=0;
         for(c2g__codaInvoiceLineItem__c objILI : lstILI) {
            if(objILI.c2g__NetValue__c<=0){
                jcount++;
            }
         }
         
         if(lstILI.size()==jcount){
          return false;
         }
         else{
            return true;
         }
    }*/
    public static boolean checkFFPeriodDate(Date ffDate){
        if(ffDate<Date.today())
            return false;
        else
            return true;
    }

    public static void BuildWrapperForThirdScreen(List<TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper> SalesInvoiceLineItemWrapperList, 
                                        Map<id,List<order_Line_Items__c>>OLImapForFetchInvoice, Set<id> OLIIdSet, Billing_Transfer_Status__c objBTS) {
        String billingParnterAccountId;
        if(objBTS.BTS_Billing_Partner_Name__c.equalsignorecase('THE BERRY COMPANY')){
           billingParnterAccountId=ObjBTS.BTS_Account__c;
        }
        else{
            billingParnterAccountId=ObjBTS.BTS_Billing_Partner_ID__c;
        }
        
        list<c2g__codaInvoiceLineItem__c> lstSILI = SalesInvoiceLineItemSOQLMethods.getInvoiceLineItemByOLI(OLIIdSet, billingParnterAccountId, objBTS.BTS_Billing_Frequency__c);
        set<date> invoiceDateset = new set<date>();
        for(c2g__codaInvoiceLineItem__c iterator:lstSILI){
            if(!invoiceDateset.contains(iterator.c2g__Invoice__r.c2g__InvoiceDate__c) && SalesInvoiceLineItemWrapperList.size()>0)
                SalesInvoiceLineItemWrapperList.add(new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper(false, null, (objBTS.BTS_Mode_of_Transfer__c=='future' ? false : true)));
            invoiceDateset.add(iterator.c2g__Invoice__r.c2g__InvoiceDate__c);
            if(iterator.Order_Line_Item__r.Media_Type__c.equals('Print') && (iterator.Order_Line_Item__r.Directory_Edition__r.Book_Status__c=='NI' || iterator.Order_Line_Item__r.Directory_Edition__r.Book_Status__c=='BOTS' ||iterator.Order_Line_Item__r.Directory_Edition__r.Book_Status__c=='BOTS-1')){    
              if(objBTS.BTS_Type_of_Transfer__c.equalsIgnoreCase('BillingFrequencyTransfer')){
                if(!invoiceDateset.contains(iterator.c2g__Invoice__r.c2g__InvoiceDate__c) && SalesInvoiceLineItemWrapperList.size() > 0)
                    SalesInvoiceLineItemWrapperList.add(new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper(true, new c2g__codaInvoiceLineItem__c(), (objBTS.BTS_Mode_of_Transfer__c=='future' ? false : true)));
                invoiceDateset.add(iterator.c2g__Invoice__r.c2g__InvoiceDate__c);
                SalesInvoiceLineItemWrapperList.add(new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper(true, iterator, (objBTS.BTS_Mode_of_Transfer__c=='future' ? false : true)));
              }
              else{
                  if(!invoiceDateset.contains(iterator.c2g__Invoice__r.c2g__InvoiceDate__c) && SalesInvoiceLineItemWrapperList.size() > 0)
                    SalesInvoiceLineItemWrapperList.add(new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper(false, new c2g__codaInvoiceLineItem__c(), (objBTS.BTS_Mode_of_Transfer__c=='future' ? false : true)));
                invoiceDateset.add(iterator.c2g__Invoice__r.c2g__InvoiceDate__c);
                //SalesInvoiceLineItemWrapperList.add(new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper(false, iterator, (objBTS.BTS_Mode_of_Transfer__c=='future' ? false : true)));
                SalesInvoiceLineItemWrapperList.add(new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper((objBTS.BTS_Mode_of_Transfer__c=='future' ? true : false), iterator, (objBTS.BTS_Mode_of_Transfer__c=='future' ? false : true)));
              }
            }
            if(iterator.Order_Line_Item__r.Media_Type__c.equals('Digital')) {
                SalesInvoiceLineItemWrapperList.add(new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper((objBTS.BTS_Mode_of_Transfer__c=='future' ? true : false), iterator, (objBTS.BTS_Mode_of_Transfer__c=='future' ? false : true)));
            }
        }
    }

 /*   public static void BuildWrapperForThirdScreen1(List<TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper> SalesInvoiceLineItemWrapperList,Map<id,List<order_Line_Items__c>>OLImapForFetchInvoice,set<id> OLIIdSet){
        list<c2g__codaInvoiceLineItem__c> lstSILI = SalesInvoiceLineItemSOQLMethods.getInvoiceLineItemByOLI(OLIIdSet);
        set<date> invoiceDateset = new set<date>();
        for(c2g__codaInvoiceLineItem__c iterator:lstSILI){
            if(!invoiceDateset.contains(iterator.c2g__Invoice__r.c2g__InvoiceDate__c) && SalesInvoiceLineItemWrapperList.size()>0)
                SalesInvoiceLineItemWrapperList.add(new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper(false,null));
            invoiceDateset.add(iterator.c2g__Invoice__r.c2g__InvoiceDate__c);
            if(iterator.Order_Line_Item__r.Media_Type__c.equals('Print') && (iterator.Order_Line_Item__r.Directory_Edition__r.Book_Status__c=='NI' || iterator.Order_Line_Item__r.Directory_Edition__r.Book_Status__c=='BOTS' ||iterator.Order_Line_Item__r.Directory_Edition__r.Book_Status__c=='BOTS-1')){    
                    if(!invoiceDateset.contains(iterator.c2g__Invoice__r.c2g__InvoiceDate__c) && SalesInvoiceLineItemWrapperList.size()>0)
                        SalesInvoiceLineItemWrapperList.add(new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper());
                    invoiceDateset.add(iterator.c2g__Invoice__r.c2g__InvoiceDate__c);
                    SalesInvoiceLineItemWrapperList.add(new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper(false,iterator));
            }
            if(iterator.Order_Line_Item__r.Media_Type__c.equals('Digital')){
                SalesInvoiceLineItemWrapperList.add(new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper(false,iterator));
            }
        }
        
    }     
    public static void BuildWrapperForThirdScreen(List<TBChBTWrapperHandlerController.SalesInvoiceWrapper> SalesInvoiceWrapperListNew, List<TBChBTWrapperHandlerController.OrderLineItemByDECanvassWrapper>OliWrapperListFinal,Map<id,List<order_Line_Items__c>>OLImapForFetchInvoice,boolean invoiceWithZeroOLI,set<id> OLIIdSet){
        boolean transfer=false;
        boolean bothOli=false;
        System.debug('==>> '+OLImapForFetchInvoice.keySet());
        list<c2g__codaInvoiceLineItem__c> lstSILI = SalesInvoiceLineItemSOQLMethods.getInvoiceLineItemByOLI(OLIIdSet);
        System.debug('==>> '+lstSILI.size());
        set<Id> setSILIIds = new set<Id>();
        List<TBChBTWrapperHandlerController.SalesInvoiceWrapper>  SalesInvoiceWrapperList = new List<TBChBTWrapperHandlerController.SalesInvoiceWrapper>();
        set<id> OLiIdSetwithGoLiveButNoInvoice = new set<id>();
        map<Id, map<Id, list<c2g__codaInvoiceLineItem__c>>> mapOLISI = new map<Id, map<Id, list<c2g__codaInvoiceLineItem__c>>>();
        map<Id, String> mapSIIDName = new map<Id, String>();
        map<Id, String> mapOLIIDName = new map<Id, String>();
        map<Id, String> mapSIStatus = new map<Id, String>();
        map<Id, Date> mapSIDate = new map<Id, Date>();
        map<Id, Date> mapSIDueDate = new map<Id, Date>();
        map<Id, DateTime> createdDateMap = new map<Id, DateTime>();
        map<Id, Decimal> mapSITotal = new map<Id, Decimal>();
        map<Id, Id> mapSICurrency = new map<Id, Id>();
        map<Id, Id> mapSIAccount = new map<Id, Id>();
        map<Id, String> mapSIAccountName = new map<Id, String>();
        map<Id, Id> mapSIPeriod = new map<Id, Id>();
        map<Id, Id> mapSIOpportunity = new map<Id, Id>();
        map<Id, Integer> mapSIMonthTransfer = new map<Id, Integer>();
        Map<Id,Map<Id,String>> mapSIDescription=new Map<Id,Map<Id,String>>();
        Map<Id,Map<Id,String>> mapSIFrequency=new Map<Id,Map<Id,String>>();
        Map<Id,Map<Id,Decimal>> mapSILIAmount=new Map<Id,Map<Id,Decimal>>();
        map<Id, integer> mapSISuccessfulPayment = new map<Id, integer>();
        map<Id, integer> mapSIPaymentRemaining = new map<Id, integer>();
        for(c2g__codaInvoiceLineItem__c iterator : lstSILI) {
            setSILIIds.add(iterator.Id);
            mapSIIDName.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.Name);
            mapOLIIDName.put(iterator.Order_Line_Item__c, iterator.Order_Line_Item__r.Name);
            mapSIStatus.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.c2g__InvoiceStatus__c);
            mapSIDate.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.c2g__InvoiceDate__c);
            mapSIDueDate.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.c2g__DueDate__c);
            createdDateMap.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.CreatedDate); 
            mapSITotal.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.c2g__NetTotal__c);
            mapSIOpportunity.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.c2g__Opportunity__c);
            mapSIPeriod.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.c2g__Period__c);
            mapSICurrency.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.c2g__InvoiceCurrency__c);
            mapSIAccount.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.c2g__Account__c);
            mapSIAccountName.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.c2g__Account__r.name); 
            mapSISuccessfulPayment.put(iterator.c2g__Invoice__c, Integer.valueOf(iterator.c2g__Invoice__r.SI_Successful_Payments__c));
            mapSIPaymentRemaining.put(iterator.c2g__Invoice__c, Integer.valueOf(iterator.c2g__Invoice__r.SI_Payments_Remaining__c));
            //mapSIDescription.put(iterator.Order_Line_Item__c, iterator.c2g__Invoice__r.c2g__InvoiceDescription__c); 
            mapSIMonthTransfer.put(iterator.c2g__Invoice__c, (iterator.c2g__Invoice__r.Total_Month_Invoice__c == null ? 1 : Integer.valueOf(iterator.c2g__Invoice__r.Total_Month_Invoice__c)));
            if(!mapOLISI.containsKey(iterator.Order_Line_Item__c)) {
                mapOLISI.put(iterator.Order_Line_Item__c, new map<Id, list<c2g__codaInvoiceLineItem__c>>());
            }
            if(!mapOLISI.get(iterator.Order_Line_Item__c).containsKey(iterator.c2g__Invoice__c)) {
                mapOLISI.get(iterator.Order_Line_Item__c).put(iterator.c2g__Invoice__c, new list<c2g__codaInvoiceLineItem__c>());
            }
            mapOLISI.get(iterator.Order_Line_Item__c).get(iterator.c2g__Invoice__c).add(iterator); 

            if(!mapSIDescription.containsKey(iterator.Order_Line_Item__c)) {
                mapSIDescription.put(iterator.Order_Line_Item__c, new Map<Id, string>());
            }
                mapSIDescription.get(iterator.Order_Line_Item__c).put(iterator.c2g__Invoice__c, iterator.c2g__LineDescription__c);
                
            if(!mapSIFrequency.containsKey(iterator.Order_Line_Item__c)) {
                mapSIFrequency.put(iterator.Order_Line_Item__c, new Map<Id, string>());
            }
                mapSIFrequency.get(iterator.Order_Line_Item__c).put(iterator.c2g__Invoice__c, iterator.Billing_Frequency__c);                   
                
            if(!mapSILIAmount.containsKey(iterator.Order_Line_Item__c)) {
                mapSILIAmount.put(iterator.Order_Line_Item__c, new Map<Id, Decimal>());
            }
                mapSILIAmount.get(iterator.Order_Line_Item__c).put(iterator.c2g__Invoice__c, iterator.c2g__NetValue__c);                    
        }
        if (OLImapForFetchInvoice.size() > 0) { 
            for (ID editionId: OLImapForFetchInvoice.keySet()) {
                 List < TBChBTWrapperHandlerController.SubSalesInvoiceWrapper > SubSalesItemWrapper1 = new List < TBChBTWrapperHandlerController.SubSalesInvoiceWrapper > ();
                    for(Order_Line_Items__c tempOLI : OLImapForFetchInvoice.get(editionId)) {
                    // System.debug('Diane #############Canvass__r.Billing_Entity__c'+tempOLI.Canvass__r.Billing_Entity__c );
                    List < TBChBTWrapperHandlerController.SubSalesInvoiceWrapper > SubSalesItemWrapper = new List < TBChBTWrapperHandlerController.SubSalesInvoiceWrapper > ();
                        System.debug('Diane #############tempOLI.Media_Type__c'+tempOLI.Media_Type__c+'---------tempOLI.Directory_Edition__r.Book_Status__c--'+tempOLI.Directory_Edition__r.Book_Status__c);
                        if(tempOLI.Media_Type__c.equals('Print'))
                        {
                            System.debug('Diane-1247 |||||||| in if tempOLI.Directory_Edition__r.Book_Status__c '+tempOLI.Directory_Edition__r.Book_Status__c);
                            if(tempOLI.Directory_Edition__r.Book_Status__c=='NI' || tempOLI.Directory_Edition__r.Book_Status__c=='BOTS' ||tempOLI.Directory_Edition__r.Book_Status__c=='BOTS-1')
                            {
                                system.debug('----mapOLISI.get(tempOLI.Id)-----'+mapOLISI);
                                system.debug('----tempOLI.Id-----'+tempOLI.Id);
                                system.debug('----mapOLISI.get(tempOLI.Id)-----'+mapOLISI.get(tempOLI.Id));
                                //system.debug('----------'+mapOLISI.get(tempOLI.Id).keySet());
                                if(mapOLISI.get(tempOLI.Id) != null) {
                                    for(ID strSIId : mapOLISI.get(tempOLI.Id).keySet()) {                               
                                        TBChBTWrapperHandlerController.SubSalesInvoiceWrapper tempObj=new TBChBTWrapperHandlerController.SubSalesInvoiceWrapper(Double.valueOf(mapSILIAmount.get(tempOLI.Id).get(strSIId)),mapSIFrequency.get(tempOLI.Id).get(strSIId), mapSIAccountName.get(strSIId),mapSIAccount.get(strSIId), mapSIOpportunity.get(strSIId),mapSIPeriod.get(strSIId),  mapSICurrency.get(strSIId),mapSIIDName.get(strSIId),mapSIDate.get(strSIId),mapSIDueDate.get(strSIId),Double.valueOf(mapSITotal.get(strSIId)),mapSIStatus.get(strSIId),strSIId,tempOLI,mapSIMonthTransfer.get(strSIId),mapOLISI.get(tempOLI.Id).get(strSIId),mapSISuccessfulPayment.get(strSIId),mapSIPaymentRemaining.get(strSIId),transfer);
                                        SubSalesItemWrapper.add(tempObj);
                                        
                                    }
                                        bothOli=true;
                                }
                                else
                                    OLiIdSetwithGoLiveButNoInvoice.add(tempOLI.Id);
                            }
                            if(SubSalesItemWrapper.size()>0)
                                SubSalesItemWrapper1.addAll(SubSalesItemWrapper);
                        }
                        
                        else if(tempOLI.Media_Type__c=='Digital')
                        {
                            if(mapOLISI.get(tempOLI.Id) != null) {
                                for(ID strSIId : mapOLISI.get(tempOLI.Id).keySet()) 
                                {
                                    if(createdDateMap.get(strSIId)>=Date.today().addMonths(-24) && createdDateMap.get(strSIId)<Date.today().adddays(1) ){
                                        TBChBTWrapperHandlerController.SubSalesInvoiceWrapper tempObj=new TBChBTWrapperHandlerController.SubSalesInvoiceWrapper(Double.valueOf(mapSILIAmount.get(tempOLI.Id).get(strSIId)),mapSIFrequency.get(tempOLI.Id).get(strSIId),mapSIAccountName.get(strSIId),mapSIAccount.get(strSIId), mapSIOpportunity.get(strSIId),  mapSIPeriod.get(strSIId),mapSICurrency.get(strSIId),mapSIIDName.get(strSIId),mapSIDate.get(strSIId),mapSIDueDate.get(strSIId),Double.valueOf(mapSITotal.get(strSIId)),mapSIStatus.get(strSIId),strSIId,tempOLI,mapSIMonthTransfer.get(strSIId),mapOLISI.get(tempOLI.Id).get(strSIId),mapSISuccessfulPayment.get(strSIId),mapSIPaymentRemaining.get(strSIId),transfer);
                                        SubSalesItemWrapper.add(tempObj);
                                    }               
                                }
                                bothOli=true;
                            }
                            else
                                OLiIdSetwithGoLiveButNoInvoice.add(tempOLI.Id);
                            if(SubSalesItemWrapper.size()>0)
                                SubSalesItemWrapper1.addAll(SubSalesItemWrapper);
                        }
                        
                    }
                    if(SubSalesItemWrapper1.size()>0)
                        SalesInvoiceWrapperList.add(new TBChBTWrapperHandlerController.SalesInvoiceWrapper(SubSalesItemWrapper1,'Directrory Edition Name',editionId));
            }
            SalesInvoiceWrapperListNew.addAll(SalesInvoiceWrapperList);
            system.debug('-------------SalesInvoiceWrapperList'+SalesInvoiceWrapperList);
            for(TBChBTWrapperHandlerController.SalesInvoiceWrapper parent:SalesInvoiceWrapperList) {
                Map<Date,List<TBChBTWrapperHandlerController.SubSalesInvoiceWrapper>> mapForInvoiceWrapper =new Map<Date,List<TBChBTWrapperHandlerController.SubSalesInvoiceWrapper>>();
                List<TBChBTWrapperHandlerController.SubSalesInvoiceWrapper> wrapListForAdd=new List<TBChBTWrapperHandlerController.SubSalesInvoiceWrapper>();
                for(TBChBTWrapperHandlerController.SubSalesInvoiceWrapper child:parent.subSalesWrapper) {
                    if(!mapForInvoiceWrapper.containsKey(child.invoiceDte)) {
                        mapForInvoiceWrapper.put(child.invoiceDte,new List<TBChBTWrapperHandlerController.SubSalesInvoiceWrapper>{child});
                    }
                    else
                        mapForInvoiceWrapper.get(child.invoiceDte).add(child);
                    
                }
                List<Date> forsortList=new List<Date>();
                forsortList.addAll(mapForInvoiceWrapper.keySet());
                forsortList.sort();
                for(Date i:forsortList) {
                    wrapListForAdd.addAll(mapForInvoiceWrapper.get(i));
                    wrapListForAdd.add(new TBChBTWrapperHandlerController.SubSalesInvoiceWrapper());
                }
                system.debug('wrapListForAdd'+wrapListForAdd+'--'+parent.editionName);
                SalesInvoiceWrapperListNew.add(new TBChBTWrapperHandlerController.SalesInvoiceWrapper(wrapListForAdd, parent.editionName,parent.dId));
                system.debug('---1309--'+SalesInvoiceWrapperListNew);
            }          
        }
        if(OLiIdSetwithGoLiveButNoInvoice.size()>0)
        {
           invoiceWithZeroOLI=bothOli;
            Map < Id, List < Order_Line_Items__c >> mapOfOliFinalPage = new Map < Id, List < Order_Line_Items__c >> (); 
            List<Order_Line_Items__c> digitalOliList =new List<Order_Line_Items__c>();                  
            if (OLImapForFetchInvoice.size() > 0){ 
         
                for (ID editionId: OLImapForFetchInvoice.keySet()) {
                        for(Order_Line_Items__c tempOLI : OLImapForFetchInvoice.get(editionId)) 
                        {
                            if(OLiIdSetwithGoLiveButNoInvoice.contains(tempOLI.id)){
                                if(tempOLI.Media_Type__c=='Print'){
                                   // if(directoryEditionIds.contains(tempOLI.Directory_Edition__c)){
                                        if (!mapOfOliFinalPage.containsKey(tempOLI.Directory_Edition__c)) {
                                            mapOfOliFinalPage.put(tempOLI.Directory_Edition__c, new List < Order_Line_Items__c > {tempOLI });
                                        } else
                                            mapOfOliFinalPage.get(tempOLI.Directory_Edition__c).add(tempOLI);
                                       // mapforEditionName.put(tempOLI.Directory_Edition__c, tempOLI.Directory_Edition__r.name);
                                    //}
                                }   
                        
                                else if(tempOLI.Media_Type__c=='Digital')
                                    digitalOliList.add(tempOLI);
                            }
                       }
                        
                }              
            }
            if(digitalOliList.size()>0)
                mapOfOliFinalPage.put('Selected Canvas', digitalOliList);
            if (mapOfOliFinalPage.size() > 0) {

                for (ID editionId: mapOfOliFinalPage.keySet()) {

                    List < TBChBTWrapperHandlerController.OrderLineItemWrapper > SubOLItemWrapper = new List < TBChBTWrapperHandlerController.OrderLineItemWrapper > ();
                    Decimal total=0.0;
                    for (Order_Line_Items__c tempOLI: mapOfOliFinalPage.get(editionId))
                    {
                        TBChBTWrapperHandlerController.OrderLineItemWrapper suboliwrap;
                        if(invoiceWithZeroOLI){
                            suboliwrap= new TBChBTWrapperHandlerController.OrderLineItemWrapper(false,tempOLI);
                            suboliwrap.isChecked=true;
                        }
                        else
                        {
                            suboliwrap= new TBChBTWrapperHandlerController.OrderLineItemWrapper(false,tempOLI);
                            suboliwrap.isChecked=false;                             
                        }
                        SubOLItemWrapper.add(suboliwrap);
                        total=total+tempOLI.UnitPrice__c;
                    }
                    if(SubOLItemWrapper.size()>0)
                        OliWrapperListFinal.add(new TBChBTWrapperHandlerController.OrderLineItemByDECanvassWrapper(editionId,'Directory Edition Name',SubOLItemWrapper));
                }
            }                   
        }       
    
    }*/
    // we will remove this method later
  /*  public static void CheckSubSequentDate_old(List<TBChBTWrapperHandlerController.SalesInvoiceWrapper> SalesInvoiceWrapperList,boolean CheckBoolForInvice,Id OLIIdforInvoice,Date selectedDate) { 
        System.debug('Diane @@@@@ '+OLIIdforInvoice);       
        Order_Line_Items__c tempOLI=[select id,Package_ID_External__c,Parent_ID__c,Parent_ID_of_Addon__c,Package_ID__c,Talus_Go_Live_Date__c,Media_Type__c from Order_Line_Items__c where id=:OLIIdforInvoice]; 
        if(CheckBoolForInvice){

            for(TBChBTWrapperHandlerController.SalesInvoiceWrapper  temp :SalesInvoiceWrapperList){
                for(TBChBTWrapperHandlerController.SubSalesInvoiceWrapper  temps:temp.subSalesWrapper){
                    if(temps.invoiceDte>=selectedDate)
                        temps.isTransfer=true;                                           
                }
            }  
         }      
                 
        if(!CheckBoolForInvice){    
                for(TBChBTWrapperHandlerController.SalesInvoiceWrapper  temp :SalesInvoiceWrapperList){
                    for(TBChBTWrapperHandlerController.SubSalesInvoiceWrapper  temps:temp.subSalesWrapper){
                        if(temps.invoiceDte<=selectedDate)
                            temps.isTransfer=false;
                    }
                }          
        }    
    } */
    public static void CheckSubSequentDate(List<TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper> SalesInvoiceWrapperList,boolean CheckBoolForInvice,Id OLIIdforInvoice,Date selectedDate) { 
        System.debug('Diane @@@@@ '+OLIIdforInvoice);       
        Order_Line_Items__c tempOLI=[select id,Package_ID_External__c,Parent_ID__c,Parent_ID_of_Addon__c,Package_ID__c,Talus_Go_Live_Date__c,Media_Type__c from Order_Line_Items__c where id=:OLIIdforInvoice]; 
        if(CheckBoolForInvice){
            for(TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper temp :SalesInvoiceWrapperList){
                    if(temp.salesinvoicelineitem.c2g__Invoice__r.c2g__InvoiceDate__c>=selectedDate)
                        temp.isTransfer=true;                                           
            }  
         }
                 
        if(!CheckBoolForInvice){    
            for(TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper temp :SalesInvoiceWrapperList){
                   // if(temp.salesinvoicelineitem.c2g__Invoice__r.c2g__InvoiceDate__c>=selectedDate)
                        temp.isTransfer=false;                                           
            }         
        }    
    }      
    public static boolean validateFFCountforProcess(list<c2g__codaCreditNote__c> lstInsertSCN,list<c2g__codaCreditNote__c> lstInsertSCNForCSClaim, List<c2g__codaCreditNoteLineItem__c> SCNLIListForUpdateForCSClaim){
        boolean bflag=true;
        if(SCNLIListForUpdateForCSClaim.size()>36){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'We can not process more than 36 CS claim Sales Credit Note Line Items in one transaction. Please deselect OLI\'s to minimize the count.');
            ApexPages.addMessage(myMsg);
            bflag=false;
        }
        else if(lstInsertSCN.size()>36){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'We can not process more than 36 CS claim Sales Credit Note in one transaction. Please deselect OLI\'s to minimize the count.');
            ApexPages.addMessage(myMsg);
            bflag=false;
        }
        else if(lstInsertSCNForCSClaim.size()>36){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'We can not process more than 36 Sales Credit Note in one transaction. Please deselect OLI\'s to minimize the count.');
            ApexPages.addMessage(myMsg);
            bflag=false;
        }
        return bflag;
    }       
}