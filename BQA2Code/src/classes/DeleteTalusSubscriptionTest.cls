@isTest(seeAllData = true)
public class DeleteTalusSubscriptionTest {

    public static testMethod void fulfillmentCancel_1() {

        Map < String, String > mapPrdt = new Map < String, String > {
            'LocalVox' => Label.LocalVox_RT_Id, 'Yodle SEM' => Label.Yodle_SEM_RT_Id, 'Marketing Messenger' => Label.Marketing_Messenger_RT_Id, 'iYP Bronze' => Label.YPC_Record_Type_Id
        };
        List < Product2 > lstPrdts = new List < Product2 > ();
        List < Order_Line_Items__c > lstOLns = new List < Order_Line_Items__c > ();
        List < Digital_Product_Requirement__c > lstDffs = new List < Digital_Product_Requirement__c > ();
        Set < Id > olIds = new Set < Id > ();

        Test.startTest();

        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'LocalVox';
        insert prdt;

        Canvass__c c = CommonUtility.createCanvasTest();
        insert c;

        Account a = CommonUtility.createAccountTest(c);
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;

        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;

        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c ordGrp = CommonUtility.createOGTest(a, ord, oppty);
        insert ordGrp;

        for (String s: mapPrdt.keySet()) {

            Product2 prdt1 = CommonUtility.createPrdtTest();
            prdt1.Product_Type__c = s;
            lstPrdts.add(prdt1);

        }

        insert lstPrdts;

        for (Product2 p: lstPrdts) {

            Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
            dff1.Talus_Subscription_Id__c = '2525';
            dff1.Talus_DFF_Id__c = '252525';
            dff1.recordTypeId = mapPrdt.get(p.Product_Type__c);
            dff1.Account__c = a.Id;
            dff1.DFF_Product__c = p.Id;
            dff1.Status__c = 'Complete';
            dff1.hours__c = '00:30,03:00;01:00,10:30;04:30,10:30;01:30,08:30;02:00,09:30;null;null';
            lstDffs.add(dff1);

        }

        insert lstDffs;

        for (Digital_Product_Requirement__c d: lstDffs) {

            Order_Line_Items__c oln1 = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp);
            oln1.Product2__c = d.DFF_Product__c;
            oln1.Product_Type__c = mapPrdt.get(d.Product_Type__c);
            oln1.Digital_Product_Requirement__c = d.Id;
            oln1.Effective_Date__c = System.Today();
            oln1.Cutomer_Cancel_Date__c = System.Today();
            oln1.Action_Code__c = 'Cancel';
            oln1.media_type__c = 'Digital';
            oln1.Parent_ID__c = TestMethodsUtility.generateRandomString();
            lstOLns.add(oln1);

        }

        insert lstOLns;

        for (Order_Line_Items__c o: lstOLns) {
            olIds.add(o.Id);
        }

        DeleteTalusSubscription.deleteSubscriptionFuture(olIds);

        Test.stopTest();
    }

    public static testMethod void fulfillmentCancel_2() {

        Map < String, String > mapPrdt = new Map < String, String > {
            'LocalVox' => Label.LocalVox_RT_Id, 'Yodle SEM' => Label.Yodle_SEM_RT_Id, 'Marketing Messenger' => Label.Marketing_Messenger_RT_Id, 'iYP Bronze' => Label.YPC_Record_Type_Id
        };
        List < Product2 > lstPrdts = new List < Product2 > ();
        List < Order_Line_Items__c > lstOLns = new List < Order_Line_Items__c > ();
        List < Order_Line_Items__c > lstOlnsAll = new List < Order_Line_Items__c > ();
        List < Digital_Product_Requirement__c > lstDffs = new List < Digital_Product_Requirement__c > ();
        Set < Id > olIds = new Set < Id > ();

        Test.startTest();

        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'LocalVox';
        insert prdt;

        Canvass__c c = CommonUtility.createCanvasTest();
        insert c;

        Account a = CommonUtility.createAccountTest(c);
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;

        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;

        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c ordGrp = CommonUtility.createOGTest(a, ord, oppty);
        insert ordGrp;

        for (String s: mapPrdt.keySet()) {

            Product2 prdt1 = CommonUtility.createPrdtTest();
            prdt1.Product_Type__c = s;
            lstPrdts.add(prdt1);

        }

        insert lstPrdts;

        for (Product2 p: lstPrdts) {

            Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
            dff1.Talus_Subscription_Id__c = '2525';
            dff1.Talus_DFF_Id__c = '252525';
            dff1.recordTypeId = mapPrdt.get(p.Product_Type__c);
            dff1.Account__c = a.Id;
            dff1.DFF_Product__c = p.Id;
            dff1.Status__c = 'Complete';
            dff1.hours__c = '00:30,03:00;01:00,10:30;04:30,10:30;01:30,08:30;02:00,09:30;null;null';
            lstDffs.add(dff1);

        }

        insert lstDffs;

        for (Digital_Product_Requirement__c d: lstDffs) {

            Order_Line_Items__c oln1 = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp);
            oln1.Product2__c = d.DFF_Product__c;
            oln1.Product_Type__c = mapPrdt.get(d.Product_Type__c);
            oln1.Digital_Product_Requirement__c = d.Id;
            oln1.Effective_Date__c = System.Today();
            oln1.Cutomer_Cancel_Date__c = System.Today() + 2;
            oln1.Action_Code__c = 'Cancel';
            oln1.media_type__c = 'Digital';
            oln1.Parent_ID__c = TestMethodsUtility.generateRandomString();
            lstOLns.add(oln1);

        }

        insert lstOLns;

        for (Order_Line_Items__c o: lstOLns) {
            olIds.add(o.Id);
        }

        //DeleteFulfillmentBatch dflmntBatch = new DeleteFulfillmentBatch(olIds);
        DeleteFulfillmentBatch dflmntBatch = new DeleteFulfillmentBatch();
        ID batchprocessid = Database.executeBatch(dflmntBatch);

        Test.stopTest();
    }
}