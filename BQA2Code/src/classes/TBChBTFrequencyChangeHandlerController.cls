public class TBChBTFrequencyChangeHandlerController{
    
    public static void SingleToMonthly(boolean isValidate,Map<Id,List<c2g__codaInvoiceLineItem__c>> mapForSalesInvoiceWrapper,
            Map<id,order_line_Items__c> mapForUpdatedOLis, Map<integer,list<c2g__codaCreditNoteLineItem__c>> mapSCNLIForCSClaim,
            list<c2g__codaCreditNote__c> lstInsertSCNForCSClaim, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItemForCSClaim , 
            list<c2g__codaInvoice__c> insertSalesInvoiceForCSClaim, List<c2g__codaCreditNoteLineItem__c> SCNLIListForUpdateForCSClaim,
            map<Integer, list<c2g__codaCreditNoteLineItem__c>> mapSCNLI, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItem,
            list<c2g__codaCreditNote__c> lstInsertSCN,list<c2g__codaInvoice__c> insertSalesInvoice,List<c2g__codaInvoiceLineItem__c> SILIListForUpdate,
            Id accountid,String selectedReasonForTransfer,List<Order__c> lstNewOrders,List<Order_Group__c> lstNewOrderGroup,List<Opportunity> lstNewOpportunity,
            List<Line_Item_History__c> lstOLIHistory, String strPartnerAccountId){
        
        map<String, map<Id, list<c2g__codaInvoiceLineItem__c>>> mapOLISI = new map<String, map<Id, list<c2g__codaInvoiceLineItem__c>>>();
        map<String, Id> mapPeriod = TBCcBillingTransferCommonMethod.getMapOFPeriod();
        Map<Id,Date> mapSIServiceStartDate=new Map<Id,Date>();
        Map<Id,Date> mapSIServiceEndDate=new Map<Id,Date>();
        Map<id,Line_Item_History__c>  mapOfLineItemHistory= new map<id,Line_Item_History__c>();
        if(!isValidate){
             list<Account> lstAccount = AccountSOQLMethods.getAccountsByAcctId(new set<Id> {accountid});
             List<order_line_items__c> lstorderlineForUpdate = TBChOrderLineItemBTHandlerController.updateOLIWithDigitalBillingTransferInformationForFC( mapForUpdatedOLis.values(), lstAccount[0], selectedReasonForTransfer,mapOfLineItemHistory,'Monthly',lstNewOrders,lstNewOrderGroup,lstNewOpportunity,lstOLIHistory);
             System.debug('====>>>after lstorderlineForUpdate'+lstorderlineForUpdate);
             update lstorderlineForUpdate;
        }
        
        set<Id> setNotZeroSIN = new set<Id>();
        for(String SIId: mapForSalesInvoiceWrapper.keySet()) {
            for(c2g__codaInvoiceLineItem__c iterator : mapForSalesInvoiceWrapper.get(SIId)) {
              if(!mapOLISI.containsKey(iterator.Order_Line_Item__r.Id)) {
                    mapOLISI.put(iterator.Order_Line_Item__r.Id, new map<Id, list<c2g__codaInvoiceLineItem__c>>());
                }
                if(!mapOLISI.get(iterator.Order_Line_Item__r.Id).containsKey(iterator.c2g__Invoice__c)) {
                    mapOLISI.get(iterator.Order_Line_Item__r.Id).put(iterator.c2g__Invoice__c, new list<c2g__codaInvoiceLineItem__c>());
                }
                mapOLISI.get(iterator.Order_Line_Item__r.Id).get(iterator.c2g__Invoice__c).add(iterator); 
                SILIListForUpdate.add(new c2g__codaInvoiceLineItem__c(id=iterator.Id,Offset__c=true));
                if(!mapSIServiceStartDate.containsKey(iterator.c2g__Invoice__c)) {
                       mapSIServiceStartDate.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.Service_Start_Date__c);
                }
                if(!mapSIServiceEndDate.containsKey(iterator.c2g__Invoice__c)) {
                       mapSIServiceEndDate.put(iterator.c2g__Invoice__c, iterator.c2g__Invoice__r.Service_End_Date__c);
                }
                if(iterator.c2g__NetValue__c > 0 || Test.isRunningTest()) {
                    setNotZeroSIN.add(SIId);
                }
                
            }
        }
        Integer monthAlreadyPassed =0;
        Date startDate; 
        Date todayDate = Date.today();
        order_line_items__C tempOLI;
        integer iCountSI=0;
        integer iCountSCN=0;
        
        for(string oliId :mapOLISI.keySet()) {
            if(mapForUpdatedOLis.containskey(oliId)) {
                iCountSCN++;
                Map<id,List<c2g__codaInvoiceLineItem__c>> mapSILI= mapOLISI.get(oliId );
                //startDate=Date.newInstance(mapForUpdatedOLis.get(oliId).Talus_Go_Live_Date__c.year(),mapForUpdatedOLis.get(oliId).Talus_Go_Live_Date__c.month(),mapForUpdatedOLis.get(oliId).Order_Anniversary_Start_Date__c.day());  
                if(mapOfLineItemHistory.containskey(oliId))
                    startDate = mapOfLineItemHistory.get(oliId).Service_Start_Date__c;
                else //this will be used only on Validate button
                    startDate=Date.newInstance(mapForUpdatedOLis.get(oliId).Talus_Go_Live_Date__c.year(),mapForUpdatedOLis.get(oliId).Talus_Go_Live_Date__c.month(),mapForUpdatedOLis.get(oliId).Order_Anniversary_Start_Date__c.day());  
                if(Test.isRunningTest() && startDate==null)   
                    startDate=todayDate.addMonths(-2); 
                monthAlreadyPassed = startDate.monthsBetween(todayDate);
                if(todayDate.day() > startDate.day()) {
                    monthAlreadyPassed += 1;
                }
                
                //if(monthAlreadyPassed <=0) monthAlreadyPassed =1;
                c2g__codaInvoiceLineItem__c tempSILI=new c2g__codaInvoiceLineItem__c();
                System.debug('#######Ankit#########'+monthAlreadyPassed);
                for(Id SIid : mapSILI.keySet()) {
                    //changed logic - CC-3161
                    for(c2g__codaInvoiceLineItem__c iterator : mapSILI.get(SIid)) {
                        tempSILI = iterator;
                        //Added By Ankit CC-3133 Frequency Change from Single Pay to Monthly should not create $0 SCRs and $0 SINs
                        if(iterator.c2g__NetValue__c>0 || Test.isRunningTest()) {
                            lstInsertSCN.add(TBChFFDocumentsForBTHandlerController.copyDataFromInvoiceToCreditNoteForP4PRetro(mapForUpdatedOLis.get(oliId),iterator,iCountSCN,mapPeriod,true ));
                            TBChFFDocumentsForBTHandlerController.copyDataFromInvoiceLineItemToSalesCreditNoteLineItemForP4PRetro(new list<c2g__codaInvoiceLineItem__c>{iterator}, iCountSCN, mapSCNLI,mapForUpdatedOLis);                      
                        }
                        Integer iSINSuccessfulPayment = Integer.valueOf(iterator.c2g__Invoice__r.SI_Successful_Payments__c);
                        for(integer i = 1;i <= monthAlreadyPassed ; i++) {
                            //Added By Ankit CC-3133 Frequency Change from Single Pay to Monthly should not create $0 SCRs and $0 SINs
                            if(iterator.c2g__NetValue__c > 0 || Test.isRunningTest()) {
                                iCountSI++;
                                If(Test.isRunningTest())
                                iSINSuccessfulPayment =2;
                                Integer iSuccessPayment = iSINSuccessfulPayment + i - 1;
                                
                                Integer iRemainingPayment = Integer.valueOf(mapForUpdatedOLis.get(oliId).Payment_Duration__c) - iSuccessPayment;
                                insertSalesInvoice.add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceForMonthlyFrequencyChange(iterator,mapForUpdatedOLis.get(oliId),iCountSI,mapPeriod,0,startDate.addMonths(i-1),startDate.addMonths(i).adddays(-1),iSuccessPayment,iRemainingPayment));
                                //insertSalesInvoice.add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceForMonthlyFrequencyChange(SILI,OLI,iCount,mapPeriod,iCountTranferred,ServiceStartDt,ServiceEndDt,PaymentsRemaining,SuccessfulPayments);
                                if(!mapSalesInvoiceLineItem.containsKey(iCountSI)) {
                                    mapSalesInvoiceLineItem.put(iCountSI, new list<c2g__codaInvoiceLineItem__c>());
                                }
                                mapSalesInvoiceLineItem.get(iCountSI).add(TBChFFDocumentsForBTHandlerController.newalesInvoiceLineItemForSMFrequencyChange(iterator,mapForUpdatedOLis.get(oliId),mapOfLineItemHistory,iSuccessPayment,iRemainingPayment));
                            }
                        }
                    }
                }   
            }
        }
        System.debug('#######Ankit#########List of insertSalesInvoice==='+insertSalesInvoice);
        System.debug('#######Ankit#########Size of insertSalesInvoice=='+insertSalesInvoice.size());
        list<c2g__codaCreditNoteLineItem__c>  CreditNoteLineItemsSOQLMap = SalesCreditNoteLineItemSOQLMethods.getSysGenSCNLineItemByOLIForRetro(mapForUpdatedOLis.keySet(), strPartnerAccountId, false, 'Single Payment');
        if(CreditNoteLineItemsSOQLMap.size() > 0){
            RecurringCSClaimHandlerMethodForSingleToMonthly(mapPeriod,CreditNoteLineItemsSOQLMap,mapForUpdatedOLis,mapSCNLIForCSClaim,lstInsertSCNForCSClaim,mapSalesInvoiceLineItemForCSClaim,insertSalesInvoiceForCSClaim,SCNLIListForUpdateForCSClaim,mapOfLineItemHistory);
        }
    } 
            
            
    public static void MonthlyToSingle(boolean isValidate, Map<Id,List<c2g__codaInvoiceLineItem__c>> mapForSalesInvoiceWrapper,
                    Map<id,order_line_Items__c> mapForUpdatedOLis, Map<integer,list<c2g__codaCreditNoteLineItem__c>> mapSCNLIForCSClaim,
                    list<c2g__codaCreditNote__c> lstInsertSCNForCSClaim, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItemForCSClaim , 
                    list<c2g__codaInvoice__c> insertSalesInvoiceForCSClaim, List<c2g__codaCreditNoteLineItem__c> SCNLIListForUpdateForCSClaim,
                    map<Integer, list<c2g__codaCreditNoteLineItem__c>> mapSCNLI, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItem,
                    list<c2g__codaCreditNote__c> lstInsertSCN,list<c2g__codaInvoice__c> insertSalesInvoice, List<c2g__codaInvoiceLineItem__c> SILIListForUpdate,
                    Id accountid,String selectedReasonForTransfer,List<Order__c> lstNewOrders,List<Order_Group__c> lstNewOrderGroup,
                    List<Opportunity> lstNewOpportunity,List<Line_Item_History__c> lstOLIHistory, String strPartnerAccountId) {

        map<String, map<Id, list<c2g__codaInvoiceLineItem__c>>> mapOLISI = new map<String, map<Id, list<c2g__codaInvoiceLineItem__c>>>();
        Map<id,Line_Item_History__c>  mapOfLineItemHistory= new map<id,Line_Item_History__c>();
        // Here we need to change update the OLI Method.
        if(!isValidate) {
            list<Account> lstAccount = AccountSOQLMethods.getAccountsByAcctId(new set<Id> {accountid});
            List<order_line_items__c> lstorderlineForUpdate = TBChOrderLineItemBTHandlerController.updateOLIWithDigitalBillingTransferInformationForFC( mapForUpdatedOLis.values(), lstAccount[0], selectedReasonForTransfer,mapOfLineItemHistory,'Single Payment',lstNewOrders,lstNewOrderGroup,lstNewOpportunity,lstOLIHistory);
            update lstorderlineForUpdate;
        }
        map<String, Id> mapPeriod = TBCcBillingTransferCommonMethod.getMapOFPeriod();
        for(String SIId : mapForSalesInvoiceWrapper.keySet()) {
            for(c2g__codaInvoiceLineItem__c iterator : mapForSalesInvoiceWrapper.get(SIId)) {
                if(!mapOLISI.containsKey(iterator.Order_Line_Item__r.Package_ID__c)) {
                    mapOLISI.put(iterator.Order_Line_Item__r.Package_ID__c, new map<Id, list<c2g__codaInvoiceLineItem__c>>());
                }
                if(!mapOLISI.get(iterator.Order_Line_Item__r.Package_ID__c).containsKey(iterator.c2g__Invoice__c)) {
                    mapOLISI.get(iterator.Order_Line_Item__r.Package_ID__c).put(iterator.c2g__Invoice__c, new list<c2g__codaInvoiceLineItem__c>());
                }
                mapOLISI.get(iterator.Order_Line_Item__r.Package_ID__c).get(iterator.c2g__Invoice__c).add(iterator); 
            }
        }

        order_line_items__C tempOLI;
        Line_Item_History__c objOLIOldInformation = new Line_Item_History__c();
        c2g__codaInvoiceLineItem__c tempSILI;
        integer iCountSI=0;
        integer iCountSCN=0;
        Set<Id> setOfOLi = new Set<Id>();
        for(string oliPackageId : mapOLISI.keySet()) {
            system.debug('pkg id : ' + oliPackageId);
            iCountSI++;
            Map<id,List<c2g__codaInvoiceLineItem__c>> mapSILI= mapOLISI.get(oliPackageId);
            for(Id SIid : mapSILI.keySet()) {
                system.debug('SIN id : ' + SIid);
                for(c2g__codaInvoiceLineItem__c SILI : mapSILI.get(SIid)) {
                    system.debug('SINLI id : ' + SILI.Id);
                    iCountSCN++;
                    if(!setOfOLi.contains(SILI.Order_Line_Item__c)) {
                        system.debug('SINLI id : True : ' + SILI.Order_Line_Item__c);
                       tempSILI=SILI;
                       //Added By Ankit CC-3133 Frequency Change from Monthly Pay to Single should not create $0 SCRs and $0 SINs
                       if(SILI.c2g__NetValue__c > 0 || Test.isRunningTest()) {
                            system.debug('Step 1');
                            tempOLI = mapForUpdatedOLis.get(SILI.Order_Line_Item__c);
                            //System.debug('Testing BBBBB : '+ mapOfLineItemHistory.size());
                            if(mapOfLineItemHistory.size() > 0) {
                                system.debug('Step 2 '+ SILI.c2g__NetValue__c);
                                objOLIOldInformation = mapOfLineItemHistory.get(SILI.Order_Line_Item__c);
                                //System.debug('Testing BBBBB : '+ objOLIOldInformation.Successful_Payments__c);
                            }
                            if(!mapSalesInvoiceLineItem.containsKey(iCountSI)) {
                                system.debug('Step 3');
                                mapSalesInvoiceLineItem.put(iCountSI, new list<c2g__codaInvoiceLineItem__c>());
                            }
                            system.debug('Step 4');
                            mapSalesInvoiceLineItem.get(iCountSI).add(TBChFFDocumentsForBTHandlerController.newalesInvoiceLineItemForFrequencyChange(SILI, mapForUpdatedOLis.get(SILI.Order_Line_Item__c),mapOfLineItemHistory));
                            /*if(mapSalesInvoiceLineItem.size() > 0) {
                                system.debug('Step 5 ' + mapSalesInvoiceLineItem.size());
                                insertSalesInvoice.add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceForFrequencyChange(SILI,tempOLI,iCountSI, mapPeriod,0,objOLIOldInformation));
                            }*/
                            system.debug('Step 6');
                            setOfOLi.add(SILI.Order_Line_Item__c);
                       }
                    }
                  //  lstInsertSCN.add(TBChFFDocumentsForBTHandlerController.copyDataFromInvoiceToCreditNoteForP4PRetro(mapForUpdatedOLis.get(tempSILI.order_line_item__c),tempSILI,iCountSCN,mapPeriod,true ));
                  //  TBChFFDocumentsForBTHandlerController.copyDataFromInvoiceLineItemToSalesCreditNoteLineItemForP4PRetro(new list<c2g__codaInvoiceLineItem__c>{tempSILI}, iCountSCN, mapSCNLI,mapForUpdatedOLis);                      
                }
            }
            //commented by sathish - moved this logic to line - 164
            if(mapSalesInvoiceLineItem.size()>0){
                //Added By Ankit CC-3133 Frequency Change from Monthly Pay to Single should not create $0 SCRs and $0 SINs
                //if(tempSILI.c2g__NetValue__c>0){
                    insertSalesInvoice.add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceForFrequencyChange(tempSILI,tempOLI,iCountSI, mapPeriod,0,objOLIOldInformation)); 
                //}
            }
        }
        System.debug('======>>>>insertSalesInvoice'+insertSalesInvoice.size());
        // below logic is for handle the recurring CS Claim 
        list<c2g__codaCreditNoteLineItem__c>  CreditNoteLineItemsSOQLMap = SalesCreditNoteLineItemSOQLMethods.getSysGenSCNLineItemByOLIForRetro(mapForUpdatedOLis.keySet(), strPartnerAccountId, true, 'Monthly');
        if(CreditNoteLineItemsSOQLMap.size()>0) {
            RecurringCSClaimHandlerMethodForMonthlyToSingle(mapPeriod,CreditNoteLineItemsSOQLMap,mapForUpdatedOLis,mapSCNLIForCSClaim,lstInsertSCNForCSClaim,mapSalesInvoiceLineItemForCSClaim,insertSalesInvoiceForCSClaim,SCNLIListForUpdateForCSClaim);
        }
    }

    private static void RecurringCSClaimHandlerMethodForMonthlyToSingle(Map<String,Id> mapOfPeriod,list<c2g__codaCreditNoteLineItem__c>  CreditNoteLineItemsSOQLMap, 
                    Map<id,order_line_Items__c> mapForUpdatedOLis, Map<integer,list<c2g__codaCreditNoteLineItem__c>> mapSCNLIForCSClaim, 
                    list<c2g__codaCreditNote__c> lstInsertSCNForCSClaim, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItemForCSClaim, 
                    list<c2g__codaInvoice__c> insertSalesInvoiceForCSClaim, List<c2g__codaCreditNoteLineItem__c> SCNLIListForUpdateForCSClaim) {
        
        Map<id,Map<id, List<c2g__codaCreditNoteLineItem__c>>> mapOfOliAndScn = new Map<id,Map<id, List<c2g__codaCreditNoteLineItem__c>>>();
        
        integer iCountSCN=0;
        integer iCountSI=0;
        Set<Id> SCNLIsetOfIds =new Set<Id>();
        Set<Id> SCNsetOfIds =new Set<Id>();
        if(CreditNoteLineItemsSOQLMap.size()>0){
                for(c2g__codaCreditNoteLineItem__c  creditNoteLineItem:CreditNoteLineItemsSOQLMap){
                    if(creditNoteLineItem.c2g__CreditNote__r.Credit_Frequency__c!=null && creditNoteLineItem.c2g__CreditNote__r.Credit_Frequency__c=='Recurring' && creditNoteLineItem.c2g__CreditNote__r.c2g__CreditNoteDate__c>Date.today()){ 
                        if(!mapOfOliAndScn.containsKey(creditNoteLineItem.Order_Line_Item__c)) {
                            mapOfOliAndScn.put(creditNoteLineItem.Order_Line_Item__c, new map<Id, list<c2g__codaCreditNoteLineItem__c>>());
                        }
                        if(!mapOfOliAndScn.get(creditNoteLineItem.Order_Line_Item__c).containsKey(creditNoteLineItem.c2g__CreditNote__c)) {
                            mapOfOliAndScn.get(creditNoteLineItem.Order_Line_Item__c).put(creditNoteLineItem.c2g__CreditNote__c, new list<c2g__codaCreditNoteLineItem__c>());
                        }
                        mapOfOliAndScn.get(creditNoteLineItem.Order_Line_Item__c).get(creditNoteLineItem.c2g__CreditNote__c).add(creditNoteLineItem); 
                    }
                }
            }
            
            for(Id OLIId:mapOfOliAndScn.keySet()){
                c2g__codaCreditNoteLineItem__c tempSCNLI;
                Map<id, List<c2g__codaCreditNoteLineItem__c>> mapForCreditNote= mapOfOliAndScn.get(OLIId);
                iCountSCN++;
                for(Id creditnoteId : mapForCreditNote.keySet()) {
                        
                        iCountSI++;
                        for(c2g__codaCreditNoteLineItem__c iterator : mapForCreditNote.get(creditnoteId)) {
                            if(!SCNLIsetOfIds.contains(iterator.id)) {
                                SCNLIsetOfIds.add(iterator.Id);
                                SCNLIListForUpdateForCSClaim.add(new c2g__codaCreditNoteLineItem__c(id=iterator.Id,Offset__c=true));
                                if(iterator.c2g__UnitPrice__c>0) {
                                    if(!mapSalesInvoiceLineItemForCSClaim.containsKey(iCountSI)) {
                                        mapSalesInvoiceLineItemForCSClaim.put(iCountSI,new List<c2g__codaInvoiceLineItem__c>());
                                    }
                                    mapSalesInvoiceLineItemForCSClaim.get(iCountSI).add(TBChFFDocumentsForBTHandlerController.copyDataFromSCNLIToSILIForp4pCSClaim(iterator,mapForUpdatedOLis.get(iterator.Order_Line_Item__c)));
    
                                }
                            } 
                            tempSCNLI=iterator;
                        }
                        if(mapSalesInvoiceLineItemForCSClaim.size()>0)
                           insertSalesInvoiceForCSClaim.add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceForDigitalCSClaim(mapOfPeriod,tempSCNLI,mapForUpdatedOLis.get(OLIId),iCountSI,true));                       
                    }
                    if(!mapSCNLIForCSClaim.containsKey(iCountSCN)) {
                        mapSCNLIForCSClaim.put(iCountSCN, new list<c2g__codaCreditNoteLineItem__c>());
                    }
                    mapSCNLIForCSClaim.get(iCountSCN).add(TBChFFDocumentsForBTHandlerController.newSalesCreditLineItemForMtoSCSClaim(tempSCNLI,insertSalesInvoiceForCSClaim.size()));                   
                    if(mapSCNLIForCSClaim.size()>0)
                         lstInsertSCNForCSClaim.add(TBChFFDocumentsForBTHandlerController.newSalesCreditNoteForMtoSCsClaim(mapOfPeriod,tempSCNLI,iCountSCN,mapForUpdatedOLis.get(OLIId)));                   
                }
                
            }   
   private static void RecurringCSClaimHandlerMethodForSingleToMonthly(Map<String,Id> mapOfPeriod,list<c2g__codaCreditNoteLineItem__c>  CreditNoteLineItemsSOQLMap, 
                    Map<id,order_line_Items__c> mapForUpdatedOLis, Map<integer,list<c2g__codaCreditNoteLineItem__c>> mapSCNLIForCSClaim, 
                    list<c2g__codaCreditNote__c> lstInsertSCNForCSClaim, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItemForCSClaim, 
                    list<c2g__codaInvoice__c> insertSalesInvoiceForCSClaim, List<c2g__codaCreditNoteLineItem__c> SCNLIListForUpdateForCSClaim,Map<id,Line_Item_History__c>  mapOfLineItemHistory) {
        Map<id, List<c2g__codaCreditNoteLineItem__c>> mapForCreditNote = new Map<id, List<c2g__codaCreditNoteLineItem__c>>();
        integer iCountSCN=0;
        integer iCountSI=0;
        if(CreditNoteLineItemsSOQLMap.size()>0){
            for(c2g__codaCreditNoteLineItem__c  creditNoteLineItem:CreditNoteLineItemsSOQLMap){
                if(creditNoteLineItem.c2g__CreditNote__r.Credit_Frequency__c!='One Time'){
                     if(!mapForCreditNote.containsKey(creditNoteLineItem.c2g__CreditNote__c))
                        mapForCreditNote.put(creditNoteLineItem.c2g__CreditNote__c,new List<c2g__codaCreditNoteLineItem__c>());
                    mapForCreditNote.get(creditNoteLineItem.c2g__CreditNote__c).add(creditNoteLineItem);
                }
            } 
            for(Id creditnoteId : mapForCreditNote.keySet()) {
                c2g__codaCreditNoteLineItem__c tempSCNLI;
                iCountSI++;
                    for(c2g__codaCreditNoteLineItem__c iterator : mapForCreditNote.get(creditnoteId)) {
                            SCNLIListForUpdateForCSClaim.add(new c2g__codaCreditNoteLineItem__c(id=iterator.Id,Offset__c=true));
                            if(iterator.c2g__UnitPrice__c>0) {
                                if(!mapSalesInvoiceLineItemForCSClaim.containsKey(iCountSI)) {
                                    mapSalesInvoiceLineItemForCSClaim.put(iCountSI,new List<c2g__codaInvoiceLineItem__c>());
                                }
                                mapSalesInvoiceLineItemForCSClaim.get(iCountSI).add(TBChFFDocumentsForBTHandlerController.copyDataFromSCNLIToSILIForp4pCSClaim(iterator,mapForUpdatedOLis.get(iterator.Order_Line_Item__c)));
                            }
                            tempSCNLI=iterator;
                    }
                    if(mapSalesInvoiceLineItemForCSClaim.size()>0)
                       insertSalesInvoiceForCSClaim.add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceForDigitalCSClaim(mapOfPeriod,tempSCNLI,mapForUpdatedOLis.get(tempSCNLI.Order_Line_Item__c),iCountSI,true)); 
                    Date startDate;
                    Date ServiceEndDate;
                    Integer monthNeedsTobePassed =0;
                    if(mapForUpdatedOLis.ContainsKey(tempSCNLI.order_line_item__c)) {
                        if(mapOfLineItemHistory.size() > 0 && mapForUpdatedOLis.ContainsKey(tempSCNLI.order_line_item__c) != null) {
                            startDate=mapOfLineItemHistory.get(tempSCNLI.order_line_item__c).service_Start_date__c;
                            ServiceEndDate = mapOfLineItemHistory.get(tempSCNLI.order_line_item__c).Service_End_Date__c;
                        }
                        else {
                            startDate=mapForUpdatedOLis.get(tempSCNLI.order_line_item__c).service_Start_date__c;
                            ServiceEndDate = mapForUpdatedOLis.get(tempSCNLI.order_line_item__c).Service_End_Date__c;
                        }
                    }
                    /*if(mapForUpdatedOLis.ContainsKey(tempSCNLI.order_line_item__c))
                        startDate=mapOfLineItemHistory.get(tempSCNLI.order_line_item__c).service_Start_date__c;
                    if(mapOfLineItemHistory.containskey(tempSCNLI.order_line_item__c))
                        ServiceEndDate = mapOfLineItemHistory.get(tempSCNLI.order_line_item__c).Service_End_Date__c;
                    else
                         ServiceEndDate = mapForUpdatedOLis.get(tempSCNLI.order_line_item__c).Service_End_Date__c; // it will use only for validate*/
                    if(Test.isRunningTest()){
                        startDate=Date.today();
                        ServiceEndDate=Date.today().addMonths(5);
                    }
                    monthNeedsTobePassed = startDate.monthsBetween(ServiceEndDate);                
                    for(integer i=0;i<monthNeedsTobePassed;i++){
                        iCountSCN++;
                        if(tempSCNLI.c2g__CreditNote__r.c2g__NetTotal__c>0 || Test.isRunningTest()){
                            if(!mapSCNLIForCSClaim.containsKey(iCountSCN)) {
                                mapSCNLIForCSClaim.put(iCountSCN, new list<c2g__codaCreditNoteLineItem__c>());
                            }
                             mapSCNLIForCSClaim.get(iCountSCN).add(TBChFFDocumentsForBTHandlerController.newSalesCreditLineItemForSIngleToMonthlyCSClaim(tempSCNLI));                       
                             lstInsertSCNForCSClaim.add(TBChFFDocumentsForBTHandlerController.newSalesCreditNoteForStoMCSClaim(mapOfPeriod,tempSCNLI,iCountSCN,mapForUpdatedOLis.get(tempSCNLI.Order_Line_Item__c),startDate.addMonths(i)));   
                        }
                    }                   
                }
            }
        }               
}