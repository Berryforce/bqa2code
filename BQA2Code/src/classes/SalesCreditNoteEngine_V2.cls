public class SalesCreditNoteEngine_V2 {
    public case objCase {get;Set;}
    public boolean creditnoteError{get;Set;}
    PageReference pg=null;
    list<Order_Line_Item_Discount__c> lstOLID = new list<Order_Line_Item_Discount__c>();
    map<Id, Order_Line_Items__c> mapOLItem = new map<Id, Order_Line_Items__c>();
    set<string> setOLITelcoPartner = new set<string> ();
    map <string,id> mapPeriodID = new map<string,id>();
    set<String> setDimension3 = new set<String>();
    public String caseID=null;
    public SalesCreditNoteEngine_V2(ApexPages.StandardController controller) {
         caseID = controller.getId();
        fetchCaseValues(caseID);
        
        
    }
    
    public Pagereference onLoad() {
        if(objCase != null) {
            creditnoteError=false;
            fetchOLItemDiscount();
            createSalesCreditNote();
            if(pg!=null)
            {
                pg.setRedirect(true);
            }
            return pg;
        }
        return null;
    }
    
    
    private void fetchCaseValues(String strCaseId) {
        list<Case> lstCase = [SELECT Id, Approved__c,CaseNumber, BCC_Duration__c, ContactId, AccountId, Account.Client_Tier__c, Related_Telco__c, Account.Telco_Partner__r.Name, Total_Max_Credit__c, IsClosed, Status, Additional_BCC__c, Contracted_Total_Value__c, Risk_Dollars_Adjusted__c, Sub_Credit__c, Total_BCC__c, Total_Credit__c, Total_Max_BCC__c, Total_Monthly_BCC__c, Total_Contract_Value__c, Adjustment_Against_Contract__c, of_Contract_Adjusted__c, RecordTypeId FROM Case WHERE Id = :strCaseId Limit 1];
        
        for(Case iterator : lstCase) {
            objCase = iterator;
        }
    }

    private void fetchOLItemDiscount() {
        lstOLID = [SELECT Id,Name, Order_Line_Item__c, BCC_Amount__c, BCC_Duraton__c, Case__c, Discount_Per_period__c, Net_Commission__c,
            Discount_Percent__c, Discount_Type__c, Effective_Date__c, Issue_as_Single_Credit__c, Max_BCC__c, Max_Credit__c, Total_Net_Commission__c, 
            Number_of_Billing_Periods_to_discount__c, Source__c, Order_Line_Item__r.Successful_Payments__c, OLI_Billing_Frequency__c,
            Total_BCC__c, User_Responsible__c,case__r.RecordTypeId, checked__c, date_of_issuance__c, isvalid__c, Order_Line_Item__r.Payment_Duration__c,
            reason__c, total_Credit_amount__c, Order_Line_Item__r.Opportunity__c, Total_Credit_num__c, Maxbcc_Num__c, case__r.ISS__c, 
            case__r.ISS_Number__c, case__r.CMR_Number__c, case__r.National_Client_Number__c, case__r.PAN1__c,
            case__r.AccountId, case__r.Additional_BCC__c, Client_Tier__c, OLI_Billing_Partner_Code__c, Order_Line_Item__r.Account__c,
            Order_Line_Item__r.Product2__c, Order_Line_Item__r.Talus_Go_Live_Date__c, Order_Line_Item__r.CreatedDate, 
            Order_Line_Item__r.Order_Anniversary_Start_Date__c, Date_Created_Reversed__c, Product_Non_OLI__c, Product__c, Dimension_1__c, 
            Dimension_2__c, Dimension_3__c, Dimension_4__c, Directory_Edition__r.Directory__c, Telco__c, Telco_Partner_Name__c, 
            Case__r.Account.Telco_Partner__c,SCNTelco__r.Id,SCNTelco__r.Account__r.Id,Order_Line_Item__r.Billing_Partner__c,Order_Line_Item__r.Next_Billing_Date__c, Credit_Issued_To1__c, Credit_Issued_To__c,Dimension_1__r.Directory__c,Directory_Edition__c,Directory_Edition__r.Edition_Code__c, Order_Line_Item__r.Billing_Frequency__c,Credit_Frequency__c FROM Order_Line_Item_Discount__c WHERE Case__c = :objCase.Id AND(Total_Credit_num__c > 0 OR Maxbcc_Num__c > 0)];
        set<Id> setOLIID = new set<Id>();
        system.debug('List of OLID----' + lstOLID);
        for(Order_Line_Item_Discount__c iterator : lstOLID) {
            if(iterator.Order_Line_Item__c != null) {
                setOLIID.add(iterator.Order_Line_Item__c);  
            }
        }
        
        mapOLItem = new map<Id, Order_Line_Items__c>([SELECT Id, Billing_Contact__c, Billing_End_Date__c, Billing_Frequency__c,
            Billing_Partner__c, Billing_Start_Date__c, isCanceled__c, Cancellation__c, Type__c, Media_Type__c, 
            Continious_Billing__c, Cutomer_Cancel_Date__c, Talus_DFF_Id__c, Discount__c, Fulfilled_on__c,
            Description__c, ListPrice__c, Opportunity__c, Order__c, Order_Anniversary_Start_Date__c,
            FulfillmentDate__c, Order_Group__c, Payment_Duration__c, Payment_Method__c, Canvass__r.Billing_Entity__c, 
            Payments_Remaining__c, Product2__c, Total_Prorated_Days_for_contract__c, Prorate_Stored_Value__c,
            Quantity__c, Prorate_Credit__c, Total_Prorate__c, Status__c, Prorate_Credit_Days__c, Talus_Go_Live_Date__c,
            Talus_Fulfillment_Date__c, Talus_OrderLineItem__c, Talus_Cancel_Date__c, Successful_Payments__c,
            Quote_Signed_Date__c, Quote_signing_method__c, UnitPrice__c, Sent_to_fulfillment_on__c, Service_End_Date__c,
            Service_Start_Date__c, Line_Status__c, Billing_Close_Date__c, Contract_End_Date__c,
            Next_Billing_Date__c, Subscription_ID__c, Talus_Subscription_ID__c, Days_B_W__c,
            Current_Billing_Period_Days2__c, Is_Billing_Cycle__c, Opportunity_line_Item_id__c, Line_Number__c, Directory__c,
            Action__c, Account__c, Client_Number__c, Zero_Cancel_Fee_Approved__c, zero_Cancel_Fee_Requested__c,
            Advertising_Data__c, CMR_Name__c, BillingChangeNoofDays__c, CMR_Number__c, Billing_Change_Prorate_Credit__c,
            Date__c, BillingChangeProrateCreditDays__c, BillingChangesPayment__c, NAT_Client_ID__c,
            Billing_Partner_Account__c, NAT__c, Billing_Partner_Change__c, Billing_Partner_Transferred__c,
            BillingTransfered__c, BillingTransferedInfo__c, Trans__c, Cancelation_Fee__c, Canvass__c,
            Digital_Product_Requirement__c, P4P_Billing__c, Current_Billing_Period_Days__c, Current_Daily_Prorate__c,
            Current_Rate__c, Date_of_Transfer__c, Directory_Code__c, Effective_Date__c,
            Evergreen_Payments__c, From__c, Fulfillment_Status__c, Geo_Code__c, Geo_Type__c,
            Last_successful_settlement__c, Directory_Heading__c, Number_of_Months_Transferred__c,
            Order_Billing_Date_Changed__c, OriginalBillingDate__c, P4P_Current_Billing_Clicks_Leads__c,
            P4P_Current_Months_Clicks_Leads__c, P4P_Price_Per_Click_Lead__c, Package_ID__c, Parent_ID__c,
            Parent_Line_Item__c, Product_Type__c, Publication_Date__c, Reason_for_Transfer__c, Scope__c, Directory_Section__c,
            Statement_Suppression__c, Telco__c,Telco__r.name,To__c, Total_Billing_Amount_Transfered__c,
            Transaction_ID__c, Transferred_To__c, Transferred_from__c, UDAC__c, User_who_initiated_the_transfer__c,
            checked__c, ProductCode__c, BAS__c, DAT__c, SPINS__c, Trans_Version__c, Directory_Edition__c, product2__r.RGU__c,
            Publication_Company__c, Publication_Code__c, Seniority_Date__c, product2__r.Family, Canvass__r.Canvass_Code__c, Is_P4P__c,
            UDAC_Group__c, To_P__c, From_C__c, Page_Number__c, Directory_Edition__r.Edition_Code__c, Edition_Code__c,
            Publication_Company__r.National_Commission__c, CMR_Name__r.National_Commission__c, CMR_Name__r.National_Commission__r.Rate__c,
            Publication_Company__r.National_Commission__r.Rate__c, Directory__r.National_Commission__c, Directory__r.National_Commission__r.Rate__c,
            Account__r.National_Commission__c,Account__r.National_Commission__r.Rate__c FROM Order_Line_Items__c
            WHERE Id IN :setOLIID]);
            //ATP-2895
            
            for (Order_Line_Items__c objOLI:mapOLItem.values())
            {
                if(objOLI.Telco__r.name != null) {
                    setOLITelcoPartner.add(objOLI.Telco__r.name);
                }
            }
            
    }
    
    private c2g__codaCreditNote__c generateSCN(Order_Line_Item_Discount__c objOLID, Order_Line_Items__c objOLI, map<String, Id> dimensionsMapValues, Date dSCNDate, Integer iCount) {
        c2g__codaCreditNote__c objSCN = new c2g__codaCreditNote__c();
        
        string c2gPeriod;
        objSCN.Case__c = objCase.Id;
        objSCN.c2g__CreditNoteDate__c = dSCNDate;  
        objSCN.Order_Line_Item_Discount__c = objOLID.Id;
                 
        
        if(CommonMethods.getRedordTypeIdByName(CommonMessages.NationalCase, CommonMessages.caseObjectName) == objCase.RecordTypeId) {
            objSCN.Transaction_Type__c = CommonMessages.scnBillingEngineNational;
            objSCN.CMR_Number__c = objOLID.case__r.CMR_Number__c;
            objSCN.National_Client_Number__c = objOLID.case__r.National_Client_Number__c;
        }
        else {
            objSCN.Transaction_Type__c = CommonMessages.scnBillingEngine;
        }
        objSCN.c2g__CreditNoteReason__c = objOLID.reason__c;
       // objSCN.c2g__Account__c = objOLID.case__r.AccountId;
        if(objOLI != Null) {
            
            objSCN.SC_Billing_Partner__c = objOLI.Billing_Partner__c;
            objSCN.SC_Media_Type__c = objOLI.Media_Type__c;
            objSCN.SC_Payment_Method__c = objOLI.Payment_Method__c;
            objSCN.SC_Billing_Entity__c = objOLI.Canvass__r.Billing_Entity__c;
            
            if(objOLI.P4P_Billing__c || objOLI.Is_P4P__c) {
            objSCN.SC_P4P__c = true;
            }
    
        //system.debug('objOLINOt Null');
            objSCN.c2g__Opportunity__c = objOLI.Opportunity__c;
            
            
            if(objOLID.SCNTelco__r.Id!= null && objOLID.Credit_Issued_To1__c == 'Telco'){
                objSCN.c2g__Account__c = objOLID.SCNTelco__r.Account__r.Id;
                    
            }
            else if(objOLID.Credit_Issued_To1__c == 'Berry'){
               objSCN.c2g__Account__c = objOLID.case__r.AccountId;
                  
             } else if (objOLID.Telco_Partner_Name__c != null && !objOLI.Billing_Partner__c.equals(CommonMessages.berryTelcoName)) {
               objSCN.c2g__Account__c = objOLI.Billing_Partner_Account__c; 
                                 
             }
             else
             {
                objSCN.c2g__Account__c = objOLID.case__r.AccountId;
             }
           
            
            if(objOLID.Credit_Frequency__c == 'Recurring'){
                objSCN.Credit_Frequency__c = 'Recurring';
            }
            else if(objOLID.Credit_Frequency__c == 'One Time'){
                objSCN.Credit_Frequency__c = 'One Time';       
                     
            }      
        }
        else
        {
            //ATp-4083
            if(objOLID.Telco__c != null){
            objSCN.c2g__Account__c =objOLID.Telco__c;}
            else
           
            { objSCN.c2g__Account__c = objOLID.case__r.AccountId;}
        }
           
           
        if(objOLID.case__r.PAN1__c != null) {
            objSCN.PAN1__c = objOLID.case__r.PAN1__c;
        }
        objSCN.Customer_Name__c = objOLID.case__r.AccountId;
        objSCN.ffbilling__CopyAccountValues__c = true;
        objSCN.ffbilling__CopyDefaultPrintedTextDefinitions__c = false;
        objSCN.ffbilling__DeriveCurrency__c = true;
        objSCN.ffbilling__DeriveDueDate__c = true;
       
        objSCN.ffbilling__DerivePeriod__c = true;
        objSCN.Auto_Number__c = iCount;
        if(objOLID.Directory_Edition__c != null){
        objSCN.Edition_Code__c = objOLID.Directory_Edition__r.Edition_Code__c;
        }
        if (dimensionsMapValues != null && objOLI != Null) {
            DimensionsValues.assignDimensions(objOLI, objSCN, null, null, dimensionsMapValues);
        }
        //ATP-3676
        if(objOLI != Null) {
            if(objOLI.Billing_Partner_Transferred__c == true)
            {
                if(objOLID.Credit_Issued_To1__c == CommonMessages.berryBillingPartner){
                    setDimension3.add(CommonMessages.BerryForDimension);                
                    dimensionsMapValues.putAll(DimensionsValues.dimension3Values(setDimension3));
                    objSCN.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.BerryForDimension);
                }else if(objOLID.Credit_Issued_To1__c == CommonMessages.Telco){
                    setDimension3.add(CommonMessages.TelcoForDimension);
                    dimensionsMapValues.putAll(DimensionsValues.dimension3Values(setDimension3));
                    objSCN.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.TelcoForDimension);
                }   
            }
        }
        
        //ATP-4326
       if(objSCN.Credit_Frequency__c == 'One Time'){        
            objSCN.ffbilling__DerivePeriod__c = false;
            if(date.today().month() > 9)
                c2gPeriod = date.today().year()+'/0'+date.today().month();
                else
                c2gPeriod = date.today().year()+'/00'+date.today().month();              
                 
            if(mapPeriodID.containsKey(c2gPeriod))        
                objSCN.c2g__Period__c= mapPeriodID.get(c2gPeriod);             
                else
                objSCN.c2g__Period__c= null;      
                system.debug('---------objSCN.ffbilling__DerivePeriod__c--'+objSCN.ffbilling__DerivePeriod__c);

        }
        else {
            objSCN.ffbilling__DerivePeriod__c = true;
            objSCN.c2g__DueDate__c = objSCN.c2g__CreditNoteDate__c.addMonths(1);
           /* if(date.today().month()  > 9)
                c2gPeriod = date.today().year()+'/0'+date.today().month();
                else
                c2gPeriod = date.today().year()+'/00'+date.today().month();
                
                
                //system.debug('----------'+c2gPeriod);
                
             if(mapPeriodID.containsKey(c2gPeriod))        
                objSCN.c2g__Period__c= mapPeriodID.get(c2gPeriod);             
                else
                objSCN.c2g__Period__c= null;  
                */
        }
        
        system.debug('---duedate--------'+objSCN.c2g__Period__c+'--------'+objSCN.c2g__DueDate__c+'-----'+objSCN.c2g__CreditNoteDate__c);
        system.debug('----------'+objSCN);
        return objSCN;
    }
    
    private c2g__codaCreditNoteLineItem__c generateSCNLI(Order_Line_Item_Discount__c objOLID, Order_Line_Items__c objOLI, map<String, Id> dimensionsMapValues, boolean bFlagNet) {
        c2g__codaCreditNoteLineItem__c objSCNLI = new c2g__codaCreditNoteLineItem__c();
        objSCNLI.c2g__Quantity__c = 1;
        objSCNLI.c2g__UnitOfWork__c = 1;        
        if(objOLI != null) {
            objSCNLI.Order_Line_Item__c = objOLI.Id;
            objSCNLI.Billing_Frequency__c=objOLI.Billing_Frequency__c;
            if(objOLID.Issue_as_Single_Credit__c) {             
                objSCNLI.c2g__UnitPrice__c = Double.valueOf(objOLID.total_Credit_amount__c);
                if(bFlagNet) {
                    objSCNLI.c2g__UnitPrice__c = objOLID.Total_Net_Commission__c * -1;
                }
            }
            else {                
                if(objOLID.OLI_Billing_Frequency__c.equals(CommonMessages.singlePayment)) {
                    objSCNLI.c2g__UnitPrice__c = Double.valueOf(objOLID.total_Credit_amount__c);
                }
                else {
                    objSCNLI.c2g__UnitPrice__c = roundValues(Double.valueOf(objOLID.total_Credit_amount__c) / Double.valueOf(objOLID.Number_of_Billing_Periods_to_discount__c));
                }
            }
        }
        else {
            objSCNLI.c2g__UnitPrice__c = Double.valueOf(objOLID.total_Credit_amount__c);
            objSCNLI.Billing_Frequency__c = 'Single Payment';
        }
        objSCNLI.Transaction_Type__c = CommonMessages.scnBillingEngine;
        if(objOLI != null) {
            if(!bFlagNet) {
                objSCNLI.c2g__Product__c = objOLI.Product2__c;
            }
            else {
                objSCNLI.c2g__Product__c = System.Label.National_Commission_Product;
            }
            if(objOLI.Directory__c != null) {
                objSCNLI.Directory__c = objOLI.Directory__c;
            }
            if(objOLI.Directory_Edition__c != null) {
                objSCNLI.Directory_Edition__c = objOLI.Directory_Edition__c;
            }
        }
        else {
            objSCNLI.c2g__Product__c = objOLID.Product__c;
            objSCNLI.c2g__Dimension1__c = objOLID.Dimension_1__c;
            objSCNLI.c2g__Dimension2__c = objOLID.Dimension_2__c;
            objSCNLI.c2g__Dimension3__c = objOLID.Dimension_3__c;
            objSCNLI.c2g__Dimension4__c = objOLID.Dimension_4__c;
            objSCNLI.Directory__c = objOLID.Directory_Edition__r.Directory__c;
            objSCNLI.Directory_Edition__c = objOLID.Directory_Edition__c;
        }
        objSCNLI.ffbilling__CalculateTaxValue2FromRate__c = true;
        objSCNLI.ffbilling__CalculateTaxValue3FromRate__c = true;
        objSCNLI.ffbilling__CalculateTaxValueFromRate__c = true;
        objSCNLI.ffbilling__DeriveLineNumber__c = true;
        objSCNLI.ffbilling__DeriveTaxRate2FromCode__c = true;
        objSCNLI.ffbilling__DeriveTaxRate3FromCode__c = true;
        objSCNLI.ffbilling__DeriveTaxRateFromCode__c = true;
        objSCNLI.ffbilling__DeriveUnitPriceFromProduct__c = false;
        objSCNLI.ffbilling__SetTaxCode2ToDefault__c = true;
        objSCNLI.ffbilling__SetTaxCode3ToDefault__c = true;
        objSCNLI.ffbilling__SetTaxCodeToDefault__c = true;
        if (dimensionsMapValues != null && objOLI != Null) {
            DimensionsValues.assignDimensions(objOLI, null, objSCNLI, null, dimensionsMapValues);
        }
        //ATP-3676
        if(objOLI != Null){
            if(objOLI.Billing_Partner_Transferred__c == true)
            {
                if(objOLID.Credit_Issued_To1__c == CommonMessages.berryBillingPartner){
                    setDimension3.add(CommonMessages.BerryForDimension);                
                    dimensionsMapValues.putAll(DimensionsValues.dimension3Values(setDimension3));
                    objSCNLI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.BerryForDimension);
                }else if(objOLID.Credit_Issued_To1__c == CommonMessages.Telco){
                    setDimension3.add(CommonMessages.TelcoForDimension);
                    dimensionsMapValues.putAll(DimensionsValues.dimension3Values(setDimension3));
                    objSCNLI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.TelcoForDimension);
                }   
            }
        }
        
        
        return objSCNLI;
    }
    
    private void generateBCCRecord() {
    
        string BrandingPartner = '';
        Berry_Cares_Certificate__c objBCC = new Berry_Cares_Certificate__c();
        objBCC.Case__c = objCase.Id;
        objBCC.Account__c = objCase.AccountId;
        
         System.debug('####CommonMessages.FrontierTelcoPartner####'+ CommonMessages.FrontierTelcoPartner );
         System.debug('####objCase.Account.Telco_Partner__r.Name####'+ objCase.Account.Telco_Partner__r.Name);
        /* 
        if(objCase.Account.Client_Tier__c!=null)
        {
            if(!objCase.Account.Telco_Partner__r.Name.equals(CommonMessages.FrontierTelcoPartner) && (objCase.Account.Client_Tier__c.equals(CommonMessages.PlatinumPartner) || objCase.Account.Client_Tier__c.equals(CommonMessages.DiamondPartner))){
                BrandingPartner = CommonMessages.BerryCaresElite;
            }
            else if(objCase.Account.Telco_Partner__r.Name.equals(CommonMessages.FrontierTelcoPartner) && (objCase.Account.Client_Tier__c.equals(CommonMessages.PlatinumPartner) || objCase.Account.Client_Tier__c.equals(CommonMessages.DiamondPartner))){
                BrandingPartner = CommonMessages.FrontierCaresElite;
            }
            else if(objCase.Account.Telco_Partner__r.Name.equals(CommonMessages.FrontierTelcoPartner)){
                BrandingPartner = CommonMessages.FrontierCares;
            }
            else{
                BrandingPartner = CommonMessages.BerryCares;
            }
        }
        else if(objCase.Account.Telco_Partner__r.Name != null && objCase.Account.Telco_Partner__r.Name.equals(CommonMessages.FrontierTelcoPartner)){
            BrandingPartner = CommonMessages.FrontierCares;
        }
        else{
            BrandingPartner = CommonMessages.BerryCares;
        }*/
        
        //ATP-2895
        if(setOLITelcoPartner.size() > 0)
        {            
            if(setOLITelcoPartner.size() == 1){
                for(string telcoPartnerName:setOLITelcoPartner)
                {
                   if(objCase.Account.Client_Tier__c != null){
                        if(telcoPartnerName.equals(CommonMessages.FrontierTelcoPartner) && (objCase.Account.Client_Tier__c.equals(CommonMessages.PlatinumPartner) || objCase.Account.Client_Tier__c.equals(CommonMessages.DiamondPartner))) 
                        {
                            BrandingPartner = CommonMessages.FrontierCaresElite;
                        }
                        else if(telcoPartnerName.equals(CommonMessages.FrontierTelcoPartner) && (objCase.Account.Client_Tier__c != CommonMessages.PlatinumPartner || objCase.Account.Client_Tier__c != CommonMessages.DiamondPartner))
                        {
                            BrandingPartner = CommonMessages.FrontierCares;
                        }
                        else if(telcoPartnerName != CommonMessages.FrontierTelcoPartner &&  (objCase.Account.Client_Tier__c.equals(CommonMessages.PlatinumPartner) || objCase.Account.Client_Tier__c.equals(CommonMessages.DiamondPartner)))
                        {
                            BrandingPartner = CommonMessages.BerryCaresElite;
                        }
                        else if(telcoPartnerName != CommonMessages.FrontierTelcoPartner && (objCase.Account.Client_Tier__c != CommonMessages.PlatinumPartner || objCase.Account.Client_Tier__c != CommonMessages.DiamondPartner))
                        {
                            BrandingPartner = CommonMessages.BerryCares;
                        }
                   }
                   else if(telcoPartnerName.equals(CommonMessages.FrontierTelcoPartner) && objCase.Account.Client_Tier__c == null )
                    {
                        BrandingPartner = CommonMessages.FrontierCares;
                    }
                    else
                    {
                        BrandingPartner = CommonMessages.BerryCares;
                    }
                }
            }
            else
            {
                if(objCase.Account.Client_Tier__c!= null){
                        if(objCase.Account.Client_Tier__c.equals(CommonMessages.PlatinumPartner) || objCase.Account.Client_Tier__c.equals(CommonMessages.DiamondPartner))
                            {
                                BrandingPartner = CommonMessages.BerryCaresElite;}
                            else
                            {
                                BrandingPartner = CommonMessages.BerryCares;                                
                            }
                    }
                    else{ BrandingPartner = CommonMessages.BerryCares;}                
            }
        }        
        else
        {
            if(objCase.Account.Client_Tier__c!= null){
                        if(objCase.Account.Client_Tier__c.equals(CommonMessages.PlatinumPartner) || objCase.Account.Client_Tier__c.equals(CommonMessages.DiamondPartner))
                            {
                                BrandingPartner = CommonMessages.BerryCaresElite;}
                            else
                            {
                                BrandingPartner = CommonMessages.BerryCares;            
                            }
                    }
                    else{ BrandingPartner = CommonMessages.BerryCares;} 
        }
        
        if(objCase.BCC_Duration__c != null){
        objBCC.Duration__c = Decimal.valueOf(objCase.BCC_Duration__c);
        }
        objBCC.Redeemable__c = true;
        objBCC.Is_Redeemed__c = false;
        system.debug('Branding----' + BrandingPartner);
        objBCC.Branding__c = BrandingPartner;
        system.debug('Case Total BCC----' + objCase.Total_BCC__c + 'Case BCC Duration ----' + objCase.BCC_Duration__c);
        if(objCase.BCC_Duration__c == null){
        objBCC.BCC_Sub_Amount__c = 0.00;
        }
        else{
            objBCC.BCC_Sub_Amount__c = checkfornullnegative(objCase.Total_BCC__c / Integer.valueOf(objCase.BCC_Duration__c));
            //objBCC.BCC_Sub_Amount__c = checkfornullnegative(objCase.Total_BCC__c);
        }
        objBCC.CheckBCCValue__c = (objBCC.Duration__c > 1 ? 'BCC_Checked' : 'BCC_Unchecked');
        objBCC.UnCheckBCCValue__c = (objBCC.Duration__c > 1 ? 'BCC_Unchecked' : 'BCC_Checked');
        objBCC.Expiration_Date__c = date.today().addYears(1);
        insert objBCC;
    }
    
    @TestVisible private Decimal roundValues(Decimal fullRate) {
        fullRate = fullRate.setScale(2, RoundingMode.HALF_UP);
        system.debug('rate : '+ fullRate);
        return fullRate;
    }
    
    private void createSalesCreditNote() {
        list <c2g__codaCreditNote__c> lstInsertSCN = new list <c2g__codaCreditNote__c> ();
        map <String, Id> dimensionsMapValues = new map <String, Id> ();
        dimensionsMapValues = DimensionsValues.getDimensionByOLI(mapOLItem.values());
        system.debug('Dimensions Map Values are ' + dimensionsMapValues);
        map <Integer, list <c2g__codaCreditNoteLineItem__c>> mapSalesCNLI = new map <Integer, list <c2g__codaCreditNoteLineItem__c>> ();
        //ATp-4326
        list <c2g__codaPeriod__c> listc2gPeriod = new list<c2g__codaPeriod__c>([select id,name from c2g__codaPeriod__c]);
        
         for(c2g__codaPeriod__c objPeriod:listc2gPeriod)
         {
            mapPeriodID.put(objPeriod.name,objPeriod.id);
         }
         
        
        Integer iCount = 1;
        for (Order_Line_Item_Discount__c iterator : lstOLID) {
            if(iterator.Total_Credit_num__c > 0) {
                Order_Line_Items__c objOLI = new Order_Line_Items__c();
                if(iterator.Order_Line_Item__c != null) {
                    objOLI = mapOLItem.get(iterator.Order_Line_Item__c);
                    if(iterator.Issue_as_Single_Credit__c) {
                        lstInsertSCN.add(generateSCN(iterator, objOLI, dimensionsMapValues, system.today(), iCount));
                        if (!mapSalesCNLI.containsKey(iCount)) {
                            mapSalesCNLI.put(iCount, new list <c2g__codaCreditNoteLineItem__c> ());
                        }                       
                        mapSalesCNLI.get(iCount).add(generateSCNLI(iterator, objOLI, dimensionsMapValues, false));
                        if(iterator.Net_Commission__c && iterator.Total_Net_Commission__c > 0) {
                            mapSalesCNLI.get(iCount).add(generateSCNLI(iterator, objOLI, dimensionsMapValues, true));
                        }
                        iCount++;
                    }
                    else {
                        if(!iterator.OLI_Billing_Frequency__c.equals(CommonMessages.singlePayment) || iterator.OLI_Billing_Frequency__c == null) {
                            Date scnDate = dateCalculation(objOLI);
                            for(integer i = 0; i < iterator.Number_of_Billing_Periods_to_discount__c; i++) {
                                if(i > objOLI.Successful_Payments__c) {
                                    scnDate = scnDate.addMonths(1);
                                }
                                lstInsertSCN.add(generateSCN(iterator, objOLI, dimensionsMapValues, scnDate, iCount));
                                if (!mapSalesCNLI.containsKey(iCount)) {
                                    mapSalesCNLI.put(iCount, new list <c2g__codaCreditNoteLineItem__c> ());
                                }
                                mapSalesCNLI.get(iCount).add(generateSCNLI(iterator, objOLI, dimensionsMapValues, false));
                                iCount++;
                            }
                        }
                        else {
                            lstInsertSCN.add(generateSCN(iterator, objOLI, dimensionsMapValues, system.today(), iCount));
                            if (!mapSalesCNLI.containsKey(iCount)) {
                                mapSalesCNLI.put(iCount, new list <c2g__codaCreditNoteLineItem__c> ());
                            }
                            mapSalesCNLI.get(iCount).add(generateSCNLI(iterator, objOLI, dimensionsMapValues, false));
                            iCount++;
                        }
                    }
                }
                else {
                    lstInsertSCN.add(generateSCN(iterator, null, null, system.today(), iCount));
                    if (!mapSalesCNLI.containsKey(iCount)) {
                        mapSalesCNLI.put(iCount, new list <c2g__codaCreditNoteLineItem__c> ());
                    }
                    mapSalesCNLI.get(iCount).add(generateSCNLI(iterator, null, dimensionsMapValues, false));                    
                    iCount++;
                }
            }
        }
        
        system.debug('Testingg Size of SCN----' + lstInsertSCN.size());
        for(c2g__codaCreditNote__c objSCN : lstInsertSCN) {
            System.debug('Testingg objSCN '+objSCN);
            /*System.debug('Testingg objSCN.Order_Line_Item_Discount__c '+objSCN.Order_Line_Item_Discount__c);
            System.debug('Testingg objSCN.c2g__Dimension3__c '+objSCN.c2g__Dimension3__c);
            System.debug('Testingg objSCN.Customer_Name__c '+objSCN.Customer_Name__c);
            System.debug('Testingg objSCN.SC_Payment_Method__c '+objSCN.SC_Payment_Method__c);
            System.debug('Testingg objSCN.ffbilling__DeriveDueDate__c '+objSCN.ffbilling__DeriveDueDate__c);
            System.debug('Testingg objSCN.ffbilling__CopyDefaultPrintedTextDefinitions__c '+objSCN.ffbilling__CopyDefaultPrintedTextDefinitions__c);
            System.debug('Testingg objSCN.Transaction_Type__c '+objSCN.Transaction_Type__c);
            System.debug('Testingg objSCN.SC_Media_Type__c '+objSCN.SC_Media_Type__c);
            System.debug('Testingg objSCN.SC_Billing_Entity__c '+objSCN.SC_Billing_Entity__c);
            System.debug('Testingg objSCN.Credit_Frequency__c '+objSCN.Credit_Frequency__c);
            System.debug('Testingg objSCN.c2g__Account__c '+objSCN.c2g__Account__c);
            System.debug('Testingg objSCN.ffbilling__CopyAccountValues__c '+objSCN.ffbilling__CopyAccountValues__c);
            System.debug('Testingg objSCN.ffbilling__DerivePeriod__c '+objSCN.ffbilling__DerivePeriod__c);
            System.debug('Testingg objSCN.c2g__DueDate__c '+objSCN.c2g__DueDate__c);
            System.debug('Testingg objSCN.ffbilling__DeriveCurrency__c '+objSCN.ffbilling__DeriveCurrency__c);
            System.debug('Testingg objSCN.c2g__Opportunity__c '+objSCN.c2g__Opportunity__c);
            System.debug('Testingg objSCN.SC_Billing_Partner__c '+objSCN.SC_Billing_Partner__c);
            System.debug('Testingg objSCN.c2g__CreditNoteReason__c '+objSCN.c2g__CreditNoteReason__c);
            System.debug('Testingg objSCN.Case__c '+objSCN.Case__c);
            System.debug('Testingg objSCN.Auto_Number__c '+objSCN.Auto_Number__c);
            System.debug('Testingg objSCN.c2g__Dimension2__c '+objSCN.c2g__Dimension2__c);
            System.debug('Testingg objSCN.c2g__Dimension1__c '+objSCN.c2g__Dimension1__c);*/
            
        }
        if(lstInsertSCN.size() > 0 && lstInsertSCN.size() <= integer.valueOf(System.Label.Sales_Credit_Note_Limit)) {
            try {
                insert lstInsertSCN;
            }
            catch(Exception ex) {
                String error = ex.getMessage() + '. ';
                    List<String> errorMessages = c2g.CODAExtensionContext.getErrorMessages();
                    for( String errorMessage : errorMessages ) { 
                        if( errorMessage != null ) error += '; ' + errorMessage; 
                    }
                    throw new InvoiceGeneratorHelperException(error);
            }
           
           /* 
            catch(Exception ex) {
                String error = ex.getMessage() + '. ';
                List<String> errorMessages = c2g.CODAExtensionContext.getErrorMessages();
                for( String errorMessage : errorMessages ) { 
                  if( errorMessage != null ) error += '; ' + errorMessage; 
                }
                throw new InvoiceGeneratorHelperException(error);
            }  
               */
            list<c2g__codaCreditNoteLineItem__c> lstInsertSCNLI = new list<c2g__codaCreditNoteLineItem__c>();
            for(c2g__codaCreditNote__c iterator: lstInsertSCN) {
                if(mapSalesCNLI.get(Integer.valueOf(iterator.Auto_Number__c)) != null) {
                    for(c2g__codaCreditNoteLineItem__c iteratorSCNLI: mapSalesCNLI.get(Integer.valueOf(iterator.Auto_Number__c))) {
                        iteratorSCNLI.c2g__CreditNote__c = iterator.Id;
                        iteratorSCNLI.c2g__UnitPrice__c = iteratorSCNLI.c2g__UnitPrice__c.setScale(2);
                        system.debug('________freq_____'+iteratorSCNLI.Billing_Frequency__c);
                        lstInsertSCNLI.add(iteratorSCNLI);
                    }
                }
            }
            system.debug('---------test-----'+lstInsertSCNLI);
            if(lstInsertSCNLI.size() > 0) {
                insert lstInsertSCNLI;
            }
 
            creditnoteError=false;
            PageReference pageRef = new PageReference('/' + objCase.Id);
            pg=pageRef;
        }
        else if(lstInsertSCN.isEmpty() || lstInsertSCN.size() == 0){
            PageReference pageRef = new PageReference('/' + objCase.Id);
            pg=pageRef;
        }

        else {
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.severity.ERROR, 'More than ' + integer.valueOf(System.Label.Sales_Credit_Note_Limit) + ' and cannot be generated at one time. Please return back to Case');  
            ApexPages.addMessage(errMsg); 
            creditnoteError=true;
            List<case> UpdteCase = [SELECT Id, CaseNumber, AccountId, IsClosed, Status FROM Case WHERE Id = :objCase.Id];
                if(UpdteCase.size()>0)  {
                    UpdteCase[0].Status = 'New';
                    update UpdteCase[0];
                }
            pg= null;
        }
                        
        if(objCase.Total_BCC__c > 0) {
            generateBCCRecord();
            creditnoteError=false;
            
            PageReference pageRef = new PageReference('/' + objCase.Id);
            pg=pageRef;
        } 
    }
    
    public Pagereference backToCase(){
        PageReference pageRef = new PageReference('/' + objCase.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public decimal checkfornullnegative(Decimal discpercent) {
        if(discpercent == null || discpercent < 0)
        return 0;
        else
        return discpercent;
    }
    
    @TestVisible private Date dateCalculation(Order_Line_Items__c objOLI) {
        if(objOLI.Next_Billing_Date__c != null) {
            return objOLI.Next_Billing_Date__c;
        }
        return Date.today();        
    }
}