public class BoostRenewalAccountManualSchedule {

    public pagereference doRun() {
        Date TodayDate = Date.today();
        string BookStatus = 'BOTS';
        set<Id> setOpportunityID = new set<Id>();
        String soql = 'Select Id,Book_Status__c,Final_Auto_Renew_Job__c,Directory__c from Directory_Edition__c where Book_Status__c = : BookStatus '+' AND Final_Auto_Renew_Job__c =: '+'TodayDate';
        List<Directory_edition__c> DirEditionLst = database.query(soql);
        set<Id> objDirId = new set<Id>();
        for(Directory_Edition__c objDE : DirEditionLst){
            objDirId.add(objDE.Directory__c);
        }
        system.debug('****Directory id****'+objDirId);
        map<Id, directory_edition__c> mapDEBOTS = new map<Id, directory_edition__c>([Select Id,Book_Status__c,Directory__c from directory_edition__c where Directory__c IN :objDirId AND Book_Status__c = 'BOTS-1']);
        if(mapDEBOTS.size() > 0) {
            list<Order_Line_Items__c> oliLst = [Select Id,Name,Opportunity__c,Directory_Edition__c,Is_Handled__c from Order_Line_Items__c where Directory_Edition__c IN :mapDEBOTS.keySet() AND Is_Handled__c = false AND isCanceled__c = false];
            if(oliLst.size()>0) {
                for(Order_Line_Items__c iterator : oliLst) {
                    setOpportunityID.add(iterator.Opportunity__c);
                }
            }
        }
        system.debug('****Opty Id****'+setOpportunityID);
        if(setOpportunityID.size()>0){
            BoostRenewalAccountsBatchController BoABC = new BoostRenewalAccountsBatchController(setOpportunityID);
            Database.executeBatch(BoABC,2);
        }
        return (new pagereference('/apex/BoostRenewalAccountManualSchedule').setredirect(true));
    }
}