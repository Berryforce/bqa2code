global class TBCscSupressionBatchScheduler implements Schedulable {
	public Interface TBCscSupressionBatchSchedulerInterface {
		void execute(SchedulableContext sc);
	}
	global void execute(SchedulableContext sc) {
		Type targetType = Type.forName('TBChSuppressionbatchHandler');
        if(targetType != null) {
            TBCscSupressionBatchSchedulerInterface obj = (TBCscSupressionBatchSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
	}
}