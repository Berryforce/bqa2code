@isTest
public class SACRACTransmission_BAIUDTest {
	static testMethod void SACRACTest() {
		Test.startTest();		
		Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.createAccount('cmr');
        Account pubAcct = TestMethodsUtility.createAccount('publication');
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(acct, ord, oppty);
        og.selected__c = true;
        insert og;
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        insert oln; 
        
        Directory__c dir = TestMethodsUtility.createDirectory();
        
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        DE.LSA_Directory_Version__c = 'Test';
        insert DE;
		
		SACRAC_Transmission__c SACRAC = new SACRAC_Transmission__c();
		SACRAC.DIRECTORY_CODE__c = dir.Directory_Code__c;
		SACRAC.CMR_Number__c = acct.AccountNumber;
		SACRAC.CMR_Number__c = acct.AccountNumber;
		SACRAC.DIRECTORY_VERSION__c = DE.LSA_Directory_Version__c;
		insert SACRAC;
		
		SACRAC.PUB_CODE__c = pubAcct.Publication_Code__c;
		update SACRAC;
		
		Test.stopTest();
	}
}