public with sharing class ExcelPageController {

    //Getter and Setters
    public String filecontent {get;set;}
    public Blob excelBody {get;set;}  
    public String dirCode {get;set;}
    
    //Constructor
    public ExcelPageController() {
         
        String dirHId = ApexPages.currentPage().getParameters().get('id');
        dirCode = System.currentPagereference().getParameters().get('dirCode');
        List<String> lst_SHMids = new List<String> ();
        Map<Id, Boolean> mpDhSH = new Map<Id, Boolean> ();
        String replcName;
        
        System.debug('************' + dirCode);
        
        if(String.isNotBlank(dirHId)){
            for(list<Section_Heading_Mapping__c> ss:[select Id, Name, Directory_Heading__c, SHM_Advertising_Content_Not_Available__c from Section_Heading_Mapping__c where( Directory_Section__c  IN (select id from Directory_Section__c where Directory__c=:dirHId) and is_Active__c=true )  limit 40000 ]) {
                    for(Section_Heading_Mapping__c s:ss){
                    lst_SHMids.add(s.Directory_Heading__c);
                    mpDhSH.put(s.Directory_Heading__c, s.SHM_Advertising_Content_Not_Available__c);
                }
            }
            //Constructing data in .csv format
            fileContent =  'Name' + ',' + 'Heading Code' + ',' + 'CRF Indicator' + ',' + 'Adv Content Indicator' +'\n';        

            for(Directory_Heading__c iterator: [SELECT Id, Name, Code__c, (SELECT Id from Cross_Reference_Headings__r) from Directory_Heading__c where Discontinued_Date__c = null and Spotzer_Industry__c = false and YPC_Heading__c = false and Id In:lst_SHMids]) {
                replcName = iterator.Name;
                if(iterator.Cross_Reference_Headings__r.size()>0){
                    fileContent += replcName.replace(',', '-') + ',' + iterator.Code__c + ',' + 'Y' + ',' + advCntInd(mpDhSh.get(iterator.Id)) +'\n';
                } else {
                    fileContent += replcName.replace(',', '-') + ',' + iterator.Code__c + ',' + 'N' + ',' + advCntInd(mpDhSh.get(iterator.Id)) +'\n';
                }
            }
        }          
    }
    
    public String advCntInd(Boolean advCnt) {
        String advCntInd;
        if(advCnt) {
            advCntInd = 'N';
        } else {
            advCntInd = 'Y';
        }
        
        return advCntInd;
    }
}