global class SplitBatchJobsScheduler implements Schedulable {
	public interface SplitBatchJobsSchedulerInterface {
		void execute(SchedulableContext sc);
	}
	global void execute(SchedulableContext sc) {
        Type targetType = Type.forName('SplitBatchJobsSchedulerHandler');
        if(targetType != null) {
            SplitBatchJobsSchedulerInterface obj = (SplitBatchJobsSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
    }
}