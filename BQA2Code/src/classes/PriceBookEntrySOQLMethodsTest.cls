@IsTest(SeeAllData=true)
public class PriceBookEntrySOQLMethodsTest {
    public static testmethod void PriceBookEntrySOQLMethodsTest() {        
        Pricebook2 objPB = [Select Name, IsStandard, IsActive, Id, Description From Pricebook2 where IsStandard = true limit 1][0];
        
        Product2 objProduct = new Product2(ProductCode = '001', Name = 'Unit Test Product', IsActive = true, IsBundle__c = false);        
        insert objProduct;
        
        PricebookEntry objPricebookEntry = [Select Id, UseStandardPrice, UnitPrice, Product2Id, Pricebook2Id, IsActive from PricebookEntry where Product2Id = :objProduct.Id limit 1];
        
        Test.startTest();
        
        PriceBookEntrySOQLMethods.getPriceBookEntryByProductID(new set<String> {objProduct.Id});
        PriceBookEntrySOQLMethods.getPriceBookEntryByProductID(new set<Id> {objProduct.Id});
        PriceBookEntrySOQLMethods.getPriceBookEntryByProductIDandIsActive(new set<Id> {objProduct.Id});
        PriceBookEntrySOQLMethods.getPriceBookEntryByUDAC(new set<String> {objProduct.ProductCode});
        PriceBookEntrySOQLMethods.getProductIdByPriceBookEntryIDs(new set<Id> {objPricebookEntry.Id});
        Test.stopTest();
    }}