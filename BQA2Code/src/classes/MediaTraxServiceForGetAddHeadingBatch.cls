global class MediaTraxServiceForGetAddHeadingBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    global String SOQL = '';
    global Boolean bolChainBatch = false;
    
    global MediaTraxServiceForGetAddHeadingBatch(){
    }
    
    global MediaTraxServiceForGetAddHeadingBatch(Boolean bolChainBtc){
        bolChainBatch = bolChainBtc;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        SOQL = 'Select Id, Name, Media_Trax_Heading_ID__c from Directory_Heading__c where Media_Trax_Heading_ID__c = null';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, list<Directory_Heading__c> lstHeading){
        MediaTraxHeadingCalloutController_V1 objMediaTraxService = new MediaTraxHeadingCalloutController_V1();
        objMediaTraxService.ProcessMediaTraxForGetAddHeadingScope(lstHeading);
    }
    
    global void finish(Database.BatchableContext bc){
        if(bolChainBatch) {
            Batch_Size__c objBatchSize = Batch_Size__c.getInstance('MediaTraxHeading');
            MediaTraxServiceForUpdateHeadingBatch objCallout = new MediaTraxServiceForUpdateHeadingBatch(true);
            database.executebatch(objCallout, Integer.valueOf(objBatchSize.Size__c));
        }
    }
}