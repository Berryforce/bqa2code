@isTest
public class NIOpportunityBundlesBatchTest{
    static testMethod void NIOpportunityBundlesBatchTest01() {
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.New_Print_Bill_Date__c=System.Today();
        objDirEd.Bill_Prep__c=System.Today();
        insert objDirEd;
        
     //   Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
     //   insert objDirEd1;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
        
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
            Product2 newProduct = TestMethodsUtility.generateproduct();
             newProduct.Product_Type__c = CommonMessages.printMediaType;
            newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
              newProduct.Family='Print';
            if(x==0){
                newProduct.Family='Digital';
            }
          
      newProduct.Vendor__c =CommonMessages.prdVendorYP;
      newProduct.ProductCode='UDAC';
            lstProduct.add(newProduct);
        }
        insert lstProduct;
        
        list<PricebookEntry> lstPBE = new list<PricebookEntry>();
        for(Product2 iterator : lstProduct) {
        
            PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
            lstPBE.add(pbe);
        }
        insert lstPBE;
                
        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        //list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        for(Product2 iterator : lstProduct) {
            Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
            objDPM.Product2__c = iterator.Id; 
        }
        insert lstDPM;
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;      
        newOpportunity.StageName='Closed UTC';
        insert newOpportunity;
    
    Listing__c newListing=TestMethodsUtility.generateListing();
    insert newListing;
    
      Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
      newOrder.Billing_Anniversary_Date__c=System.Today();
      update newOrder;
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
             newOrLI.Parent_ID__c='2434-4545';
             newOrLI.Listing__c=newListing.Id;
        List<Order_Line_Items__c> listnewOrli = new List<Order_Line_Items__c>();
        listnewOrli.add(newOrLI);
     //   insert listnewOrli;    
        
        
    List<Modification_Order_Line_Item__c> MOLIList = new List<Modification_Order_Line_Item__c>();
    Modification_Order_Line_Item__c mol = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI.id);
   // mol.Directory_Section__c = objDS.Id;
    mol.Order_Group__c = newOrderSet.Id;
    mol.Action_Code__c = 'Renew';
     mol.Product_Type__c='SEO';
    mol.Product2__c =lstProduct[0].id;
    MOLIList.add(mol);
       insert MOLIList;
    
    
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        Integer i=0;
        for(PricebookEntry iterator : lstPBE) {
            i++;
            OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
            objOLI.PricebookEntryId = iterator.Id;
            objOLI.Billing_Duration__c = 12;
            objOLI.Directory__c = objDir.Id;
            objOLI.Directory_Edition__c = objDirEd.Id;
            objOLI.Full_Rate__c = 30.00;
            objOLI.UnitPrice = 30.00;
            objOLI.Package_ID__c = '123456';
            objOLI.Billing_Partner__c = objOLI.Id;
            objOLI.OpportunityId = newOpportunity.Id;
            objOLI.Directory_Heading__c = objDH.Id;
            objOLI.Directory_Heading1__c = objDH.Id;
            objOLI.Directory_Heading2__c = objDH.Id;
            objOLI.Directory_Heading3__c = objDH.Id;
            objOLI.Directory_Heading4__c = objDH.Id;
            objOLI.Directory_Section__c = objDS.Id;
            objOLI.Renewals_Action__c=CommonMessages.NewRenewalAction;
            objOLI.Parent_ID__c ='2014-8235_tst'+i;
            //objOLI.Parent_ID_of_Addon__c='2014-8235_tst1';
            objOLI.Effective_Date__c = System.Today();
            objOLI.Is_P4P__c =true;
            lstOLI.add(objOLI);
            mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;
        
        NIOpportunityBundlesBatch objNI = new NIOpportunityBundlesBatch(lstOLI);
        Database.executeBatch(objNI);
        objNI.execute(null, lstOLI);
    }
}