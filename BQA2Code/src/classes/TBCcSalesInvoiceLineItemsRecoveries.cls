public class TBCcSalesInvoiceLineItemsRecoveries {
    public string accountid {get;set;}
    public list<TBCcSILIRecoveriesWrapper> lstSILIWrapper{get;set;}
    public Dummy_Object__c objDummy{get;set;}
    
    public  TBCcSalesInvoiceLineItemsRecoveries(ApexPages.StandardController controller) {
        objDummy = new Dummy_Object__c();
        accountId = controller.getId();
        lstSILIWrapper = new list<TBCcSILIRecoveriesWrapper>();
        fetchSILIbyDocumentNumber();
      }
    public void cloneSIN() {
        system.debug('Cloned SI');
        set<Id> setSIId = new set<Id>();
        list<c2g__codaInvoiceLineItem__c> lstcloneSILI = new list<c2g__codaInvoiceLineItem__c>();
        map<Id,decimal> mapUnitpriceSILI = new map<Id,decimal>();
        map<Id,c2g__codaInvoice__c> mapcloneSI = new map<Id,c2g__codaInvoice__c>();
        string strPeriod = CommonMethods.getCurrentPeriod();
        c2g__codaPeriod__c objPeriod = [Select Id,Name from c2g__codaPeriod__c where Name=:strPeriod];
        boolean bflag=false;
        for(TBCcSILIRecoveriesWrapper iteratorWrapper : lstSILIWrapper) {
            if(iteratorWrapper.ischecked) {
                setSIId.add(iteratorWrapper.objSILI.c2g__Invoice__c);
                if(!mapUnitpriceSILI.containskey(iteratorWrapper.objSILI.Id) && iteratorWrapper.unitPrice != null) {
                    mapUnitpriceSILI.put(iteratorWrapper.objSILI.Id,iteratorWrapper.unitPrice);
                }
            }
            else {
                iteratorWrapper.unitPrice=null;
            }
        }
        Savepoint sp = Database.setSavepoint();
        try {
            if(setSIId.size()> 0) {
                list<c2g__codaInvoice__c> lstSI = SalesInvoiceSOQLMethods.getSalesInvoiceByInvoiceID(setSIId);
                if(lstSI.size()> 0) {
                    for(c2g__codaInvoice__c iteratorSI : lstSI) {
                        c2g__codaInvoice__c objnewSI = new c2g__codaInvoice__c();
                        objnewSI = iteratorSI.clone(false);
                        objnewSI.c2g__Period__c = objPeriod.Id;
                        objnewSI.c2g__InvoiceStatus__c = 'In Progress';
                        objnewSI.Transaction_Type__c = 'WR - Recovery';
                        objnewSI.Invoice_Reason__c = 'Recovery';
                        if(String.isNotEmpty(objnewSI.SI_Payment_Method__c)) {
                            objnewSI.SI_Payment_Method__c = 'Statement';
                        }
                        if(!mapcloneSI.containskey(iteratorSI.Id)) {
                            mapcloneSI.put(iteratorSI.Id,objnewSI);
                        }
                    }
                    if(mapcloneSI.size()> 0) {
                        insert mapcloneSI.values();
                    }
                }
            }
            if(mapUnitpriceSILI.size()> 0) {
                list<c2g__codaInvoiceLineItem__c> listSILI = SalesInvoiceLineItemSOQLMethods.getInvoiceLineItemById(mapUnitpriceSILI.keyset());
                if(listSILI.size()> 0) {
                    for(c2g__codaInvoiceLineItem__c objSILI : listSILI) {
                        c2g__codaInvoiceLineItem__c objnewSILI = new c2g__codaInvoiceLineItem__c();
                        objnewSILI = objSILI.clone(false);
                        objnewSILI.c2g__UnitPrice__c = mapUnitpriceSILI.get(objSILI.Id);
                        if(mapcloneSI.size()> 0) {
                            objnewSILI.c2g__Invoice__c = mapcloneSI.get(objSILI.c2g__Invoice__c).Id;
                        }
                        lstcloneSILI.add(objnewSILI);
                    }
                }
                if(lstcloneSILI.size()> 0) {
                    insert lstcloneSILI;
                    bflag=true;
                }
            }
        }catch(exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,ex.getMessage());
            ApexPages.addMessage(myMsg);
            Database.RollBack(sp);
        }
        if(bflag) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'Sales Invoices and Line Items are created');
            ApexPages.addMessage(myMsg);
        }
    }
    public list<TBCcSILIRecoveriesWrapper> fetchSILIbyDocumentNumber() {
        set<string> setdocNumber = new set<string>();
        if(accountid != null) {
            setdocNumber.addAll(fetchDocumentNumber(accountid));
        }
        if(setdocNumber.size()> 0) {
            list<c2g__codaInvoiceLineItem__c> lstSILI = SalesInvoiceLineItemSOQLMethods.getInvoiceLineItembyDocNumber(setdocNumber);
            if(lstSILI.size()> 0) {
                lstSILIWrapper=wrapperlistCreation(lstSILI);
            }
        }       
        return lstSILIWrapper;
    }
    public pagereference doSearch() {
        list<c2g__codaInvoiceLineItem__c> lstSILIsearch = new list<c2g__codaInvoiceLineItem__c>();
        lstSILIWrapper = new list<TBCcSILIRecoveriesWrapper>();
        Date InvoiceStDate;
        Date InvoiceEdDate;
        set<string> setdocNumber = new set<string>();
        system.debug('From date'+objDummy.From_Date__c);
        system.debug('To date'+objDummy.To_Date__c);
        if(objDummy.From_Date__c != null) { 
            InvoiceStDate = objDummy.From_Date__c;
        }
        if(objDummy.To_Date__c != null) { 
            InvoiceEdDate = objDummy.To_Date__c;
        }
        if(accountid != null) {
            setdocNumber.addAll(fetchDocumentNumber(accountid));
        }
        if(setdocNumber.size()> 0) {
            list<c2g__codaInvoiceLineItem__c> lstSILI = SalesInvoiceLineItemSOQLMethods.getInvoiceLineItembyDocNumInvoiceDate(setdocNumber,InvoiceStDate,InvoiceEdDate);
            system.debug('Invoice Line item list'+lstSILI.size());
            if(lstSILI.size()> 0) {
                lstSILIWrapper=wrapperlistCreation(lstSILI);
            }
        }
        /*else if(objDummy.From_Date__c == null && objDummy.To_Date__c == null) {
          ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Please Select Invoice From/to dates for Search');
          ApexPages.addMessage(myMsg);
        }*/
        return null;
    }
    
    private static list<TBCcSILIRecoveriesWrapper> wrapperlistCreation(list<c2g__codaInvoiceLineItem__c> lstSILI) {
        list<TBCcSILIRecoveriesWrapper> lstobjSILIWrapper = new list<TBCcSILIRecoveriesWrapper>();
        for(c2g__codaInvoiceLineItem__c iteratorSILI :lstSILI) {
            TBCcSILIRecoveriesWrapper objWrapper = new TBCcSILIRecoveriesWrapper();
            objWrapper.objSILI = iteratorSILI;
            lstobjSILIWrapper.add(objWrapper);
        }
        return  lstobjSILIWrapper;  
    }
    private static set<string> fetchDocumentNumber(string acctid) {
        set<string> setdocNum = new set<string>();
        list<ffps_ct__MatchingWriteOff__c> lstwriteOff = [SELECT Id, ffps_ct__LineType__c, ffps_ct__FlatTransaction__r.ffps_ct__DocumentNumber__c, Account__c FROM ffps_ct__MatchingWriteOff__c where Account__c =: acctid and ffps_ct__LineType__c= 'Write-off' and ffps_ct__FlatTransaction__c != null and MWO_Sales_Invoice_Indicator__c = true];
        if(lstwriteOff.size()> 0) {
            for(ffps_ct__MatchingWriteOff__c iterator :lstwriteOff) {
                if(iterator.ffps_ct__FlatTransaction__r.ffps_ct__DocumentNumber__c != null) {
                    setdocNum.add(iterator.ffps_ct__FlatTransaction__r.ffps_ct__DocumentNumber__c);
                }
            }
        }
        return setdocNum;
    }
    
    public class TBCcSILIRecoveriesWrapper{
        public boolean ischecked{get;set;}
        public decimal unitPrice {get;set;}
        public c2g__codaInvoiceLineItem__c objSILI{get;set;}
    }
}