public with sharing class BugsandEnhancementsSearchContv3 {  
   public list <case> cs {get;set;}  
   public string searchstring {get;set;}  
   public BugsandEnhancementsSearchContv3 (ApexPages.StandardController controller) {  
   }  
   public void search()
   {  
     string searchquery='select Id, CaseNumber, Status, RecordType.Name, JIRA_Assigned_To__c, CreatedBy.Alias, CreatedDate, Priority, JIRA_Link__c, Environment_and_Application__c, Case_Category__c, Subject, Description, Opened_By_Person_s_Email__c from Case where RecordType.Name IN (\'Bug\', \'Enhancement\') AND ((CaseNumber like \'%' + String.escapeSingleQuotes(searchstring) + '%\') OR (Opened_By_Person_s_Email__c like \'%' + String.escapeSingleQuotes(searchstring) + '%\')) Order By CaseNumber desc Limit 40';  
     cs= Database.query(searchquery);  
   }  
   public void clear(){  
   cs.clear();  
   }  
 }