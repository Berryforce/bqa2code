//Modified by Mythili for ticket ATP-4263.
//Added Database.Stateful to track the total number of errorneous records in the job
global class LocalBillingCreateStmtBatch Implements Database.Batchable <sObject>,Database.Stateful {
    global Date ObjDummyDate = null;
    global Integer numOfErrorRecords=0;
    global String accStartNumber=null;
    global String accEndNumber=null;
    
    global LocalBillingCreateStmtBatch(Date ObjDummyDate){
    	this.ObjDummyDate=ObjDummyDate;
    }
    
    global LocalBillingCreateStmtBatch(Date ObjDummyDate,String accStartNum,String accEndNum) {
        this.ObjDummyDate=ObjDummyDate;
        //for ticket CC-2200
        this.accStartNumber=accStartNum;  
        this.accEndNumber=accEndNum;  
    }
//ATP-4215 added AR Balance in query
    global Database.queryLocator start(Database.BatchableContext bc) {
        set<String> setStatus = new set<String>{'Complete'};
        Integer DayofStatement = ObjDummyDate.day();
        String strAcctCustRT = System.Label.TestAccountCustomerRT;
        String strQuery = 'Select Id, Name, RecordType.Name, Collection_Status__c, Final_Statement_Sent_Out__c,AR_Balance__c, '+             
        '(Select Id, AccountId, Name, PrimaryBilling__c,Phone, Paperless__c,Email From Contacts WHERE PrimaryBilling__c = true Limit 1), '+
        '(SELECT Account__r.Name,Account__c,Amount_Due__c,Balance_Last_Statement__c,CreatedDate,STMNT_Statement_Logo__c FROM Statement_Header__r Order By CreatedDate DESC limit 1) From Account where DAY_IN_MONTH(Billing_Anniversary_Date__c) =: DayofStatement AND AR_Balance__c > 10.00 AND Statement_Suppression__c = false and recordtypeid = :strAcctCustRT ';
        //where ID IN: setAccountId';
        if(accStartNumber!=null && accEndNumber!=null){  
        	strQuery+=' and Account_Number_For__c>=:accStartNumber and Account_Number_For__c<=:accEndNumber';
        }
        system.debug('Query&&&&&&'+strQuery);
        return Database.getQueryLocator(strQuery);    
    }

    global void execute(Database.BatchableContext bc, list<Account> accList) {
        try {
            if(accList.size()>0)
                localBillingCreateStatementHandler.createStatement(accList, ObjDummyDate);
        }
        catch(CustomException objExp) {
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+objExp.strType+'. Error Message : '+objExp.getMessage(), objExp.strStackTrace, 'Local Billing Statement Creation Batch', accList[0].Id);
            numOfErrorRecords=numOfErrorRecords+1;
        }
        catch(Exception objExp) {
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+objExp.getTypename()+'. Error Message : '+objExp.getMessage(), objExp.getStackTraceString(), 'Local Billing Statement Creation Batch', accList[0].Id);
            numOfErrorRecords=numOfErrorRecords+1;
        }
    }

    global void finish(Database.BatchableContext bc) {
        AsyncApexJob asyncBatch = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        //list<String> lstEmailAddress = new list<String>{asyncBatch.CreatedBy.Email};
        String strErrorMessage = '';
        if(asyncBatch.NumberOfErrors > 0){
            strErrorMessage = asyncBatch.ExtendedStatus;
        }
        CommonEmailUtils.sendHTMLEmailForTargetObject(asyncBatch.CreatedById, 'Create Statement Batch Process is ' + asyncBatch.Status, 'The batch Apex job processed ' + asyncBatch.TotalJobItems +'. Total number of errors '+numOfErrorRecords + '. Please check Exception/Error tab see if any error.');
    }
}