global class SensitiveHeadingUpdateBatchSchedule implements Schedulable {
   public Interface SensitiveHeadingUpdateBatchScheduleInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('SensitiveHeadingUpdateBatchScheduleHndlr');
        if(targetType != null) {
            SensitiveHeadingUpdateBatchScheduleInterface obj = (SensitiveHeadingUpdateBatchScheduleInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}