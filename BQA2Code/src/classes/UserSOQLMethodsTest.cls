@IsTest
public class UserSOQLMethodsTest {
    @isTest static void UserSOQLMethodsCoverage() {
        Set<Id> setUserIds = new Set<Id>();
        Profile objPF = TestMethodsUtility.getStandardUserProfile();
        User newUser = TestMethodsUtility.generateUser(objPF.Id, system.Label.TestRolePerformanceAdvisorId);
        insert newUser;
        system.assertNotEquals(newUser, null);
        setUserIds.add(newUser.Id);
        System.assertNotEquals(null, UserSOQLMethods.getUserDetailsByID(setUserIds));
    }
}