public class LocalBillingCommonMethods {
	public static boolean apexBatchJobStatus(Set<Id> setBatchIds) {
        Map<id,AsyncApexJob> mapBatchFFJob = new Map<Id,AsyncApexJob>();
        Integer iCount = 0;
        if(setBatchIds.size() > 0) {
        	mapBatchFFJob = new Map<Id,AsyncApexJob>([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:setBatchIds]);
        	for(ID idBatch : setBatchIds) {
        		if(mapBatchFFJob.get(idBatch) != null) {
        			if(mapBatchFFJob.get(idBatch).Status == 'Completed') {
        				iCount++;
        			}
        		}
        	}
        	
        	if(mapBatchFFJob.size() == iCount) {
        		return true;
        	}
        	return false;
        }
        return true;
    }
}