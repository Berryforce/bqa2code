global class DataFulFillmentCleanUpBatchImpl implements Database.Batchable<SObject>
{
    public String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
      return Database.getQueryLocator(query);
    }
    

    global void execute(Database.BatchableContext BC, List<sObject> scope){
    
      delete scope;
      DataBase.emptyRecycleBin(scope);
    }

    global void finish(Database.BatchableContext BC)
    {
      AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email, CreatedById FROM AsyncApexJob WHERE Id =:BC.getJobId()];
    
      // Send an email to the Apex job's submitter notifying of job completion. 
      /*String[] toAddresses = new String[] {a.CreatedBy.Email};
      mail.setToAddresses(toAddresses);*/
      CommonEmailUtils.sendTextEmailForTargetObject(a.CreatedById, 'Data Fulfillment Deletion Job ' + a.Status, 
      												'The Data Fulfillment job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.');
    }

}