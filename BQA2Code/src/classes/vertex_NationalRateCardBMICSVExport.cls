/***********************************************************************************************
* Controller Name   : vertex_NationalRateCardBMICSVExport    
* Date              : 07/28/2015  
* Author            : Surya Kiran Chintala   
* Purpose           : Exporting "Directory Product Mappings" related to Directory from Rate Table       
* Change History    : 
* Date                  Programmer              Reason  
* -------------------- ------------------- -------------------------    
* 07/28/2015            Surya Kiran CH          Initial Version  
**************************************************************************************************/ 
public with sharing class vertex_NationalRateCardBMICSVExport {
    public string strLablevaluesExport{get;set;}
    public string strFieldValuesExport{get;set;}
    
    
    public vertex_NationalRateCardBMICSVExport(){
        String dirId = ApexPages.currentPage().getParameters().get('id');
        
        if(dirId!=null){
            strLablevaluesExport='UDAC' +','+ 'Full Rate' +','+ 'Directory'+'\n';
            strFieldValuesExport='';
            for(Directory_Product_Mapping__c dpms : [select id,Product_Code_UDAC__c,FullRate__c,Directory__r.Name,Directory__r.Directory_Code__c from Directory_Product_Mapping__c where Directory__c=:dirId AND National_Only__c =false order by Product_Code_UDAC__c]){
                
                String prodCode = dpms.Product_Code_UDAC__c!=null ? dpms.Product_Code_UDAC__c : '';
                String fullRate = dpms.FullRate__c!=null ? String.valueOf(dpms.FullRate__c) : '' ;
                String Directory = dpms.Directory__r.Directory_Code__c!=null ? dpms.Directory__r.Directory_Code__c : '';
                
                strFieldValuesExport += prodCode +','+ fullRate +','+ Directory +',';
                strFieldValuesExport += '\n';
            }
        }
    }
}