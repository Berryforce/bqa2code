/***********************************************************
Controller class to add products to an existing Subscription
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 04/09/2015
$Id$
************************************************************/
public with sharing class AddSubscriptionProductController {

    //Getter and setters
    private Digital_Product_Requirement__c dff;

    public String dffId {
        get;
        set;
    }

    public String newFurl {
        get;
        set;
    }

    public String orderSetId {
        get;
        set;
    }

    public String mOrderSetId {
        get;
        set;
    }

    List < Digital_Product_Requirement__c > allDffs = new List < Digital_Product_Requirement__c > ();
    List < Digital_Product_Requirement__c > lstUpdtDffs = new List < Digital_Product_Requirement__c > ();
    List < Order_Line_Items__c > lstUpdtOL = new List < Order_Line_Items__c > ();
    List < Modification_Order_Line_Item__c > lstUpdtMOL = new List < Modification_Order_Line_Item__c > ();

    Map < String, String > mpPrdIds = new Map < String, String > ();

    //Get nigel endpoint url from custom settings
    public static final string url = Label.Talus_Nigel_Url;

    public String stDate;
    public String edDate;
    public Datetime tdayDt = datetime.now();
    public Datetime dtm;
    public Date dt;

    //Constructor
    public AddSubscriptionProductController(ApexPages.StandardController controller) {

        if (!Test.isRunningTest()) {
            List < String > flds = new List < String > {
                'Id', 'Name', 'Test_PackageId__c', 'OrderLineItemID__r.Order_Group__c', 'ModificationOrderLineItem__r.Order_Group__c'
            };
            controller.addFields(flds);
        }

        this.dff = (Digital_Product_Requirement__c) Controller.getRecord();

    }

    public pageReference sbrprdt() {

        dffId = dff.id;
        if (!Test.isRunningTest()) {
            orderSetId = dff.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        } else if (Test.isRunningTest()) {
            Digital_Product_Requirement__c dprRcd = [SELECT Id, OrderLineItemID__r.Order_Group__c, ModificationOrderLineItem__r.Order_Group__c, Test_PackageId__c from Digital_Product_Requirement__c where id = : dffId];
            orderSetId = dprRcd.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dprRcd.ModificationOrderLineItem__r.Order_Group__c;
        }

        allDffs = CommonUtility.addOnPrdtsSubmit(orderSetId, mOrderSetId);

        if (allDffs.size() > 0) {

            Set < String > setpckskus = new Set < String > ();
            Set < String > setprdSkus = new Set < String > ();

            //Logic to pull all product related properties to send to talus
            for (Digital_Product_Requirement__c pkgDffs: allDffs) {
                setpckskus.add(pkgDffs.Test_PackageId__c);
                setprdSkus.add(pkgDffs.Test_ProductId__c);
            }

            Map < String, List < Product_Properties__c >> mapPkgPrpty = CommonUtility.pkgPrdtPrpts(setpckskus, setprdSkus);

            for (Digital_Product_Requirement__c dff: allDffs) {

                try {

                    if(!Test.isRunningTest()){

                        if (String.isNotBlank(dff.Talus_Subscription_Id__c)) {

                            newFurl = url + dff.Account__r.TalusAccountId__c + '/subscriptions/' + dff.Talus_Subscription_Id__c + '/';
                            String rspns = TalusRequestUtilityGET.initiateRequest(newFurl);
                            System.debug('************' + rspns);
                            
                            Map < String, Object > nRspns = (Map < String, Object > ) JSON.deserializeUntyped(rspns);

                            List < Object > gLstObj = (List < Object > ) nRspns.get('products');
                            integer i;
                            for (i = 0; i < gLstObj.size(); i++) {
                                mpPrdIds.put(String.valueof(((Map < String, Object > ) gLstObj[i]).get('code')), String.valueof(((Map < String, Object > ) gLstObj[i]).get('id')));
                            }
                            System.debug('************' + mpPrdIds);

                        }

                    }

                    if (mpPrdIds.containsKey(dff.Id)) {
                        
                        dff.Talus_DFF_Id__c = mpPrdIds.get(dff.Id);
                        dff.Fulfillment_Submit_Status__c = 'Complete';
                        lstUpdtDffs.add(dff);
                        System.debug('************Subscription Addon Exists************' + dff.Udac__c + lstUpdtDffs);
                        CommonUtility.msgConfirm(dff.Udac__c + ' already exists in Subscription, so updating ' +  dff.Udac__c + ' with its product Id');

                    } else {

                        //Instantiate JSON Generator method to form JSON data
                        JSONGenerator jsonGen = JSON.createGenerator(true);
                        String newJsonString;

                        //Starting JSON Generator
                        jsonGen.writeStartObject();
                        jsonGen.writeStringField('code', String.valueof(dff.id));
                        jsonGen.writeStringField('sku', dff.Test_ProductId__c);
                        jsonGen.writeStringField('external_id', String.valueof(dff.UDAC__c));
                        if (dff.setup_notes__c != null) {
                            jsonGen.writeStringField('setup_notes', String.valueof(dff.setup_notes__c));
                        } else if (dff.setup_notes__c == null) {
                            jsonGen.writeStringField('setup_notes', '');
                        }

                        if (dff.OrderLineItemID__c != null) {
                            if (dff.OrderLineItemID__r.Effective_Date__c < system.today()) {
                                stDate = tdayDt.format('yyyy-MM-dd', 'UTC');
                            } else {
                                dt = dff.OrderLineItemID__r.Effective_Date__c;
                                dtm = datetime.newInstance(dt.year(), dt.month(), dt.day());
                                stDate = String.valueof(dtm.format('yyyy-MM-dd', 'UTC'));
                            }
                        } else {
                            if (dff.ModificationOrderLineItem__r.Effective_Date__c < system.today()) {
                                stDate = tdayDt.format('yyyy-MM-dd', 'UTC');
                            } else {
                                dt = dff.ModificationOrderLineItem__r.Effective_Date__c;
                                dtm = datetime.newInstance(dt.year(), dt.month(), dt.day());
                                stDate = String.valueof(dtm.format('yyyy-MM-dd', 'UTC'));
                            }
                        }

                        edDate = String.valueof(System.Today().addDays(30000));

                        //Set Start and End dates
                        jsonGen.writeStringField('scheduled_start_date', stDate);
                        jsonGen.writeStringField('scheduled_end_date', edDate);

                        //End point url to add DFF into existing Subscription
                        newFurl = url + dff.Account__r.TalusAccountId__c + '/subscriptions/' + dff.Talus_Subscription_Id__c + '/subscription_products/';

                        List < Product_Properties__c > NewPrpts = new List < Product_Properties__c > ();

                        if (mapPkgPrpty.get(dff.Test_ProductId__c) != null) {
                            NewPrpts = mapPkgPrpty.get(dff.Test_ProductId__c);
                        }

                        if (NewPrpts.size() <= 0) {

                            jsonGen.writeFieldName('properties');
                            jsonGen.writeStartObject();

                            jsonGen.writeEndObject();

                        } else if (NewPrpts.size() > 0) {

                            jsonGen.writeFieldName('properties');
                            jsonGen.writeStartObject();

                            for (Product_Properties__c newPrdPrpts: NewPrpts) {

                                String newPrptyName = newPrdPrpts.Name__c + '__c';

                                if (newPrdPrpts.Type__c == 'string') {

                                    String newPrpty = String.valueof(dff.get(newPrptyName));

                                    //System.debug('****************STRINGVAL****************' + newPrpty);

                                    if (String.isNotBlank(newPrpty)) {
                                        jsonGen.writeStringField(newPrdPrpts.Name__c, newPrpty);
                                    } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));

                                } else if (newPrdPrpts.Type__c == 'int') {

                                    Integer newPrpty = Integer.valueof(dff.get(newPrptyName));
                                    if (newPrpty != null) {
                                        jsonGen.writeNumberField(newPrdPrpts.Name__c, newPrpty);
                                    } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));

                                } else if (newPrdPrpts.Type__c == 'date') {

                                    //System.debug('************DATEJSON************' + dff.get(newPrptyName) + newPrdPrpts.Name__c);      

                                    if (dff.get(newPrptyName) != null) {
                                        Date newPrpty = Date.valueof(dff.get(newPrptyName));
                                        Date myDateMy = date.newinstance(newPrpty.year(), newPrpty.month(), newPrpty.day());

                                        jsonGen.writeStringField(newPrdPrpts.Name__c, String.valueof(myDateMy));
                                    } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));

                                } else if (newPrdPrpts.Type__c == 'email') {

                                    String newPrpty = String.valueof(dff.get(newPrptyName));
                                    if (newPrpty != null) {
                                        jsonGen.writeStringField(newPrdPrpts.Name__c, newPrpty);
                                    } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));

                                } else if (newPrdPrpts.Type__c == 'stringarray') {

                                    String newPrpty = String.valueof(dff.get(newPrptyName));
                                    if (Test.isRunningTest()) {
                                        newPrpty = 'test1; test2;';
                                    }
                                    if (newPrpty != null) {
                                        List < String > lst = new List < String > ();
                                        lst = newPrpty.split(';');

                                        jsonGen.writeObjectField(newPrdPrpts.Name__c, lst);
                                    } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));

                                } else if (newPrdPrpts.Type__c == 'bool') {

                                    Boolean newPrpty = boolean.valueof(dff.get(newPrptyName));
                                    jsonGen.writeBooleanField(newPrdPrpts.Name__c, newPrpty);
                                } else if (newPrdPrpts.Type__c == 'decimal') {

                                    Integer newPrpty = Integer.valueof(dff.get(newPrptyName));
                                    if (newPrpty != null) {
                                        jsonGen.writeNumberField(newPrdPrpts.Name__c, newPrpty);
                                    } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));
                                } else if (newPrdPrpts.Type__c == 'hoursofoperation') {

                                    String newPrpty = String.valueof(dff.get(newPrptyName));

                                    if (newPrpty != null) {
                                        List < String > lst = new List < String > ();
                                        lst = newPrpty.split(';');

                                        //System.debug('************123452HoursOfOperation123452************' + lst.size());

                                        if (newPrpty != null) {
                                            jsonGen.writeFieldName(newPrdPrpts.Name__c);
                                            jsonGen.writeStartArray();
                                            for (String s: lst) {
                                                if (s != 'null') {

                                                    List < String > lst1 = s.split(',');

                                                    jsonGen.writeStartArray();
                                                    for (String ss: lst1) {

                                                        jsonGen.writeString(ss);

                                                    }
                                                    jsonGen.writeEndArray();
                                                } else {
                                                    jsonGen.writeNull();
                                                }
                                            }
                                            jsonGen.writeEndArray();
                                        }
                                    } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));
                                }

                            }

                            jsonGen.writeEndObject();

                        }

                        jsongen.writeEndObject();

                        //Retrieve JSON data in a string format and make sure to use this method only once
                        newJsonString = jsongen.getAsString();

                        system.debug('************JSON DATA FINAL************' + newJsonString);
                        
                        HttpResponse newRes = CommonUtility.postUtil(newJsonString, newFurl, String.valueof('POST'));
                        
                        String newResponsestring = newRes.getBody();

                        if (Test.isRunningTest()) {
                            newRes.setStatusCode(201);
                            newResponsestring = '{ "code":null, "created":"2013-03-05T15:26:44.313581", "deleted":null, "end_date":"02/28/2014", "external_id":null, "external_references": [ { "key":"subscription", "reference_id":"108147", "service":"Geneva" } ], "fulfillment_date":null, "id":453, "last_updated":"2013-03-05T15:26:44.312943", "products": [ { "code":null, "created":"2013-03-05T15:26:44", "deleted":null, "external_references": [ { "key":"subscription_product", "reference_id":"196083", "service":"Geneva" } ], "id":652, "last_updated":"2013-03-05T15:26:44", "package_product_id":18, "product_id":1, "properties": { "ClicksContracted":"12000" }, "status":"active" } ], "start_date":"04/01/2013", "status":"active" }';
                        }

                        System.debug('************RESPONSE CODE************' + newRes.getStatusCode() + '************RESPONSESTING************' + newResponsestring);

                        if (newRes != null && newRes.getStatusCode() == 201) {

                            Map < String, Object > newResps = (Map < String, Object > ) JSON.deserializeUntyped(newResponsestring);

                            System.debug('************JSON DATA DESERIALIZED************' + newResps + '************' + newResps.get('products'));
                            
                            CommonUtility.msgConfirm('Successfully add ' + dff.Udac__c + ' product to Spotzer Bundle');

                            //Update current DFF with Talus returned product Id
                            dff.Talus_DFF_Id__c = String.valueof(newResps.get('id'));
                            dff.Fulfillment_Submit_Status__c = 'Complete';
                            lstUpdtDffs.add(dff);

                            if (String.isNotBlank(dff.OrderLineItemID__c)) {
                                dff.OrderLineItemID__r.Status__c = 'New';
                                dff.OrderLineItemID__r.Sent_to_fulfillment_on__c = system.today();
                                dff.OrderLineItemID__r.Talus_Subscription_ID__c = dff.Talus_Subscription_ID__c;
                                dff.OrderLineItemID__r.Talus_DFF_Id__c = String.valueof(newResps.get('id'));
                                lstUpdtOL.add(dff.orderLineItemId__r);
                            } else if (String.isNotBlank(dff.ModificationOrderLineItem__c)) {
                                dff.ModificationOrderLineItem__r.Status__c = 'New';
                                dff.ModificationOrderLineItem__r.Sent_to_fulfillment_on__c = system.today();
                                dff.ModificationOrderLineItem__r.Talus_Subscription_ID__c = dff.Talus_Subscription_ID__c;
                                dff.ModificationOrderLineItem__r.Talus_DFF_Id__c = String.valueof(newResps.get('id'));
                                lstUpdtMOL.add(dff.ModificationOrderLineItem__r);
                            }

                        } else if (newRes != null && newRes.getStatusCode() != 201) {
                            //CommonUtility.msgFatal('Submission failed for UDAC: ' + dff.Udac__c + ' due to Exception: ' + newResponsestring);
                            if(Label.DFF_Callout == 'False') {
                                CommonUtility.msgInfo('Callouts to the Fulfillment Vendor are blocked for Debugging Purpose. Please contact your System Admin and Sorry for the Inconvinience.....');
                            } else {
                            	CommonUtility.msgError(String.valueof(CommonUtility.dffValdtnFlds(newResponsestring)));
                        	}
                        }

                    }

                } catch (exception e) {
                    CommonUtility.msgFatal(String.valueof('Exception Occured: ' + e.getMessage()));
                }

            }

        }

        try {

            if (lstUpdtDffs.size() > 0) {
                update lstUpdtDffs;
            }

            if (lstUpdtOL.size() > 0) {
                update lstUpdtOL;
            }

            if (lstUpdtMOL.size() > 0) {
                update lstUpdtMOL;
            }

        } catch (exception e) {
            CommonUtility.msgFatal(String.valueof('Exception Occured: ' + e.getMessage()));
        }

        return null;

    }

    //Method for returning to current DFF record
    public pageReference rtnDff() {

        PageReference pageRef = new PageReference('/' + dffId);
        return pageRef;

    }

}