@IsTest(SeeAllData=true)
public class close_case_controllerTest {

    public static testMethod void close_case_controllerTest01(){
        Account Acc = TestMethodsUtility.createAccount('cmr');
        Case caseObj = TestMethodsUtility.createCase('CS Internal Berry Team', Acc);
        string CaseID= caseObj.Id;
        
        PageReference pageRef1 = new PageReference('/apex/closecase?id='+CaseID);                
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController con = new ApexPages.StandardController(new case());                     
        close_case_controller ctl = new close_case_controller(con); 
        
          Test.startTest();
            ctl.processSCN();
          Test.stopTest();
    } 
 }