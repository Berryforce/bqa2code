public class TBCcBillingTransferFirstScreenController {
    public string accountid {get;set;} // this is for receiving the account id from VF Page
    public string selectedMediaType {get;set;}
    public string selectedTypeOfTransfer {get;set;}
    public string selectedBillingPartner {get;set;}
    public string selectedBillingFrequency {get;set;}
    public string selectedModeOfTransfer {get;set;}
    public string selectedCanvass {get;set;}
    public string selectedDE {get;set;}
    public string selectedGoLive {get;set;}
    public boolean isAlreadyRunning{get;set;}
    public boolean bDESection{get;set;}
    public boolean bUnlockButton {get;set;}
    public list<SelectOption> lstCanvass {get;set;}
    public list<SelectOption> lstDE {get;set;}
    public list<SelectOption> lstTypeofTransfer {get;set;}
    public list<SelectOption> lstPartner {get;set;}
    public list<selectOption> lstBillingPartnerName{get;set;}
    public list<selectOption> lstGoLive{get;set;}
    public list<selectOption> lstModeofTransfer{get;set;}
    public string selectedBillingPartnerName{get;set;}
    public boolean  bPartnernameList{get;set;}
    public boolean isMigratedOLI{get;set;}
    public TBCcBillingTransferFirstScreenController(ApexPages.StandardController controller) {
        accountId = controller.getId(); //Apexpages.currentPage().getParameters().get('id'); // getting account id from VF Page it will use in whole BT process
        isAlreadyRunning=false;
        bDESection = true;
        bUnlockButton = false;
        bPartnernameList=false;
        //checking the lock scenario for this account.
        List<Billing_Transfer_Status__c> billingTransferStatusList = fetchBillingTransferStatus(); 
        if(billingTransferStatusList.size()>0){
            if(billingTransferStatusList[0].CreatedById == UserInfo.getUserId()) {
                bUnlockButton = true;
            }
            isAlreadyRunning=true;
            CommonMethods.addError('Billing Transfer Process for this account is already running by <b>' + billingTransferStatusList[0].CreatedBy.Name +'</b>. If you get this error continously then Please contact your system administrator.');        
        }
        else{
            initilizeValues();
        }
    }
    
    private void initilizeValues() {
        /*****   initializing screen 1 variables    *****/
        lstCanvass = new list<SelectOption>();
        lstDE = new list<SelectOption>();
        lstTypeofTransfer = new list<SelectOption>();
        lstGoLive = new list<SelectOption>();
        lstModeofTransfer = new list<SelectOption>();
        lstBillingPartnerName= new List<selectOption>();
        lstPartner = new list<SelectOption>();
        /*****   End initializing screen 1 variables    *****/
        // getting canvas values for first screen
        lstCanvass.add(new SelectOption('none', '--None--'));
        lstTypeofTransfer.add(new SelectOption('none', '--None--'));
        lstDE.add(new SelectOption('none', '--None--'));
        lstGoLive.add(new SelectOption('none', '--None--'));
        lstModeofTransfer.add(new SelectOption('none', '--None--'));
        lstBillingPartnerName.add(new selectOption('none','--None--'));
        lstPartner.add(new SelectOption('none', '--None--'));
        selectedCanvass = 'none';
        selectedDE = 'none';
        selectedMediaType = 'none';
        selectedBillingPartner = 'none';
        selectedModeOfTransfer = 'none';
        selectedGoLive = 'none';
        selectedBillingPartnerName = 'none';
        isMigratedOLI=false;
        loadCanvassPickList();
    }
    
    public void unLockCurrentUser() {
        delete fetchBillingTransferStatus();
        isAlreadyRunning=false;
        bUnlockButton = false;
        bDESection = true;
        initilizeValues();
    }
    
    public void loadPicklistBasedOnMediaType() {
        if(!selectedMediaType.equals('none')) {
            loadGoLivePickList();
            loadModeofTransferPickList();
            loadTypeofTransferPickList();
            loadPartnerPickList();
            if(!selectedMediaType.equals('digital')) {
                loadDirectoryEdition();
            }
        }
    }
    
    private List<Billing_Transfer_Status__c> fetchBillingTransferStatus() {
        return TBCsBillingTransferStatusSOQLMethods.fetchBillingTransferStatusByAccountId(accountid);
    }

    public void loadGoLivePickList() {
        lstGoLive = new list<SelectOption>();
        lstGoLive.add(new SelectOption('none', '--None--'));
        selectedGoLive = 'none';
        if(selectedMediaType.equalsIgnoreCase('digital')) {
            lstGoLive.add(new SelectOption('both','Go Live and Non-Go Live'));
            selectedGoLive = 'both';
        }
        else if(selectedMediaType.equalsIgnoreCase('print')) {
            lstGoLive.add(new SelectOption('golive','Go Live'));
            lstGoLive.add(new SelectOption('nongolive','Non-Go Live'));
            lstGoLive.add(new SelectOption('both','Go Live and Non-Go Live'));
        }
        else if(selectedMediaType.equalsIgnoreCase('p4p')) {
            lstGoLive.add(new SelectOption('golive','Go Live'));
            lstGoLive.add(new SelectOption('nongolive','Non-Go Live'));
        }
    }

    public void loadModeofTransferPickList() {
        lstModeofTransfer = new list<SelectOption>();
        lstModeofTransfer.add(new SelectOption('none', '--None--'));
        selectedModeOfTransfer = 'none';
        if(selectedTypeOfTransfer.equalsIgnoreCase('PaymentMethodTransfer')) {
            lstModeofTransfer.add(new SelectOption('updateoli','Update Order Line Items'));
            selectedModeOfTransfer = 'updateoli';
        }
        else if(selectedTypeOfTransfer.equalsIgnoreCase('AccountContatTransfer')) {
            lstModeofTransfer.add(new SelectOption('retro','Retro Transfer'));
            lstModeofTransfer.add(new SelectOption('future','Future Transfer'));
            lstModeofTransfer.add(new SelectOption('updateoli','Update Order Line Items'));
        }
        else if(selectedTypeOfTransfer.equalsIgnoreCase('BillingFrequencyTransfer')) {
            lstModeofTransfer.add(new SelectOption('retro','Retro Transfer'));
            selectedModeOfTransfer = 'retro';
        }
        else {
            lstModeofTransfer.add(new SelectOption('retro','Retro Transfer'));
            lstModeofTransfer.add(new SelectOption('future','Future Transfer'));
        }
        if(selectedTypeOfTransfer.equals('BillingFrequencyTransfer')) {
            loadDirectoryEdition();
        }
    }
    
    public void loadTypeofTransferPickList() {
        lstTypeofTransfer = new list<SelectOption>();
        lstTypeofTransfer.add(new SelectOption('none', '--None--'));
        selectedModeOfTransfer = 'none';
        if(selectedMediaType.equalsIgnoreCase('digital')) {
            bDESection = true;
            bPartnernameList=false;
            lstTypeofTransfer.add(new SelectOption('BillingPartnerTransfer','Billing Partner Transfer'));
            lstTypeofTransfer.add(new SelectOption('PaymentMethodTransfer','Payment Method Transfer'));
            lstTypeofTransfer.add(new SelectOption('BillingContactTransfer','Billing Contact Change'));
        }
        else if(selectedMediaType.equalsIgnoreCase('print')) {
            bDESection = false;
            bPartnernameList=false;
            lstTypeofTransfer.add(new SelectOption('AccountContatTransfer','Account/Contact Transfer'));
            lstTypeofTransfer.add(new SelectOption('AccountContatBillingPartnerPaymentMethodTransfer','Account/Contact/Billing Partner/Payment Method Transfer'));
            lstTypeofTransfer.add(new SelectOption('BillingPartnerTransfer','Billing Partner Transfer'));
            lstTypeofTransfer.add(new SelectOption('BillingFrequencyTransfer','Billing Frequency Transfer'));
            lstTypeofTransfer.add(new SelectOption('PaymentMethodTransfer','Payment Method Transfer'));
        }
        else if(selectedMediaType.equalsIgnoreCase('p4p')) {
            bDESection = false;
            bPartnernameList=true;
            lstTypeofTransfer.add(new SelectOption('AccountContatTransfer','Account/Contat Transfer'));
            selectedModeOfTransfer = 'AccountContatTransfer';
        }
    }
    
    public void loadPartnerPickList() {
        lstPartner = new list<SelectOption>();
        if(selectedMediaType.equalsIgnoreCase('p4p')) {
            lstPartner.add(new SelectOption('berry','Berry Billing'));
            selectedBillingPartner = 'berry';
        }
        else if(selectedMediaType.equalsIgnoreCase('print') || selectedMediaType.equalsIgnoreCase('digital')) {
            lstPartner.add(new SelectOption('none', '--None--'));
            lstPartner.add(new SelectOption('berry','Berry Billing'));
            lstPartner.add(new SelectOption('telco','Telco Billing'));
            selectedBillingPartner = 'none';
        }
        loadBillngPartnerName();
    }
    public void loadDirectoryEdition() {
        if(String.isNotBlank(accountid) && String.isNotBlank(selectedCanvass) && !selectedCanvass.equalsIgnoreCase('none')) {
            system.debug('selectedMediaType : '+ selectedMediaType);
            list<String> lstBookStatus = new list<String>();
            if(selectedTypeOfTransfer.equals('BillingFrequencyTransfer')) {
                lstBookStatus = new list<String>{'BOTS', 'NI'};
            }
            else {
                lstBookStatus = new list<String>{'BOTS-1', 'BOTS', 'NI'};
            }
            
            list<Order_Group__c> lstOS = OrderGroupSOQLMethods.getOrderSetBillingTransferFirstScreen(accountid, selectedCanvass, selectedMediaType, lstBookStatus);
            system.debug('#####lstOS###'+lstOS);
            map<Id, set<Id>> mapDirIdwithDEIds = new map<Id, set<Id>>();
            map<Id, String> mapDEIdwithDEIds = new map<Id, String>();
            map<Id, String> mapDirIdwithName = new map<Id, String>();
            lstDE = new list<SelectOption>();
            lstDE.add(new SelectOption('none', '--None--'));
            for(Order_Group__c iterator : lstOS) {
                list<Order_Line_Items__c> tempOLIList = iterator.Order_Line_Items__r;
                if(tempOLIList.size() > 0) {
                    for(Order_Line_Items__c OLI : tempOLIList) {
                        if(OLI.Directory_Edition__c != null) {
                            if(!mapDirIdwithDEIds.containsKey(OLI.Directory__c)) {
                                mapDirIdwithDEIds.put(OLI.Directory__c, new set<ID>());
                            }
                            mapDirIdwithDEIds.get(OLI.Directory__c).add(OLI.Directory_Edition__c);
                            if(!mapDEIdwithDEIds.containsKey(OLI.Directory_Edition__c)) {
                                mapDEIdwithDEIds.put(OLI.Directory_Edition__c, OLI.Directory_Edition_Name__c);
                            }
                            if(!mapDirIdwithName.containsKey(OLI.Directory__c)) {
                                mapDirIdwithName.put(OLI.Directory__c, OLI.Directory__r.Name);
                            }
                        }
                    }
                }
            }
            if(mapDirIdwithDEIds.size() > 0) {
                for(Id idDir : mapDirIdwithDEIds.keySet()) {
                    set<Id> setDEID = mapDirIdwithDEIds.get(idDir);
                    string completeDirectoryEdition = '';
                    string completeDirectoryIds = '';
                    for(Id idDE : setDEID) {
                        completeDirectoryEdition += mapDEIdwithDEIds.get(idDE) + '/';
                        completeDirectoryIds += idDE + ';';
                    }
                    lstDE.add(new SelectOption(String.valueOf(idDir)+'-->'+completeDirectoryIds.subString(0, completeDirectoryIds.lastIndexOf(';')), mapDirIdwithName.get(idDir)+' --> '+completeDirectoryEdition.subString(0, completeDirectoryEdition.lastIndexOf('/'))));
                }
            }
        }
    }

    public void loadBillngPartnerName() {
        lstBillingPartnerName= new List<selectOption>();
        lstBillingPartnerName.add(new selectOption('none','--None--'));
        if(selectedBillingPartner.equalsIgnoreCase('berry') && !selectedCanvass.equalsIgnoreCase('none')){
            lstBillingPartnerName.add(new selectOption('THE BERRY COMPANY','THE BERRY COMPANY'));
            selectedBillingPartnerName = 'THE BERRY COMPANY';
        }
        else if(selectedBillingPartner.equalsIgnoreCase('telco') && !selectedCanvass.equalsIgnoreCase('none')){
            list<Directory_Mapping__c> lstDL = [select Id, Name, Canvass__c, Directory__c, Telco__c, Default_Directory_Telco__c, Default_Canvass_Telco__c, Telco__r.Account__c, Telco__r.Telco_Account_Name__c FROM Directory_Mapping__c where Canvass__c = : selectedCanvass AND DM_Use_As_Billing_Partner__c = true];
            for (Directory_Mapping__c iteratorDL: lstDL) {
                if (String.isNotBlank(iteratorDL.Telco__r.Telco_Account_Name__c) && !iteratorDL.Telco__r.Telco_Account_Name__c.equalsIgnorecase(CommonMessages.berryTelcoName)) {
                    lstBillingPartnerName.add(new selectOption(iteratorDL.Telco__r.Telco_Account_Name__c,iteratorDL.Telco__r.Telco_Account_Name__c));
                }
            }
        }
    }
    public pageReference nextButtonClick() {
        boolean bFlag = true;
        if(selectedCanvass.equalsIgnoreCase('none')){
            CommonMethods.addError('Please select canvass to process');
            bFlag = false;
        }
        else if(selectedMediaType.equalsIgnoreCase('none')){
            CommonMethods.addError('Please select media type to process');
            bFlag = false;
        }
        else if(selectedGoLive.equalsIgnoreCase('none')){
            CommonMethods.addError('Please select go-live/non go-live to process');
            bFlag = false;   
        }
        else if(selectedTypeOfTransfer.equalsIgnoreCase('none')){
            CommonMethods.addError('Please select type of transfer to process');
            bFlag = false;
        }
        else if(selectedModeOfTransfer.equalsIgnoreCase('none')){
            CommonMethods.addError('Please select mode of transfer to process');
            bFlag = false;
        }

        if(selectedBillingPartner.equalsIgnoreCase('none')){
            CommonMethods.addError('Please select billing partner to process');
            bFlag = false;
        }
        else if(selectedBillingPartnerName.equalsIgnoreCase('none')){
            CommonMethods.addError('Please select billing partner name to process');
            bFlag = false;          
        }

        if(selectedTypeOfTransfer.equalsIgnoreCase('BillingFrequencyTransfer')){
            if(selectedBillingFrequency.equalsIgnoreCase('none')){
                CommonMethods.addError('Please select appropriate billing frequency to process.');
                bFlag = false;
            }
        }
        
        
        if(selectedMediaType.equalsIgnoreCase('p4p')) {
            if(selectedGoLive.equalsIgnoreCase('both')) {
                CommonMethods.addError('You can not select Go Live and Non-Go Live. Please select Go Live or Non-Go Live');
                bFlag = false;
            }
            else {

            }
        }
        else if(selectedMediaType.equalsIgnoreCase('digital')) {
            if(!selectedGoLive.equalsIgnoreCase('both')) {
                CommonMethods.addError('Please select Go Live and Non-Go Live');
                bFlag = false;
            }
            if(selectedBillingPartner.equalsIgnoreCase('berry') && selectedTypeOfTransfer.equalsIgnoreCase('paymentmethodtransfer') && !selectedModeOfTransfer.equalsIgnoreCase('updateoli')) {
                CommonMethods.addError('Mode of transfer should be Update Order Line Items');
                bFlag = false;
            }
        }
        else if(selectedMediaType.equalsIgnoreCase('print')) {
            if(selectedBillingPartner.equalsIgnoreCase('berry') && selectedTypeOfTransfer.equalsIgnoreCase('paymentmethodtransfer') && !selectedModeOfTransfer.equalsIgnoreCase('updateoli')) {
                CommonMethods.addError('Mode of transfer should be Update Order Line Items');
                bFlag = false;
            }
            if(selectedTypeOfTransfer.equalsIgnoreCase('BillingFrequencyTransfer') && !selectedModeOfTransfer.equalsIgnoreCase('retro')) {
                CommonMethods.addError('Mode of transfer should be Retro Transfer');
                bFlag = false;
            }
        }
        if(!selectedMediaType.equalsIgnoreCase('p4p') && selectedBillingPartner.equalsIgnoreCase('telco') && selectedTypeOfTransfer.equalsIgnoreCase('PaymentMethodTransfer')) {
                CommonMethods.addError('This transfer is not valid because Telco Partner does have only Telco Billing payment method');
                bFlag = false;          
        }

        if(!selectedMediaType.equalsIgnoreCase('digital') && selectedGoLive.equalsIgnoreCase('nongolive') && selectedModeOfTransfer.equalsIgnoreCase('retro')) {
            CommonMethods.addError('You can not select retro when there is non go live.');
            bFlag = false;
        }
        
        List<Billing_Transfer_Status__c> billingTransferStatusList = fetchBillingTransferStatus(); 
        if(billingTransferStatusList.size()>0) {
            if(billingTransferStatusList[0].CreatedById == UserInfo.getUserId()) {
                bUnlockButton = true;
            }
            CommonMethods.addError('Billing Transfer Process for this account is already running by <b>' + billingTransferStatusList[0].CreatedBy.Name +'</b>. If you get this error continously then Please contact your system administrator.');
            bFlag = false;        
        }
        string strDirectoryId = '';
        string strDirectoryEditionsId = '';
        if(!selectedMediaType.equalsIgnoreCase('digital')) {
            if(selectedDE.equalsIgnoreCase('none')) {
                CommonMethods.addError('Please select directory/directory editions');
                bFlag = false;
            }
            else {
                list<String> lstDir = selectedDE.split('-->');
                if(lstDir.size() > 0) {
                    strDirectoryId = lstDir[0];
                    strDirectoryEditionsId = lstDir[1];
                }
            }
        }
        
        if(bFlag) {
            list<Order_Line_Items__c> lstOLIs = new list<Order_Line_Items__c>();
            if(selectedTypeOfTransfer.equalsIgnoreCase('BillingFrequencyTransfer')) {
                lstOLIs=OrderLineItemSOQLMethods.generateQueryStringForBillingTansfer(accountId, selectedCanvass, strDirectoryId, strDirectoryEditionsId, selectedGoLive, selectedMediaType, selectedBillingPartner,selectedBillingPartnerName,null,selectedBillingFrequency);
                if(!selectedBillingFrequency.equalsIgnoreCase('none') && selectedBillingFrequency.equalsIgnoreCase('single')) {
                    map<Id, Order_Line_Items__c> mapOLI = new map<Id, Order_Line_Items__c>(lstOLIs);
                    if(isMigratedOLI) {
                        for(Order_Line_Items__c iter : [Select id, unitprice__c, (Select id from Sales_Invoice_Line_Items__r) from Order_Line_Items__c where Id in:mapOLI.KeySet() AND unitprice__c>0]){
                            if(iter.Sales_Invoice_Line_Items__r.size() > 0) {
                                mapOLI.remove(iter.Id);
                            }
                            //iCount++;
                        }
                    }
                    else  // this will be for non-migrated values
                    {
                        for(Order_Line_Items__c iter:[Select id, unitprice__c, (Select id from Sales_Invoice_Line_Items__r) from Order_Line_Items__c where Id in:mapOLI.KeySet()]){
                            if(iter.Sales_Invoice_Line_Items__r.size() == 0 && iter.unitprice__c > 0) {
                                mapOLI.remove(iter.Id);
                            }
                            //iCount++;
                        }
                    }
                    lstOLIs = mapOLI.values();
                }
            } else{
                lstOLIs=OrderLineItemSOQLMethods.generateQueryStringForBillingTansfer(accountId, selectedCanvass, strDirectoryId, strDirectoryEditionsId, selectedGoLive, selectedMediaType, selectedBillingPartner,selectedBillingPartnerName,null,null);
            }
            
            if(lstOLIs.size() <= 0) {
                CommonMethods.addError('No record found for above selection.');
                bFlag = false; 
            }
            /*else {
                //Check for Migrated OLI
                Integer iCount=0;
                map<Id, Order_Line_Items__c> mapOLI = new map<Id, Order_Line_Items__c>(lstOLIs);
                if(isMigratedOLI) {
                    for(Order_Line_Items__c iter : [Select id, unitprice__c, (Select id from Sales_Invoice_Line_Items__r) from Order_Line_Items__c where Id in:mapOLI.KeySet() AND unitprice__c>0]){
                        if(iter.Sales_Invoice_Line_Items__r.size() > 0) {
                            mapOLI.remove(iter.Id);
                        }
                        //iCount++;
                    }
                }
                else  // this will be for non-migrated values
                {
                    for(Order_Line_Items__c iter:[Select id, unitprice__c, (Select id from Sales_Invoice_Line_Items__r) from Order_Line_Items__c where Id in:mapOLI.KeySet()]){
                        if(iter.Sales_Invoice_Line_Items__r.size() == 0 && iter.unitprice__c > 0) {
                            mapOLI.remove(iter.Id);
                        }
                        //iCount++;
                    }
                }
                lstOLIs = mapOLI.values();
            }*/

            if(bFlag) {
                createBillingTransferLockandParameter(strDirectoryId, strDirectoryEditionsId);
                if(selectedMediaType.equalsIgnoreCase('p4p')) {
                    return new PageReference('/apex/TBCvBillingTransferP4PScreenPage?id='+accountid);
                }
                else if(selectedMediaType.equalsIgnoreCase('digital')) {
                    return new PageReference('/apex/TBCvBillingTransferDigitalScreenPage?id='+accountid);
                }
                else if(selectedMediaType.equalsIgnoreCase('print')) {
                    if(selectedTypeOfTransfer.equalsIgnoreCase('BillingPartnerTransfer') || selectedTypeOfTransfer.equalsIgnoreCase('paymentmethodtransfer')){
                        return new PageReference('/apex/TBCvBillingTransferPrintBPPMChangePage?id='+accountid);
                    }
                    else if(selectedTypeOfTransfer.equalsIgnoreCase('AccountContatTransfer')){
                        return new PageReference('/apex/TBCvBillingTransferPrintAcctContactPage?id='+accountid);
                    }
                    else if(selectedTypeOfTransfer.equalsIgnoreCase('AccountContatBillingPartnerPaymentMethodTransfer')){
                        return new PageReference('/apex/TBCvBillingTransferPrintScreenForAllPage?id='+accountid);
                    }
                    else if(selectedTypeOfTransfer.equalsIgnoreCase('BillingFrequencyTransfer')){
                        return new PageReference('/apex/TBCvBTBillingFrequencyChange?id='+accountid);
                    }
                    
                }
            }
        }
        return null;
    }
    
    /**** Private Methods ****/
    // this method will be used for getting canvas values.
    private void loadCanvassPickList() {
        Set < Id > canvassIdSet = new Set < Id > ();
        for(opportunity iterator: [select id, Account_Primary_Canvass__r.name, Account_Primary_Canvass__r.id from opportunity where AccountId =:accountId and IsClosed = true]) {
            if (iterator.Account_Primary_Canvass__r.name != null && !canvassIdSet.contains(iterator.Account_Primary_Canvass__r.Id)) {
                lstCanvass.add(new SelectOption(iterator.Account_Primary_Canvass__r.Id, iterator.Account_Primary_Canvass__r.name));
                canvassIdSet.add(iterator.Account_Primary_Canvass__r.Id);
            }
        }
    }
    
    // this method will use to insert billing transfer status record   
   @testvisible private Billing_Transfer_Status__c createBillingTransferLockandParameter(String strDirIds, String strDirEditionIds) {
        list<Directory_Mapping__c> lstDL = [select Id, Telco__r.Account__c, Telco__r.Telco_Account_Name__c FROM Directory_Mapping__c where Telco__r.Telco_Account_Name__c =:selectedBillingPartnerName AND Canvass__c = : selectedCanvass AND DM_Use_As_Billing_Partner__c = true];
        String strPartnerId = '';
        if(lstDL.size() > 0) {
            for(Directory_Mapping__c iterator : lstDL) {
                strPartnerId = iterator.Telco__r.Account__c;
            }
        }
        Billing_Transfer_Status__c BillingTransferStatusObj= new Billing_Transfer_Status__c(BTS_Account__c=accountid,BTS_Billing_Partner__c=selectedBillingPartner,
            BTS_Canvass__c=selectedCanvass,BTS_Go_Live__c=selectedGoLive,BTS_Media_Type__c=selectedMediaType,BTS_Mode_of_Transfer__c=selectedModeOfTransfer,
            BTS_Type_of_Transfer__c=selectedTypeOfTransfer,BTS_Billing_Frequency__c=selectedBillingFrequency, BTS_Directory_Id__c=strDirIds,BTS_Directory_Edition_Id__c=strDirEditionIds,
            BTS_Billing_Partner_Name__c=selectedBillingPartnerName,BTS_Billing_Partner_ID__c = strPartnerId,BTS_Migrated_OLI__c=isMigratedOLI);
        insert BillingTransferStatusObj;
        return BillingTransferStatusObj;
    }
}