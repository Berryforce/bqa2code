/********************************************************************
Apex Class to create UDAC Package Information from Fulfillment Vendor
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 10/07/2014
$Id$
*********************************************************************/
public with sharing class CreateUDACPackages {

    //Get endpoint information
    //public static final string Requesturl = TalusOauth__c.getInstance('Talus Oauth').Package_Catalog_URL__c;
    public static final string Requesturl = Label.Talus_Package_Catalog_URL;

    //@future(callout=true)
    public static void invokeGET() {

        //Get Package catalog information by making GET callout   
        String responseString = TalusRequestUtilityGET.initiateRequest(Requesturl);

        system.debug('************Catalog Response************' + responseString);

        if (responseString != null) {

            //Deserialize response data to retrieve every package information    
            TalusPackageIdWrapper pkgrecrds = (TalusPackageIdWrapper) System.JSON.deserialize(responseString, TalusPackageIdWrapper.class);
            
           //System.debug('************PackageRecords************' + pkgrecrds);

            List < TalusPackageIdWrapper.Objects > objlst = pkgrecrds.objects;

            List < Package_ID__c > lstpkgids = new List < Package_ID__c > ();

            //Query for all package id records created in sfdc
            List < Package_ID__c > prvsPkgs = [SELECT id from Package_ID__c where name != null];

            if (prvsPkgs.size() > 0) {

                //Delete all existing package related information in sfdc
                delete prvsPkgs;

            }
            //System.debug('************Packages Count************' + objlst.size() + objlst);
            for (TalusPackageIdWrapper.Objects obj: objlst) {

                //Create new package id record for each Package that we retrieve from GET call
                Package_ID__c pk = new Package_ID__c();
                pk.Name = string.valueof(obj.sku);
                pk.UDAC_Package_ID__c = string.valueof(obj.sku);
                pk.Name__c = string.valueof(obj.name);
                pk.Resource_URI__c = string.valueof(obj.resource_uri);
                pk.Description__c = string.valueof(obj.description);
                pk.Status__c = string.valueof(obj.status);
                lstpkgids.add(pk);

            }

            if (lstpkgids.size() > 0) {

                //Insert all the package id records
                insert lstpkgids;

            }

        }

    }

}