global class SetCommissionReadyBatchSchedulerHndlr implements SetCommissionReadyBatchScheduler.SetCommissionReadyBatchSchedulerInterface {
	global void execute(SchedulableContext sc) {
        SetCommissionReadyBatchController obj = new SetCommissionReadyBatchController();
        Database.executeBatch(obj, 2000);
    }
}