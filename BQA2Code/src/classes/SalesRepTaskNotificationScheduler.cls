global class SalesRepTaskNotificationScheduler implements Schedulable {
	public Interface SalesRepTaskNotificationSchedulerInterface {
		void execute(SchedulableContext sc);
	} 
	
	global void execute(SchedulableContext sc) {
		Type targetType = Type.forName('SalesRepTaskNotificationSchedulerHndlr');
		if(targetType != null) {
			SalesRepTaskNotificationSchedulerInterface obj = (SalesRepTaskNotificationSchedulerInterface)targetType.newInstance();
			obj.execute(sc);
		}
	}
}