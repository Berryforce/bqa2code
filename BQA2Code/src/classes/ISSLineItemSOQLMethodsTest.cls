@isTest
public class ISSLineItemSOQLMethodsTest {
    static testmethod void ISSLineItemSOQLMethodsTest() {
		Canvass__c newCanvass = TestMethodsUtility.createCanvass();
        Account newAccount = TestMethodsUtility.generateAccount('customer', false);
        newAccount.Primary_Canvass__c = newCanvass.Id;
        insert newAccount;

        Directory__c newDirectory1 = TestMethodsUtility.createDirectory();        
        Directory_Mapping__c newDirectoryMapping = TestMethodsUtility.createDirectoryMapping();
        
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Directory_Edition__c newDirectoryEdition = TestMethodsUtility.generateDirectoryEdition(newDirectory1.Id, newAccount.Primary_Canvass__c, newTelco.Id, newDirectoryMapping.Id);        
        newDirectoryEdition.Book_Status__c='NI';        
        insert newDirectoryEdition;  
        ISS__c isscreate = TestMethodsUtility.generateISS();
        insert isscreate;
        ISSLineItemSOQLMethods.getISSLIForSendRemittanceWithoutRemSheet('0558', newDirectoryEdition.Edition_Code__c);
        ISSLineItemSOQLMethods.getISSLIForSendRemittanceWithRemSheet('0558', newDirectoryEdition.Edition_Code__c);
        ISSLineItemSOQLMethods.getISSLIForSendISSWithRemSheet('0558', newDirectoryEdition.Edition_Code__c);
        ISSLineItemSOQLMethods.getISSLIForSendISSWithoutRemSheet('0558', newDirectoryEdition.Edition_Code__c);
        ISSLineItemSOQLMethods.getISSLIByIds(new set<id> {isscreate.id});
    }
}