global class LetterRenewalReportScheduler implements Schedulable {
   public Interface LetterRenewalReportSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('LetterRenewalReportSchedulerHndlr');
        if(targetType != null) {
            LetterRenewalReportSchedulerInterface obj = (LetterRenewalReportSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}