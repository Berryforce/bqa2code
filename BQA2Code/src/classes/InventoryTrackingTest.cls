@isTest
public class InventoryTrackingTest {
    static testMethod void testInventory() {
        Test.StartTest();   
        Account telcoAcct = TestMethodsUtility.createAccount('telco');
		Telco__c telco = TestMethodsUtility.createTelco(telcoAcct.Id);
		Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMapping = TestMethodsUtility.generateDirectoryProductMapping(dir);
        dirProdMapping.Is_Active__c = true;
        insert dirProdMapping;
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.createAccount('cmr');
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
                                                
        apexpages.currentpage().getparameters().put('Cid',canvass.id);
        apexpages.currentpage().getparameters().put('Aid',acct.id);
        apexpages.currentpage().getparameters().put('Oid',oppty.id);
        apexpages.currentpage().getparameters().put('index', '0');
        InventoryTracking invTrack = new InventoryTracking();
        invTrack.exportPage();
        invTrack.returnAcct();
        invTrack.returnOppty();
        invTrack.searchDirProd();
        invTrack.fetchDirList();
        invTrack.DPMList.add(dirProdMapping);
        invTrack.AddProductQueue();
        
        Directory_Mapping__c dirMapping = TestMethodsUtility.createDirectoryMapping(telco);
        ApexPages.currentPage().getParameters().put('DMId', dirMapping.Id);
        InventoryTrackingExcel invTrackExl = new InventoryTrackingExcel();
        Test.stopTest();
    }
}