@IsTest
public class NationalOrderCreationHandlerV2Test{

  public static testMethod void NationalOrderCreationHandlerV2Test01(){
        Account acct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(acct.Id);
        Canvass__c c = TestMethodsUtility.generateCanvass(telco);
        c.Billing_Entity__c='Test';
        insert c;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
        //Telco__c objTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id Limit 1];
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Account objCMRAcc = TestMethodsUtility.generateCMRAccount();
        objCMRAcc.Is_Active__c = true;
        insert objCMRAcc;
        
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Recives_Electronice_File__c=true;
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory(); 
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd .New_Print_Bill_Date__c=date.today();
        objDirEd .Bill_Prep__c=date.parse('01/01/2013');
        objDirEd.XML_Output_Total_Amount__c=100;
        objDirEd.Pub_Date__c=System.today().addMOnths(1);
        insert objDirEd;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c=System.today().addMOnths(2);
        insert objDirE;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Is_Anchor__c=true;
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        insert objProd1;
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        newProduct.Trademark_Product__c='Trademark Finding Line';
        newProduct.ProductCode='SS';
        insert newProduct;
        
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 100, Product2Id = newProduct.ID, Pricebook2Id = newPriceBook.id, IsActive = true);
        insert pbe;
        
        Test.startTest(); 
        National_Staging_Order_Set__c objNSOS = TestMethodsUtility.generateManualNSRT();
        objNSOS.CMR_Name__c = objCMRAcc.id;
        
        objNSOS.Is_Ready__c = true;
        objNSOS.Directory__c = objDir.id;
        insert objNSOS;
        
        List<National_Staging_Line_Item__c> ListNSLI=new List<National_Staging_Line_Item__c>();
        National_Staging_Line_Item__c objNSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI.Ready_for_Processing__c = true;
        objNSLI.UDAC__c='SS';
        objNSLI.Standing_Order__c=true;
        objNSLI.National_Staging_Header__c = objNSOS.id;
        objNSLI.National_Graphics_ID__c='GRAPHLOGO';
        objNSLI.Is_Changed__c=true;
        objNSLI.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI);
        
        National_Staging_Line_Item__c objNSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI1.Ready_for_Processing__c = true;
        objNSLI1.UDAC__c='SS';
        objNSLI1.Standing_Order__c=true;
        objNSLI1.National_Graphics_ID__c='GRAPHLOGO';
        objNSLI1.National_Staging_Header__c = objNSOS.id;
        objNSLI1.Is_Changed__c=true;
        objNSLI1.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI1);
        
        National_Staging_Line_Item__c objNSLI2 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI2.Ready_for_Processing__c = true;
        objNSLI2.New_Transaction__c =true;
        objNSLI2.Standing_Order__c=true;
        objNSLI2.National_Graphics_ID__c='GRAPHLOGO';
        objNSLI2.National_Staging_Header__c = objNSOS.id;
        objNSLI2.Is_Changed__c=true;
        objNSLI2.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI2);
       
        insert ListNSLI;
        OpportunityLineItem ol1 = new OpportunityLineItem ();
        ol1.OpportunityId = newOpportunity.Id; 
        ol1.PricebookEntryId = pbe.Id;
        ol1.Quantity = 1;
        ol1.UnitPrice = 1;
        ol1.Discount=0.0;
        ol1.UDAC__c='STR';
        ol1.Section_Code__c='010';
        ol1.Type__c='Caption';
        ol1.Heading_Code__c='0123';
        ol1.Advertising_Data__c='TEST';
        ol1.Action__c='I';
        ol1.Heading__c='TEXT';
        ol1.Distribution_Area__c='US';
        ol1.Directory_Heading__c=objDH.Id;
        ol1.CMR_Number__c='TEST';
        ol1.CMR_Name__c=objCMRAcc.Id;
        ol1.Directory__c=objDir.Id;
        ol1.Directory_Edition__c=objDirE.Id;
        ol1.Full_Rate__c=100.00;
        ol1.Line_Number__c='100';
        ol1.NAT__c='N';
        ol1.Publication_Date__c=Date.today();
        ol1.Trans__c='I';
        ol1.DAT__c='N';
        ol1.SPINS__c='A';
        ol1.Date__c=Date.today();
        ol1.National_Staging_Line_Item__c=ListNSLI[0].Id;
        ol1.OP_National_Graphics_ID__c='TEST232';
        insert ol1;
        
        OpportunityLineItem ol2 = new OpportunityLineItem ();
        ol2.OpportunityId = newOpportunity.Id; 
        ol2.PricebookEntryId = pbe.Id;
        ol2.Quantity = 1;
        ol2.UnitPrice = 1;
        ol2.Discount=0.0;
        ol2.UDAC__c='STR';
        ol2.Section_Code__c='010';
        ol2.Type__c='Caption';
        ol2.Heading_Code__c='0123';
        ol2.Advertising_Data__c='TEST';
        ol2.Action__c='I';
        ol2.Heading__c='TEXT';
        ol2.Distribution_Area__c='US';
        ol2.Directory_Heading__c=objDH.Id;
        ol2.CMR_Number__c='TEST';
        ol2.CMR_Name__c=objCMRAcc.Id;
        ol2.Directory__c=objDir.Id;
        ol2.Directory_Edition__c=objDirE.Id;
        ol2.Full_Rate__c=100.00;
        ol2.Line_Number__c='100';
        ol2.NAT__c='N';
        ol2.Publication_Date__c=Date.today();
        ol2.Trans__c='I';
        ol2.DAT__c='N';
        ol2.SPINS__c='A';
        ol2.Date__c=Date.today();
        ol2.National_Staging_Line_Item__c=ListNSLI[0].Id;
        ol2.Trade_Mark_Parent_Id__c=ol1.Id;
        insert ol2;
        
        OpportunityLineItem ol3 = new OpportunityLineItem ();
        ol3.OpportunityId = newOpportunity.Id; 
        ol3.PricebookEntryId = pbe.Id;
        ol3.Quantity = 1;
        ol3.UnitPrice = 1;
        ol3.Discount=0.0;
        ol3.UDAC__c='STR';
        ol3.Section_Code__c='010';
        ol3.Type__c='Caption';
        ol3.Heading_Code__c='0123';
        ol3.Advertising_Data__c='TEST';
        ol3.Action__c='O';
        ol3.Heading__c='TEXT';
        ol3.Distribution_Area__c='US';
        ol3.Directory_Heading__c=objDH.Id;
        ol3.CMR_Number__c='TEST';
        ol3.CMR_Name__c=objCMRAcc.Id;
        ol3.Directory__c=objDir.Id;
        ol3.Directory_Edition__c=objDirE.Id;
        ol3.Full_Rate__c=100.00;
        ol3.Line_Number__c='100';
        ol3.NAT__c='N';
        ol3.Publication_Date__c=Date.today();
        ol3.Trans__c='I';
        ol3.DAT__c='N';
        ol3.SPINS__c='A';
        ol3.Date__c=Date.today();
        ol3.National_Staging_Line_Item__c=ListNSLI[0].Id;
        ol3.Trade_Mark_Parent_Id__c=ol1.Id;
        insert ol3;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Partner__c='Hawain Telecom',
        Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id,Opportunity_line_Item_id__c =ol1.Id,
        Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
        Directory_Edition__c = objDirE.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
        Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1,Directory_Section__c = objDS.id);
        insert objOrderLineItem;
        
        Order_Line_Items__c objOrderLineItemNew = new Order_Line_Items__c(Billing_Partner__c='Hawain Telecom',
        Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id,Opportunity_line_Item_id__c =ol1.Id,
        Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
        Directory_Edition__c = objDirE.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
        Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1,Directory_Section__c = objDS.id);
        insert objOrderLineItemNew;
        
        map<Id, Order_Line_Items__c> mapOSAndOPP=new map<Id,Order_Line_Items__c>();
        mapOSAndOPP.put(newOpportunity.Id,objOrderLineItem);
        map<Id, Order_Line_Items__c> mapOLIByNSLI=new map<Id, Order_Line_Items__c>();
        mapOLIByNSLI.put(ListNSLI[0].Id,objOrderLineItemNew);
        set<id> setOLI=new set<id>();
        setOLI.add(objOrderLineItemNew.Id);
        setOLI.add(objOrderLineItem.Id);
        NationalOrderCreationHandler_V2 NOCH=new NationalOrderCreationHandler_V2();
        NationalOrderCreationHandler_V2.createNationalOrder(new set<id>{newOpportunity.Id},new set<id>{objCMRAcc.id});
        Order_Group__c org=NationalOrderCreationHandler_V2.newOrderSetNational(newOpportunity, mapOSAndOPP);
        Order_Line_Items__c oli=NationalOrderCreationHandler_V2.newOrderLineItemNational(ol1,newOpportunity,mapOLIByNSLI,null,setOLI);
        
        map<Id, Order_Line_Items__c> mapOLIByNSLINew=new map<Id, Order_Line_Items__c>();
        mapOLIByNSLINew.put(ListNSLI[1].Id,objOrderLineItem);
        Order_Line_Items__c oliNew=NationalOrderCreationHandler_V2.newOrderLineItemNational(ol2,newOpportunity,mapOLIByNSLINew,null,setOLI);
        
        Test.StopTest();
  }
}