global class NationalOrderAutomationScheduler implements Schedulable {
	public Interface NationalOrderAutomationSchedulerInterface {
		void execute(SchedulableContext sc);
	}
	
	global void execute(SchedulableContext sc) {
		Type targetType = Type.forName('NationalOrderAutomationSchedulerHndlr');
		if(targetType != null) {
			NationalOrderAutomationSchedulerInterface obj = (NationalOrderAutomationSchedulerInterface)targetType.newInstance();
			obj.execute(sc);
		}
	}
}